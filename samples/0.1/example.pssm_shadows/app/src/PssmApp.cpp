/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ___________________________________( ^3^)_____________________________________
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 */

/* 
 * File:   PssmApp.cpp
 * Author: frankiezafe
 * 
 * Created on November 19, 2016, 6:07 PM
 */

#include "PssmApp.h"

using namespace std;
using namespace Ogre;
using namespace polymorph;

void PssmApp::loadResources(void) {

    ResourceGroupManager::getSingleton().initialiseResourceGroup("COMMON");
    ResourceGroupManager::getSingleton().initialiseResourceGroup("PSSM");
    ResourceGroupManager::getSingleton().initialiseResourceGroup("Essential");
    ResourceGroupManager::getSingleton().initialiseResourceGroup("Configuration");
    ResourceGroupManager::getSingleton().initialiseResourceGroup("Custom");

}

void PssmApp::createSceneManager(void) {

    ballnum = 0;
    balls = 0;

    objMgr.parse("Configuration", "config.xml");

    PolymorphApplication::createSceneManager();

    objMgr.setSceneManager(sceneMgr);

    loadedShadowNode = Ogre::IdString("Pssm_ShadowNode_HD");

}

void PssmApp::createCompositor(void) {

    compositorMgr = root->getCompositorManager2();
    compositorwpname = "PssmCompositor";
    const Ogre::IdString workspaceName(compositorwpname);

    cout << ">>>>>>> " << loadedShadowNode.getFriendlyText() << endl;

    CompositorNodeDef* cnd = compositorMgr->getNodeDefinitionNonConst("PssmRenderer");
    cout << ">>>>>>> " << cnd->getNameStr() << endl;
    CompositorTargetDef* ctd = cnd->getTargetPass(0);
    cout << ">>>>>>> " << ctd->getCompositorPassesNonConst().size() << endl;
    CompositorPassSceneDef* cpd = static_cast<CompositorPassSceneDef*> (
            cnd->getTargetPass(0)->getCompositorPassesNonConst()[1]);
    cpd->mShadowNode = loadedShadowNode;
    cout << ">>>>>>> " << cpd->mShadowNode.getFriendlyText() << endl;

    // for code generation of textures:
    // see http://www.ogre3d.org/forums/viewtopic.php?f=25&t=93560

    compositorwp = compositorMgr->addWorkspace(
            sceneMgr, window, cam,
            workspaceName, true);

}

void PssmApp::createScene(void) {

    // keep default behavior
    PolymorphApplication::createScene();

    polymorph::PBullet::gravity(btVector3(0, -50, 0));
    polymorph::PBullet::start(sceneMgr);
    polymorph::PBullet::time_multiplier(0);
    bullet_on = false;
    tm_bullet = 0;
    tm_bullet_target = 0;

    if (objMgr.nextScene()) {

        uint32_t scid = objMgr.getSceneID();
        objMgr.loadScene(scid);

        Real cdiv = 1.f / 255;
        Real screen_current = 0;
        Real screen_steps = 1.f / 25;
        PRange< Ogre::ColourValue > gradient(
                Ogre::ColourValue(0.611764706, 0.71372549, 0.709803922, 0.2),
                Ogre::ColourValue(1, 1, 1, 0.09)
                );
        uint32_t ncount = objMgr.getNodeCount();
        for (uint32_t i = 0; i < ncount; ++i) {
            PNode* p = objMgr.node(i);
            std::string pname = p->getName();
            if (pname.find("screen") != std::string::npos) {
                PMaterial* mat = new PMaterial();
                mat->clone(p->getMaterial());
                mat->diffuse(gradient.lerp(screen_current));
                mat->emissive(
                        gradient.lerp(screen_current) *
                        Ogre::ColourValue(1, 1, 1, 0.4)
                        );
                p->material(mat, true);
                screen_current += screen_steps;
            }
        }

        PNode* b = objMgr.node("ball");
        if (b) {
            ballnum = 100;
            balls = new PNode*[ ballnum ];
            PNodeData bdata;
            bdata.visible = true;
            bdata.internal = true;
            bdata.sphere = true;
            bdata.bullet_type = BT_DYNAMIC_SPHERE;
            bdata.bullet_restless = true;
            bdata.bullet_friction = 0.4;
            bdata.bullet_restitution = 0.2;
            for (uint32_t i = 0; i < ballnum; ++i) {
                stringstream ss;
                ss << "bb_" << i;
                bdata.name = ss.str();
                Real scf = Math::RangeRandom(0, 1);
                scf = pow(scf, 3.5);
                bdata.origin_scale = b->getScale() * 1.5 + scf * 120;
                bdata.origin_pos = Vector3(
                        Math::RangeRandom(-30, 30),
                        Math::RangeRandom(-30, 150),
                        Math::RangeRandom(-30, 30)
                        );
                balls[i] = new PNode(sceneMgr, &bdata);
                //                balls[i]->sphere(sceneMgr);
                //                balls[i]->scale(b->getScale() * Math::RangeRandom(0.3, 6));
                //                balls[i]->move(
                //                        
                //                        );
                balls[i]->material(b->getMaterial());
                //                balls[i]->physics(BT_DYNAMIC_SPHERE);
            }
        }
    }

    //************** TEMP **************
    if (compositorwpname.compare("PssmCompositor") == 0) {

        //sceneMgr->setShadowCasterRenderBackFaces(false);
        //sceneMgr->setShadowDirectionalLightExtrusionDistance(400.0f);
        //sceneMgr->setShadowTextureCasterMaterial( "shadowscaster_pssm" );
        sceneMgr->setShadowFarDistance(700.0f);

        PMaterial mbase;
        mbase.load("PSSM", "pssm_texture_debug");
        PMaterial c2d;
        c2d.clone(&mbase);
        Ogre::TextureUnitState* textureUnit = c2d.getMaterial().get()->getTechnique(0)->getPass(0)->
                getTextureUnitState(0);
        CompositorShadowNode *shadowNode = compositorwp->findShadowNode(loadedShadowNode);
        Ogre::TexturePtr tex = shadowNode->getLocalTextures()[0].textures[0];
        textureUnit->setTextureName(tex->getName());
        int display_s0 = 10 * tex->getWidth() / 256;

        PMaterial c2d2;
        c2d2.clone(&mbase);
        textureUnit = c2d2.getMaterial().get()->getTechnique(0)->getPass(0)->
                getTextureUnitState(0);
        shadowNode = compositorwp->findShadowNode(loadedShadowNode);
        tex = shadowNode->getLocalTextures()[1].textures[0];
        textureUnit->setTextureName(tex->getName());
        int display_s1 = 10 * tex->getWidth() / 256;

        PMaterial c2d3;
        c2d3.clone(&mbase);
        textureUnit = c2d3.getMaterial().get()->getTechnique(0)->getPass(0)->
                getTextureUnitState(0);
        shadowNode = compositorwp->findShadowNode(loadedShadowNode);
        tex = shadowNode->getLocalTextures()[2].textures[0];
        textureUnit->setTextureName(tex->getName());
        int display_s2 = 10 * tex->getWidth() / 256;

        Ogre::OverlayManager& overlayManager = Ogre::OverlayManager::getSingleton();

        // Create an overlay
        debugOverlay = overlayManager.create("OverlayName");

        // Create a panel
        Ogre::OverlayContainer* panel = static_cast<Ogre::OverlayContainer*> (
                overlayManager.createOverlayElement("Panel", "PanelName0"));
        panel->setMetricsMode(Ogre::GMM_PIXELS);
        panel->setPosition(10, 10);
        panel->setDimensions(display_s0, display_s0);
        panel->setMaterialName(c2d.getName());
        debugOverlay->add2D(panel);

        panel = static_cast<Ogre::OverlayContainer*> (
                overlayManager.createOverlayElement("Panel", "PanelName1"));
        panel->setMetricsMode(Ogre::GMM_PIXELS);
        panel->setPosition(20 + display_s0, 10);
        panel->setDimensions(display_s1, display_s1);
        panel->setMaterialName(c2d2.getName());
        debugOverlay->add2D(panel);

        panel = static_cast<Ogre::OverlayContainer*> (
                overlayManager.createOverlayElement("Panel", "PanelName2"));
        panel->setMetricsMode(Ogre::GMM_PIXELS);
        panel->setPosition(30 + display_s0 + display_s1, 10);
        panel->setDimensions(display_s2, display_s2);
        panel->setMaterialName(c2d3.getName());
        debugOverlay->add2D(panel);

        debugOverlay->show();
    }

    cam_rot = 0;
    cam_dist = 0;

    trayMgr->hideFrameStats();

}

void PssmApp::draw() {

    cam_rot += draw_event.timeSinceLastFrame * 0.1;
    cam_dist += draw_event.timeSinceLastFrame * 0.3;
    Real d = 150 + ((1 + Math::Sin(cam_dist)) * 0.5) * 400;
    cam->setPosition(
            Vector3(
            Math::Cos(cam_rot) * d,
            50,
            Math::Sin(cam_rot) * d
            ));
    cam->lookAt(
            objMgr.node("floor")->getTrans() +
            Vector3(0, 20, 0));

    //    objMgr.light("sun")->yaw( Radian( draw_event.timeSinceLastFrame ) );

    objMgr.light("sun")->orientation(
            cam->getOrientation() *
            Euler(
            Radian(Math::HALF_PI * 0.2),
            Radian(Math::HALF_PI),
            Radian(Math::HALF_PI*-0.2)
            ));

    for (uint32_t i = 0; i < ballnum; ++i) {
        if (balls[i]->getTrans().y < -120) {
            balls[i]->move(
                    Math::RangeRandom(-30, 30),
                    Math::RangeRandom(120, 250),
                    Math::RangeRandom(-30, -60)
                    );
            balls[i]->resetVelocities();
        }
    }
    
    tm_bullet += (tm_bullet_target - tm_bullet) * draw_event.timeSinceLastFrame;
    polymorph::PBullet::time_multiplier(tm_bullet);

}

bool PssmApp::keyPressed(const OIS::KeyEvent &arg) {

    // keep default behavior
    PolymorphApplication::keyPressed(arg);

    if (arg.key == OIS::KC_R) {
        for (uint32_t i = 0; i < ballnum; ++i) {
            balls[i]->move(
                    Math::RangeRandom(-30, 30),
                    Math::RangeRandom(-30, 150),
                    Math::RangeRandom(-30, 30)
                    );
        }
    } else if (arg.key == OIS::KC_SPACE) {
        if (bullet_on)
            tm_bullet_target=0;
        else
            tm_bullet_target=1;
        bullet_on = !bullet_on;
    }

}

bool PssmApp::mouseMoved(const OIS::MouseEvent &arg) {

    // keep default behavior
    PolymorphApplication::mouseMoved(arg);

}
