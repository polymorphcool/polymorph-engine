#version 120
//#extension GL_NV_non_square_matrices : enable

attribute vec4 vertex;
attribute vec3 normal;
attribute vec2 uv0;

#ifndef AVOIDER

float psDepth;
uniform float shadowConstantBias;
uniform mat4x4 worldViewProj;
uniform vec4 depthRange0;

#endif

void main() {

#ifdef AVOIDER
    
    gl_Position = vec4(0, 0, 0, 0);
    gl_TexCoord[0] = vec4(0, 0, 0, 0);
    
#else
    
    gl_Position = worldViewProj * vertex;
    //Linear depth
    psDepth = (gl_Position.z - depthRange0.x + shadowConstantBias) * depthRange0.w;
    //We can't make the depth buffer linear without Z out in the fragment shader;
    //however we can use a cheap approximation ("pseudo linear depth")
    //see http://yosoygames.com.ar/wp/2014/01/linear-depth-buffer-my-ass/
    gl_Position.z = gl_Position.z * (gl_Position.w * depthRange0.w);
    gl_TexCoord[0] = vec4(psDepth, psDepth, psDepth, psDepth);

#endif
    
}
