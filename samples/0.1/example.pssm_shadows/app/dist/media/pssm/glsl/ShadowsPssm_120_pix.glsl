#version 120

#define NUM_SHADOW_SAMPLES_1D 2.0
#define SHADOW_FILTER_SCALE 0.5
#define SHADOW_SAMPLES NUM_SHADOW_SAMPLES_1D*NUM_SHADOW_SAMPLES_1D

vec4 offsetSample(vec4 uv, vec2 offset, float invMapSize) {
    return vec4(uv.xy + offset * invMapSize * uv.w, uv.z, uv.w);
}

float calcDepthShadowLod(
        sampler2D shadowMap, 
        vec4 uv, 
        float invShadowMapSize
    ) {
    
    // 4-sample PCF
//    vec4 finalUv = offsetSample(uv, vec2(0,0), invShadowMapSize);
//    finalUv.xy /= finalUv.w;
//    float depth = texture2DLod(shadowMap, finalUv.xy, 0.0).x;
//    if ( depth < 0 && depth > uv.z ) {
//        return 1.0;
//    } else {
//        return uv.z / depth;
//    }

    float shadow = 0.0;
    float totald = 0.0;
    float offset = (NUM_SHADOW_SAMPLES_1D / 2.0 - 0.5) * SHADOW_FILTER_SCALE;
    for (float y = -offset; y <= offset; y += SHADOW_FILTER_SCALE) {
        for (float x = -offset; x <= offset; x += SHADOW_FILTER_SCALE) {
            vec4 finalUv = offsetSample(uv, vec2(x, y), invShadowMapSize);
            finalUv.xy /= finalUv.w;
            float depth = texture2D(shadowMap, finalUv.xy).x;
            if (depth >= 1.0 || depth >= uv.z) {
                shadow += 1.0;
            }
        }
    }

    shadow /= SHADOW_SAMPLES;
    
    return shadow;
}

float calcPSSMDepthShadow(
        sampler2D shadowMap0, 
        sampler2D shadowMap1, 
        sampler2D shadowMap2,
        vec4 lsPos0, 
        vec4 lsPos1, 
        vec4 lsPos2,
        float invShadowmapSize0, 
        float invShadowmapSize1, 
        float invShadowmapSize2,
        vec4 pssmSplitPoints, 
        float camDepth
    ) {

    float shadow;
    vec4 splitColour;
    // calculate shadow
    if (camDepth <= pssmSplitPoints.x) {
        splitColour = vec4(0.3, 0.0, 0, 0);
        shadow = calcDepthShadowLod(shadowMap0, lsPos0, invShadowmapSize0);
    } else if (camDepth <= pssmSplitPoints.y) {
        splitColour = vec4(0, 0.3, 0, 0);
        shadow = calcDepthShadowLod(shadowMap1, lsPos1, invShadowmapSize1);
    } else {
        splitColour = vec4(0.0, 0.0, 0.3, 0);
        shadow = calcDepthShadowLod(shadowMap2, lsPos2, invShadowmapSize2);
    }
    return shadow;
}

#define NUM_LIGHTS 8

uniform vec4 lightAmbient;
uniform vec4 lightPosition[NUM_LIGHTS];
uniform vec4 lightDiffuse[NUM_LIGHTS];

uniform float invShadowMapSize0;
uniform float invShadowMapSize1;
uniform float invShadowMapSize2;
uniform vec4 pssmSplits;

uniform sampler2D shadowMap0;
uniform sampler2D shadowMap1;
uniform sampler2D shadowMap2;

uniform float shadow_alpha;

float map(float value, float inMin, float inMax, float outMin, float outMax) {
  return outMin + (outMax - outMin) * (value - inMin) / (inMax - inMin);
}

void main() {
    
    vec2 psUv = gl_TexCoord[0].xy;
    vec3 psWorldPos = gl_TexCoord[1].xyz;
    vec3 psNorm = gl_TexCoord[2].xyz;
    vec4 psLightSpacePos0 = gl_TexCoord[3].xyzw;
    vec4 psLightSpacePos1 = gl_TexCoord[4].xyzw;
    vec4 psLightSpacePos2 = gl_TexCoord[5].xyzw;
    float psDepth = gl_TexCoord[6].x;

    float fShadow = calcPSSMDepthShadow(
            shadowMap0, 
            shadowMap1, 
            shadowMap2,
            psLightSpacePos0, 
            psLightSpacePos1, 
            psLightSpacePos2,
            invShadowMapSize0, 
            invShadowMapSize1, 
            invShadowMapSize2,
            pssmSplits, 
            psDepth);

    vec3 negLightDir = lightPosition[0].xyz - (psWorldPos * lightPosition[0].w);
    negLightDir = normalize(negLightDir);
    vec3 normal = normalize(psNorm);
    vec4 color = max(0.0, dot(negLightDir, normal)) * lightDiffuse[0] * fShadow + lightAmbient;
    int i = 1;
    for (i = 1; i < NUM_LIGHTS; ++i) {
        vec3 negLightDir = lightPosition[i].xyz - (psWorldPos * lightPosition[i].w);
        negLightDir = normalize(negLightDir);
        color += max(0.0, dot(negLightDir, normal)) * lightDiffuse[i];
    }

    //float lum = dot(vec3(0.30, 0.59, 0.11), color.xyz);
    gl_FragColor = vec4( color.xyz * fShadow, ( 1.0 - fShadow ) * shadow_alpha );

//    gl_FragColor = vec4( fShadow,fShadow,fShadow,1 );
//    gl_FragColor = vec4( 0,0,0, (1-fShadow) );
//    gl_FragColor = vec4( (1-fShadow),(1-fShadow),(1-fShadow),1 );
        
}
