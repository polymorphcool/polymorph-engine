#version 120
//#extension GL_NV_non_square_matrices : enable

attribute vec4 vertex;
attribute vec3 normal;
attribute vec2 uv0;

vec2 psUv;
vec3 psWorldPos;
vec3 psOutNorm;
vec4 psLightSpacePos0;
vec4 psLightSpacePos1;
vec4 psLightSpacePos2;

float psDepth;
uniform mat4x4 world;
uniform mat4x4 worldViewProj;

uniform mat4x4 texViewProjMatrix0;
uniform mat4x4 texViewProjMatrix1;
uniform mat4x4 texViewProjMatrix2;

uniform vec4 depthRange0;
uniform vec4 depthRange1;
uniform vec4 depthRange2;

void main() {
    
    gl_Position = worldViewProj * vertex;

    psWorldPos = (world * vertex).xyz;
    psOutNorm = mat3x3(world) * normal;
    psUv = uv0;

    // Calculate the position of vertex in light space to do shadows
    vec4 shadowWorldPos = vec4(psWorldPos, 1.0);
    psLightSpacePos0 = texViewProjMatrix0 * shadowWorldPos;
    // Linear depth
    psLightSpacePos0.z = (psLightSpacePos0.z - depthRange0.x) * depthRange0.w;

    psLightSpacePos1 = texViewProjMatrix1 * shadowWorldPos;
    psLightSpacePos1.z = (psLightSpacePos1.z - depthRange1.x) * depthRange1.w;
    psLightSpacePos2 = texViewProjMatrix2 * shadowWorldPos;
    psLightSpacePos2.z = (psLightSpacePos2.z - depthRange2.x) * depthRange2.w;

    psDepth = gl_Position.z;

    gl_TexCoord[0] = psUv.xyyy;
    gl_TexCoord[1] = psWorldPos.xyzz;
    gl_TexCoord[2] = psOutNorm.xyzz;
    gl_TexCoord[3] = psLightSpacePos0;
    gl_TexCoord[4] = psLightSpacePos1;
    gl_TexCoord[5] = psLightSpacePos2;
    gl_TexCoord[6] = vec4( psDepth,psDepth,psDepth,psDepth );

}