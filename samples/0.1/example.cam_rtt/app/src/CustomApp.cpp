/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ___________________________________( ^3^)_____________________________________
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 */

/* 
 * File:   CustomApp.cpp
 * Author: frankiezafe
 * 
 * Created on November 19, 2016, 6:07 PM
 */

#include "CustomApp.h"

using namespace std;
using namespace Ogre;
using namespace polymorph;

void CustomApp::createScene( void ) {

    // keep default behavior
    PolymorphApplication::createScene( );

    sunA.sun( sceneMgr );
    sunA.move( 0, 200, 0 );
    sunA.pitch( Radian( Math::PI * 0.25 ) );
    sunB.sun( sceneMgr );
    sunB.move( 0, -200, 0 );
    sunB.pitch( Radian( Math::PI * 1.25 ) );

    empty.empty( sceneMgr );
    empty.scale( 1 );

    // creation of a custom material with a texture slot
    MaterialPtr custom_mat = Ogre::MaterialManager::getSingleton( ).create(
            "custom_mat", "Internal" );
    gnum = 12;
    grid = new PNode[ gnum ];
    for ( int i = 0; i < gnum; ++i ) {
        Real a = Math::TWO_PI * Real( i ) / gnum;
        if ( i % 2 == 0 ) {
            grid[ i ].cube( sceneMgr );
            grid[ i ].scale( 70, 140, 70 );
        } else {
            grid[ i ].sphere( sceneMgr );
            grid[ i ].scale( 70 );
        }
        grid[ i ].move( Vector3(
                Math::Cos( a ) * 200,
                0,
                Math::Sin( a ) * 200
                ) );
        grid[ i ].yaw( Radian( a ) );
        if ( i % 2 == 0 ) {
            grid[ i ].material( "Internal", "custom_mat" );
        }
        empty.attach( &( grid[ i ] ) );
    }

    custom_cam.init( sceneMgr, "custom_cam" );
    custom_cam.debug( true );
    custom_cam.showAxis( true );
    custom_cam.scale( 1 );

    // loading the custom compositor from resources, 
    // media/compositor/custom.compositor
    custom_wpname = "CustomCompositor";
    const Ogre::IdString cwName( custom_wpname );
    // creation of a custom texture
    TexturePtr texture = Ogre::TextureManager::getSingleton( ).createManual(
            "custom_texture",
            "Internal", TEX_TYPE_2D, 512, 1024, 0, PF_FLOAT16_RGB,
            TU_RENDERTARGET
            );
    // getting the render target
    RenderTexture * rt = texture->getBuffer( )->getRenderTarget( );
    // creation of a new workspace rendering cam to custom render target
    custom_wp = compositorMgr->addWorkspace(
            sceneMgr, rt,
            custom_cam.getOgreCam( ),
            cwName, true );
    // and using the texture in the material of the cubes
    custom_mat->getTechnique( 0 )->getPass( 0 )->createTextureUnitState( )->setTexture( texture );

}

void CustomApp::draw( ) {

    custom_cam.fov( Ogre::Radian( mouse_rel.x * Math::PI ) );
//    empty.pitch( Radian( draw_event.timeSinceLastFrame * 0.25 ) );
    empty.yaw( Radian( draw_event.timeSinceLastFrame * 0.5 ) );

    for ( int i = 0; i < gnum; ++i ) {
        grid[ i ].yaw( Radian( draw_event.timeSinceLastFrame * 0.2 ) );
    }

}

bool CustomApp::keyPressed( const OIS::KeyEvent &arg ) {

    // keep default behavior
    PolymorphApplication::keyPressed( arg );
    
    switch( arg.key ) {
    
        case OIS::KC_SPACE:
            if ( custom_cam.getLookAt() ) custom_cam.lookAt( 0 );
            else custom_cam.lookAt( &grid[0] );
            break;
    
    }

}

bool CustomApp::mouseMoved( const OIS::MouseEvent &arg ) {

    // keep default behavior
    PolymorphApplication::mouseMoved( arg );

    mouse_rel.x = arg.state.X.abs * 1.0 / window->getWidth();
    mouse_rel.y = arg.state.Y.abs * 1.0 / window->getHeight();
    
}
