/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ___________________________________( ^3^)_____________________________________
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
*/

/* 
 * File:   CustomApp.h
 * Author: Eurydice Cardon
 * 
 * Created during February 2017
 */

#ifndef CUSTOMAPP_H
#define CUSTOMAPP_H

#include "PolymorphApplication.h"
#include "AstralBody.h"

//#define AB_DEBUG

struct AstralBodyData {
    int parent;
    std::string name;
    float dimension;
    float orbitDistance;
    float orbitDuration;
    float orbitInclination;
    float rotationDuration;
    float rotationInclination;
};

class CustomApp : public PolymorphApplication {

public:
    
    void createScene(void);
    
    void draw(); // called at each frame
    
    bool keyPressed(const OIS::KeyEvent &arg);
    
    bool mouseMoved(const OIS::MouseEvent &arg);
    
private:
    
    void initStellarData();
    
    bool freeze;
    
    //polymorph::PNode moonRender;
    //polymorph::PCamera moonCam;
    //Ogre::String            custom_wpname;
    //CompositorWorkspace*    custom_wp;
    
    AstralBody* sunSystem;
    polymorph::PLight sunLight;
    std::vector<AstralBodyData> datas;
    std::vector<int> dataParents;
    std::vector<std::string> dataNames;
    std::vector<float> dataDimensions;
    std::vector<float> dataOrbitDistances;
    std::vector<float> dataOrbitDurations;
    std::vector<float> dataOrbitInclinations;
    std::vector<float> dataRotationDurations;
    std::vector<float> dataRotationInclinations;
    
    polymorph::PNode separator;
    
    AstralBody* trappist_1System;
    polymorph::PLight trappist_1Light;
    float trappist_1SystemScale;
    std::vector<int> trappist_1System_DataParents;
    std::vector<std::string> trappist_1System_DataNames;
    std::vector<float> trappist_1System_DataDimensions;
    std::vector<float> trappist_1System_DataOrbitDistances;
    std::vector<float> trappist_1System_DataOrbitDurations;
    std::vector<float> trappist_1System_DataOrbitInclinations;
    std::vector<float> trappist_1System_DataRotationDurations;
    std::vector<float> trappist_1System_DataRotationInclinations;
    
    
    /*
    AstralBody sun;
    AstralBody mercury;
    AstralBody venus;
    AstralBody earth;
    AstralBody mars;
    AstralBody jupiter;
    AstralBody saturn;
    AstralBody uranus;
    AstralBody neptune;
    AstralBody moon;
    AstralBody io;
    AstralBody europa;
    AstralBody ganymede;
    AstralBody callisto;
    //AstralBody pluto;
    //Sorry pluto, you wait with the rest of dwarf planets
    //AstralBody ceres;
    //AstralBody haumea;
    //AstralBody makemake;
    //AstralBody eris;
     */
    
    //AstralBody TRAPPIST_1;
    //AstralBody TRAPPIST_1b;
    //AstralBody TRAPPIST_1c;
    //AstralBody TRAPPIST_1d;
    //AstralBody TRAPPIST_1e;
    //AstralBody TRAPPIST_1f;
    //AstralBody TRAPPIST_1g;
    //AstralBody TRAPPIST_1h;
    
    Ogre::Real lrot;
    Ogre::Vector2 mouse_rel;
    Ogre::Vector2 mouse_abs;
    Ogre::Vector2 mouse_abs_norm;
    
    float EARTH_RADIUS;
    float ASTRONOMICAL_UNIT;
    float SIDEREAL_PERIOD;
    float SOLAR_DAY;
    
    float TIME_SCALE;
    
};

#endif /* CUSTOMAPP_H */

