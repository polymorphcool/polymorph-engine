/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   AstralBody.h
 * Author: Eurydice Cardon
 *
 * Created on February 13, 2017, 4:17 PM
 */

#ifndef ASTRALBODY_H
#define ASTRALBODY_H

#include "PolymorphApplication.h"
#include "PNode.h"

class AstralBody : public polymorph::PNode {

public:
    
    AstralBody();
    
    AstralBody(Ogre::SceneManager* sceneMgr,
               AstralBody* parent,
               std::string name = "Unknown",
               float size = 1,
               float orbitDistance = 0,
               float orbitDuration = 0,
               float orbitInclination = 0,
               float rotationSpeed = 0,
               float rotationInclination = 0
               );
    
    void init(Ogre::SceneManager* sceneMgr,
              AstralBody* parent,
              std::string name = "Unknown",
              float size = 1,
              float orbitDistance = 0,
              float orbitDuration = 0,
              float orbitInclination = 0,
              float rotationSpeed = 0,
              float rotationInclination = 0
              );
    
    void draw(float deltaTime);
    
//private:
    
    float _alpha;
    Vector3 p;
    
    polymorph::PNode _pivot;
    
    std::string _name;
    float _size; //1000 km (Eath = 6.378 * 1000 km)
    float _orbitDistance; //0.01 UA (Earth ref = 100 * 0.01 UA)
    float _orbitDuration; //years (Earth ref = 1.0)
    float _orbitInclination; //degrees (Earth ref = 0°)
    float _rotationDuration; //Earth ref = 1 (day)?
    float _rotationInclination; //Earth = 24... ?
};

#endif /* ASTRALBODY_H */
