/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   AstralBody.cpp
 * Author: Eurydice Cardon
 * 
 * Created on February 13, 2017, 4:17 PM
 */

#include "AstralBody.h"
#include "CustomApp.h"

using namespace std;
using namespace Ogre;
using namespace polymorph;

AstralBody::AstralBody() {
}

AstralBody::AstralBody( Ogre::SceneManager* sceneMgr, AstralBody* parent,
                       string name, float size, float orbitDistance,
                       float orbitDuration, float orbitInclination,
                       float rotationDuration, float rotationInclination) {
    init( sceneMgr, parent, name, size, orbitDistance, orbitDuration, orbitInclination,
          rotationDuration, rotationInclination);
}

void AstralBody::init( Ogre::SceneManager* sceneMgr, AstralBody* parent,
                    string name, float size, float orbitDistance,
                    float orbitDuration, float orbitInclination,
                    float rotationDuration, float rotationInclination) {
    _alpha = Math::RangeRandom( 0, Math::TWO_PI );
    _name = name;
    _size = size;
    _orbitDistance = orbitDistance;
    _orbitDuration = orbitDuration;
    _orbitInclination = orbitInclination;
    _rotationDuration = rotationDuration;
    _rotationInclination = rotationInclination;
    
    //_pivot.plane( sceneMgr );
    //_pivot.scale( _orbitDistance, true );
    //_pivot.pitch( Radian( _orbitInclination ) );
    sphere( sceneMgr );
    if( parent ) {
        //parent->attach( &_pivot );
        parent->attach( this );
    }
    //_pivot.attach( this );
    move( _orbitDistance, 0, 0 );
    scale( _size, true );
    material( "General", name );
    
    //pitch(rotationInclination);
    //Euler via orbitInclination ?
}

void AstralBody::draw(float deltaTime) {
    
    if( _orbitDuration != 0 ) {
        
        _alpha -= deltaTime / _orbitDuration;
        
        if( _alpha < -Math::TWO_PI ) {
            _alpha += Math::TWO_PI;
        }
        
        p.x = Math::Cos( _alpha ) * _orbitDistance;
        p.z = Math::Sin( _alpha ) * _orbitDistance;
        move( p );
    }
    
    //yaw(deltaTime / rotationDuration);
}