/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ___________________________________( ^3^)_____________________________________
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
*/

/* 
 * File:   CustomApp.cpp
 * Author: Eurydice Cardon
 * 
 * Created during February 2017
 */

#include "CustomApp.h"
#include "AstralBody.cpp"

using namespace std;
using namespace Ogre;
using namespace polymorph;

void CustomApp::createScene( void ) {

    // keep default behavior
    PolymorphApplication::createScene( );

    sceneMgr->setAmbientLight( Ogre::ColourValue( 0.1, 0.1, 0.1 ) );
    
    EARTH_RADIUS = 6.371f; //6 371 km
    //150M km distance from earth to sun * 100 (scale) :
    ASTRONOMICAL_UNIT = 1.0f * 100;
    //365.25636f solar days
    SIDEREAL_PERIOD = 1.0f;
    SOLAR_DAY = SIDEREAL_PERIOD / 365.25636f;
    
    TIME_SCALE = 0.1f;
    
    
    freeze = false;
    
    separator.cube( sceneMgr );
    separator.scale( 500, 1, 500 );
    
    trappist_1SystemScale = 0.05f;
    initStellarData();
    trappist_1Light.point( sceneMgr );
    trappist_1System[0].attach( &trappist_1Light );
    trappist_1System[0].move( 0, 100, 0 );
    trappist_1System[0].scale( 20 );
    
    sunLight.point( sceneMgr );
    sunSystem[0].attach( &sunLight );
    sunSystem[0].move( 0, -100, 0 );
    
    //MOON RENDER:
    /*
    moonRender.cube( sceneMgr );
    moonRender.scale( 100 );
    moonRender.move( -1, 1, -200 );
    moonRender.material( "Internal", "custom_mat" );
    // creation of a custom material with a texture slot
    MaterialPtr custom_mat = Ogre::MaterialManager::getSingleton( ).create(
            "custom_mat", "Internal" );
    
    moonCam.init( sceneMgr, "custom_cam" );
    moonCam.debug( true );
    moonCam.showAxis( true );
    moonCam.scale( 1 );

    // loading the custom compositor from resources, 
    // media/compositor/custom.compositor
    custom_wpname = "CustomCompositor";
    const Ogre::IdString cwName( custom_wpname );
    // creation of a custom texture
    TexturePtr texture = Ogre::TextureManager::getSingleton( ).createManual(
            "custom_texture",
            "Internal", TEX_TYPE_2D, 512, 1024, 0, PF_FLOAT16_RGB,
            TU_RENDERTARGET
            );
    // getting the render target
    RenderTexture * rt = texture->getBuffer( )->getRenderTarget( );
    // creation of a new workspace rendering cam to custom render target
    custom_wp = compositorMgr->addWorkspace(
            sceneMgr, rt,
            moonCam.getOgreCam( ),
            cwName, true );
    // and using the texture in the material of the cubes
    custom_mat->getTechnique( 0 )->getPass( 0 )->createTextureUnitState( )->setTexture( texture );
     */
}

void CustomApp::draw( ) {
    
    if( freeze ) {
        return;
    }
    
    for( int i = 0; i < trappist_1System_DataNames.size(); ++i ) {
        trappist_1System[i].draw( draw_event.timeSinceLastFrame * TIME_SCALE );
    }
    
    for( int i = 0; i < dataNames.size(); ++i ) {
        sunSystem[i].draw( draw_event.timeSinceLastFrame * TIME_SCALE );
    }
    
    mouse_rel = Ogre::Vector2::ZERO;
}

bool CustomApp::keyPressed( const OIS::KeyEvent &arg ) {

    // keep default behavior
    PolymorphApplication::keyPressed( arg );

    // show/hide overlay panels
    if ( arg.key == OIS::KC_H ) {
        if ( trayMgr->isCursorVisible( ) ) trayMgr->hideAll( );
        else trayMgr->showAll( );
    }
    
    if ( arg.key == OIS::KC_SPACE ) {
        freeze = !freeze;
    }
}

bool CustomApp::mouseMoved( const OIS::MouseEvent &arg ) {

    // keep default behavior
    PolymorphApplication::mouseMoved( arg );

    mouse_rel.x = arg.state.X.rel;
    mouse_rel.y = arg.state.Y.rel;
    mouse_abs.x = arg.state.X.abs;
    mouse_abs.y = arg.state.Y.abs;

    mouse_abs_norm.x = mouse_abs.x / window->getWidth( );
    mouse_abs_norm.y = mouse_abs.y / window->getHeight( );

}

void CustomApp::initStellarData() {
    
    //SOLAR SYSTEM:
    AstralBodyData d;
    //d.name ="Sun";
    dataNames.push_back("Sun");
    dataNames.push_back("Mercury");
    dataNames.push_back("Venus");
    dataNames.push_back("Earth");
    dataNames.push_back("Mars");
    dataNames.push_back("Jupiter");
    dataNames.push_back("Saturn");
    dataNames.push_back("Uranus");
    dataNames.push_back("Neptune");
    //Satellites:
    dataNames.push_back("Moon");
    dataNames.push_back("Io");
    dataNames.push_back("Europa");
    dataNames.push_back("Ganymede");
    dataNames.push_back("Callisto");
    //Dwarf planets:
    //dataNames.push_back("Pluto");
    //dataNames.push_back("Ceres");
    //dataNames.push_back("Haumea");
    //dataNames.push_back("Makemake");
    //dataNames.push_back("Eris");
    
    dataParents.push_back(-1);
    dataParents.push_back(0);
    dataParents.push_back(0);
    dataParents.push_back(0);
    dataParents.push_back(0);
    dataParents.push_back(0);
    dataParents.push_back(0);
    dataParents.push_back(0);
    dataParents.push_back(0);
    dataParents.push_back(3); //Moon
    dataParents.push_back(5);
    dataParents.push_back(5);
    dataParents.push_back(5);
    dataParents.push_back(5);
    //dataParents.push_back(0); //Pluto
    //dataParents.push_back(0);
    //dataParents.push_back(0);
    //dataParents.push_back(0);
    //dataParents.push_back(0);
    
    dataDimensions.push_back(10.0f);
    dataDimensions.push_back(2.439f);
    dataDimensions.push_back(6.052f);
    dataDimensions.push_back(EARTH_RADIUS);
    dataDimensions.push_back(3.397f);
    dataDimensions.push_back(71.5f);
    dataDimensions.push_back(60.3f);
    dataDimensions.push_back(25.6f);
    dataDimensions.push_back(24.8f);
    dataDimensions.push_back(2.5f); //? Moon
    dataDimensions.push_back(3.0f); //?
    dataDimensions.push_back(3.0f); //?
    dataDimensions.push_back(3.0f); //?
    dataDimensions.push_back(3.0f); //?
    //dataDimensions.push_back(1.0f); //Pluto
    //dataDimensions.push_back(1.0f);
    //dataDimensions.push_back(1.0f);
    //dataDimensions.push_back(1.0f);
    //dataDimensions.push_back(1.0f);
    
    dataOrbitDistances.push_back(0.0f);
    dataOrbitDistances.push_back(38.71f);
    dataOrbitDistances.push_back(72.333f);
    dataOrbitDistances.push_back(ASTRONOMICAL_UNIT);
    dataOrbitDistances.push_back(152.366f);
    dataOrbitDistances.push_back(520.336f);
    dataOrbitDistances.push_back(953.707f);
    dataOrbitDistances.push_back(1919.13f);
    dataOrbitDistances.push_back(3006.9f);
    dataOrbitDistances.push_back(10.0f); //? Moon
    dataOrbitDistances.push_back(120.0f); //?
    dataOrbitDistances.push_back(140.0f); //?
    dataOrbitDistances.push_back(160.0f); //?
    dataOrbitDistances.push_back(180.0f); //?
    //dataOrbitDistances.push_back(10000.0f); //Pluto
    //dataOrbitDistances.push_back(10000.0f); //?
    //dataOrbitDistances.push_back(10000.0f); //?
    //dataOrbitDistances.push_back(10000.0f); //?
    //dataOrbitDistances.push_back(10000.0f); //?
    
    dataOrbitDurations.push_back(0.0f);
    dataOrbitDurations.push_back(0.240846f);
    dataOrbitDurations.push_back(0.6152f);
    dataOrbitDurations.push_back(SIDEREAL_PERIOD);
    dataOrbitDurations.push_back(1.8808f);
    dataOrbitDurations.push_back(11.862f);
    dataOrbitDurations.push_back(29.457f);
    dataOrbitDurations.push_back(84.018f);
    dataOrbitDurations.push_back(164.78f);
    dataOrbitDurations.push_back(SOLAR_DAY * 27.32f);
    dataOrbitDurations.push_back(1.0f); //?
    dataOrbitDurations.push_back(1.0f); //?
    dataOrbitDurations.push_back(1.0f); //?
    dataOrbitDurations.push_back(1.0f); //?
    dataOrbitDurations.push_back(1.0f); //? Pluto
    dataOrbitDurations.push_back(1.0f); //?
    dataOrbitDurations.push_back(1.0f); //?
    dataOrbitDurations.push_back(1.0f); //?
    dataOrbitDurations.push_back(1.0f); //?
    
    dataOrbitInclinations.push_back(0.0f);
    dataOrbitInclinations.push_back(7.0049f);
    dataOrbitInclinations.push_back(3.3947f);
    dataOrbitInclinations.push_back(0.0f);
    dataOrbitInclinations.push_back(1.8506f);
    dataOrbitInclinations.push_back(1.3053f);
    dataOrbitInclinations.push_back(2.4845f);
    dataOrbitInclinations.push_back(0.7699f);
    dataOrbitInclinations.push_back(1.7692f);
    dataOrbitInclinations.push_back(0.0f); //? Moon
    dataOrbitInclinations.push_back(0.0f); //?
    dataOrbitInclinations.push_back(0.0f); //?
    dataOrbitInclinations.push_back(0.0f); //?
    dataOrbitInclinations.push_back(0.0f); //?
    //dataOrbitInclinations.push_back(0.0f); //? Pluto
    //dataOrbitInclinations.push_back(0.0f); //?
    //dataOrbitInclinations.push_back(0.0f); //?
    //dataOrbitInclinations.push_back(0.0f); //?
    //dataOrbitInclinations.push_back(0.0f); //?
    
    //dataRotationDurations.push_back();
    //dataRotationInclinations.push_back();
    
    //TRAPPIST-1:
    trappist_1System_DataNames.push_back("TRAPPIST-1");
    trappist_1System_DataNames.push_back("TRAPPIST-1b");
    trappist_1System_DataNames.push_back("TRAPPIST-1c");
    trappist_1System_DataNames.push_back("TRAPPIST-1d");
    trappist_1System_DataNames.push_back("TRAPPIST-1e");
    trappist_1System_DataNames.push_back("TRAPPIST-1f");
    trappist_1System_DataNames.push_back("TRAPPIST-1g");
    trappist_1System_DataNames.push_back("TRAPPIST-1h");
    
    trappist_1System_DataParents.push_back(-1);
    trappist_1System_DataParents.push_back(0);
    trappist_1System_DataParents.push_back(0);
    trappist_1System_DataParents.push_back(0);
    trappist_1System_DataParents.push_back(0);
    trappist_1System_DataParents.push_back(0);
    trappist_1System_DataParents.push_back(0);
    trappist_1System_DataParents.push_back(0);
    
    trappist_1System_DataDimensions.push_back(1.0f); //?
    trappist_1System_DataDimensions.push_back(EARTH_RADIUS * 1.086f * trappist_1SystemScale);
    trappist_1System_DataDimensions.push_back(EARTH_RADIUS * 1.056f * trappist_1SystemScale);
    trappist_1System_DataDimensions.push_back(EARTH_RADIUS * 0.772f * trappist_1SystemScale);
    trappist_1System_DataDimensions.push_back(EARTH_RADIUS * 0.918f * trappist_1SystemScale);
    trappist_1System_DataDimensions.push_back(EARTH_RADIUS * 1.045f * trappist_1SystemScale);
    trappist_1System_DataDimensions.push_back(EARTH_RADIUS * 1.127f * trappist_1SystemScale);
    trappist_1System_DataDimensions.push_back(EARTH_RADIUS * 0.755f * trappist_1SystemScale);
    
    trappist_1System_DataOrbitDistances.push_back(0.0f);
    trappist_1System_DataOrbitDistances.push_back(ASTRONOMICAL_UNIT * 0.01111f);
    trappist_1System_DataOrbitDistances.push_back(ASTRONOMICAL_UNIT * 0.01522f);
    trappist_1System_DataOrbitDistances.push_back(ASTRONOMICAL_UNIT * 0.021f);
    trappist_1System_DataOrbitDistances.push_back(ASTRONOMICAL_UNIT * 0.028f);
    trappist_1System_DataOrbitDistances.push_back(ASTRONOMICAL_UNIT * 0.037f);
    trappist_1System_DataOrbitDistances.push_back(ASTRONOMICAL_UNIT * 0.045f);
    trappist_1System_DataOrbitDistances.push_back(ASTRONOMICAL_UNIT * 0.063f);
    
    trappist_1System_DataOrbitDurations.push_back(0.0f);
    trappist_1System_DataOrbitDurations.push_back(SOLAR_DAY * 1.51087081f);
    trappist_1System_DataOrbitDurations.push_back(SOLAR_DAY * 2.4218233f);
    trappist_1System_DataOrbitDurations.push_back(SOLAR_DAY * 4.049610f);
    trappist_1System_DataOrbitDurations.push_back(SOLAR_DAY * 6.099615f);
    trappist_1System_DataOrbitDurations.push_back(SOLAR_DAY * 9.206690f);
    trappist_1System_DataOrbitDurations.push_back(SOLAR_DAY * 12.35294f);
    trappist_1System_DataOrbitDurations.push_back(SOLAR_DAY * 20.0f);
    
    trappist_1System_DataOrbitInclinations.push_back(0.0f);
    trappist_1System_DataOrbitInclinations.push_back(89.65f);
    trappist_1System_DataOrbitInclinations.push_back(89.67f);
    trappist_1System_DataOrbitInclinations.push_back(89.75f);
    trappist_1System_DataOrbitInclinations.push_back(89.86f);
    trappist_1System_DataOrbitInclinations.push_back(89.68f);
    trappist_1System_DataOrbitInclinations.push_back(89.71f);
    trappist_1System_DataOrbitInclinations.push_back(89.8f);
    
    //trappist_1System_DataRotationDurations.push_back();
    //trappist_1System_DataRotationInclinations.push_back();
    
    //TRAPPIST-1 SYSTEM:
    trappist_1System = new AstralBody[trappist_1System_DataNames.size()];
    
    for( int i = 0; i < trappist_1System_DataNames.size(); ++i ) {
        
        AstralBody* p;
        
        if( trappist_1System_DataParents[i] < 0 ) {
            p = 0;
        } else {
            p = &trappist_1System[trappist_1System_DataParents[i]];
        }
        
        AstralBody* a = &trappist_1System[i];
        a->init( sceneMgr,
                 p,
                 trappist_1System_DataNames[i],
                 trappist_1System_DataDimensions[i],
                 trappist_1System_DataOrbitDistances[i],
                 trappist_1System_DataOrbitDurations[i],
                 trappist_1System_DataOrbitInclinations[i]//,
                 //trappist_1System_DataRotationDurations[i],
                 //trappist_1System_DataRotationInclinations[i]
                );
#ifdef AB_DEBUG
        trappist_1System[i].debug( true );
#endif
    }
    
    //SOLAR SYSTEM:
    sunSystem = new AstralBody[dataNames.size()];
    
    for( int i = 0; i < dataNames.size(); ++i ) {
        
        AstralBody* p;
        
        if( dataParents[i] < 0 ) {
            p = 0;
        } else {
            p = &sunSystem[dataParents[i]];
        }
        
        AstralBody* a = &sunSystem[i];
        cout << "INITIALIZED ? " << a << endl;
        cout << "INITIALIZED &bodies ? " << &sunSystem[i] << endl;
        cout << "INITIALIZED & ? " << &a << endl;
        a->init( sceneMgr,
                 p,
                 dataNames[i],
                 dataDimensions[i],
                 dataOrbitDistances[i],
                 dataOrbitDurations[i],
                 dataOrbitInclinations[i]//,
                 //dataRotationDurations[i],
                 //dataRotationInclinations[i]
                );
#ifdef AB_DEBUG
        sunSystem[i].debug( true );
#endif
    }
    
}