/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ___________________________________( ^3^)_____________________________________
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
*/

/* 
 * File:   CustomApp.h
 * Author: frankiezafe
 *
 * Created on November 19, 2016, 6:07 PM
 */

#ifndef CUSTOMAPP_H
#define CUSTOMAPP_H

#include "PolymorphApplication.h"

//#define PUREDATA_EDITION

class CustomApp : 
    public PolymorphApplication, 
            public libpd::PdListener {

public:
    
    CustomApp();
    
    void setupResources(void);
    
    void createScene(void);
    
    void draw();
    
    bool keyPressed(const OIS::KeyEvent &arg);
    
    bool mouseMoved(const OIS::MouseEvent &arg);
    
    // puredata listeners
    
    void pdObjectTranslation( 
        const std::string name, const Ogre::Vector3& v);
    
    void pdObjectRotation( 
        const std::string name, const Ogre::Quaternion& rot);
    
    void pdObjectScale( 
        const std::string name, const Ogre::Vector3& v);
    
    void pdObjectAll( 
        const std::string name, 
        const Ogre::Vector3& trans,
        const Ogre::Quaternion& rot,
        const Ogre::Vector3& scale);
    
private:
    
    polymorph::PObjectManager pobjMgr;
    libpd::PdEngine pde;
    
    Radian cube_angle;
    
    Ogre::Vector2 mouse_rel;
    
};

#endif /* CUSTOMAPP_H */

