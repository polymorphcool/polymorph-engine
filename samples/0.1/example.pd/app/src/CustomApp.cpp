/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ___________________________________( ^3^)_____________________________________
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
*/

/* 
 * File:   CustomApp.cpp
 * Author: frankiezafe
 * 
 * Created on November 19, 2016, 6:07 PM
 */

#include "CustomApp.h"

using namespace std;
using namespace Ogre;
using namespace polymorph;

CustomApp::CustomApp(): 
PolymorphApplication()
{
    pobjMgr.verbose( true );
    pobjMgr.parse();
}

void CustomApp::setupResources(void) {
    
    PolymorphApplication::setupResources( pobjMgr.getProject() );
    
}

void CustomApp::createScene(void) {

    // keep default behavior
    PolymorphApplication::createScene();
    
    pde.verbose( true );
    
#ifdef PUREDATA_EDITION
    
    cout << ">>> starting pd in edition mode" << endl;
    
    // starting edit mode
    // pde will send data to 127.0.0.1:25000
    // and listen to 25001 by default
    pde.edition( true );
    
#else
    
    cout << ">>> starting pd in release mode" << endl;
    
    // you can load ALL resources path in pd engine, brute force style
//    pobjMgr.setupPdEngine( &pde );
    // or restrict resources based on GROUP name, a more elegant way
    // see bin/configuration.xml
    pobjMgr.setupPdEngine( &pde, "PD" );
    // initialising
    pde.init();
    // after an attempt to start the engine, opening the main patch
    if ( pde.start() ) {
        pde.openPatch( "../media/pd/main.pd" );
    }

#endif

    pde.addListener( this );
    
    pobjMgr.setSceneManager( sceneMgr );
    pobjMgr.nextScene();
    
}

void CustomApp::draw() {
    
    PNode* cube = pobjMgr.node( "my_cube" );
    
    // moving and rotating the cube
    cube_angle += Radian( draw_event.timeSinceLastFrame );
    cube->move(
        Math::Cos( cube_angle ) * 250,
        0,
        Math::Sin( cube_angle ) * 250
    );
    cube->orientation( Ogre::Vector3( mouse_rel.x * 360, 0, mouse_rel.y * 360 ) );
    
    // and send it to pd, see media/pd/main.pd for details
    pde.send( cube );
    
}

bool CustomApp::keyPressed(const OIS::KeyEvent &arg) {

    // keep default behavior
    PolymorphApplication::keyPressed( arg );
    
}

bool CustomApp::mouseMoved(const OIS::MouseEvent &arg) {

    // keep default behavior
    PolymorphApplication::mouseMoved( arg );
    
    mouse_rel.x = Real( arg.state.X.abs ) / window->getWidth();
    mouse_rel.y = Real( arg.state.Y.abs ) / window->getHeight();
    
}

void CustomApp::pdObjectTranslation( 
    const std::string name, const Ogre::Vector3& v) {

    PNode* p = pobjMgr.node( name );
    if ( p ) {
        p->move( v );
    }
    
}

void CustomApp::pdObjectRotation( 
    const std::string name, const Ogre::Quaternion& rot) {

    PNode* p = pobjMgr.node( name );
    if ( p ) {
        p->orientation( rot );
    }
    
}

void CustomApp::pdObjectScale( 
    const std::string name, const Ogre::Vector3& v) {

    PNode* p = pobjMgr.node( name );
    if ( p ) {
        p->scale( v );
    }
    
}

void CustomApp::pdObjectAll( 
    const std::string name, 
    const Ogre::Vector3& trans,
    const Ogre::Quaternion& rot,
    const Ogre::Vector3& scale) {
    
    PNode* p = pobjMgr.node( name );
    if ( p ) {
        p->move( trans );
        p->orientation( rot );
        p->scale( scale );
    }
    
}