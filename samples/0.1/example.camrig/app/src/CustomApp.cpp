/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ___________________________________( ^3^)_____________________________________
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 */

/* 
 * File:   CustomApp.cpp
 * Author: frankiezafe
 * 
 * Created on November 19, 2016, 6:07 PM
 */

#include "CustomApp.h"

using namespace std;
using namespace Ogre;
using namespace polymorph;

void CustomApp::createScene(void) {

    // keep default behavior
    PolymorphApplication::createScene();

    cube.cube(sceneMgr, "cute cube");
    cube.scale(50);
    light.sun(sceneMgr);
    light.pitch(Ogre::Radian(0.3));
    light.roll(Ogre::Radian(-0.3));

}

void CustomApp::createCamera(void) {

    // overwriting standard camera creation,
    // using the camera of the rig instead

    // creation of a configuration of the rig
    cd.name = "local_player_cam";
    cd.dist.set(200);
    cd.origin_yaw_range = PRange<Ogre::Real>(-45, 0, 45);
    cd.pivot_roll_range = PRange<Ogre::Real>(0, 20, 90);

    // loading the configuration in the rig
    camrig.configure(cd);
    // linking it to scene manager
    camrig.init(sceneMgr);
    // fitting the camera to the screen ratio
    camrig.fit(window);
    
    // the yaw of origin can now be expressed in percents
    current_yaw = cd.origin_yaw_range.percent( cd.origin_yaw_range.init );
    speed_yaw = 0;

    // the roll of pivot can now be expressed in percents
    current_roll = cd.pivot_roll_range.percent( cd.pivot_roll_range.init );
    speed_roll = 0;

}

void CustomApp::createCompositor(void) {

    // standard camera form PolymorphApplication is no set anymore!
    // The compositor has to be linked with the camera rig

    compositorMgr = root->getCompositorManager2();
    compositorwpname = "CompositorBaseWorkspace";
    const Ogre::IdString workspaceName(compositorwpname);
    if (!compositorMgr->hasWorkspaceDefinition(workspaceName)) {
        compositorMgr->createBasicWorkspaceDef(
                "CompositorBaseWorkspace",
                Ogre::ColourValue(0.4f, 0.4f, 0.4f),
                Ogre::IdString()
                );
    }
    compositorwp = compositorMgr->addWorkspace(
            sceneMgr, window,
            camrig.getCam(), // << important modification
            workspaceName, true);

}

void CustomApp::draw() {

    // call update to render the eulers of the rig
    camrig.update();
    
    // rotation of origin of the rig
    speed_yaw += ( ( ( 1 - mouse.x ) - current_yaw) - speed_yaw ) * 0.35;
    if (speed_yaw > 1) {
        speed_yaw = 1;
    } else if (speed_yaw < -1) {
        speed_yaw = -1;
    }
    current_yaw += speed_yaw * 0.8;
    camrig.yawRange(current_yaw, false); // << falde by default, to affect the origin
    
    // rooling the pivot will NOT alter the "TOP" of the camera,
    // and therfore, it will avoid gimbal locks when the roll is bringing the camera
    // near of the poles
    // rotation of pivot of the rig
    speed_roll += ( (mouse.y - current_roll) - speed_roll ) * 0.35;
    if (speed_roll > 1) {
        speed_roll = 1;
    } else if (speed_roll < -1) {
        speed_roll = -1;
    }
    current_roll += speed_roll * 0.8;
    camrig.rollRange(current_roll, true); // true to affect the pivot

}

bool CustomApp::keyPressed(const OIS::KeyEvent &arg) {

    // keep default behavior
    PolymorphApplication::keyPressed(arg);

}

bool CustomApp::keyReleased(const OIS::KeyEvent &arg) {

    // keep default behavior
    PolymorphApplication::keyReleased(arg);

}

bool CustomApp::mouseMoved(const OIS::MouseEvent &arg) {

    // keep default behavior
    PolymorphApplication::mouseMoved(arg);

    mouse = Ogre::Vector2(
            arg.state.X.abs * 1.f / window->getWidth(),
            arg.state.Y.abs * 1.f / window->getHeight()
            );

}

void CustomApp::windowResized(Ogre::RenderWindow* rw) {

    // each time window is resized, the ratio might have changed
    // the camrig camera has to be adapted

    // main window is "window"
    if (rw == window) {
        camrig.fit(window);
    }

}