/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ___________________________________( ^3^)_____________________________________
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 */

/* 
 * File:   CustomApp.h
 * Author: frankiezafe
 *
 * Created on November 19, 2016, 6:07 PM
 */

#ifndef CUSTOMAPP_H
#define CUSTOMAPP_H

#include "PolymorphApplication.h"

class ContactNode {
public:
    polymorph::PNode* node;
    Ogre::Vector3 position;
    Ogre::Real impulse;
    bool alive;
    ContactNode():
    node(0),
    position(0,0,0),
    impulse(0),
    alive(true)
    {}
    ~ContactNode() {
        if ( node ) {
            delete node;
        }
    }
};

class CustomApp :
public PolymorphApplication,
public polymorph::PBulletContactPairListener {
public:

    CustomApp();

    void createScene(void);

    void draw();
    
    bool keyPressed(const OIS::KeyEvent &arg);

    bool mouseMoved(const OIS::MouseEvent &arg);

    void generate();

    void contactPairStart(const polymorph::PBulletContactPair& pair);
    
    void contactPairUpdate(const polymorph::PBulletContactPair& pair);
    
    void contactPairEnd(const polymorph::PBulletContactPair& pair);

private:

    polymorph::PMaterial objectmat;
    polymorph::PMaterial contactmat_burst;
    polymorph::PMaterial contactmat_hot;
    polymorph::PMaterial contactmat_cold;
    polymorph::PLight sun;

    uint16_t ground_count;
    polymorph::PNode* ground;
    
    uint16_t balls_count;
    polymorph::PNode** balls;

    polymorph::PRange< Ogre::Real > size_range;
    polymorph::PNodeData sphere_data;
    polymorph::PNodeData cube_data;
    polymorph::PNodeData visualisation_data;
    
    std::map< polymorph::PBCPkey, ContactNode > contact_nodes;
    typedef std::map< polymorph::PBCPkey, ContactNode >::iterator 
        ContactIterator;

};

#endif /* CUSTOMAPP_H */

