/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ___________________________________( ^3^)_____________________________________
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 */

/* 
 * File:   CustomApp.cpp
 * Author: frankiezafe
 * 
 * Created on November 19, 2016, 6:07 PM
 */

#include <map>

#include "CustomApp.h"

using namespace std;
using namespace Ogre;
using namespace polymorph;

static CustomApp* _instance;
 
CustomApp::CustomApp() :
PolymorphApplication()
{
}

void CustomApp::createScene(void) {

    // keep default behavior
    PolymorphApplication::createScene();
    
    PBullet::addContactPairListener(this);
    PBullet::enableContactEvents(true);
    PBullet::start(root);
    PBullet::gravity(btVector3(0, -100, 0));

    size_range.set( 3, 50 );
    
    sun.sun(sceneMgr);
    sun.pitch(Radian(0.1));

    objectmat.create("Essential", "objectmat");
    objectmat.polygon(PM_WIREFRAME);
    objectmat.diffuse(0, 0.9, 0, 0.2);
    objectmat.emissive(0.8, 0.8, 0.8);
    objectmat.getMaterial().getPointer()->setDepthWriteEnabled(false);
    objectmat.getMaterial().getPointer()->setSceneBlending(SBT_TRANSPARENT_ALPHA);
    objectmat.fogMode(Ogre::FOG_LINEAR);
    objectmat.fogDistance(500, 1000);
    objectmat.fogColor(0.4, 0.4, 0.4);

    contactmat_burst.create("Essential", "contactmat_burst");
    contactmat_burst.diffuse(0, 0, 0);
    contactmat_burst.emissive(1, 0, 0);

    contactmat_hot.create("Essential", "contactmat_hot");
    contactmat_hot.diffuse(0, 0, 0);
    contactmat_hot.emissive(1, 1, 0);

    contactmat_cold.create("Essential", "contactmat_cold");
    contactmat_cold.diffuse(0, 0, 0);
    contactmat_cold.emissive(0, 1, 1);
    
    sphere_data.visible = true;
    sphere_data.internal = true;
    sphere_data.sphere = true;
    sphere_data.bullet_type = BT_DYNAMIC_SPHERE;
    sphere_data.bullet_restless = true;
    sphere_data.mat_group = "Essential";
    sphere_data.mat_name = "objectmat";
    
    cube_data = sphere_data;
    cube_data.sphere = false;
    cube_data.cube = true;
    cube_data.bullet_type = BT_DYNAMIC_BOX;
    
    visualisation_data.visible = true;
    visualisation_data.internal = true;
    visualisation_data.sphere = true;
    visualisation_data.origin_scale = Vector3(5,5,5);
    visualisation_data.mat_group = "Essential";
    visualisation_data.mat_name = "contactmat_cold";

    ground = new PNode();
    ground->cube(sceneMgr, "ground");
    ground->move(0, -100, 0);
    ground->scale(700, 10, 500);
    ground->physics(BT_STATIC_BOX);
    ground->getMaterial()->diffuse(0.16, 0.15, 0.17);
    //    ground->debug( true );

    balls_count = 100;
    balls = 0;

    cam->setPosition(0, 300, 500);
    cam->lookAt(0, -100, 0);

//    trayMgr->hideFrameStats();

}

void CustomApp::generate() {

    if (!balls) {
        balls = new PNode*[ balls_count ];
        for (uint16_t i = 0; i < balls_count; ++i) {
            stringstream ss;
            ss << "ball_" << i;
            if (Math::RangeRandom(0, 1) > 0.35) {
                sphere_data.name = ss.str();
                sphere_data.origin_scale = size_range.lerp( Math::RangeRandom(0,1) );
                balls[i] = new PNode( sceneMgr, &sphere_data );
            } else {
                cube_data.name = ss.str();
                cube_data.origin_scale = Vector3 (
                        size_range.lerp( Math::RangeRandom(0,1) ),
                        size_range.lerp( Math::RangeRandom(0,1) ),
                        size_range.lerp( Math::RangeRandom(0,1) )
                        );
                balls[i] = new PNode( sceneMgr, &cube_data );
            }
            balls[i]->move(
                    Ogre::Math::RangeRandom(-20, 20),
                    Ogre::Math::RangeRandom(0, 1200),
                    Ogre::Math::RangeRandom(-20, 20)
                    );
            balls[i]->impulse(
                    Ogre::Math::RangeRandom(-50, 50),
                    Ogre::Math::RangeRandom(-50, 50),
                    Ogre::Math::RangeRandom(-50, 50)
                    );
        }
    } else {
        for (uint16_t i = 0; i < balls_count; ++i) {
            balls[i]->resetVelocities();
            balls[i]->move(
                    Ogre::Math::RangeRandom(-20, 20),
                    Ogre::Math::RangeRandom(0, 1200),
                    Ogre::Math::RangeRandom(-20, 20)
                    );
            balls[i]->impulse(
                    Ogre::Math::RangeRandom(-50, 50),
                    Ogre::Math::RangeRandom(-50, 50),
                    Ogre::Math::RangeRandom(-50, 50)
                    );
        }
    }

}

void CustomApp::draw() {
        
    std::vector< PBCPkey > zombies;
    zombies.reserve( contact_nodes.size() );
    ContactIterator it = contact_nodes.begin();
    ContactIterator ite = contact_nodes.end();
    
    for( ; it != ite; ++it ) {
        
        ContactNode& cn = it->second;
        
        if ( !cn.alive ) {
            zombies.push_back( it->first );
            continue;
        }
        Real im = abs(cn.impulse) * 0.000001;
        
        if ( !cn.node ) {
            cn.node = new PNode(sceneMgr, &visualisation_data);
            cn.node->material( &contactmat_burst );
        } else if ( im > 0.01 ) {
            cn.node->material( &contactmat_hot );
        } else {
            cn.node->resetMaterial();
        }
        cn.node->move( cn.position );
        cn.node->scale((3 + im));
        
    }
    
    std::vector< PBCPkey >::iterator itz = zombies.begin();
    std::vector< PBCPkey >::iterator itze = zombies.end();
    for( ; itz != itze; ++itz ) {
        contact_nodes.erase( (*itz) );
    }

}

bool CustomApp::keyPressed(const OIS::KeyEvent &arg) {

    // keep default behavior
    PolymorphApplication::keyPressed(arg);

    if (arg.key == OIS::KC_SPACE) {
        generate();
    }

}

bool CustomApp::mouseMoved(const OIS::MouseEvent &arg) {

    // keep default behavior
    PolymorphApplication::mouseMoved(arg);

}

void CustomApp::contactPairStart(const polymorph::PBulletContactPair& pair) {

    contact_nodes[ pair.UID() ].impulse = pair.current_values().impulse;
    contact_nodes[ pair.UID() ].position = PUtil::convert( pair.current_values().world );

}

void CustomApp::contactPairUpdate(const polymorph::PBulletContactPair& pair) {

    contact_nodes[ pair.UID() ].impulse = pair.delta_values().impulse;
    contact_nodes[ pair.UID() ].position = PUtil::convert( pair.current_values().world );

}

void CustomApp::contactPairEnd(const polymorph::PBulletContactPair& pair) {
    
    contact_nodes[ pair.UID() ].alive = false;
    
}