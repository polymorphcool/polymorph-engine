/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ___________________________________( ^3^)_____________________________________
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
*/

/* 
 * File:   CustomApp.cpp
 * Author: frankiezafe
 * 
 * Created on November 19, 2016, 6:07 PM
 */

#include "CustomApp.h"

using namespace std;
using namespace Ogre;
using namespace polymorph;

CustomApp::CustomApp(): 
PolymorphApplication(),
mouse_button(0)
{
    polyMgr.verbose( true );
    polyMgr.parse();
}

void CustomApp::setupResources(void) {
    
    PolymorphApplication::setupResources( polyMgr.getProject() );
    
}

void CustomApp::createCompositor( void ) {
    
    compositorMgr = root->getCompositorManager2( );
    compositorwpname = "CustomCompositor";
    const Ogre::IdString workspaceName( compositorwpname );
    compositorwp = compositorMgr->addWorkspace( sceneMgr, window, cam, workspaceName, true );
    
    // this is the tricky part!
    // you have to know in wich node and in wich pass your shader is used
    // see connect >>Cleaner<< 0 1 CompositorSampleStdRenderer 0 1 in media/compositor/custom.compositor
    CompositorPassQuad* cpd = (CompositorPassQuad* ) compositorwp->getNodeSequence()[0]->_getPasses()[1];
    GpuProgramParametersSharedPtr gpp = cpd->getPass()->getFragmentProgramParameters();
    
    // check bg.material for definition of the shader params
    // center is a float2 (vec2) => stored at float 0 & 1
    compositor_bg_center = gpp->getFloatPointer(0);
    // radius is a float => stored at float 2
    compositor_bg_radius = gpp->getFloatPointer(2);
    // ratio is a float => stored at float 3
    compositor_bg_ratio = gpp->getFloatPointer(3);
    // center_color is a float4 => stored at float 4, 5, 6 & 7
    compositor_bg_center_color = gpp->getFloatPointer(4);
    // border_color is a float4 => stored at float 8, 9, 10 & 11
    compositor_bg_border_color = gpp->getFloatPointer(8);
    
}

void CustomApp::createScene(void) {

    // keep default behavior
    PolymorphApplication::createScene();
    
    polyMgr.setSceneManager( sceneMgr );
    polyMgr.nextScene();
    
    PNode* f = polyMgr.node( "floor" );
    PNode* n = polyMgr.node( "psigle" );
    f->attach( n );
    
}

void CustomApp::draw() {

//    cout << draw_event.timeSinceLastFrame << endl;
    
    PNode* n = polyMgr.node( "floor" );
    n->yaw( Ogre::Radian( draw_event.timeSinceLastFrame * 0.1 ) );
    
    switch( mouse_button ) {
    
        case 1: // left
            compositor_bg_radius[ 0 ] = mouse_norm.distance( Ogre::Vector2( 0.5,0.5 ) );
            break;
            
        case 2: // right
            compositor_bg_center[ 0 ] = mouse_norm.x;
            compositor_bg_center[ 1 ] = mouse_norm.y;
            break;
        
        default:
            {
                PLight* sun = polyMgr.light( "main_sun" );
                sun->specular( 
                    mouse_norm.x, 
                    mouse_norm.y, 
                    compositor_bg_center_color[ 2 ]
                );
                compositor_bg_center_color[ 0 ] = mouse_norm.x;
                compositor_bg_center_color[ 1 ] = mouse_norm.y;
            }   
            break;
        
    }
    
}

bool CustomApp::keyPressed(const OIS::KeyEvent &arg) {

    // keep default behavior
    PolymorphApplication::keyPressed( arg );
    
}

bool CustomApp::mousePressed(const OIS::MouseEvent& arg, OIS::MouseButtonID id) {
    
    // keep default behavior
    PolymorphApplication::mousePressed( arg, id );
    mouse_button = ( (uint) id ) + 1;
    
}

bool CustomApp::mouseReleased(const OIS::MouseEvent& arg, OIS::MouseButtonID id) {
    
    // keep default behavior
    PolymorphApplication::mouseReleased( arg, id );
    mouse_button = 0;
    
}

bool CustomApp::mouseMoved(const OIS::MouseEvent &arg) {

    // keep default behavior
    PolymorphApplication::mouseMoved( arg );
    
    mouse_norm = Ogre::Vector2(
        Ogre::Real( arg.state.X.abs ) / window->getWidth(),
        Ogre::Real( arg.state.Y.abs ) / window->getHeight()
    );
    
}