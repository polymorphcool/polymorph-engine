/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ___________________________________( ^3^)_____________________________________
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
*/

/* 
 * File:   CustomApp.h
 * Author: frankiezafe
 *
 * Created on November 19, 2016, 6:07 PM
 */

#ifndef CUSTOMAPP_H
#define CUSTOMAPP_H

#include "PolymorphApplication.h"
#include "Compositor/Pass/PassQuad/OgreCompositorPassQuad.h"

#define RESET_ANIMATION_DURATION    3

class CustomApp : public PolymorphApplication {

public:
    
    CustomApp();
    
    void setupResources(void);
    
    void createCompositor( void );
    
    void createScene(void);
    
    void draw();
    
    bool keyPressed(const OIS::KeyEvent &arg);
    
    bool mousePressed(const OIS::MouseEvent& arg, OIS::MouseButtonID id);
    bool mouseReleased(const OIS::MouseEvent &arg, OIS::MouseButtonID id);
    bool mouseMoved(const OIS::MouseEvent &arg);
    
private:
    
    polymorph::PObjectManager polyMgr;
    
    bool resetAnim;
    Ogre::Real resetTimer;
    
    Ogre::Vector2 mouse_norm;
    Ogre::Vector2 mouse_norm_rel;
    uint mouse_button;
    
    polymorph::PNode bone_hightlight;
    polymorph::PNode* sophiekhan;
    size_t bones_current;
    size_t bones_num;
    polymorph::PBone** bones;
    Ogre::Quaternion* bquats_init;
    Ogre::Quaternion* bquats_current;
    Ogre::Vector3* bpos_init;
    Ogre::Vector3* bpos_current;
    
};

#endif /* CUSTOMAPP_H */