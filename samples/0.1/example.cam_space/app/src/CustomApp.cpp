/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ___________________________________( ^3^)_____________________________________
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 */

/* 
 * File:   CustomApp.cpp
 * Author: frankiezafe
 * 
 * Created on November 19, 2016, 6:07 PM
 */

#include "CustomApp.h"

using namespace std;
using namespace Ogre;
using namespace polymorph;

void CustomApp::createScene( void ) {

    // keep default behavior
    PolymorphApplication::createScene( );

    trayMgr->hideAll( );

    copy = false;

    empty.empty( sceneMgr );
    empty.debug( true );
    empty.scale( 1 );

    custom_cam.init( sceneMgr, "custom_cam" );
    custom_cam.debug( true );
    custom_cam.showAxis( true );
    custom_cam.move( 0, 0, 100, true );

    ball_cam.sphere( sceneMgr );
    ball_cam.debug( true );
    ball_cam.scale( 10 );
    ball_cam.move( 100, 0, -200 );
    custom_cam.attach( &ball_cam );


    // here below is pure Ogre calls
    uint tsize = 256;
    // loading the custom compositor from resources, 
    // media/compositor/custom.compositor
    custom_wpname = "CustomCompositor";
    const Ogre::IdString cwName( custom_wpname );
    // creation of a custom texture
    TexturePtr texture = Ogre::TextureManager::getSingleton( ).createManual(
            "custom_texture",
            "Internal", TEX_TYPE_2D, tsize, tsize, 2, PF_R8G8B8,
            TU_RENDERTARGET
            );
    // getting the render target
    RenderTexture * rt = texture->getBuffer( )->getRenderTarget( );
    // creation of a new workspace rendering cam to custom render target
    custom_wp = compositorMgr->addWorkspace(
            sceneMgr, rt,
            custom_cam.getOgreCam( ),
            cwName, true );
    // creation of a custom material
    MaterialPtr custom_mat = Ogre::MaterialManager::getSingleton( ).create(
            "debugOverlay", "Internal" );
    // biniding of the RTT texture to the texture slot of the material
    custom_mat->getTechnique( 0 )->getPass( 0 )->createTextureUnitState( )->setTexture( texture );
    // displaying the camera pov in an overlay
    Ogre::OverlayManager& overlayManager = Ogre::OverlayManager::getSingleton( );
    Overlay* debugCameraOverlay = overlayManager.create( "debugCameraOverlay" );
    // creation of a 2d pnael
    Ogre::OverlayContainer* panel = static_cast < Ogre::OverlayContainer* > (
            overlayManager.createOverlayElement( "Panel", "PanelName0" ) );
    panel->setMetricsMode( Ogre::GMM_PIXELS );
    panel->setPosition( 0, 0 );
    panel->setDimensions( tsize, tsize );
    panel->setMaterialName( "debugOverlay" );
    debugCameraOverlay->add2D( panel );
    debugCameraOverlay->show( );

}

void CustomApp::draw( ) {

    //    custom_cam.setFovy(Ogre::Radian(mouse_rel.x * Math::PI));
    empty.pitch( Radian( draw_event.timeSinceLastFrame * 0.25 ) );
    empty.yaw( Radian( draw_event.timeSinceLastFrame * 0.5 ) );
    ball_cam.yaw( Radian( draw_event.timeSinceLastFrame * 0.5 ) );
    custom_cam.yaw( Radian( draw_event.timeSinceLastFrame * 0.5 ) );

    empty.move(
            ( mouse_rel.x - 0.5 ) * 300,
            ( -mouse_rel.y + 0.5 ) * 300,
            0
            );

    if ( copy ) {
        Vector3 p = custom_cam.worldToCam( empty.getTrans( ) );
        ball_cam.move( p );
        Quaternion q = custom_cam.worldToCam( empty.getOrientation( ) );
        ball_cam.orientation( q );
    }

}

bool CustomApp::keyPressed( const OIS::KeyEvent &arg ) {

    // keep default behavior
    PolymorphApplication::keyPressed( arg );

    switch ( arg.key ) {

        case OIS::KC_SPACE:
            copy = !copy;
            break;

        default:
            break;

    }

}

bool CustomApp::mouseMoved( const OIS::MouseEvent &arg ) {

    // keep default behavior
    PolymorphApplication::mouseMoved( arg );

    mouse_rel.x = arg.state.X.abs * 1.0 / window->getWidth( );
    mouse_rel.y = arg.state.Y.abs * 1.0 / window->getHeight( );

}