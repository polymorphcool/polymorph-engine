/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ___________________________________( ^3^)_____________________________________
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
*/

/* 
 * File:   CustomApp.cpp
 * Author: frankiezafe
 * 
 * Created on November 19, 2016, 6:07 PM
 */

#include "CustomApp.h"

using namespace std;
using namespace Ogre;
using namespace polymorph;

void CustomApp::createScene( void ) {

    // keep default behavior
    PolymorphApplication::createScene( );

    sceneMgr->setAmbientLight( Ogre::ColourValue( 0.1, 0.1, 0.1 ) );

    cube.cube( sceneMgr );
    cube.scale( 20 );

    cube2.cube( sceneMgr );
    cube2.scale( 10 );
    cube2.move( 0, 60, 0 );
    cube.attach( &cube2 );

    floor.cube( sceneMgr );
    floor.scale( 100, 5, 100 );
    floor.move( 0, -100, 0 );

    lpoint.sun( sceneMgr );
    lpoint.debug( true );
    //    lpoint.range( 300 );

    PUtil::print( lpoint.getTrans( ) );
    PUtil::print( lpoint.getOrientation( ) );
    PUtil::print( lpoint.getScale( ) );

    lpoint.move( 0, 50, 0 );

    lpoint2.point( sceneMgr );
    lpoint2.debug( true );
    lpoint2.move( 70, 10, 70 );
    lpoint3.point( sceneMgr );
    lpoint3.debug( true );
    lpoint3.diffuse( 0.5, 0.5, 0.5 );
    lpoint3.move( -70, -10, -70 );

    cube.attach( &lpoint2 );
    cube.attach( &lpoint3 );

}

void CustomApp::draw( ) {

    //    cout << draw_event.timeSinceLastFrame << endl;
    //    PUtil::print( mouse_rel );
    //    PUtil::print( mouse_abs );
    //    PUtil::print( mouse_abs_norm );

    //    lpoint.range( mouse_abs.x );

    lrot += 1.f * draw_event.timeSinceLastFrame;
    lpoint.roll( Ogre::Radian( draw_event.timeSinceLastFrame ) );
    //    lpoint.move(
    //            Math::Cos( lrot ) * 150,
    //            Math::Sin( lrot ) * 600,
    //            0
    //    );

    Ogre::ColourValue diff(
            ( Math::Cos( lrot * 0.3 + 1 ) ) * 0.5,
            ( Math::Sin( lrot * 0.3 + 1 ) ) * 0.5,
            1
            );

    lpoint2.diffuse( diff * 0.5 );
    
    diff.r = ( Math::Cos( lrot * 0.1 + 1 ) ) * 0.5;
    diff.g = 0;
    diff.b = ( Math::Cos( lrot * 0.1 + 1 ) ) * 0.5;
    
    lpoint3.diffuse( diff * 0.5 );

    //    lpoint2.move(
    //            Math::Cos( -lrot ) * 100,
    //            0,
    //            Math::Sin( -lrot ) * 100
    //    );
    //    lpoint3.move( lpoint2.getTrans() * -1 );

    cube.yaw( Radian( 0.1f * draw_event.timeSinceLastFrame ) );
    cube.pitch( Radian( 0.2f * draw_event.timeSinceLastFrame ) );
    //    lpoint.roll( Radian( mouse_rel.x * draw_event.timeSinceLastFrame ) );

    mouse_rel = Ogre::Vector2::ZERO;

}

bool CustomApp::keyPressed( const OIS::KeyEvent &arg ) {

    // keep default behavior
    PolymorphApplication::keyPressed( arg );

    // show/hide overlay panels
    if ( arg.key == OIS::KC_H ) {
        if ( trayMgr->isCursorVisible( ) ) trayMgr->hideAll( );
        else trayMgr->showAll( );
    }

}

bool CustomApp::mouseMoved( const OIS::MouseEvent &arg ) {

    // keep default behavior
    PolymorphApplication::mouseMoved( arg );

    mouse_rel.x = arg.state.X.rel;
    mouse_rel.y = arg.state.Y.rel;
    mouse_abs.x = arg.state.X.abs;
    mouse_abs.y = arg.state.Y.abs;

    mouse_abs_norm.x = mouse_abs.x / window->getWidth( );
    mouse_abs_norm.y = mouse_abs.y / window->getHeight( );

}