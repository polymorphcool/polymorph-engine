/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ___________________________________( ^3^)_____________________________________
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
*/

/* 
 * File:   CustomApp.cpp
 * Author: Eurydice Cardon
 * 
 * Created during February 2017
 */

#include "CustomApp.h"

using namespace std;
using namespace Ogre;
using namespace polymorph;

void CustomApp::createScene( void ) {

    // keep default behavior
    PolymorphApplication::createScene( );

    sceneMgr->setAmbientLight( Ogre::ColourValue( 0.1, 0.1, 0.1 ) );

    lpoint.sun( sceneMgr );
    lpoint.debug( true );
    //lpoint.range( 300 );
    lpoint.move( 0, 0, 150 );
    
    
    //CELLSHADING TEST:
    /*
    cellshaded.sphere( sceneMgr );
    cellshaded.scale( 100 );
    cellshaded.material("General", "Examples/CelShading");
    Ogre::SubEntity* e = cellshaded.entity()->getSubEntity(0);
    e->setCustomParameter( 1, Vector4( 1, 1, 1, 1 ) );
    e->setCustomParameter( 2, Vector4( 0.5, 0, 0.5, 1 ) );
    e->setCustomParameter( 3, Vector4( 0, 0, 0.8, 1 ) );
    e->setCustomParameter( 4, Vector4( 0, 150, 0, 0));
    cellshaded.attach( &lpoint );
    */
    
    lpoint2.point( sceneMgr );
    lpoint2.debug( true );
    lpoint2.move( 70, 10, 70 );
    lpoint3.point( sceneMgr );
    lpoint3.debug( true );
    lpoint3.diffuse( 0.5, 0.5, 0.5 );
    lpoint3.move( -70, -10, -70 );
    pivot.empty( sceneMgr );
    pivot.attach( &lpoint2 );
    pivot.attach( &lpoint3 );
    
    changeAlpha = false;
    freeze = false;
    isAnimatingSubmesh = false;
    animSubmeshStep = 0.0f;
    isAnimatingNodes = false;
    animNodesStep = 0.0f;
    currentSubmesh = 1;
    animPolyStep = 0.0f;
    
    initMaterials();
    
    initNodes();
    
    polyAnchor.empty( sceneMgr );
    polymesh.load( sceneMgr, "General", "polymorph-sigle.mesh" );
    polymesh.scale( 200 );
    polymesh.move( 0, -75, 0) ;
    polyAnchor.attach( &polymesh );
    polymesh.material( "General", materials[currentNodes[1]], false, 1 );
    polymesh.material( "General", materials[currentNodes[0]], false, 0 );

    initTray();
    
}

void CustomApp::draw( ) {
    
    if( freeze ) {
        return;
    }
    
    //CELLSHADING TEST:
    //cellshaded.yaw(Radian (draw_event.timeSinceLastFrame));
    //Ogre::SubEntity* e = cellshaded.entity()->getSubEntity(0);
    //e->setCustomParameter( 4, Vector4( lpoint.getTrans()));
    
    if( isAnimatingSubmesh ) {
        animateSubmeshChange();
    }
    
    if( isAnimatingNodes ) {
        animateNodeChange();
    }
    
    animatePolymesh();
    animateRainbowMat();
    
    lrot += 1.f * draw_event.timeSinceLastFrame;
    lpoint.roll( Ogre::Radian( draw_event.timeSinceLastFrame * 0.25f) );

    Ogre::ColourValue diff(
            ( Math::Cos( lrot * 0.3 + 1 ) ) * 0.5,
            ( Math::Sin( lrot * 0.3 + 1 ) ) * 0.5,
            1
            );
    
    diff.r = ( Math::Cos( lrot * 0.1 + 1 ) ) * 0.5;
    diff.g = 0;
    diff.b = ( Math::Cos( lrot * 0.1 + 1 ) ) * 0.5;

    pivot.yaw( Radian( 0.1f * draw_event.timeSinceLastFrame ) );
    pivot.pitch( Radian( 0.2f * draw_event.timeSinceLastFrame ) );
    
    mouse_rel = Ogre::Vector2::ZERO;

}

bool CustomApp::keyPressed( const OIS::KeyEvent &arg ) {

    PolymorphApplication::keyPressed( arg );

    if ( arg.key == OIS::KC_H ) {
        if ( trayMgr->isCursorVisible( ) ) trayMgr->hideAll( );
        else trayMgr->showAll( );
    }
    
    switch( arg.key ) {
        case OIS::KC_A:
            pMaterials[currentNodes[currentSubmesh]].ambient( getRandomColour() );
            break;
        case OIS::KC_D:
            pMaterials[currentNodes[currentSubmesh]].diffuse( getRandomColour() );
            break;
        case OIS::KC_E:
            pMaterials[currentNodes[currentSubmesh]].emissive( getRandomColour() );
            break;
        case OIS::KC_S:
            pMaterials[currentNodes[currentSubmesh]].specular( getRandomColour() );
            break;
        case OIS::KC_T:
            changeTexture( true );
            break;
        case OIS::KC_Q:
            trayMgr->checkBoxToggled( alphaCheckBox );
            changeAlpha = !changeAlpha;
            break;
        case OIS::KC_P:
            pMaterials[currentNodes[currentSubmesh]].print( true );
            break;
    }
    
    if ( arg.key == OIS::KC_SPACE ) {
        freeze = !freeze;
    }
    
    if ( !isAnimatingNodes && !freeze ) {
        switch (arg.key) {
            case OIS::KC_UP:
                nextSubmesh();
                break;
            case OIS::KC_DOWN:
                previousSubmesh();
                break;
        }
    }
    
    if ( !isAnimatingSubmesh && !freeze ) {
        switch (arg.key) {
            case OIS::KC_LEFT:
                nextNode();
                break;
            case OIS::KC_RIGHT:
                previousNode();
                break;
        }
    }
    
    updateTray();
}

bool CustomApp::mouseMoved( const OIS::MouseEvent &arg ) {

    // keep default behavior
    PolymorphApplication::mouseMoved( arg );

    mouse_rel.x = arg.state.X.rel;
    mouse_rel.y = arg.state.Y.rel;
    mouse_abs.x = arg.state.X.abs;
    mouse_abs.y = arg.state.Y.abs;

    mouse_abs_norm.x = mouse_abs.x / window->getWidth( );
    mouse_abs_norm.y = mouse_abs.y / window->getHeight( );

}

void CustomApp::buttonHit(OgreBites::Button* button) {
    
    PolymorphApplication::buttonHit( button );
    
    if( button->getName() == "colorbutton") {
        pMaterials[currentNodes[currentSubmesh]].ambient( getRandomColour() );
        pMaterials[currentNodes[currentSubmesh]].diffuse( getRandomColour() );
        pMaterials[currentNodes[currentSubmesh]].emissive( getRandomColour() );
        pMaterials[currentNodes[currentSubmesh]].specular( getRandomColour() );
    }
}

void CustomApp::initTray() {
    
    trayMgr->setTrayWidgetAlignment(
            OgreBites::TL_TOPLEFT, GHA_LEFT );
    trayMgr->setTrayPadding( 2 );
    alphaCheckBox = trayMgr->createCheckBox( OgreBites::TL_BOTTOMRIGHT,
            "alphabox", "Allow alpha change", 250 );
    randColorButton = trayMgr->createButton( OgreBites::TL_BOTTOMRIGHT,
            "colorbutton", "Random Color");
    textureMenu = trayMgr->createLongSelectMenu( OgreBites::TL_BOTTOMRIGHT,
            "texturemenu", "Texture", 250, 10, textures);
    applyButton = trayMgr->createButton( OgreBites::TL_TOP,
            "applybutton", "Apply");
    controlsTextBox = trayMgr->createTextBox( OgreBites::TL_TOPLEFT,
            "controlsbox", "Controls", 250, 120 );
    controlsTextBox->setText( "UP to select balloon\n"
            "DOWN to select basket\n"
            "LEFT/RIGHT to select material\n"
            "P to print material");
}

void CustomApp::updateTray() {
    //Nothing to see here
}

void CustomApp::initMaterials() {
    materials.push_back("user_custom");
    materials.push_back("cistex");
    materials.push_back("ruby");
    materials.push_back("sapphire");
    materials.push_back("period");
    materials.push_back("radioactive");
    materials.push_back("ghost");
    materials.push_back("blending");
    materials.push_back("grid_flat");
    materials.push_back("grid_gouraud");
    materials.push_back("grid_phong");
    materials.push_back("grid_wireframe");
    materials.push_back("grid_points");
    materials.push_back("insect");
    materials.push_back("cubic");
    materials.push_back("Examples/CelShading");
    
    pMaterials = new PMaterial[materials.size()];
    for( int i = 0; i < materials.size(); ++i ) {
        PMaterial* m = &pMaterials[i];
        m->load( "General", materials[i] );
    }
    
    //Default materials index
    currentNodes.push_back( DEFAULT_BASKET );
    currentNodes.push_back( DEFAULT_BALLOON );
    
    for (int i = 0; i < currentNodes.size(); ++i) {
        lastNodes.push_back(currentNodes[i]);
    }
    
    textures.push_back("clouds.jpg");
    textures.push_back("cystitex.png");
    textures.push_back("grid.png");
    textures.push_back("picked.png");
    textures.push_back("Material_dirt.jpg");
    textures.push_back("Material_grass.png");
    
    rainbowMaterial.load( "General", "user_custom" );
    for( int i = 0; i < 12; ++i ) {
        rainbowValues.push_back( Math::RangeRandom( 0, 1 ) );
        rainbowValuesAscend.push_back( true );
    }
    rainbowMaterial.ambient( Ogre::ColourValue( rainbowValues[0],
                                                rainbowValues[1],
                                                rainbowValues[2]));
    rainbowMaterial.diffuse( Ogre::ColourValue( rainbowValues[3],
                                                rainbowValues[4],
                                                rainbowValues[5]));
    rainbowMaterial.emissive( Ogre::ColourValue( rainbowValues[6],
                                                 rainbowValues[7],
                                                 rainbowValues[8]));
    rainbowMaterial.specular( Ogre::ColourValue( rainbowValues[9],
                                                 rainbowValues[10],
                                                 rainbowValues[11]));
}

void CustomApp::initNodes() {
    spheres = new PNode[materials.size()];
    spheresPivot.empty( sceneMgr );
    for( int i = 0; i < materials.size(); ++i ) {
        PNode* n = &spheres[i];
        n->sphere( sceneMgr );
        n->scale( DEFAULT_SPHERE_SIZE );
        n->translate( 0, 0, 500 );
        n->material( pMaterials[i].getGroup(), pMaterials[i].getName() );
        spheresPivot.attach( n );
        spheresPivot.yaw(Radian(-Math::PI * 2 / materials.size()));
    }
    spheresPivot.move( 0, 69, -500 );
    spheres[currentNodes[1]].scale( 0 );
    spheresPivot.yaw( Radian (-Math::PI * 2 / materials.size()) * currentNodes[1]);
    
    cubes = new PNode[materials.size()];
    cubesPivot.empty( sceneMgr );
    for( int i = 0; i < materials.size(); ++i ) {
        PNode* n = &cubes[i];
        n->cube( sceneMgr );
        n->scale( 0 );
        n->translate( 0, 0, 500 );
        n->material( pMaterials[i].getGroup(), pMaterials[i].getName() );
        cubesPivot.attach( n );
        cubesPivot.yaw(Radian(-Math::PI * 2 / materials.size()));
    }
    cubesPivot.move( 0, -42, -500 );
    cubes[currentNodes[0]].scale( 0 );
    cubesPivot.yaw( Radian (-Math::PI * 2 / materials.size()) * currentNodes[0]);
}

void CustomApp::previousSubmesh() {
    if (currentSubmesh > 0) {
        currentSubmesh--;
        isAnimatingSubmesh = true;
        animSubmeshStep = 0.0f;
    }
}

void CustomApp::nextSubmesh() {
    if (currentSubmesh < currentNodes.size() - 1) {
        currentSubmesh++;
        isAnimatingSubmesh = true;
        animSubmeshStep = 0.0f;
    }
}

void CustomApp::previousNode() {
    initPreviousNodeAnim();
    lastNodes[currentSubmesh] = currentNodes[currentSubmesh];
    if (currentNodes[currentSubmesh] <= 0) {
        currentNodes[currentSubmesh] = materials.size() - 1;
    } else {
        currentNodes[currentSubmesh]--;
    }
    polymesh.material(pMaterials[currentNodes[currentSubmesh]].getGroup(),
            pMaterials[currentNodes[currentSubmesh]].getName(),
            false,
            currentSubmesh);
}

void CustomApp::nextNode() {
    initNextNodeAnim();
    lastNodes[currentSubmesh] = currentNodes[currentSubmesh];
    if (currentNodes[currentSubmesh] >= materials.size() - 1) {
        currentNodes[currentSubmesh] = 0;
    } else {
        currentNodes[currentSubmesh]++;
    }
    polymesh.material(pMaterials[currentNodes[currentSubmesh]].getGroup(),
            pMaterials[currentNodes[currentSubmesh]].getName(),
            false,
            currentSubmesh);
}

void CustomApp::initPreviousNodeAnim() {
    switch( currentSubmesh ) {
        case 0:
            if( !isAnimatingNodes ) {
                pivotQuatStart = cubesPivot.getOrientation();
                pivotQuatEnd = pivotQuatStart *
                            Quaternion(Radian(Math::PI * 2 / materials.size()),
                                        Vector3( 0, 1, 0 ));
            } else {
                pivotQuatStart = cubesPivot.getOrientation();
                pivotQuatEnd = pivotQuatEnd *
                            Quaternion(Radian(Math::PI * 2 / materials.size()),
                                        Vector3( 0, 1, 0 ));
            }
            animNodesStep = 0.0f;
            isAnimatingNodes = true;
            break;
        case 1:
            if( !isAnimatingNodes ) {
                pivotQuatStart = spheresPivot.getOrientation();
                pivotQuatEnd = pivotQuatStart *
                            Quaternion(Radian(Math::PI * 2 / materials.size()),
                                        Vector3( 0, 1, 0 ));
            } else {
                pivotQuatStart = spheresPivot.getOrientation();
                pivotQuatEnd = pivotQuatEnd *
                            Quaternion(Radian(Math::PI * 2 / materials.size()),
                                        Vector3( 0, 1, 0 ));
            }
            animNodesStep = 0.0f;
            isAnimatingNodes = true;
            break;
    }
}

void CustomApp::initNextNodeAnim() {
    switch( currentSubmesh ) {
        case 0:
            if( !isAnimatingNodes ) {
                pivotQuatStart = cubesPivot.getOrientation();
                pivotQuatEnd = pivotQuatStart *
                            Quaternion(Radian(-Math::PI * 2 / materials.size()),
                                        Vector3( 0, 1, 0 ));
            } else {
                pivotQuatStart = cubesPivot.getOrientation();
                pivotQuatEnd = pivotQuatEnd *
                            Quaternion(Radian(-Math::PI * 2 / materials.size()),
                                        Vector3( 0, 1, 0 ));
            }
            animNodesStep = 0.0f;
            isAnimatingNodes = true;
            break;
        case 1:
            if( !isAnimatingNodes ) {
                pivotQuatStart = spheresPivot.getOrientation();
                pivotQuatEnd = pivotQuatStart *
                            Quaternion(Radian(-Math::PI * 2 / materials.size()),
                                        Vector3( 0, 1, 0 ));
            } else {
                pivotQuatStart = spheresPivot.getOrientation();
                pivotQuatEnd = pivotQuatEnd *
                            Quaternion(Radian(-Math::PI * 2 / materials.size()),
                                        Vector3( 0, 1, 0 ));
            }
            animNodesStep = 0.0f;
            isAnimatingNodes = true;
            break;
    }
}

void CustomApp::animatePolymesh() {
    
    animPolyStep += draw_event.timeSinceLastFrame * 0.5f;
    
    polyAnchor.move( 0, sin(animPolyStep) * 12, 0 );
    polyAnchor.yaw( Radian( 0.05f * draw_event.timeSinceLastFrame ));
}

void CustomApp::animateSubmeshChange() {
    
    animSubmeshStep += draw_event.timeSinceLastFrame * 5;
    if( animSubmeshStep > 1) {
        animSubmeshStep = 1;
    }
    
    int s;
    switch (currentSubmesh) {
        case 0:
            //Shrink spheres
            s = (1 - animSubmeshStep) * 50;
            for (int i = 0; i < materials.size(); ++i) {
                spheres[i].scale( s );
            }
            spheres[currentNodes[1]].scale( 0 );
            //Grow cubes
            s = animSubmeshStep * 42;
            for (int i = 0; i < materials.size(); ++i) {
                cubes[i].scale( s );
            }
            cubes[currentNodes[0]].scale( 0 );
            break;
        case 1:
            //Shrink cubes
            s = (1 - animSubmeshStep) * 42;
            for (int i = 0; i < materials.size(); ++i) {
                cubes[i].scale( s );
            }
            cubes[currentNodes[0]].scale( 0 );
            //Grow spheres
            s = animSubmeshStep * 50;
            for (int i = 0; i < materials.size(); ++i) {
                spheres[i].scale( s );
            }
            spheres[currentNodes[1]].scale( 0 );
            break;
    }
    if( animSubmeshStep >= 1) {
        animSubmeshStep = 0.0f;
        isAnimatingSubmesh = false;
    }
}

void CustomApp::animateNodeChange() {
    animNodesStep += draw_event.timeSinceLastFrame * 5;
    if( animNodesStep > 1) {
        animNodesStep = 1.0f;
    }
    
    switch( currentSubmesh ) {
        case 0:
            //BUGGING ?
            //cout << "CUBE NODE CHANGE : " << animNodesStep << endl;
            cubesPivot.orientation(Quaternion::Slerp(animNodesStep,
                                    pivotQuatStart,
                                    pivotQuatEnd));
            cubes[lastNodes[0]].scale( animNodesStep * DEFAULT_CUBE_SIZE );
            cubes[currentNodes[0]].scale( (1 - animNodesStep) * DEFAULT_CUBE_SIZE );
            break;
        case 1:
            spheresPivot.orientation(Quaternion::Slerp(animNodesStep,
                                    pivotQuatStart,
                                    pivotQuatEnd));
            spheres[lastNodes[1]].scale( animNodesStep * DEFAULT_SPHERE_SIZE );
            spheres[currentNodes[1]].scale( (1 - animNodesStep) * DEFAULT_SPHERE_SIZE );
            break;
    }
    
    if( animNodesStep >= 1) {
        animNodesStep = 0.0f;
        isAnimatingNodes = false;
        cout << "Animating nodes finished" << endl;
    }
}

Ogre::ColourValue CustomApp::getRandomColour() {
    if( alphaCheckBox->isChecked() ) {
        return Ogre::ColourValue( Math::RangeRandom( 0, 1 ),
                                  Math::RangeRandom( 0, 1 ),
                                  Math::RangeRandom( 0, 1 ),
                                  Math::RangeRandom( 0, 1 ) );
    }
    return Ogre::ColourValue( Math::RangeRandom( 0, 1 ),
                              Math::RangeRandom( 0, 1 ),
                              Math::RangeRandom( 0, 1 ) );
}

std::string CustomApp::getRandomTexture() {
    return textures[rand() % (textures.size())];
}

void CustomApp::changeTexture(bool random) {
    string t;
    if( random ) {
        t = getRandomTexture();
    } else {
        t = textureMenu->getSelectedItem();
    }
    
    if( pMaterials[currentNodes[currentSubmesh]].getTextureUnitCount() < 1 ) {
        pMaterials[currentNodes[currentSubmesh]].addTexture("General", t);
    } else {
        pMaterials[currentNodes[currentSubmesh]].setTexture("General", t);
    }
}

void CustomApp::itemSelected(OgreBites::SelectMenu* menu) {
    if( menu->getName() == "texturemenu") {
        changeTexture(false);
    }
}

void CustomApp::animateRainbowMat() {
    for( int i = 0; i < 12; ++i ) {
        if( rainbowValuesAscend[i] ) {
            rainbowValues[i] += draw_event.timeSinceLastFrame *
                                Math::RangeRandom( 0, 0.5f );
            if( rainbowValues[i] > 1.0f ) {
                rainbowValues[i] = 1.0f - (rainbowValues[i] - 1.0f);
                rainbowValuesAscend[i] = false;
            }
        } else {
            rainbowValues[i] -= draw_event.timeSinceLastFrame *
                                Math::RangeRandom( 0, 0.5f );;
            if( rainbowValues[i] < 0.0f ) {
                rainbowValues[i] = Math::Abs(rainbowValues[i]);
                rainbowValuesAscend[i] = true;
            }
        }
    }
    
    rainbowMaterial.ambient( Ogre::ColourValue( rainbowValues[0],
                                                rainbowValues[1],
                                                rainbowValues[2]));
    rainbowMaterial.diffuse( Ogre::ColourValue( rainbowValues[3] * 0.5f,
                                                rainbowValues[4] * 0.5f,
                                                rainbowValues[5] * 0.5f));
    rainbowMaterial.emissive( Ogre::ColourValue( rainbowValues[6] * 0.2f,
                                                 rainbowValues[7] * 0.2f,
                                                 rainbowValues[8] * 0.2f));
    rainbowMaterial.specular( Ogre::ColourValue( rainbowValues[9],
                                                 rainbowValues[10],
                                                 rainbowValues[11]));
}