/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ___________________________________( ^3^)_____________________________________
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
*/

/* 
 * File:   CustomApp.h
 * Author: Eurydice Cardon
 * 
 * Created during February 2017
 */

#ifndef CUSTOMAPP_H
#define CUSTOMAPP_H

#include "PolymorphApplication.h"

class CustomApp : public PolymorphApplication {

public:
    
    void createScene(void);
    void draw();
    bool keyPressed(const OIS::KeyEvent &arg);
    bool mouseMoved(const OIS::MouseEvent &arg);
    void buttonHit(OgreBites::Button* button);
    void itemSelected(OgreBites::SelectMenu* menu);
    
private:
    
    void initMaterials();
    void initTray();
    void updateTray();
    void initNodes();
    
    void previousNode();
    void nextNode();
    void initPreviousNodeAnim();
    void initNextNodeAnim();
    void animateNodeChange();
    
    void previousSubmesh();
    void nextSubmesh();
    void animateSubmeshChange();
    
    void animatePolymesh();
    
    Ogre::ColourValue getRandomColour();
    std::string getRandomTexture();
    void animateRainbowMat();
    void changeTexture(bool random);
    
    polymorph::PNode pivot;
    polymorph::PNode* spheres;
    polymorph::PNode spheresPivot;
    polymorph::PNode* cubes;
    polymorph::PNode cubesPivot;
    polymorph::PNode polymesh;
    polymorph::PNode polyAnchor;
    
    polymorph::PMaterial* pMaterials;
    
    std::vector<std::string> materials;
    std::vector<int> currentNodes;
    std::vector<int> lastNodes;
    int currentSubmesh;
    
    bool isAnimatingSubmesh;
    float animSubmeshStep;
    bool isAnimatingNodes;
    float animNodesStep;
    float animPolyStep;
    
    polymorph::PMaterial rainbowMaterial;
    std::vector<float> rainbowValues;
    std::vector<bool> rainbowValuesAscend;
    
    bool freeze;
    bool changeAlpha;
    
    OgreBites::Button* applyButton;
    OgreBites::Button* randColorButton;
    OgreBites::TextBox* controlsTextBox;
    OgreBites::CheckBox* alphaCheckBox;
    OgreBites::SelectMenu* textureMenu;
    Ogre::StringVector textures;
    
    Ogre::MaterialPtr customMaterial;
    
    Quaternion pivotQuatStart;
    Quaternion pivotQuatEnd;
    
    polymorph::PNode cellshaded;
    
    polymorph::PLight lpoint;
    polymorph::PLight lpoint2;
    polymorph::PLight lpoint3;
    
    Ogre::Real lrot;
    Ogre::Vector2 mouse_rel;
    Ogre::Vector2 mouse_abs;
    Ogre::Vector2 mouse_abs_norm;
    
    const int DEFAULT_BASKET = 0;
    const int DEFAULT_BALLOON = 0;
    
    const int DEFAULT_CUBE_SIZE = 42;
    const int DEFAULT_SPHERE_SIZE = 50;
};

#endif /* CUSTOMAPP_H */

