/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ___________________________________( ^3^)_____________________________________
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
*/

/* 
 * File:   CustomApp.cpp
 * Author: frankiezafe
 * 
 * Created on November 19, 2016, 6:07 PM
 */

#include "CustomApp.h"

using namespace std;
using namespace Ogre;
using namespace polymorph;
using namespace libpd;

void CustomApp::createScene( void ) {

    // keep default behavior
    PolymorphApplication::createScene( );
    
    sceneMgr->setAmbientLight( Ogre::ColourValue( 0.1, 0.1, 0.1 ) );

    // creation and start of sound engine
    pde = new PdEngine();
    pde->verbose( true );
    pde->addSearchPath( "../media/pd/" );
    pde->init();

    pom.verbose( true );
    pom.parse( "General", "config.xml" );
    // set scene manager to create 3d objects
    pom.setSceneManager( sceneMgr );
    // set pd engine to create sounds
    pom.setPdEngine( pde );
    // load the first scene
    pom.nextScene();
    
    // you can also create sounds manually
    // adapt the pd patch to recognise these names
    sound_ambient.ambient( sceneMgr, pde, "handmade_ambient" );
    sound_ambient.debug( true );
    sound_ambient.move( -30,0,0 );
    sound_ambient.play();
    
    // point sound is not set to play
    // the pd patch reacts to location modification, not play or stop
    sound_pt.point( sceneMgr, pde, "handmade_point" );
    sound_pt.debug( true );
    // disable the object to avoid puredata to see the modfication
    sound_pt.move( 30,0,0 );
    
    // and finally starting puredata
    if ( pde->start() ) {
        pde->openPatch( "../media/pd/demo.pd" );
    }
    
}

void CustomApp::draw( ) {

    mouse_rel = Ogre::Vector2::ZERO;
    
}

bool CustomApp::keyPressed( const OIS::KeyEvent &arg ) {

    // keep default behavior
    PolymorphApplication::keyPressed( arg );

    // show/hide overlay panels
    if ( arg.key == OIS::KC_H ) {
        
        if ( trayMgr->isCursorVisible( ) ) trayMgr->hideAll( );
        else trayMgr->showAll( );
        
    } else if ( arg.key == OIS::KC_UP ) {
        
        sound_pt.translate( Ogre::Vector3::UNIT_Y );
        
    } else if ( arg.key == OIS::KC_DOWN ) {
        
        PSound* ps = pom.sound( "bing" );
        if ( ps ) ps->translate( Ogre::Vector3::NEGATIVE_UNIT_Y );
        
    } else if ( arg.key == OIS::KC_SPACE ) {
        
        sound_ambient.visible( !sound_ambient.isVisible() );
        
    } else if ( arg.key == OIS::KC_P ) {
        
        sound_ambient.togglePlay();
        
    }
}

bool CustomApp::mouseMoved( const OIS::MouseEvent &arg ) {

    // keep default behavior
    PolymorphApplication::mouseMoved( arg );

    mouse_rel.x = arg.state.X.rel;
    mouse_rel.y = arg.state.Y.rel;
    mouse_abs.x = arg.state.X.abs;
    mouse_abs.y = arg.state.Y.abs;

    mouse_abs_norm.x = mouse_abs.x / window->getWidth( );
    mouse_abs_norm.y = mouse_abs.y / window->getHeight( );

}