#if __VERSION__ < 330

#version 120

uniform sampler2D tex;
uniform sampler2D tex2;
uniform float percent;

void main()
{
    vec2 texcoord = vec2( gl_TexCoord[0] );
    vec4 colA = texture2D( tex, texcoord, 0.0 );
    vec4 colB = texture2D( tex2, texcoord, 0.0 );
    gl_FragColor = mix( colA, colB, percent );
    //gl_FragColor = vec4( 1.0,0.0,0.0,1.0 );
    //gl_FragColor = colA;
}

#else

#version 330

uniform sampler2D tex;
uniform sampler2D tex2;
uniform float percent;

in vec2 uv0;
out vec4 fragColour;

void main()
{
    vec4 colA = texture2D( tex, uv0 );
    vec4 colB = texture2D( tex2, uv0 );
    fragColour = mix( colA, colB, percent );
    //fragColour = vec4( 1.0,0.0,0.0,1.0 );
}

#endif
