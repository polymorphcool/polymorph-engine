#if __VERSION__ < 330

#version 120

#define M_PI 3.1415926535897932384626433832795

uniform sampler2D tex;
uniform vec2 center;
uniform float radius;
uniform float ratio;
uniform vec4 center_color;
uniform vec4 border_color;
uniform float persistance;

void main()
{
    vec2 texcoord = vec2( gl_TexCoord[0] );
    vec4 col = texture2D( tex, texcoord, 0.0 );
    vec4 ncol;
    texcoord.y = 0.5 + ((texcoord.y-0.5) / ratio);
    float d = distance( texcoord, center );
    if ( d > radius ) {
        ncol = border_color;
    } else {
        // sinus fall off
        d = ( sin( ( ( d / radius ) - 0.5 ) * M_PI ) + 1.0 ) * 0.5;
        ncol = mix( center_color, border_color, d );
    }
    gl_FragColor = mix( ncol, col, persistance );
}

#else

#version 330

#define M_PI 3.1415926535897932384626433832795

uniform sampler2D tex;
uniform vec2 center;
uniform float radius;
uniform float ratio;
uniform vec4 center_color;
uniform vec4 border_color;
uniform float persistance;

in vec2 uv0;
out vec4 fragColour;

void main()
{
    vec4 col = texture2D( tex, uv0 );
    vec4 ncol;
    uv0.y = 0.5 + ((uv0.y-0.5) / ratio);
    float d = distance( uv0, center );
    if ( d > radius ) {
        ncol = border_color;
    } else {
        // sinus fall off
        d = ( sin( ( ( d / radius ) - 0.5 ) * M_PI ) + 1.0 ) * 0.5;
        ncol = mix( center_color, border_color, d );
    }
    fragColour = mix( ncol, col, persistance );
}

#endif
