#if __VERSION__ < 330

#version 120

uniform sampler2D tex;
uniform float time;
uniform float horiz;
uniform float verti;
uniform float percent;

float snoise(vec2 co) {
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

void main()
{
    
    vec2 texcoord = vec2( gl_TexCoord[0] );
    vec2 workcoord = texcoord;
    float n = snoise( texcoord * time );
    workcoord.x += ( -(horiz * 0.5) + n * horiz );
    n = snoise( workcoord * time );
    workcoord.y += ( -(verti * 0.5) + n * verti );
    vec4 col = texture2D( tex, texcoord, 0.0 );
    vec4 wcol = texture2D( tex, workcoord, 0.0 );
    gl_FragColor = col + wcol * percent;

}

#else

#version 330

uniform sampler2D tex;
uniform float time;

in vec2 uv0;
out vec4 fragColour;

void main()
{
    vec4 col = texture2D( tex, uv0 );
    fragColour = col;
}

#endif
