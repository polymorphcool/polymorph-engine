/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ___________________________________( ^3^)_____________________________________
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
*/

/* 
 * File:   CustomApp.h
 * Author: frankiezafe
 *
 * Created on January 11, 2017, 10:00 AM
 */

#ifndef CUSTOMAPP_H
#define CUSTOMAPP_H

#include "PolymorphApplication.h"
#include "Compositor/Pass/PassQuad/OgreCompositorPassQuad.h"

class CustomApp : public PolymorphApplication {

public:
    
    CustomApp();
    
    void setupResources(void);
    
    void createCompositor( void );
    
    void createScene(void);
    
    void draw();
    
    bool keyPressed(const OIS::KeyEvent &arg);
    
    bool mousePressed(const OIS::MouseEvent& arg, OIS::MouseButtonID id);
    bool mouseReleased(const OIS::MouseEvent &arg, OIS::MouseButtonID id);
    bool mouseMoved(const OIS::MouseEvent &arg);
    
private:
    
    polymorph::PObjectManager polyMgr;
    
    Ogre::Vector2 mouse_norm;
    Ogre::Vector2 mouse_rel;
    uint mouse_button;
    
    bool drawEnabled;
    bool noiseH;
    bool noiseV;
    
    float* compositor_bg_center;
    float* compositor_bg_radius;
    float* compositor_bg_ratio;
    float* compositor_bg_center_color;
    float* compositor_bg_border_color;
    float* compositor_persistance;
    
    float* noiser_time;
    float* noiser_horiz;
    float* noiser_verti;
    float* noiser_percent;
    
};

#endif /* CUSTOMAPP_H */

