/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ___________________________________( ^3^)_____________________________________
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 */

/* 
 * File:   CustomApp.cpp
 * Author: frankiezafe
 * 
 * Created on January 11, 2017, 10:00 AM
 */

#include "CustomApp.h"

using namespace std;
using namespace Ogre;
using namespace polymorph;

CustomApp::CustomApp( ) :
PolymorphApplication( ),
mouse_button( 0 ),
drawEnabled(true),
noiseH(false),
noiseV(false)
{
    polyMgr.verbose( true );
    polyMgr.parse( );
}

void CustomApp::setupResources( void ) {

    PolymorphApplication::setupResources( polyMgr.getProject( ) );

}

void CustomApp::createCompositor( void ) {

    compositorMgr = root->getCompositorManager2( );
    compositorwpname = "CustomCompositor";
    const Ogre::IdString workspaceName( compositorwpname );
    compositorwp = compositorMgr->addWorkspace( sceneMgr, window, cam, workspaceName, true );

    // getting the shader linked to CompositorSampleStdRenderer.render_quad
    // see media/compositor/custom.compositor
    CompositorPassQuad* cpd = ( CompositorPassQuad* ) compositorwp->getNodeSequence( )[1]->_getPasses( )[0];
    GpuProgramParametersSharedPtr gpp = cpd->getPass( )->getFragmentProgramParameters( );
    compositor_bg_center = gpp->getFloatPointer( 0 );
    compositor_bg_radius = gpp->getFloatPointer( 2 );
    compositor_bg_ratio = gpp->getFloatPointer( 3 );
    compositor_bg_center_color = gpp->getFloatPointer( 4 );
    compositor_bg_border_color = gpp->getFloatPointer( 8 );
    compositor_persistance = gpp->getFloatPointer( 12 );
    compositor_persistance[0] = 0;

    // getting noiser
    cpd = ( CompositorPassQuad* ) compositorwp->getNodeSequence( )[2]->_getPasses( )[1];
    gpp = cpd->getPass( )->getFragmentProgramParameters( );
    noiser_time = gpp->getFloatPointer( 0 );
    noiser_horiz = gpp->getFloatPointer( 1 );
    noiser_verti = gpp->getFloatPointer( 2 );
    noiser_percent = gpp->getFloatPointer( 3 );

}

void CustomApp::createScene( void ) {

    // keep default behavior
    PolymorphApplication::createScene( );

    polyMgr.setSceneManager( sceneMgr );
    polyMgr.nextScene( );
    
    trayMgr->hideAll();

}

void CustomApp::draw( ) {

    if ( !drawEnabled ) return;

    noiser_time[0] += draw_event.timeSinceLastFrame;
    if ( compositor_persistance[0] < 0.995 ) {
        compositor_persistance[0] += draw_event.timeSinceLastFrame * 0.1;
    }
    
    if ( noiseH ) {
        if ( noiser_horiz[0] < 3.5 ) noiser_horiz[0] += draw_event.timeSinceLastFrame;
        if ( noiser_verti[0] > 0.01 ) noiser_verti[0] = noiser_verti[0] * 0.9;
        if ( noiser_percent[0] < 0.8 ) noiser_percent[0] += draw_event.timeSinceLastFrame * 0.6;
    } else if ( noiseV ) {
        if ( noiser_horiz[0] > 0.01 ) noiser_horiz[0] = noiser_horiz[0] * 0.9;
        if ( noiser_verti[0] < 3.5 ) noiser_verti[0] += draw_event.timeSinceLastFrame;
        if ( noiser_percent[0] < 0.8 ) noiser_percent[0] += draw_event.timeSinceLastFrame * 0.6;
    } else {
        if ( noiser_horiz[0] > 0.01 ) noiser_horiz[0] = noiser_horiz[0] * 0.9;
        if ( noiser_verti[0] > 0.01 ) noiser_verti[0] = noiser_verti[0] * 0.9;
        if ( noiser_percent[0] > 0.1 ) noiser_percent[0] = noiser_percent[0] * 0.9;
    }

    PNode* n = polyMgr.node( "gearthing4" );
    Ogre::Real relx = ( mouse_norm.x - 0.5 ) * 2;
    Ogre::Real rely = ( mouse_norm.y - 0.5 ) * 2;
    Quaternion ry( Ogre::Radian( -relx * Math::PI ), Vector3( 0, 1, 0 ) );
    Quaternion rx( Ogre::Radian( rely * Math::HALF_PI ), Vector3( 1, 0, 0 ) );
    n->orientation( ry * rx );
    n->yaw( Ogre::Radian( draw_event.timeSinceLastFrame * 0.1 ), true );
    n->roll( Ogre::Radian( draw_event.timeSinceLastFrame * 0.1 ), true );

    switch ( mouse_button ) {

        case 1: // left
            break;

        case 2: // right
        {
            Ogre::Vector3 p = n->getTrans( );
            p.x += mouse_rel.x * 600;
            p.y -= mouse_rel.y * 600;
            n->move( p );
        }
            compositor_bg_center[ 0 ] += mouse_rel.x;
            compositor_bg_center[ 1 ] += mouse_rel.y;
            break;

        default:
            break;

    }

    mouse_rel = Ogre::Vector2( 0,0 );

}

bool CustomApp::keyPressed( const OIS::KeyEvent &arg ) {

    // keep default behavior
    PolymorphApplication::keyPressed( arg );

    if ( arg.key == OIS::KC_BACK ) {
        compositor_persistance[0] = 0;
    } else if ( arg.key ==OIS::KC_SPACE ) {
        drawEnabled = !drawEnabled;
    } else if ( arg.key ==OIS::KC_H ) {
        noiseH = !noiseH;
        if ( noiseH ) noiseV = false;
    } else if ( arg.key ==OIS::KC_V ) {
        noiseV = !noiseV;
        if ( noiseV ) noiseH = false;
    }

}

bool CustomApp::mousePressed( const OIS::MouseEvent& arg, OIS::MouseButtonID id ) {

    // keep default behavior
    PolymorphApplication::mousePressed( arg, id );
    mouse_button = ( ( uint ) id ) + 1;

}

bool CustomApp::mouseReleased( const OIS::MouseEvent& arg, OIS::MouseButtonID id ) {

    // keep default behavior
    PolymorphApplication::mouseReleased( arg, id );
    mouse_button = 0;

}

bool CustomApp::mouseMoved( const OIS::MouseEvent &arg ) {

    // keep default behavior
    PolymorphApplication::mouseMoved( arg );

    mouse_norm = Ogre::Vector2(
            Ogre::Real( arg.state.X.abs ) / window->getWidth( ),
            Ogre::Real( arg.state.Y.abs ) / window->getHeight( )
            );
    mouse_rel = Ogre::Vector2(
            Ogre::Real( arg.state.X.rel ) / window->getWidth( ),
            Ogre::Real( arg.state.Y.rel ) / window->getHeight( )
            );

}