/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ___________________________________( ^3^)_____________________________________
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 */

/* 
 * File:   CustomApp.cpp
 * Author: frankiezafe
 * 
 * Created on November 19, 2016, 6:07 PM
 */

#include "CustomApp.h"

using namespace std;
using namespace Ogre;
using namespace polymorph;

void CustomApp::createScene(void) {

    walls = 0;
    balls = 0;

    // keep default behavior
    PolymorphApplication::createScene();

    PBullet::start(root);
    PBullet::gravity(Vector3(0, -40, 0));

    wall_origin.empty(sceneMgr, "wall_origin");
    loadScene();

    sun.sun(sceneMgr);
    sun.pitch(Radian(0.1));

    //    trayMgr->hideFrameStats();

    totaltime = 0;

    enable_update = true;

}

void CustomApp::loadScene() {

    if (walls) {
        wall_count = 0;
        delete [] walls;
    }
    if (balls) {
        balls_count = 0;
        delete [] balls;
    }

    Real ballsize = 10;
    Real ballsize_deviation = 6;
    Real tickness = 20;
    Real spacesize = 280;

    wall_count = 7;
    walls = new PNode[ wall_count ];
    for (uint8_t i = 0; i < wall_count; ++i) {
        stringstream ss;
        ss << "wall_" << int( i);
        walls[i].cube(sceneMgr, ss.str());
        walls[i].getMaterial()->polygon(PM_WIREFRAME);
    }

    // actual walls
    walls[0].scale(spacesize, tickness, spacesize);
    walls[0].move(0, -(spacesize - tickness) * 0.5, 0);
    walls[1].scale(spacesize, tickness, spacesize);
    walls[1].move(0, (spacesize - tickness) * 0.5, 0);
    walls[2].scale(spacesize, spacesize, tickness);
    walls[2].move(0, 0, -(spacesize - tickness) * 0.5);
    walls[3].scale(spacesize, spacesize, tickness);
    walls[3].move(0, 0, (spacesize - tickness) * 0.5);
    walls[4].scale(tickness, spacesize, spacesize);
    walls[4].move(-(spacesize - tickness) * 0.5, 0, 0);
    walls[5].scale(tickness, spacesize, spacesize);
    walls[5].move((spacesize - tickness) * 0.5, 0, 0);

    // rotor
    walls[6].scale(spacesize * 1.5, spacesize * 0.5, tickness);
    walls[6].move(0, -(spacesize * 0.25), 0);

    for (uint8_t i = 0; i < wall_count; ++i) {
        walls[i].physics(BT_STATIC_BOX);
        walls[i].mass(0);
        walls[i].restless(true);
        wall_origin.attach(&walls[i]);
    }

    uint8_t s = 10;
    balls_count = s * s * s;
    balls = new PNode[ balls_count ];
    uint16_t i = 0;
    for (uint8_t x = 0; x < s; ++x) {
        for (uint8_t y = 0; y < s; ++y) {
            for (uint8_t z = 0; z < s; ++z) {
                stringstream ss;
                ss << "wall_" << i;
                balls[i].sphere(sceneMgr, ss.str());
                balls[i].move(
                        (x - (s * 0.5)) *
                        (ballsize + ballsize_deviation) *
                        1.01 + Math::RangeRandom(-0.1, 0.1),
                        (y - (s * 0.5)) *
                        (ballsize + ballsize_deviation) *
                        1.01 + Math::RangeRandom(-0.1, 0.1),
                        (z - (s * 0.5)) *
                        (ballsize + ballsize_deviation) *
                        1.01 + Math::RangeRandom(-0.1, 0.1)
                        );
                balls[i].scale(
                        ballsize +
                        Math::RangeRandom(-ballsize_deviation, ballsize_deviation)
                        );
                balls[i].physics(BT_DYNAMIC_SPHERE);
                balls[i].restless(true);
                PMaterial* mat = balls[i].getMaterial();
                mat->diffuse(
                        Math::RangeRandom(0.5, 1),
                        Math::RangeRandom(0.6, 0.8),
                        Math::RangeRandom(0.4, 0.9)
                        );
                ++i;
            }
        }
    }

}

void CustomApp::before_draw() {

}

void CustomApp::draw() {

    if (!enable_update) {
        return;
    }

    totaltime += draw_event.timeSinceLastFrame;
    walls[6].yaw(Radian(draw_event.timeSinceLastFrame * -0.3));

    wall_origin.yaw(Radian(draw_event.timeSinceLastFrame * 0.1));
    wall_origin.roll(Radian(draw_event.timeSinceLastFrame * 0.05));

    Real prob = 0.01;
    Real mag = 20000;
    Vector3 deviation(0.2, 1, 0.2);
    for (uint16_t i = 0; i < balls_count; ++i) {
        if (Math::RangeRandom(0, 1) > prob) {
            continue;
        }
        balls[i].impulse(
                Vector3(
                Math::RangeRandom(-mag, mag),
                Math::RangeRandom(0, mag),
                Math::RangeRandom(-mag, mag)
                ) * deviation
                );
    }

    //    walls[6].yaw(Radian(draw_event.timeSinceLastFrame * -0.3));

}

bool CustomApp::keyPressed(const OIS::KeyEvent &arg) {

    // keep default behavior
    PolymorphApplication::keyPressed(arg);

    if (arg.key == OIS::KC_R) {

        //        PBullet::start( sceneMgr );
        PBullet::stop();
        loadScene();
        enable_update = false;

    } else if (arg.key == OIS::KC_SPACE) {

        enable_update = !enable_update;

        if (enable_update) {
            PBullet::start(root);
        } else {
            PBullet::stop();
        }

    }

}

bool CustomApp::mouseMoved(const OIS::MouseEvent &arg) {

    // keep default behavior
    PolymorphApplication::mouseMoved(arg);

}