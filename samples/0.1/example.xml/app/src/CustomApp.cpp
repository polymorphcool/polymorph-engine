/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ___________________________________( ^3^)_____________________________________
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
*/

/* 
 * File:   CustomApp.cpp
 * Author: frankiezafe
 * 
 * Created on November 19, 2016, 6:07 PM
 */

#include "CustomApp.h"

using namespace std;
using namespace Ogre;
using namespace polymorph;

CustomApp::CustomApp(): 
PolymorphApplication()
{
    polyMgr.verbose( true );
    polyMgr.parse();
}

void CustomApp::setupResources(void) {
    
    PolymorphApplication::setupResources( polyMgr.getProject() );
    
}

void CustomApp::createScene( void ) {

    colorrot = 0;
    
    // adding a widget
    trayMgr->setTrayWidgetAlignment(
            OgreBites::TL_TOPLEFT, GHA_LEFT );
    trayMgr->setTrayPadding( 2 );
    trayMgr->createLabel(
            OgreBites::TL_TOPLEFT,
            "li",
            "press 'n' for next scene",
            170 );
    trayMgr->createLabel(
            OgreBites::TL_TOPLEFT,
            "llsc",
            "loaded scene",
            170 );
    label_scenenum = trayMgr->createLabel(
            OgreBites::TL_TOPLEFT,
            "llscn",
            "undefined",
            170 );

    // keep default behavior
    PolymorphApplication::createScene( );

    sceneMgr->setAmbientLight( Ogre::ColourValue( 0.1, 0.1, 0.1 ) );

//    polyMgr.verbose( true );
//    polyMgr.parse( "General", "config.xml" );

    PProjectData& ppdata = polyMgr.getProject( );

    // if you want to get all scenes ids
    size_t scenes_num;
    uint32_t* scenes_ids = 0;

    ppdata.getSceneIds( scenes_ids, scenes_num );

    cout << "available scenes in this xml: " << scenes_num << endl;

    if ( scenes_num > 0 ) {

        for ( int i = 0; i < scenes_num; ++i ) {

            cout << "scene id[" << i << "]: " << scenes_ids[ i ] << endl;

            // getting the scene
            PSceneData* psdata = ppdata.scene( scenes_ids[ i ] );

            // if you want to get all object's names
            size_t objects_num;
            std::string* object_names;
            psdata->getObjectNames( object_names, objects_num );


            cout << "\tobjects:" << endl;
            if ( objects_num ) {

                for ( int j = 0; j < objects_num; ++j ) {

                    cout << "\t\tname[" << j << "]: " <<
                            object_names[ j ] << endl;

                    // if you need access to an object by its name
                    // PObjectData is a base class, it does not contains all
                    // information. There is no efficient way to cast the
                    // PObjectData to PNodeData for instance
                    // see below for access by class

                    PObjectData* pod = psdata->object( object_names[ j ] );

                    cout << "\t\t\tpos: " <<
                            pod->origin_pos.x << ", " <<
                            pod->origin_pos.y << ", " <<
                            pod->origin_pos.z << endl;

                    // is it a node?
                    PNodeData* pnd = psdata->node( object_names[ j ] );
                    if ( pnd ) cout << "\t\t\tit's a node" << endl;

                    // is it a light?
                    PLightData* pld = psdata->light( object_names[ j ] );
                    if ( pld ) cout << "\t\t\tit's a light" << endl;

                    // !important! 
                    // this is NOT an test you should keep in production

                }

                delete[] object_names;
                object_names = 0;

            }

            // if you want to get all node's names
            psdata->getNodeNames( object_names, objects_num );
            cout << "\tnodes:" << endl;
            if ( objects_num ) {

                for ( int j = 0; j < objects_num; ++j ) {

                    cout << "\t\tname[" << j << "]: " <<
                            object_names[ j ] << endl;

                }

                delete[] object_names;
                object_names = 0;

            }

            // if you want to get all light's names
            psdata->getLightNames( object_names, objects_num );
            cout << "\tlights:" << endl;
            if ( objects_num ) {

                for ( int j = 0; j < objects_num; ++j ) {

                    cout << "\t\tname[" << j << "]: " <<
                            object_names[ j ] << endl;

                }

                delete[] object_names;
                object_names = 0;

            }

        }

        delete[] scenes_ids;

    }

    // loading scenes
    uint32_t sceneID = 0;
    // this will generate an error: scene manager is not setup in manager
    polyMgr.loadScene( sceneID );

    // now it will do the job
    polyMgr.setSceneManager( sceneMgr );
    polyMgr.loadScene( sceneID );
    
    label_scenenum->setCaption(
        StringConverter::toString( polyMgr.getSceneID() )
    );
    
    finetuneScene();

}

void CustomApp::finetuneScene() {
    if ( polyMgr.getSceneID() == 0 ) {
        PNode * f = polyMgr.node( "floor" );
        PNode * n = polyMgr.node( "psigle" );
        f->attach( n );
        PLight * l = polyMgr.light( "main_sun" );
        f->attach( l );
    }
}

void CustomApp::draw( ) {
    
    Ogre::Radian r( draw_event.timeSinceLastFrame );
    
    // this gets the number of PNodes available in the current scene
    uint32_t ncount = polyMgr.getNodeCount();
    
    // to control each scene, you can switch on sceneid
    switch( polyMgr.getSceneID() ) {
        
        // constant rotation Y of all nodes
        case 0:
            {
                PNode * n = polyMgr.node( "floor" );
                Ogre::Real relx = ( mouse_abs_norm.x - 0.5 ) * 2;
                Ogre::Real rely = ( mouse_abs_norm.y - 0.5 ) * 2;
                Quaternion ry( Ogre::Radian( -relx * Math::PI ), Vector3( 0,1,0 ) );
                Quaternion rx( Ogre::Radian( rely * Math::HALF_PI ), Vector3( 1,0,0 ) );
                n->orientation( ry * rx );
            }
            break;
        
        // constant rotation Y of all nodes
        case 2:
            for ( uint32_t i = 0; i < ncount; ++i ) {
                polyMgr.node( i )->yaw( r );
            }
            break;
        
        // different roation on each node
        case 1:
            for ( uint32_t i = 0; i < ncount; ++i ) {
                polyMgr.node( i )->yaw( r * 0.01 * i );
            }
            break;
        
        // getting sun light, rotation on Z and changing diffuse
        case 3:
            {
                PLight * l = polyMgr.light( "main_sun" );
                l->roll( r );
                colorrot += r;
                l->diffuse(
                        ( Math::Cos( colorrot ) + 1 ) * 0.5,
                        1,
                        ( Math::Sin( colorrot ) + 1 ) * 0.5
                        );
            }
            break;
        default:
            break;
    }

    mouse_rel = Ogre::Vector2::ZERO;
    
    // note: the most efficient way to access specific nodes or light is
    // to store a pointer locally, not relying on PObjectManager all the time.
    // node( string ) and light( string ) uses maps > it's slower than doing
    // PNode* my_local_node = polyMgr.node( "name" ) once, and after that doing
    // my_local_node->move( x,y,z )
    
}

bool CustomApp::keyPressed( const OIS::KeyEvent &arg ) {

    // keep default behavior
    PolymorphApplication::keyPressed( arg );

    // show/hide overlay panels
    if ( arg.key == OIS::KC_H ) {
        
        if ( trayMgr->isCursorVisible( ) ) trayMgr->hideAll( );
        else trayMgr->showAll( );
        
    } else if ( arg.key == OIS::KC_N ) {
        
        polyMgr.nextScene( );
        finetuneScene();
        
        label_scenenum->setCaption(
            StringConverter::toString( polyMgr.getSceneID() )
        );
        
    }

}

bool CustomApp::mouseMoved( const OIS::MouseEvent &arg ) {

    // keep default behavior
    PolymorphApplication::mouseMoved( arg );
    
    mouse_rel.x = arg.state.X.rel;
    mouse_rel.y = arg.state.Y.rel;
    mouse_abs.x = arg.state.X.abs;
    mouse_abs.y = arg.state.Y.abs;

    mouse_abs_norm.x = mouse_abs.x / window->getWidth( );
    mouse_abs_norm.y = mouse_abs.y / window->getHeight( );

}