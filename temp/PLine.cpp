/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   PLine.cpp
 * Author: frankiezafe
 * 
 * Created on September 24, 2017, 12:09 PM
 */

#include "PLine.h"

using namespace std;
using namespace Ogre;
using namespace polymorph;

PLine::PLine() :
PNode(),
_mo(0),
_vnum(0),
_vertices(0),
_refresh_required(false) {

}

PLine::~PLine() {
}

bool PLine::allocate(
        Ogre::SceneManager* sm,
        uint16_t vertices_num,
        Ogre::Vector3* verts) {

    if (_origin || !sm || vertices_num < 2) {
        Ogre::LogManager::getSingleton().logMessage(
                Ogre::LML_NORMAL,
                "PLine::allocate, impossible to create line.");
        return false;
    }

    _sceneMgr = sm;
    _vnum = vertices_num;
    _origin = _sceneMgr->getRootSceneNode()->createChildSceneNode();
    _object = _origin->createChildSceneNode();
    _vertices = new Vector3[ vertices_num ];

    _mo = sm->createManualObject(SCENE_DYNAMIC);
    _mo->begin("BaseWhiteNoLighting", RenderOperation::OT_LINE_STRIP);
    _mo->estimateVertexCount(vertices_num);
    _mo->estimateIndexCount(vertices_num);
    for (uint16_t i = 0; i < _vnum; ++i) {
        if (verts) {
            _vertices[i] = verts[i];
        } else {
            _vertices[i] = Vector3::ZERO;
        }
        _mo->position(_vertices[i]);
        _mo->index(i);
    }
    _mo->end();

    _object->attachObject(_mo);

    _sceneMgr->addRenderQueueListener(this);

}

Ogre::Vector3 PLine::getVertex(uint16_t i) {

    if (!_vertices || i >= _vnum) {
        return Ogre::Vector3::ZERO;
    }
    return Ogre::Vector3(_vertices[i]);

}

void PLine::vertex(uint16_t i,
        Ogre::Real x,
        Ogre::Real y,
        Ogre::Real z,
        bool absolute) {
    vertex(i, Ogre::Vector3(x, y, z), absolute);
}

void PLine::vertex(
        uint16_t i,
        const Ogre::Vector3& src,
        bool absolute) {

    if (!_vertices || i >= _vnum) {
        return;
    }
    if ( !absolute ) {
        Ogre::Vector3 abs;
        abs = _object->_getDerivedOrientation() * src;
        abs *= _object->_getDerivedScale();
        abs += _object->_getDerivedPosition();
        _vertices[i] = abs;
    } else {
        _vertices[i] = src;
    }
    _refresh_required = true;

}

void PLine::preRenderQueues() {

    if (_refresh_required) {

        _refresh_required = false;
        _mo->beginUpdate(0);
        for (uint16_t i = 0; i < _vnum; ++i) {
            _mo->position(_vertices[i]);
        }
        _mo->end();

    }

}