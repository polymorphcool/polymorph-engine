/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ___________________________________( ^3^)_____________________________________
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
*/

/* 
 * File:   CustomApp.h
 * Author: frankiezafe
 *
 * Created on November 19, 2016, 6:07 PM
 */

#ifndef CUSTOMAPP_H
#define CUSTOMAPP_H

#include "PolymorphApplication.h"

#define TRAY_WIDTH              300
#define TRAY_DYN_WIDTH          350

class CustomApp : public PolymorphApplication {

public:
    
    // tip: overwrite setup() when you really need to
    // overwrite create*() methods, they'll do the job in the right order
    
    void createScene(void);
    
    void createTray();
    
    void before_draw();
    
    void draw();
    
    bool keyPressed(const OIS::KeyEvent &arg);
    
    bool mouseMoved(const OIS::MouseEvent &arg);
    
private:
    
    bool reload_resources;
    
    polymorph::PNode* proot;
    
    OgreBites::Label*           ui_label;
    OgreBites::SelectMenu*      ui_select_thick;
    OgreBites::SelectMenu*      ui_select_long;
    OgreBites::TextBox*         ui_textbox;
    OgreBites::Button*          ui_button;
    OgreBites::Slider*          ui_slider;
    OgreBites::CheckBox*        ui_checkbox;
    OgreBites::ParamsPanel*     ui_params;
    OgreBites::Separator*       ui_separator;
    OgreBites::ProgressBar*     ui_progressbar;
    
    Ogre::Real loaded;
    
};

#endif /* CUSTOMAPP_H */

