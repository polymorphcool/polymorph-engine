/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ___________________________________( ^3^)_____________________________________
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
*/

/* 
 * File:   CustomApp.cpp
 * Author: frankiezafe
 * 
 * Created on November 19, 2016, 6:07 PM
 */

#include "CustomApp.h"

using namespace std;
using namespace Ogre;
using namespace polymorph;

void CustomApp::createScene(void) {

    // keep default behavior
    PolymorphApplication::createScene();
    reload_resources = false;
    
    PLight* sun = new PLight();
    sun->sun( sceneMgr );
    sun->move( 0, -100, 0 );
    sun->roll( Ogre::Radian( -0.1 ) );
    
    proot = new PNode();
    proot->empty( sceneMgr );
    for( int8_t z = -5; z <= 5; ++z ) {
    for( int8_t x = -5; x <= 5; ++x ) {
        PNode* cube = new PNode();
        cube->cube( sceneMgr );
        cube->scale( 45 );
        cube->move( x * 50, 0, z * 50 );
        proot->attach( cube );
    }
    }
    proot->pitch( Ogre::Radian( 0.2 ) );
    
}

void CustomApp::createTray() {
    
    PolymorphApplication::createTray();

    uint8_t itemNum = 20;
    
    Ogre::StringVector selectitems;
    for( uint8_t i = 0; i < itemNum; ++i ) {
        stringstream ss;
        ss << "menu [" << int(i) << "]";
        selectitems.push_back( ss.str() );
    }
    
    ui_label = trayMgr->createLabel(
            OgreBites::TL_TOPLEFT,
            "ui_label", "ui_label",
            TRAY_WIDTH );
    
    ui_select_thick = trayMgr->createThickSelectMenu(
            OgreBites::TL_TOPLEFT,
            "ui_select_thick", "ui_select_thick",
            TRAY_WIDTH, 15, 
            selectitems );
    
    ui_separator = trayMgr->createSeparator(
            OgreBites::TL_TOPLEFT,
            "ui_separator",
            TRAY_WIDTH );
    
    ui_select_long = trayMgr->createLongSelectMenu(
            OgreBites::TL_TOPLEFT,
            "ui_select_long", "ui_select_long",
            TRAY_WIDTH, TRAY_WIDTH * 0.5, 15, 
            selectitems );
    
    ui_textbox = trayMgr->createTextBox(
            OgreBites::TL_TOPLEFT,
            "ui_textbox", "ui_textbox",
            TRAY_WIDTH, 150 );
    for( uint8_t i = 0; i < itemNum; ++i ) {
        ui_textbox->appendText( selectitems[i] + "\n" );
    }
    
    ui_button = trayMgr->createButton(
            OgreBites::TL_TOPLEFT,
            "ui_button", "ui_button",
            TRAY_WIDTH );
    
    trayMgr->createSeparator(
        OgreBites::TL_TOPLEFT, "ui_separator2", TRAY_WIDTH );
    
    ui_slider = trayMgr->createLongSlider(
            OgreBites::TL_TOPLEFT, "ui_slider", "x",
            TRAY_WIDTH, TRAY_WIDTH - 100, 80, -50, 50, 120);
    
    trayMgr->createLongSlider(
            OgreBites::TL_TOPLEFT, "ui_slidery", "y",
            TRAY_WIDTH, TRAY_WIDTH - 100, 80, -50, 50, 120);
    
    trayMgr->createLongSlider(
            OgreBites::TL_TOPLEFT, "ui_sliderz", "z",
            TRAY_WIDTH, TRAY_WIDTH - 100, 80, -50, 50, 120);
    
    trayMgr->createSeparator( 
        OgreBites::TL_TOPLEFT, "ui_separator3", TRAY_WIDTH );
    
    ui_checkbox = trayMgr->createCheckBox(
            OgreBites::TL_TOPLEFT, "ui_checkbox", "ui_checkbox",
            TRAY_WIDTH );
    
    ui_progressbar = trayMgr->createProgressBar(
            OgreBites::TL_TOPLEFT, "ui_progressbar", "ui_progressbar",
            TRAY_WIDTH, TRAY_WIDTH * 0.6 );
    
    loaded = 0;
    ui_progressbar->setProgress( loaded );
    ui_progressbar->setComment( "waiting" );
    
    ui_params = trayMgr->createParamsPanel(
            OgreBites::TL_BOTTOMRIGHT, "ui_params",
            TRAY_WIDTH, 
            selectitems );
    
}

void CustomApp::before_draw() {
    
    if ( reload_resources ) {
        
        OverlayManager& om = OverlayManager::getSingleton();
        ResourceGroupManager& rgm = ResourceGroupManager::getSingleton();
        
        delete trayMgr;
        om.destroyAllOverlayElements( false );
        om.destroyAllOverlayElements( true );
        om.destroyAll();
        
        rgm.destroyResourceGroup( "Essential" );
        PolymorphApplication::setupResources();
        rgm.initialiseAllResourceGroups();
        
        DataStreamPtr tmpl = rgm.openResource( "SdkTrays.overlay", "Essential" );
        std::cout << "SdkTrays.overlay: " << !tmpl.isNull() << std::endl;
        
        om.parseScript( tmpl, "Essential" );
        createTray();
        
        std::cout << "'Essential' reloaded" << std::endl;
        reload_resources = false;
        
    }
    
}

void CustomApp::draw() {

    //cout << draw_event.timeSinceLastFrame << endl;
    
    proot->yaw( Ogre::Radian( draw_event.timeSinceLastFrame * 0.1 ) );
    
    Ogre::Real prev_loaded = loaded;
    loaded += draw_event.timeSinceLastFrame * 0.1;
    ui_progressbar->setProgress( loaded );
    if ( prev_loaded < 0.5 && loaded >= 0.5 ) {
        ui_progressbar->setComment( "50% done" );
    } else if ( prev_loaded < 0.9 && loaded >= 0.9 ) {
        ui_progressbar->setComment( "nearly finished..." );
    } else if ( prev_loaded < 1 && loaded >= 1 ) {
        ui_progressbar->setComment( "fully loaded!" );
    }
    if ( loaded > 1.5 ) {
        loaded = 0;
    }
    
}

bool CustomApp::keyPressed(const OIS::KeyEvent &arg) {

    // keep default behavior
    PolymorphApplication::keyPressed( arg );
    
    if ( arg.key == OIS::KC_SPACE ) {
        reload_resources = true;
    }
    
}

bool CustomApp::mouseMoved(const OIS::MouseEvent &arg) {

    // keep default behavior
    PolymorphApplication::mouseMoved( arg );
    
}