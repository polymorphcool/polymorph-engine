#/*
#-----------------------------------------------------------------------------
#Filename:    CMakeLists.txt
#-----------------------------------------------------------------------------
#
# 
# _________ ____  .-. _________/ ____ .-. ____ 
# __|__  (_)_/(_)(   )____<    \|    (   )  (_)
#                 `-'                 `-'      
# 
#
# art & game engine
# 
#____________________________________  ?   ____________________________________
#                                    (._.)
# 
# 
# This file is part of polymorph package
# For the latest info, see http://polymorph.cool/
# 
# Copyright (c) 2016 polymorph.cool
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
# 
#___________________________________( ^3^)_____________________________________
# 
# ascii font: rotated by MikeChat & myflix
# have fun and be cool :)
# 
#*/

cmake_minimum_required(VERSION 2.6)

project(PolymorphProject)

if ("${EXEC_NAME}" STREQUAL "")
    set(EXEC_NAME "EmptyApp")
endif()

set(OGRE_DEPENDECIES_DIR ${OGRE_HOME})

set(APP_NAME ${EXEC_NAME})

if(WIN32)
    set(CMAKE_MODULE_PATH "$ENV{OGRE_HOME}/cmake/;${CMAKE_MODULE_PATH}")
endif(WIN32)

if(EXISTS "${OGRE_HOME}/lib/OGRE/cmake")
    set(CMAKE_MODULE_PATH "${OGRE_HOME}/lib/OGRE/cmake/;${CMAKE_MODULE_PATH}")
    set(OGRE_DIR "${OGRE_HOME}/lib/OGRE/cmake/")
    set(OGRE_SAMPLES_INCLUDEPATH "${OGRE_HOME}/samples/Common/include/") 
else ()
    message(SEND_ERROR "Failed to find module path.")
endif(EXISTS "${OGRE_HOME}/lib/OGRE/cmake")
 
if (CMAKE_BUILD_TYPE STREQUAL "")
    # CMake defaults to leaving CMAKE_BUILD_TYPE empty. This screws up
    # differentiation between debug and release builds.
    set(CMAKE_BUILD_TYPE "RelWithDebInfo" CACHE STRING "Choose the type of build, options are: None (CMAKE_CXX_FLAGS or CMAKE_C_FLAGS used) Debug Release RelWithDebInfo MinSizeRel." FORCE)
endif ()
 
set(CMAKE_DEBUG_POSTFIX "_d")
 
set(CMAKE_INSTALL_PREFIX "${CMAKE_CURRENT_BINARY_DIR}/dist")
 
find_package(OGRE REQUIRED)
if(NOT OGRE_FOUND)
	message(SEND_ERROR "Failed to find OGRE.")
endif()

find_package(OIS REQUIRED)
if(NOT OIS_FOUND)
	message(SEND_ERROR "Failed to find OIS.")
endif()

find_package(Polymorph REQUIRED)
if(NOT Polymorph_FOUND)
	message(SEND_ERROR "Failed to find Polymorph.")
endif()

# Find Boost
#if (NOT OGRE_BUILD_PLATFORM_IPHONE)
#	if (WIN32 OR APPLE)
#		set(Boost_USE_STATIC_LIBS TRUE)
#	else ()
#		# Statically linking boost to a dynamic Ogre build doesn't work on Linux 64bit
#		set(Boost_USE_STATIC_LIBS ${OGRE_STATIC})
#	endif ()
#	if (MINGW)
#		# this is probably a bug in CMake: the boost find module tries to look for
#		# boost libraries with name libboost_*, but CMake already prefixes library
#		# search names with "lib". This is the workaround.
#		set(CMAKE_FIND_LIBRARY_PREFIXES ${CMAKE_FIND_LIBRARY_PREFIXES} "")
#	endif ()
#	set(Boost_ADDITIONAL_VERSIONS "1.44" "1.44.0" "1.42" "1.42.0" "1.41.0" "1.41" "1.40.0" "1.40" "1.39.0" "1.39" "1.38.0" "1.38" "1.37.0" "1.37" )
#	# Components that need linking (NB does not include header-only components like bind)
#	set(OGRE_BOOST_COMPONENTS thread date_time)
#	find_package(Boost COMPONENTS ${OGRE_BOOST_COMPONENTS} QUIET)
#	if (NOT Boost_FOUND)
#		# Try again with the other type of libs
#		set(Boost_USE_STATIC_LIBS NOT ${Boost_USE_STATIC_LIBS})
#		find_package(Boost COMPONENTS ${OGRE_BOOST_COMPONENTS} QUIET)
#	endif()
#	find_package(Boost QUIET)
#endif()
 
file(GLOB_RECURSE HDRS
    "src/*.h"
)

file(GLOB_RECURSE SRCS
    "src/*.cpp"
)
 
include_directories( 
    ${OIS_INCLUDE_DIRS}
    ${OGRE_INCLUDE_DIRS}
    ${OGRE_Overlay_INCLUDE_DIRS}
    ${Polymorph_INCLUDE_DIR}
)
 
add_executable(${APP_NAME} WIN32 ${HDRS} ${SRCS})
 
set_target_properties(${APP_NAME} PROPERTIES DEBUG_POSTFIX _d)
 
target_link_libraries(${APP_NAME} ${OGRE_LIBRARIES} ${OIS_LIBRARIES} ${OGRE_Overlay_LIBRARIES}  ${Polymorph_LIBRARY} )
 
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/dist/bin)
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/dist/media)
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/dist/lib)
 
# post-build copy for win32
if(WIN32 AND NOT MINGW)
	add_custom_command( TARGET ${APP_NAME} PRE_BUILD
		COMMAND if not exist .\\dist\\bin mkdir .\\dist\\bin )
	add_custom_command( TARGET ${APP_NAME} POST_BUILD
		COMMAND copy \"$(TargetPath)\" .\\dist\\bin )
	add_custom_command( TARGET ${APP_NAME} POST_BUILD
		COMMAND copy \"$(TargetPath)\" .\\dist\\lib )
endif(WIN32 AND NOT MINGW)

if(MINGW OR UNIX)
	set(EXECUTABLE_OUTPUT_PATH ${PROJECT_BINARY_DIR}/dist/bin)
endif(MINGW OR UNIX)
 
if(WIN32)
 
	install(TARGETS ${APP_NAME}
		RUNTIME DESTINATION bin
		CONFIGURATIONS All)
 
	install(DIRECTORY ${CMAKE_SOURCE_DIR}/dist/Media
		DESTINATION ./
		CONFIGURATIONS Release RelWithDebInfo Debug
	)
 
	install(FILES ${CMAKE_SOURCE_DIR}/dist/bin/plugins.cfg
		${CMAKE_SOURCE_DIR}/dist/bin/resources.cfg
		DESTINATION bin
		CONFIGURATIONS Release RelWithDebInfo
	)
 
	install(FILES ${CMAKE_SOURCE_DIR}/dist/bin/plugins_d.cfg
		${CMAKE_SOURCE_DIR}/dist/bin/resources_d.cfg
		DESTINATION bin
		CONFIGURATIONS Debug
	)
 
        # NOTE: for the 1.7.1 sdk the OIS dll is called OIS.dll instead of libOIS.dll
        # so you'll have to change that to make it work with 1.7.1
	install(FILES ${OGRE_PLUGIN_DIR_REL}/OgreMain.dll
		${OGRE_PLUGIN_DIR_REL}/RenderSystem_Direct3D9.dll
		${OGRE_PLUGIN_DIR_REL}/RenderSystem_GL.dll
		${OGRE_PLUGIN_DIR_REL}/OIS.dll
		${OGRE_PLUGIN_DIR_DBG}/OgreOverlay.dll
		DESTINATION bin
		CONFIGURATIONS Release RelWithDebInfo
	)

	install(FILES ${OGRE_PLUGIN_DIR_DBG}/OgreMain_d.dll
		${OGRE_PLUGIN_DIR_DBG}/RenderSystem_Direct3D9_d.dll
		${OGRE_PLUGIN_DIR_DBG}/RenderSystem_GL_d.dll
		${OGRE_PLUGIN_DIR_DBG}/OIS_d.dll
		${OGRE_PLUGIN_DIR_DBG}/OgreOverlay_d.dll
		DESTINATION bin
		CONFIGURATIONS Debug
	)
 
   # as of sdk 1.7.2 we need to copy the boost dll's as well
   # because they're not linked statically (it worked with 1.7.1 though)
#   install(FILES ${Boost_DATE_TIME_LIBRARY_RELEASE}
#      ${Boost_THREAD_LIBRARY_RELEASE}
#      DESTINATION bin
#      CONFIGURATIONS Release RelWithDebInfo
#   )
# 
#   install(FILES ${Boost_DATE_TIME_LIBRARY_DEBUG}
#      ${Boost_THREAD_LIBRARY_DEBUG}
#      DESTINATION bin
#      CONFIGURATIONS Debug
#   )
endif(WIN32)

if(UNIX)
 
	install(TARGETS ${APP_NAME}
		RUNTIME DESTINATION bin
		CONFIGURATIONS All)
 
	install(DIRECTORY ${CMAKE_SOURCE_DIR}/dist/media
		DESTINATION ./
		CONFIGURATIONS Release RelWithDebInfo Debug
	)
	
    if(EXISTS "${CMAKE_SOURCE_DIR}/dist/lib")
        install(DIRECTORY ${CMAKE_SOURCE_DIR}/dist/lib
            DESTINATION ./
            CONFIGURATIONS Release RelWithDebInfo Debug
        )
    endif()
    
    if(EXISTS "${CMAKE_SOURCE_DIR}/dist/bin/plugins.cfg")
        # custom plugins.cfg, do not use the polymorph one
        install(FILES ${CMAKE_SOURCE_DIR}/dist/bin/plugins.cfg
            DESTINATION bin
            CONFIGURATIONS Release RelWithDebInfo Debug
        )
    else()
        # using the default plugins.cfg, and loading default .so
        install(FILES ${OGRE_HOME}/share/polymorph/linux/plugins.cfg
            DESTINATION bin
            CONFIGURATIONS Release RelWithDebInfo Debug
        )
        # copying Plugin_CgProgramManager.so.2.0.0
        if(EXISTS "${CMAKE_SOURCE_DIR}/dist/lib/Plugin_CgProgramManager.so.2.0.0")
            # do nothing, a custom .so has been placed
        else()
            install(FILES ${OGRE_HOME}/lib/OGRE/Plugin_CgProgramManager.so.2.0.0
                DESTINATION lib
                CONFIGURATIONS Release RelWithDebInfo Debug
            )
        endif()
        # copying RenderSystem_GL.so.2.0.0
        if(EXISTS "${CMAKE_SOURCE_DIR}/dist/lib/RenderSystem_GL.so.2.0.0")
            # do nothing, a custom .so has been placed
        else()
            install(FILES ${OGRE_HOME}/lib/OGRE/RenderSystem_GL.so.2.0.0
                DESTINATION lib
                CONFIGURATIONS Release RelWithDebInfo Debug
            )
        endif()
    endif()

    if(EXISTS "${CMAKE_SOURCE_DIR}/dist/bin/configuration.xml")
        install(FILES ${CMAKE_SOURCE_DIR}/dist/bin/configuration.xml
                DESTINATION bin
                CONFIGURATIONS Release RelWithDebInfo Debug
        )
    else()
        if(EXISTS "${CMAKE_SOURCE_DIR}/dist/bin/resources.cfg")
            install(FILES ${CMAKE_SOURCE_DIR}/dist/bin/resources.cfg
                    DESTINATION bin
                    CONFIGURATIONS Release RelWithDebInfo Debug
            )
        endif()
    endif()
 
endif(UNIX)