
#define NUM_SHADOW_SAMPLES_1D 2.0
#define SHADOW_FILTER_SCALE 0.5

#define SHADOW_SAMPLES NUM_SHADOW_SAMPLES_1D*NUM_SHADOW_SAMPLES_1D

vec4 offsetSample(vec4 uv, vec2 offset, float invMapSize)
{
	return vec4(uv.xy + offset * invMapSize * uv.w, uv.z, uv.w);
}

float calcDepthShadowLod(sampler2D shadowMap, vec4 uv, float invShadowMapSize)
{
	// 4-sample PCF
	float shadow = 0.0;
	float offset = (NUM_SHADOW_SAMPLES_1D / 2.0 - 0.5) * SHADOW_FILTER_SCALE;
	for (float y = -offset; y <= offset; y += SHADOW_FILTER_SCALE)
	{
		for (float x = -offset; x <= offset; x += SHADOW_FILTER_SCALE)
		{
			vec4 finalUv = offsetSample(uv, vec2(x, y), invShadowMapSize);
			finalUv.xy /= finalUv.w;
            float depth = texture2DLod( shadowMap, finalUv.xy, 0.0 ).x;
			if( depth >= 1.0 || depth >= uv.z )
				shadow += 1.0;
		}
	}
	shadow /= SHADOW_SAMPLES;

	return shadow;
}

#define NUM_LIGHTS 8

uniform vec4 lightAmbient;
uniform vec4 lightPosition[NUM_LIGHTS];
uniform vec4 lightDiffuse[NUM_LIGHTS];

uniform float invShadowMapSize0;
uniform float invShadowMapSize1;
uniform float invShadowMapSize2;

uniform sampler2D diffuseMap;
uniform sampler2D shadowMap0;

void main()
{
    vec2 psUv         = gl_TexCoord[0].xy;
    vec3 psWorldPos   = gl_TexCoord[1].xyz;
    vec3 psNorm       = gl_TexCoord[2].xyz;
    vec4 psLightSpacePos0 = gl_TexCoord[3].xyzw;
	float fShadow = calcDepthShadowLod( shadowMap0, psLightSpacePos0, invShadowMapSize0 );
	vec3 negLightDir = lightPosition[0].xyz - (psWorldPos * lightPosition[0].w);
	negLightDir	= normalize( negLightDir );
	vec3 normal = normalize( psNorm );
    gl_FragColor = max( 0.0, dot( negLightDir, normal ) ) * lightDiffuse[0] * fShadow + lightAmbient;
	int i=1;
	for( i=1; i<NUM_LIGHTS; ++i )
	{
		vec3 negLightDir = lightPosition[i].xyz - (psWorldPos * lightPosition[i].w);
		negLightDir	= normalize( negLightDir );
        gl_FragColor += max( 0.0, dot( negLightDir, normal ) ) * lightDiffuse[i];
	}
    gl_FragColor *= texture2D( diffuseMap, psUv );

}
