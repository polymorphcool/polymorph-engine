#version 120
#extension GL_NV_non_square_matrices : enable

attribute vec4 vertex;
attribute vec3 normal;
attribute vec2 uv0;

#ifdef DEPTH_SHADOWCASTER
	float psDepth;
	//float shadowConstantBias;
#else
	vec2 psUv;
	vec3 psWorldPos;
	vec3 psOutNorm;
	vec4 psLightSpacePos0;
	uniform mat4x4 texViewProjMatrix0;
	uniform mat4x4 world;
#endif

uniform mat4x4 worldViewProj;
uniform vec4 depthRange0;

void main()
{
	gl_Position	= worldViewProj * vertex;

#ifdef DEPTH_SHADOWCASTER

	//Linear depth
    //psDepth	= (gl_Position.z - depthRange0.x + shadowConstantBias) * depthRange0.w;
	psDepth	= (gl_Position.z - depthRange0.x + 0.001) * depthRange0.w;

	//We can't make the depth buffer linear without Z out in the fragment shader;
	//however we can use a cheap approximation ("pseudo linear depth")
	//see http://yosoygames.com.ar/wp/2014/01/linear-depth-buffer-my-ass/
    gl_Position.z = gl_Position.z * (gl_Position.w * depthRange0.w);
	
#else

	psWorldPos	= (world * vertex).xyz;
	psOutNorm	= mat3x3(world) * normal;
	psUv		= uv0;

	// Calculate the position of vertex in light space to do shadows
	vec4 shadowWorldPos = vec4( psWorldPos, 1.0 );
	psLightSpacePos0 = texViewProjMatrix0 * shadowWorldPos;
	
	// Linear depth
    psLightSpacePos0.z = (psLightSpacePos0.z - depthRange0.x) * depthRange0.w;
	
#endif

#ifdef DEPTH_SHADOWCASTER

    //gl_TexCoord[0] = psDepth.xxxx;
	gl_TexCoord[0] = gl_Position
	
#else

    gl_TexCoord[0] = psUv.xyyy;
    gl_TexCoord[1] = psWorldPos.xyzz;
    gl_TexCoord[2] = psOutNorm.xyzz;
    gl_TexCoord[3] = psLightSpacePos0;
	
#endif

}
