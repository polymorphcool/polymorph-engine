/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ___________________________________( ^3^)_____________________________________
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 */

/* 
 * File:   CustomApp.cpp
 * Author: frankiezafe
 * 
 * Created on November 19, 2016, 6:07 PM
 */

#include "CustomApp.h"

using namespace std;
using namespace Ogre;
using namespace polymorph;

void CustomApp::createCompositor(void) {

    compositorMgr = root->getCompositorManager2();
    compositorwpname = "FocusedShadow";
    const Ogre::IdString workspaceName(compositorwpname);
    if (!compositorMgr->hasWorkspaceDefinition(workspaceName)) {
        compositorMgr->createBasicWorkspaceDef(
                "FocusedShadow",
                Ogre::ColourValue(0.4f, 0.4f, 0.4f),
                Ogre::IdString()
                );
    }
    compositorwp = compositorMgr->addWorkspace(
            sceneMgr, window, cam, workspaceName, true);
    
//    return compositorManager->addWorkspace( mSceneMgr, mWindow, mCamera, workspaceName, true );
//    
//    CompositorTargetDef *targetDef = nodeDef->getTargetPass(0);
//    compositorwpname = "CompositorBaseWorkspace";
//    const Ogre::IdString workspaceName(compositorwpname);
//    if (!compositorMgr->hasWorkspaceDefinition(workspaceName)) {
//        compositorMgr->createBasicWorkspaceDef(
//                "CompositorBaseWorkspace",
//                Ogre::ColourValue(0.4f, 0.4f, 0.4f),
//                Ogre::IdString()
//                );
//    }
//    compositorwp = compositorMgr->addWorkspace(sceneMgr, window, cam, workspaceName, true);

}

void CustomApp::createScene(void) {

    // keep default behavior
    PolymorphApplication::createScene();

    root_node.empty(sceneMgr);

    sun.sun(sceneMgr);
    sun.move(0, 100, 80);
    sun.pitch(Radian(0.73));
    sun.yaw(Radian(0.1));
    sun.debug(true);
    root_node.attach(&sun);

    floor.cube(sceneMgr);
    floor.scale(250, 2, 250);
    floor.move(0, -125, 0);
    root_node.attach(&floor);

    wall.cube(sceneMgr);
    wall.scale(250, 250, 2);
    wall.move(0, 0, -125);
    root_node.attach(&wall);

    box.cube(sceneMgr);
    box.scale(30);
    root_node.attach(&box);
    
    PMaterial shadow_mat;
    shadow_mat.load( "General", "FocusedShadowMaterial" );
    floor.material( &shadow_mat, true );
    wall.material( &shadow_mat, true );
    box.material( &shadow_mat, true );
    
    // visualising shadow texture
    IdString shadowNodeName = "FocusedShadow_ShadowNode";
    PMaterial c2d;
    c2d.load("General", "Material2d");
    Ogre::TextureUnitState* textureUnit = c2d.getMaterial().get()->getTechnique(0)->getPass(0)->
            getTextureUnitState(0);
    CompositorShadowNode *shadowNode = compositorwp->findShadowNode(shadowNodeName);
    Ogre::TexturePtr tex = shadowNode->getLocalTextures()[0].textures[0];
    textureUnit->setTextureName(tex->getName());
    Ogre::OverlayManager& overlayManager = Ogre::OverlayManager::getSingleton();
    // Create an overlay
    Overlay *debugOverlay = overlayManager.create("OverlayName");
    // Create a panel
    Ogre::OverlayContainer* panel = static_cast<Ogre::OverlayContainer*> (
            overlayManager.createOverlayElement("Panel", "PanelName0"));
    panel->setMetricsMode(Ogre::GMM_PIXELS);
    panel->setPosition(10, 10);
    panel->setDimensions(300, 300);
    panel->setMaterialName("Material2d");
    debugOverlay->add2D(panel);
    debugOverlay->show();

}

void CustomApp::draw() {

    root_node.yaw(Radian(draw_event.timeSinceLastFrame * 0.1));
    root_node.roll(Radian(draw_event.timeSinceLastFrame * 0.08));

}

bool CustomApp::keyPressed(const OIS::KeyEvent &arg) {

    // keep default behavior
    PolymorphApplication::keyPressed(arg);

}

bool CustomApp::mouseMoved(const OIS::MouseEvent &arg) {

    // keep default behavior
    PolymorphApplication::mouseMoved(arg);

}