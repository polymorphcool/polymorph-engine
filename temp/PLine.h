/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   PLine.h
 * Author: frankiezafe
 *
 * Created on September 24, 2017, 12:09 PM
 */

#ifndef PLINE_H
#define PLINE_H

#include "PNode.h"

namespace polymorph {

    class PLine : public PNode, public Ogre::RenderQueueListener {
    public:

        PLine();
        virtual ~PLine();

        bool allocate(
                Ogre::SceneManager* sm,
                uint16_t vertices_num,
                Ogre::Vector3* verts = 0);

        void vertex(uint16_t i,
                Ogre::Real x,
                Ogre::Real y,
                Ogre::Real z,
                bool absolute = true
                );

        void vertex(
                uint16_t i,
                const Ogre::Vector3& src,
                bool absolute = true);

        Ogre::Vector3 getVertex(uint16_t i);

        inline uint16_t getVertexNum() {
            return _vnum;
        }

        void preRenderQueues();

    protected:

        Ogre::ManualObject* _mo;
        uint16_t _vnum;
        Ogre::Vector3* _vertices;
        bool _refresh_required;

    private:

    };

};

#endif /* PLINE_H */

