== about shaders management in ogre ==

Polymorphism is active on the level of the rendering engine 
(opengl or directx), but NOT in the details of their capabilities.

For instance, if you create 2 pixels shaders for 2 different version of 
opengl, lets say 120 & 330, ogre is not capable to identify witch suits
best the current computer capabilities (or i didn't found how, that´s 
a possibility).

=== resources loading ===

The declaration order in resources.cfg IS important! In this example,
there is 2 shaders with the same filename, **tester_pix.glsl**. 
One is the default (media/shaders/glsl, version 120), the other is 
using version 330 (media/shaders/glsl330).

In resources.cfg, if both shaders are loaded in the same group, the last
loaded overwrites the previous one.

 [Shaders]
 FileSystem=../media/shaders/glsl
 FileSystem=../media/shaders/glsl330 

=> **tester_pix.glsl** v330 loaded !!!

 [Shaders]
 FileSystem=../media/shaders/glsl330
 FileSystem=../media/shaders/glsl 

=> **tester_pix.glsl** v120 loaded !!!

=== doc ===

* https://ogrecave.github.io/ogre/api/1.10/Material-Scripts.html#Declaring-Vertex_002fGeometry_002fFragment-Programs
