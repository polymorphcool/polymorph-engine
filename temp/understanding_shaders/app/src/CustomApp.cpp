/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ___________________________________( ^3^)_____________________________________
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
*/

/* 
 * File:   CustomApp.cpp
 * Author: frankiezafe
 * 
 * Created on November 19, 2016, 6:07 PM
 */

#include "CustomApp.h"

using namespace std;
using namespace Ogre;
using namespace polymorph;

void CustomApp::createScene(void) {

    // keep default behavior
    PolymorphApplication::createScene();
    
    p = new PNode();
    p->cube( sceneMgr, "cube" );
    p->scale( 100 );
    p->material( "Shaders", "shader_tester" );
    
}

void CustomApp::draw() {
    
    p->yaw( Radian( draw_event.timeSinceLastFrame ) );
    p->roll( Radian( draw_event.timeSinceLastFrame ) );
    
}

bool CustomApp::keyPressed(const OIS::KeyEvent &arg) {

    // keep default behavior
    PolymorphApplication::keyPressed( arg );
    
}

bool CustomApp::mouseMoved(const OIS::MouseEvent &arg) {

    // keep default behavior
    PolymorphApplication::mouseMoved( arg );
    
}
