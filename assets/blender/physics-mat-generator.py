import os

num = 30
prefixes = [ "box_", "sphere_", "cylinder_", "capsule_", "cone_", "convex_" ]
constant = ""

basematname = "physics"

basemat = '''
material physics
{
    receive_shadows off 
    technique
    {
        pass
        {
            ambient 0.8 0.8 0.8 1.0
            diffuse 0.8 0.8 0.8 1.0
            specular 0.5 0.5 0.5 1.0 12.5
            emissive 1.0 0.0 1.0 1.0
            lighting off
            polygon_mode wireframe
        }
    }
}
'''

out = basemat

for i in range( 0, num + 1 ):
	
	id = ""
	if  i < 10:
		id += "0"
	id += str( i )
	
	for p in prefixes:
		s = "\nmaterial " + p + constant + id + " : " + basematname + "\n{}"
		out += s

f = open( basematname + ".material", "w" )
f.write(out)
f.close()

print( out )