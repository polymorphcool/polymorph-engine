'''
Material generator for physics polymorph engine.

How to use:
    - define the number of materials you need by changing num
    - run the script
    
previous materials will be deleted and replaced by new ones

this script is part of polymorph engine assets
'''

import bpy
from mathutils import Color

# configure your script to match names in ogre
num = 30
prefixes = [ "box_", "sphere_", "cylinder_", "capsule_", "cone_", "convex_" ]
constant = "physics"
    
for i in range( 0, num ):
    id = ""
    if  i < 10:
        id += "0"
    id += str( i )
    
    for p in prefixes:
        name = p + constant + id
        if name in bpy.data.materials.keys():
            prev_m = bpy.data.materials[ name ]
            prev_m.user_clear()
            bpy.data.materials.remove( prev_m )
        mat = bpy.data.materials.new( name )
        diffuse = Color()
        diffuse.hsv = i / (num+1), 1, 1
        mat.diffuse_color = diffuse