'''
Material attribution depending on object name.

How to use:
    - select a serie of objects
    - run the script

if you're also using the material generator, make sure prefixes are in sync!
this script does NOT generate the materials

this script is part of polymorph engine assets
'''

import bpy

prefixes = [ "box_", "sphere_", "cylinder_", "capsule_", "cone_", "convex_" ]
constant = "physics"
prefixes_count = [ 0,0,0,0,0,0 ]

def makeId( i ):
    
    id = ""
    if  i < 10:
        id += "0"
    id += str( i )
    return id

def getMat( name ):
    
    global prefixes
    global constant
    global prefixes_count
    
    if name[:4] == 'Cube':
        matname = prefixes[0] + constant + makeId( prefixes_count[0] )
        prefixes_count[0] += 1
        return matname
    elif name[:6] == 'Sphere':
        matname = prefixes[1] + constant + makeId( prefixes_count[1] )
        prefixes_count[1] += 1
        return matname
    elif name[:8] == 'Cylinder':
        matname = prefixes[2] + constant + makeId( prefixes_count[2] )
        prefixes_count[2] += 1
        return matname
    elif name[:7] == 'Capsule':
        matname = prefixes[3] + constant + makeId( prefixes_count[3] )
        prefixes_count[3] += 1
        return matname
    elif name[:4] == 'Cone':
        matname = prefixes[4] + constant + makeId( prefixes_count[4] )
        prefixes_count[4] += 1
        return matname
    else:
        matname = prefixes[5] + constant + makeId( prefixes_count[5] )
        prefixes_count[5] += 1
        return matname
    

sels = bpy.context.selected_objects
for o in sels:
    print( o.name )
    m = getMat( o.name )
    if m in bpy.data.materials.keys():
        for i in range(len(o.material_slots)):
            bpy.ops.object.material_slot_remove({'object': o})
        o.data.materials.append( bpy.data.materials[ m ] )
        print( o, m )
    else:
        s = "ERROR: no material '" + m + "' found! Did you run material-generator-for-physics.py first?"
        print( '\x1b[0;31;40m' + s + '\x1b[0m' )
    #print( o )