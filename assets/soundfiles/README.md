# DETAILS on  SOUFDFILES
These soundfiles are used to test the pd patches in the assets folder.

## Sources
All files are comming from freesound.org and have been renamed to have shorter names.

## URL

# ambience3.ogg :
real-name : 128366__mysterious__ambient4.ogg
url : http://freesound.org/people/_mysterious_/sounds/128366/

# ambient4.ogg :
real-name : 220894__diboz__ibrkr01-drone.ogg
url : http://freesound.org/people/Diboz/sounds/220894/

# drone.ogg :
real-name : 260572__isaac200000__ambience3.ogg
url : http://freesound.org/people/Isaac200000/sounds/260572/

# ferambie.ogg : 
real-name : 316829__lalks__ferambie.ogg
url : http://freesound.org/people/Lalks/sounds/316829/

# kerriflute.mp3 :
real-name : 88794__ethang__kerriflute.mp3
url : http://freesound.org/people/ethang/sounds/88794/

# oceanamin.ogg :
real-name : 334628__vultraz168__oceanamin.ogg
url : http://freesound.org/people/vultraz168/sounds/334627/

# parallel-harmonics.mp3 :
real-name : 9463__guitarz1970__parallel-harmonics-1.mp3
url : http://freesound.org/people/Guitarz1970/sounds/9463/ 

# stardust.ogg :
real-name : 329754__chairpepper__stardust.ogg
url : http://freesound.org/people/ChairPepper/sounds/329754/

# bladerunner.aiff
real-name : 6715__mbazzy__bladerunner.aiff
url : http://freesound.org/people/mbazzy/sounds/6715/

# helupema.aiff
real-name : 137759__helupema__ambient-sound.aiff
url : http://freesound.org/people/helupema/sounds/137759/

# theCommons.aiff 
real-name : 253115__simongray__the-commons.aiff
url : http://freesound.org/people/simongray/sounds/253115/

# end2.aiff
real-name : 316620__benjaminharveydesign__end2.aiff
url : http://freesound.org/people/benjaminharveydesign/sounds/316620/

# cybertrash.wav
real-name : 12517__noisecollector__granular-ambience-9-cyberthrash.wav
url : http://freesound.org/people/noisecollector/sounds/12517/

# alienbomb.wav
real-name : 39062__alienbomb__ambient-field.wav
url : http://freesound.org/people/alienbomb/sounds/39062/

# ferambie.wav
real-name : 316828__lalks__ferambie.wav
url : http://freesound.org/people/lalks/sounds/316828/

# angry-drone.wav
real-name : 371194__dubfred__monster-angry-drone-in-c.wav
url : http://freesound.org/people/dubfred/sounds/371194/
