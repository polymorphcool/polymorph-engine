# PD

Notes about how to use and deploy the pd assets in a polymorph project.

## setup

If you ran the installation script of polymorph, Pd is already installed.

If not, you have to: 

* install Puredata from http://puredata.info
* via deken, install **mrpeach**
* via deken, install **flatgui**
* via deken, install **pdogg**
* via deken, install **pan**
* via deken, install **cyclone**
* via deken, install **ext13**

for all: latest version

## Modes

Two modes are prepared in the polymorph engine:

* **Edition mode**: allows to use puredata with its user inter, face.
* **Release mode**: patches are loaded by the application.

### Edition

steps to make it work (to do)

### Release

steps to make it work (to do)
