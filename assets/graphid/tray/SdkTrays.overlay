template container Panel(SdkTrays/Cursor)
{
	metrics_mode pixels
	transparent true
	
	// You can offset the image to change the cursor "hotspot"
	element Panel(CursorImage)
	{
        metrics_mode pixels
        material SdkTrays/Cursor
        width 20
        height 20
		top -10
		left -10
	}
}

template container BorderPanel(SdkTrays/Tray)
{
	metrics_mode pixels
	material SdkTrays/Tray
	uv_coords 0.25 0.25 0.75 0.75
	
	border_material SdkTrays/Tray
	border_size 16 16 16 16
	border_topleft_uv     0.00 0.00 0.25 0.25
	border_top_uv         0.25 0.00 0.75 0.25
	border_topright_uv    0.75 0.00 1.00 0.25
	border_left_uv        0.00 0.25 0.25 0.75
	border_right_uv       0.75 0.25 1.00 0.75
	border_bottomleft_uv  0.00 0.75 0.25 1.00
	border_bottom_uv      0.25 0.75 0.75 1.00
	border_bottomright_uv 0.75 0.75 1.00 1.00
}

template container BorderPanel(SdkTrays/Button)
{
	metrics_mode pixels
	material SdkTrays/Button/Up
	horz_align center
	height 20
	uv_coords 0.145 0.000 0.855 1.000
	
	border_material SdkTrays/Button/Up
	border_size 8 8 0 0
	border_left_uv  0.000 0.000 0.125 1.000
	border_right_uv 0.875 0.000 1.000 1.000

	element TextArea(ButtonCaption)
	{
        metrics_mode pixels
        horz_align center
        vert_align center
        alignment center
        font_name SdkTrays/Caption
        char_height 14
        space_width 7
        colour 1 1 1
	}
}

template container BorderPanel(SdkTrays/TextBox)
{
	metrics_mode pixels
	material SdkTrays/TextBox
	horz_align center
	uv_coords 0.4 0.4 0.6 0.6
	
	border_material SdkTrays/TextBox
	
	container BorderPanel(TextBoxCaptionBar)
	{
		metrics_mode pixels
		material SdkTrays/MiniTray
		height 20
		top 0
		left 0
		uv_coords 0.375 0.375 0.625 0.625
		
		border_material SdkTrays/MiniTray
		border_size 10 10 10 10
		border_topleft_uv     0.000 0.000 0.375 0.375
		border_top_uv         0.375 0.000 0.625 0.375
		border_topright_uv    0.625 0.000 1.0 0.375
		border_left_uv        0.000 0.375 0.375 0.625
		border_right_uv       0.625 0.375 1.000 0.625
		border_bottomleft_uv  0.000 0.625 0.375 1.000
		border_bottom_uv      0.375 0.625 0.625 1.000
		border_bottomright_uv 0.625 0.625 1.000 1.000

		element TextArea(TextBoxCaption)
		{
			metrics_mode pixels
			horz_align center
			alignment center
			top 7
			font_name SdkTrays/Caption
			char_height 14
			space_width 7
			colour 1 1 1
		}
	}

	element TextArea(TextBoxText)
	{
        metrics_mode pixels
        font_name SdkTrays/Value
        char_height 14
        space_width 8
        colour 1 1 1
	}

	container BorderPanel(TextBoxScrollTrack)
	{
        metrics_mode pixels
        material SdkTrays/ScrollTrack
        width 16
		horz_align right
		left -23
		uv_coords 0.0 0.31 1.0 0.69
		
		border_material SdkTrays/ScrollTrack
		border_size 0 0 10 10
		border_top_uv    0.0 0.0 1.0 0.31
		border_bottom_uv 0.0 0.69 1.0 1.0
        
		element Panel(TextBoxScrollHandle)
		{
			metrics_mode pixels
			material SdkTrays/VerticalHandle
			horz_align center
			left -8
			width 16
			height 16
		}
	}
}

template container BorderPanel(SdkTrays/SelectMenu)
{
	metrics_mode pixels
	material SdkTrays/MiniTray
	//horz_align left
	height 54
	uv_coords 0.375 0.375 0.625 0.625
	
	border_material SdkTrays/MiniTray
	border_size 10 10 10 10
	border_topleft_uv     0.000 0.000 0.375 0.65
	border_top_uv         0.375 0.000 0.625 0.65
	border_topright_uv    0.625 0.000 1.0 0.65
	border_left_uv        0.000 0.65 0.5 0.9
	border_right_uv       0.625 0.65 1.000 0.9
	border_bottomleft_uv  0.000 0.9 0.375 1.000
	border_bottom_uv      0.375 0.9 0.625 1.000
	border_bottomright_uv 0.625 0.9 1.000 1.000

	element TextArea(MenuCaption)
	{
        metrics_mode pixels
        horz_align center
        alignment center
        top 7
        font_name SdkTrays/Caption
        char_height 14
        space_width 7
        colour 1 1 1
	}
	
	container BorderPanel(MenuSmallBox)
	{
		metrics_mode pixels
		//material SdkTrays/MiniTextBox
		material SdkTrays/Black
		height 29
		top 24
		left 5
		uv_coords 0.4 0.4 0.6 0.6
		
		border_material SdkTrays/MiniTextBox
		border_size 0 0 0 0
		border_topleft_uv     0.0 0.0 0.4 0.4
		border_top_uv         0.4 0.0 0.6 0.4
		border_topright_uv    0.6 0.0 1.0 0.4
		border_left_uv        0.0 0.4 0.4 0.6
		border_right_uv       0.6 0.4 1.0 0.6
		border_bottomleft_uv  0.0 0.6 0.4 1.0
		border_bottom_uv      0.4 0.6 0.6 1.0
		border_bottomright_uv 0.6 0.6 1.0 1.0

		element TextArea(MenuSmallText)
		{
			metrics_mode pixels
			left 3
			top 12
			font_name SdkTrays/Value
			char_height 14
			space_width 8
			colour 1 1 1
		}
	}
	
	container BorderPanel(MenuExpandedBox)
	{
		metrics_mode pixels
		material SdkTrays/Black
		left 0
		top 0
		uv_coords 0.375 0.375 0.625 0.625
		
		border_material SdkTrays/Black
		border_size 0 0 0 0
		
		container BorderPanel(MenuScrollTrack)
		{
			metrics_mode pixels
			material SdkTrays/ScrollTrack
			width 16
			horz_align right
			left -20
			top 4
			uv_coords 0.0 0.31 1.0 0.69

			border_material SdkTrays/ScrollTrack
			border_size 0 0 10 10
			border_top_uv    0.0 0.0 1.0 0.31
			border_bottom_uv 0.0 0.69 1.0 1.0

			element Panel(MenuScrollHandle)
			{
				metrics_mode pixels
				material SdkTrays/VerticalHandle
				horz_align center
				left -8
				width 16
				height 16
			}
		}
	}
}

template container BorderPanel(SdkTrays/SelectMenuItem)
{
	metrics_mode pixels
	material SdkTrays/Button/Up
	left 4
	height 20
	uv_coords 0.145 0.000 0.855 1.000
	
	border_material SdkTrays/Button/Up
	border_size 0 0 0 0
	border_topleft_uv     0.00 0.00 0.125 0.125
	border_top_uv         0.875 0.00 0.875 0.125
	border_topright_uv    0.875 0.00 1.00 0.125
	border_left_uv        0.00 0.125 0.125 0.875
	border_right_uv       0.875 0.125 1.00 0.875
	border_bottomleft_uv  0.00 0.875 0.125 1.00
	border_bottom_uv      0.125 0.875 0.875 1.00
	border_bottomright_uv 0.875 0.875 1.00 1.00

	element TextArea(MenuItemText)
	{
		metrics_mode pixels
		left 3
		top 4
		font_name SdkTrays/Value
		char_height 14
		space_width 8
		colour 1 1 1
		caption Special Delivery
	}
}

template container BorderPanel(SdkTrays/Slider)
{
	metrics_mode pixels
	material SdkTrays/MiniTrayTranspa
	horz_align left
	height 20
	uv_coords 0 0 1 1
	
	border_material SdkTrays/MiniTrayTranspa
	border_size 0 0 0 0

	element TextArea(SliderCaption)
	{
        metrics_mode pixels
        left 0
        font_name SdkTrays/Caption
        char_height 14
        space_width 7
        colour 1 1 1
	}
	
	container BorderPanel(SliderValueBox)
	{
		metrics_mode pixels
		material SdkTrays/MiniTextBoxBlack
		horz_align right
		height 20
		top 0
		uv_coords 0 0 1 1
		
		border_material SdkTrays/MiniTextBoxBlack
		border_size 0 0 0 0

		element TextArea(SliderValueText)
		{
			metrics_mode pixels
			left 3
			top 4
			font_name SdkTrays/Value
			char_height 14
			space_width 8
			colour 1 1 1
		}
	}
		
	container BorderPanel(SliderTrack)
	{
		metrics_mode pixels
		material SdkTrays/SliderTrack
		left 8
		top 9
		height 2
		vert_align bottom
		uv_coords 0.49 0.0 0.51 1.0
		
		border_material SdkTrays/SliderTrack
		border_size 0 0 0 0
		border_left_uv  0.0 0.0 0.25 1.0
		border_right_uv 0.75 0.0 1.0 1.0
		
		element Panel(SliderHandle)
		{
			metrics_mode pixels
			material SdkTrays/Handle
			vert_align top
			top -9
			width 20
			height 20
		}
	}
}

template container BorderPanel(SdkTrays/Label)
{
	metrics_mode pixels
	material SdkTrays/Label
	horz_align center
	height 32
	uv_coords 0.49 0.000 0.51 1.000
	
	border_material SdkTrays/Label
	border_size 16 16 0 0
	border_left_uv  0.000 0.000 0.49 1.000
	border_right_uv 0.51 0.000 1.000 1.000

	element TextArea(LabelCaption)
	{
        metrics_mode pixels
        horz_align center
        alignment center
        top 10
        font_name SdkTrays/Caption
        char_height 14
        space_width 9
        colour 1 1 1
	}
}

template container Panel(SdkTrays/Separator)
{
	metrics_mode pixels
	material SdkTrays/Separator
	horz_align center
	height 10
}

template container BorderPanel(SdkTrays/ParamsPanel)
{
	metrics_mode pixels
	material SdkTrays/TextBox
	horz_align center
	uv_coords 0.4 0.4 0.6 0.6
	
	border_material SdkTrays/TextBox
	border_size 13 13 13 13
	border_topleft_uv     0.0 0.0 0.4 0.4
	border_top_uv         0.4 0.0 0.6 0.4
	border_topright_uv    0.6 0.0 1.0 0.4
	border_left_uv        0.0 0.4 0.4 0.6
	border_right_uv       0.6 0.4 1.0 0.6
	border_bottomleft_uv  0.0 0.6 0.4 1.0
	border_bottom_uv      0.4 0.6 0.6 1.0
	border_bottomright_uv 0.6 0.6 1.0 1.0

	element TextArea(ParamsPanelNames)
	{
        metrics_mode pixels
        left 15
        top 10
        font_name SdkTrays/Value
        char_height 14
        space_width 8
        colour 1 1 1
	}

	element TextArea(ParamsPanelValues)
	{
        metrics_mode pixels
        horz_align right
        alignment right
        left -15
        top 10
        font_name SdkTrays/Value
        char_height 14
        space_width 8
        colour 1 1 1
	}
}

template container BorderPanel(SdkTrays/CheckBox)
{
	metrics_mode pixels
	material SdkTrays/MiniTrayTranspa
	horz_align left
	height 20
	uv_coords 0 0 1 1
	
	border_material SdkTrays/MiniTrayTranspa
	border_size 0 0 0 0

	element TextArea(CheckBoxCaption)
	{
        metrics_mode pixels
        left 0
        top 4
        font_name SdkTrays/Caption
        char_height 14
        space_width 7
        colour 1 1 1
	}
	
	container BorderPanel(CheckBoxSquare)
	{
		metrics_mode pixels
		material SdkTrays/MiniTextBox
		horz_align right
		width 20
		height 20
		left -20
		top 0
		uv_coords 0 0 1 1
		
		border_material SdkTrays/MiniTextBox
		border_size 0 0 0 0
		
		element TextArea(CheckBoxX)
		{
			metrics_mode pixels
			left 3
			top 4
			font_name SdkTrays/Caption
			char_height 14
			space_width 9
			colour 0 1 1
			caption on
		}
	}
}

template container Panel(SdkTrays/Logo)
{
	metrics_mode pixels
	material SdkTrays/Logo
	horz_align center
	width 57
	height 98
}

template container Panel(SdkTrays/Picture)
{
	metrics_mode pixels
	material SdkTrays/Shade
	
	element BorderPanel(PictureFrame)
	{
		metrics_mode pixels
		material SdkTrays/Frame
		left -8
		top -8
		uv_coords 0.49 0.49 0.51 0.51
		
		border_material SdkTrays/Frame
		border_size 15 15 15 15
		border_topleft_uv     0.00 0.00 0.49 0.49
		border_top_uv         0.49 0.00 0.51 0.49
		border_topright_uv    0.51 0.00 1.00 0.49
		border_left_uv        0.00 0.49 0.49 0.51
		border_right_uv       0.51 0.49 1.00 0.51
		border_bottomleft_uv  0.00 0.51 0.49 1.00
		border_bottom_uv      0.49 0.51 0.51 1.00
		border_bottomright_uv 0.51 0.51 1.00 1.00
	}
}

template container BorderPanel(SdkTrays/ProgressBar)
{
	metrics_mode pixels
	material SdkTrays/MiniTray
	horz_align center
	height 55
	uv_coords 0.375 0.375 0.625 0.625
	
	border_material SdkTrays/MiniTray
	border_size 10 10 10 10
	border_topleft_uv     0.000 0.000 0.375 0.65
	border_top_uv         0.375 0.000 0.625 0.65
	border_topright_uv    0.625 0.000 1.0 0.65
	border_left_uv        0.000 0.65 0.5 0.9
	border_right_uv       0.625 0.65 1.000 0.9
	border_bottomleft_uv  0.000 0.9 0.375 1.000
	border_bottom_uv      0.375 0.9 0.625 1.000
	border_bottomright_uv 0.625 0.9 1.000 1.000

	element TextArea(ProgressCaption)
	{
        metrics_mode pixels
        left 12
        top 10
        font_name SdkTrays/Caption
        char_height 14
        space_width 7
        colour 1 1 1
	}
	
	container BorderPanel(ProgressCommentBox)
	{
		metrics_mode pixels
		material SdkTrays/MiniTextBoxTransparent
		horz_align right
		height 20
		top 5
		uv_coords 0.4 0.4 0.6 0.6
		
		border_material SdkTrays/MiniTextBoxTransparent
		border_size 0 0 0 0

		element TextArea(ProgressCommentText)
		{
			metrics_mode pixels
			left 11
			top 6
			font_name SdkTrays/Value
			char_height 14
			space_width 8
			colour 1 1 1
		}
	}
		
	container BorderPanel(ProgressMeter)
	{
		metrics_mode pixels
		material SdkTrays/MiniTextBoxTransparent
		left 5
		height 0
		vert_align bottom
		top -30
		uv_coords 0.31 0.0 0.69 1.0
		
		border_material SdkTrays/MiniTextBoxTransparent
		border_size 0 0 0 0
		border_left_uv  0.0 0.0 0.31 1.0
		border_right_uv 0.69 0.0 1.0 1.0
		
		element BorderPanel(ProgressFill)
		{
			metrics_mode pixels
			material SdkTrays/Progressbar
			width 1
			height 12
			top 6
			left 6
			uv_coords 0.1 0.0 0.8 1.0
			border_material SdkTrays/Progressbar
			border_size 0 0 0 0
		}
	}
}