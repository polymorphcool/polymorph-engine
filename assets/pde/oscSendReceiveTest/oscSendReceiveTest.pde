/**
 * oscP5sendreceive by andreas schlegel
 * example shows how to send and receive osc messages.
 * oscP5 website at http://www.sojamo.de/oscP5
 */
 
import oscP5.*;
import netP5.*;
  
OscP5 oscP5;
NetAddress ogre;

boolean pressed;

void setup() {
  
  size( 400, 400 );
  frameRate( 100 );
  oscP5 = new OscP5(this,23000);
  ogre = new NetAddress("127.0.0.1",25000);
  pressed = false;
    
}


void draw() {
  
  background(0);
  
  noStroke();
  if ( pressed ) fill( 255,0,0 );
  else fill( 255 );
  ellipse( mouseX, mouseY, 5,5 );
  
  OscMessage msg = new OscMessage("/mouse");
  msg.add( mouseX );
  msg.add( mouseY );
  msg.add( mouseX * 1.f / width );
  msg.add( mouseY * 1.f / height );  
  msg.add( pressed );
  oscP5.send(msg, ogre); 
  
}

void mousePressed() {
  pressed = true;
}

void mouseReleased() {
  pressed = false;
}

/* incoming osc message are forwarded to the oscEvent method. */
void oscEvent( OscMessage msg ) {
  
  /* print the address pattern and the typetag of the received OscMessage */
  print("### received an osc message.");
  print(" addrpattern: "+msg.addrPattern());
  print(" typetag: "+msg.typetag());
  println(" at "+frameCount + ", " + millis());
  
}