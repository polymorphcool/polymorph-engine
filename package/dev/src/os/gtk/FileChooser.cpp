/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2017 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
 */

/* 
 * File:   FileChooser.cpp
 * Author: frankiezafe
 * 
 * Created on February 6, 2017, 6:27 PM
 */

#include "FileChooser.h"

using namespace std;
using namespace Ogre;
using namespace polymorph;

void FileChooser::open(
        FileChooserResult& r,
        const std::string& path,
        const std::string& title
        ) {

    std::string dtitle = "Open file";
    if ( !title.empty() ) dtitle = title;
    
    // source: http://stackoverflow.com/questions/30751178/gtk3-file-chooser-in-a-non-gtk-application#30815396
    r.init( );
    GtkWidget* w = gtk_file_chooser_dialog_new(
            dtitle.c_str(), NULL, GTK_FILE_CHOOSER_ACTION_OPEN,
            "Cancel", GTK_RESPONSE_CANCEL,
            "Open", GTK_RESPONSE_OK,
            NULL );

    if ( !path.empty( ) ) gtk_file_chooser_set_current_folder( GTK_FILE_CHOOSER( w ), path.c_str( ) );

    if ( gtk_dialog_run( GTK_DIALOG( w ) ) == GTK_RESPONSE_OK ) {
        r.success = true;
        r.file = gtk_file_chooser_get_filename( GTK_FILE_CHOOSER( w ) );
        if ( !r.file.empty( ) ) {
            std::size_t found = r.file.find_last_of( "/\\" );
            r.folder = r.file.substr( 0, found );
        }
    }

    gtk_widget_destroy( GTK_WIDGET( w ) );

    while ( gtk_events_pending( ) ) gtk_main_iteration( );

    return;

}

void FileChooser::openDir(
        FileChooserResult& r,
        const std::string& path,
        const std::string& title
        ) {

    std::string dtitle = "Select folder";
    if ( !title.empty() ) dtitle = title;

    // source: http://stackoverflow.com/questions/30751178/gtk3-file-chooser-in-a-non-gtk-application#30815396
    r.init( );
    GtkWidget* w = gtk_file_chooser_dialog_new(
            dtitle.c_str(), NULL, GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER,
            "Cancel", GTK_RESPONSE_CANCEL,
            "Open", GTK_RESPONSE_OK,
            NULL );

    if ( !path.empty( ) ) gtk_file_chooser_set_current_folder( GTK_FILE_CHOOSER( w ), path.c_str( ) );

    if ( gtk_dialog_run( GTK_DIALOG( w ) ) == GTK_RESPONSE_OK ) {
        r.success = true;
        r.folder = gtk_file_chooser_get_current_folder( GTK_FILE_CHOOSER( w ) );
    }

    gtk_widget_destroy( GTK_WIDGET( w ) );

    while ( gtk_events_pending( ) ) gtk_main_iteration( );

    return;

}

void FileChooser::openOgreExport(
        FileChooserResult& r,
        const std::string& path,
        const std::string& title
        ) {

    std::string dtitle = "Open ogre export(s)";
    if ( !title.empty() ) dtitle = title;

    // source: http://stackoverflow.com/questions/30751178/gtk3-file-chooser-in-a-non-gtk-application#30815396
    r.init( );
    GtkWidget* w = gtk_file_chooser_dialog_new(
            dtitle.c_str(), NULL, GTK_FILE_CHOOSER_ACTION_OPEN,
            "Cancel", GTK_RESPONSE_CANCEL,
            "Open", GTK_RESPONSE_OK,
            NULL );

    gtk_file_chooser_set_select_multiple( GTK_FILE_CHOOSER( w ), true );

    GtkFileFilter* filter = gtk_file_filter_new( );
    gtk_file_filter_add_pattern( filter, "*.xml" );
    gtk_file_filter_add_pattern( filter, "*.skeleton" );
    gtk_file_filter_add_pattern( filter, "*.material" );
    gtk_file_filter_set_name( filter, "Ogre exports" );

    gtk_file_chooser_add_filter( GTK_FILE_CHOOSER( w ), filter );

    if ( !path.empty( ) ) gtk_file_chooser_set_current_folder( GTK_FILE_CHOOSER( w ), path.c_str( ) );

    if ( gtk_dialog_run( GTK_DIALOG( w ) ) == GTK_RESPONSE_OK ) {
        r.success = true;
        GSList* fl = gtk_file_chooser_get_filenames( GTK_FILE_CHOOSER( w ) );
        for (; fl != 0; fl = fl->next ) {
            const char* data = static_cast < const char* > ( fl->data );
            r.files.push_back( data );
        }
        if ( !r.files.empty( ) ) {
            std::size_t found = r.files[0].find_last_of( "/\\" );
            r.folder = r.files[0].substr( 0, found );
        }
    }

    gtk_widget_destroy( GTK_WIDGET( w ) );

    while ( gtk_events_pending( ) ) gtk_main_iteration( );

    return;

}