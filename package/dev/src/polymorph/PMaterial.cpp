/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2017 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
 */

/* 
 * File:   PMaterial.cpp
 * Author: frankiezafe
 * 
 * Created on February 16, 2017, 3:49 PM
 */

#include "PMaterial.h"

using namespace polymorph;

PMaterial::PMaterial( ) {
}

PMaterial::PMaterial( const PMaterial& src, std::string name ) {

    if ( src._mat.get( ) ) {

        // duplication of material
        _group = src._group;

        if ( name.empty( ) ) {

            _name = src._name;
            makeNameUnique( );

        } else {

            if ( PUtil::materialLoaded( _group, name ) ) {
                Ogre::LogManager::getSingleton( ).logMessage(
                        Ogre::LML_NORMAL,
                        "PMaterial::PMaterial, failed to create '" +
                        _group + ":" + name +
                        "', material already exists" );
                return;
            }

            _name = name;

        }

        _mat = src._mat.get( )->clone( _name, true, _group );

    }

}

PMaterial::~PMaterial( ) {
}

bool PMaterial::create(
        const std::string& grp,
        const std::string& matn
        ) {

    if ( _mat.get( ) ) {
        Ogre::LogManager::getSingleton( ).logMessage(
                Ogre::LML_NORMAL,
                "PMaterial::create, failed to create '" +
                grp + ":" + matn +
                "', current material: '" +
                _group + ":" + _name + "'" );
        return false;
    }

    if ( !PUtil::groupExists( grp ) ) {
        Ogre::LogManager::getSingleton( ).logMessage(
                Ogre::LML_CRITICAL,
                "PMaterial::create, resource group '" +
                grp + "' does not exists!" );
        return false;
    }

    if ( PUtil::materialExists( grp, matn ) ) {
        Ogre::LogManager::getSingleton( ).logMessage(
                Ogre::LML_NORMAL,
                "PMaterial::create, material '" +
                grp + ":" + matn +
                "' already exists." );
        return false;
    }

    if ( PUtil::materialLoaded( grp, matn ) ) {
        Ogre::LogManager::getSingleton( ).logMessage(
                Ogre::LML_NORMAL,
                "PMaterial::create, material '" +
                grp + ":" + matn +
                "' already loaded." );
        return false;
    }

    if ( _mat.get( ) ) {
        Ogre::LogManager::getSingleton( ).logMessage(
                Ogre::LML_TRIVIAL,
                "PMaterial::create, material '" + _group + ":" + _name +
                "' already loaded, will be replaced by " +
                grp + ":" + matn );
    }

    Ogre::MaterialManager& matmgr = Ogre::MaterialManager::getSingleton( );
    _group = grp;
    _name = matn;
    _mat = matmgr.create( matn, grp );

    Ogre::LogManager::getSingleton( ).logMessage(
            Ogre::LML_TRIVIAL,
            "PMaterial::create, material '" + _group + ":" + _name +
            "' succesfully created." );

    return true;

}

bool PMaterial::load(
        const std::string& grp,
        const std::string& matn ) {

    if ( _mat.get( ) ) {
        Ogre::LogManager::getSingleton( ).logMessage(
                Ogre::LML_NORMAL,
                "PMaterial::load, failed to load '" +
                grp + ":" + matn +
                "', current material: '" +
                _group + ":" + _name + "'" );
        return false;
    }

    Ogre::MaterialPtr tmp = PUtil::getMaterial( grp, matn );
    if ( tmp.get( ) ) {
        _group = grp;
        _name = matn;
        _mat = tmp;
        return true;
    }

    return false;

}

bool PMaterial::load( Ogre::MaterialPtr ptr ) {

    if ( !ptr.get( ) ) {
        Ogre::LogManager::getSingleton( ).logMessage(
                Ogre::LML_NORMAL,
                "PMaterial::load, material pointer is NULL." );
        return false;
    }

    if ( _mat.get( ) ) {
        Ogre::LogManager::getSingleton( ).logMessage(
                Ogre::LML_NORMAL,
                "PMaterial::load, failed to load '" +
                ptr.get( )->getGroup( ) + ":" + ptr.get( )->getName( ) +
                "', current material: '" +
                _group + ":" + _name + "'" );
        return false;
    }

    if ( _mat.get( ) == ptr.get( ) ) {
        Ogre::LogManager::getSingleton( ).logMessage(
                Ogre::LML_TRIVIAL,
                "PMaterial::load, same material: '" +
                ptr.get( )->getGroup( ) + ":" + ptr.get( )->getName( ) +
                "', current material: '" +
                _group + ":" + _name + "'" );
        return true;
    }


    _mat = ptr;
    _group = ptr.get( )->getGroup( );
    _name = ptr.get( )->getName( );

    return true;

}

bool PMaterial::clone( PMaterial* src ) {

    if ( !src->getMaterial( ).get( ) ) {
        Ogre::LogManager::getSingleton( ).logMessage(
                Ogre::LML_NORMAL,
                "PMaterial::clone, material pointer is NULL." );
        return false;
    }

    if ( _mat.get( ) ) {
        Ogre::LogManager::getSingleton( ).logMessage(
                Ogre::LML_NORMAL,
                "PMaterial::clone, failed to load '" +
                src->getGroup( ) + ":" + src->getName( ) +
                "', current material: '" +
                _group + ":" + _name + "'" );
        return false;
    }

    _group = src->getGroup( );
    _name = src->getName( );
    makeNameUnique( );

    _mat = src->getMaterial( ).get( )->clone( _name );

    // std::cout << "material cloned: " << _group << ":" << _name << std::endl;

    return true;

}

/////////////////////////
//// MODIFICATION ///////
/////////////////////////

void PMaterial::diffuse(
        const Ogre::ColourValue& colour,
        ushort technique,
        ushort pass ) {

    if ( !_mat.get( ) || !exists( technique, pass ) ) return;
    Ogre::Pass* p = getPass( technique, pass );
    p->setDiffuse( colour );

}

void PMaterial::specular(
        const Ogre::ColourValue& colour,
        ushort technique,
        ushort pass ) {

    if ( !_mat.get( ) || !exists( technique, pass ) ) return;
    Ogre::Pass* p = getPass( technique, pass );
    p->setSpecular( colour );

}

void PMaterial::shininess(
        const Ogre::Real& v,
        ushort technique,
        ushort pass ) {

    if ( !_mat.get( ) || !exists( technique, pass ) ) return;
    Ogre::Pass* p = getPass( technique, pass );
    if ( !p->getLightingEnabled( ) ) {
        p->setLightingEnabled( true );
    }
    p->setShininess( v );

}

void PMaterial::ambient(
        const Ogre::ColourValue& colour,
        ushort technique,
        ushort pass ) {

    if ( !_mat.get( ) || !exists( technique, pass ) ) return;
    Ogre::Pass* p = getPass( technique, pass );
    p->setAmbient( colour );

}

void PMaterial::emissive(
        const Ogre::ColourValue& colour,
        ushort technique,
        ushort pass ) {

    if ( !_mat.get( ) || !exists( technique, pass ) ) return;
    Ogre::Pass* p = getPass( technique, pass );
    p->setEmissive( colour );

}

void PMaterial::fogColor(
        const Ogre::ColourValue& colour,
        ushort technique,
        ushort pass ) {

    if ( !_mat.get( ) || !exists( technique, pass ) ) return;
    Ogre::Pass* p = getPass( technique, pass );
    p->setFog(
            true,
            p->getFogMode( ),
            colour,
            p->getFogDensity( ),
            p->getFogStart( ),
            p->getFogEnd( )
            );

}

void PMaterial::fogMode(
        const Ogre::FogMode& mode,
        ushort technique,
        ushort pass ) {

    if ( !_mat.get( ) || !exists( technique, pass ) ) return;
    Ogre::Pass* p = getPass( technique, pass );
    p->setFog(
            true,
            mode,
            p->getFogColour( ),
            p->getFogDensity( ),
            p->getFogStart( ),
            p->getFogEnd( )
            );

}

void PMaterial::fogDistance(
        const Ogre::Real& start,
        const Ogre::Real& end,
        ushort technique,
        ushort pass ) {

    if ( !_mat.get( ) || !exists( technique, pass ) ) return;
    Ogre::Pass* p = getPass( technique, pass );
    p->setFog(
            true,
            p->getFogMode( ),
            p->getFogColour( ),
            p->getFogDensity( ),
            start, end
            );

}

void PMaterial::fogDensity(
        const Ogre::Real& density,
        ushort technique,
        ushort pass ) {

    if ( !_mat.get( ) || !exists( technique, pass ) ) return;
    Ogre::Pass* p = getPass( technique, pass );
    p->setFog(
            true,
            p->getFogMode( ),
            p->getFogColour( ),
            density,
            p->getFogStart( ),
            p->getFogEnd( )
            );

}

void PMaterial::fog(
        bool overwrite,
        const Ogre::FogMode& mode,
        const Ogre::ColourValue& colour,
        const Ogre::Real& density,
        const Ogre::Real& start,
        const Ogre::Real& end,
        ushort technique,
        ushort pass ) {

    if ( !_mat.get( ) || !exists( technique, pass ) ) return;
    Ogre::Pass* p = getPass( technique, pass );
    p->setFog( overwrite, mode, colour, density, start, end );

}

void PMaterial::shading(
        const Ogre::ShadeOptions& sm,
        ushort technique,
        ushort pass ) {

    if ( !_mat.get( ) || !exists( technique, pass ) ) return;
    Ogre::Pass* p = getPass( technique, pass );
    p->setShadingMode( sm );

}

void PMaterial::polygon(
        const Ogre::PolygonMode& pm,
        ushort technique,
        ushort pass ) {

    if ( !_mat.get( ) || !exists( technique, pass ) ) return;
    Ogre::Pass* p = getPass( technique, pass );
    p->setPolygonMode( pm );

}

int PMaterial::addPass(
        ushort technique,
        std::string name ) {

    if ( !_mat.get( ) || !exists( technique ) ) return -1;
    Ogre::Pass* p = _mat->getTechnique( technique )->createPass( );
    if ( !name.empty( ) ) {
        p->setName( name );
    }
    return getPassCount( technique ) - 1;

}

int PMaterial::addTexture(
        std::string groupname,
        std::string imagename,
        ushort technique,
        ushort pass ) {

    if ( !_mat.get( ) || !exists( technique, pass ) ) return -1;

    Ogre::Pass* p = getPass( technique, pass );
    Ogre::TextureUnitState* tus = p->createTextureUnitState( );

    Ogre::TexturePtr ptr = PUtil::getTexture( groupname, imagename );
    if ( ptr.get( ) ) {
        tus->setTexture( ptr );
    }

    return getTextureUnitCount( technique, pass ) - 1;
}

int PMaterial::addTexture(
        const Ogre::TexturePtr& ptr,
        ushort technique,
        ushort pass ) {

    if ( !_mat.get( ) || !exists( technique, pass ) ) return -1;

    Ogre::Pass* p = getPass( technique, pass );
    Ogre::TextureUnitState* tus = p->createTextureUnitState( );

    if ( ptr.get( ) ) {
        tus->setTexture( ptr );
    }

    return getTextureUnitCount( technique, pass ) - 1;
}

bool PMaterial::setTexture(
        std::string groupname,
        std::string imagename,
        ushort technique,
        ushort pass,
        ushort textureunit ) {

    if ( !_mat.get( ) || !exists( technique, pass, textureunit ) ) return false;

    Ogre::TexturePtr ptr = PUtil::getTexture( groupname, imagename );
    if ( !ptr.get( ) ) {
        return false;
    }

    Ogre::TextureUnitState* tus = getTextureUnit( technique, pass, textureunit );
    tus->setTexture( ptr );

    return true;

}

bool PMaterial::setTexture(
        const Ogre::TexturePtr& ptr,
        ushort technique,
        ushort pass,
        ushort textureunit ) {

    if (
            !_mat.get( ) ||
            !ptr.get( ) ||
            !exists( technique, pass, textureunit )
            ) return false;

    Ogre::TextureUnitState* tus = getTextureUnit( technique, pass, textureunit );
    tus->setTexture( ptr );

    return true;

}

uint PMaterial::getTechniqueCount( ) {

    if ( !_mat.get( ) ) return 0;
    return _mat.get( )->getNumTechniques( );

}

uint PMaterial::getPassCount(
        ushort technique ) {

    if ( !_mat.get( ) || !exists( technique ) ) return 0;
    return _mat.get( )->getTechnique( technique )->getNumPasses( );

}

uint PMaterial::getTextureUnitCount(
        ushort technique,
        ushort pass ) {

    if ( !_mat.get( ) || !exists( technique, pass ) ) return 0;
    return _mat.get( )->getTechnique( technique )->
            getPass( pass )->getNumTextureUnitStates( );

}

bool PMaterial::exists( ushort technique ) {
    if ( !_mat.get( ) )
        return false;
    if ( technique >= _mat.get( )->getNumTechniques( ) )
        return false;
    return true;
}

bool PMaterial::exists( ushort technique, ushort pass ) {
    if ( !_mat.get( ) )
        return false;
    if ( technique >= _mat.get( )->getNumTechniques( ) )
        return false;
    if ( pass >= _mat.get( )->getTechnique( technique )->getNumPasses( ) )
        return false;
    return true;
}

bool PMaterial::exists( ushort technique, ushort pass, ushort texunit ) {

    if ( texunit < getTextureUnitCount( technique, pass ) ) {
        return true;
    }
    return false;

}

bool PMaterial::equals( PMaterial* other ) {

    if ( !other ) return false;
    if ( !_mat.get( ) ) return false;
    if ( !other->getMaterial( ).get( ) ) return false;

    if ( _mat.get( ) == other->getMaterial( ).get( ) ) {
        return true;
    }

    return false;

}

bool PMaterial::equals( PMaterial& other ) {

    if ( !_mat.get( ) ) return false;
    if ( !other.getMaterial( ).get( ) ) return false;

    if ( _mat.get( ) == other.getMaterial( ).get( ) ) {
        return true;
    }

    return false;

}

void PMaterial::print( bool full ) {

    std::cout << "PMaterial: " << std::endl <<
            "\tgroup: " << _group << std::endl <<
            "\tname: " << _name << std::endl;

    if ( !_mat.get( ) ) {

        std::cout << "\tmaterial: undefined" << std::endl;

    } else {

        ushort tnum = _mat->getNumTechniques( );

        std::cout <<
                
                "\treceive shadows: " << _mat->getReceiveShadows( ) << std::endl <<
                "\ttransparency cast shadows: " << _mat->getTransparencyCastsShadows( ) << std::endl <<
                "\tsize: " << _mat->getSize( ) << std::endl <<                
                "\ttechniques: " << tnum << std::endl;

        if ( !full ) return;

        for ( ushort i = 0; i < tnum; ++i ) {

            Ogre::Technique* t = _mat->getTechnique( i );
            ushort pnum = t->getNumPasses( );
            std::cout << "\t\ttechnique[" << i << "]" << std::endl <<
                    "\t\t\tname: " << t->getName( ) << std::endl <<
                    "\t\t\tscheme: " << t->getSchemeName( ) << std::endl <<
                    "\t\t\tlod index: " << t->getLodIndex( ) << std::endl <<
                    "\tpasses: " << pnum << std::endl;

            for ( ushort j = 0; j < pnum; ++j ) {

                Ogre::Pass* p = t->getPass( j );
                std::cout << "\t\tpass[" << j << "]" << std::endl <<
                        "\t\t\tname: " << p->getName( ) << std::endl <<
                        "\t\t** programs:" << std::endl <<
                        "\t\t\tvertex program: " << p->getVertexProgramName( ) << std::endl <<
                        "\t\t\tfragment program: " << p->getFragmentProgramName( ) << std::endl <<
                        "\t\t\tcompute program: " << p->getComputeProgramName( ) << std::endl <<
                        "\t\t\tshadow caster fragment program: " << p->getShadowCasterFragmentProgramName( ) << std::endl <<
                        "\t\t\tshadow caster vertex program: " << p->getShadowCasterVertexProgramName( ) << std::endl <<
                        "\t\t\ttesselation domain program: " << p->getTessellationDomainProgramName( ) << std::endl <<
                        "\t\t\ttesselation hull program: " << p->getTessellationHullProgramName( ) << std::endl;

                std::cout << "\t\t** colors:" << std::endl;
                std::cout << "\t\t\tambient: " << PUtil::str( p->getAmbient( ) ) << std::endl;
                std::cout << "\t\t\tdiffuse: " << PUtil::str( p->getDiffuse( ) ) << std::endl;
                std::cout << "\t\t\temissive: " << PUtil::str( p->getEmissive( ) ) << std::endl;
                std::cout << "\t\t\tfog colour: " << PUtil::str( p->getFogColour( ) ) << std::endl;
                std::cout << "\t\t\tself illumination: " << PUtil::str( p->getSelfIllumination( ) ) << std::endl;
                std::cout << "\t\t\tspecular: " << PUtil::str( p->getSpecular( ) ) << std::endl;

                std::cout << "\t\t** values:" << std::endl;
                std::cout << "\t\t\talpha reject value: " << int( p->getAlphaRejectValue( ) ) << std::endl;
                std::cout << "\t\t\tdepth bias constant: " << p->getDepthBiasConstant( ) << std::endl;
                std::cout << "\t\t\tdepth bias slope scale: " << p->getDepthBiasSlopeScale( ) << std::endl;
                std::cout << "\t\t\tfog density: " << p->getFogDensity( ) << std::endl;
                std::cout << "\t\t\tfog end: " << p->getFogEnd( ) << std::endl;
                std::cout << "\t\t\tfog start: " << p->getFogStart( ) << std::endl;
                std::cout << "\t\t\thash: " << p->getHash( ) << std::endl;
                std::cout << "\t\t\tindex: " << int( p->getIndex( ) ) << std::endl;
                std::cout << "\t\t\titeration depth bias: " << p->getIterationDepthBias( ) << std::endl;
                std::cout << "\t\t\tlight count per iteration: " << int( p->getLightCountPerIteration( ) ) << std::endl;
                std::cout << "\t\t\tlight mask: " << int( p->getLightMask( ) ) << std::endl;
                std::cout << "\t\t\tmax simultaneous lights: " << int( p->getMaxSimultaneousLights( ) ) << std::endl;
                std::cout << "\t\t\tnum shadow content textures: " << int( p->getNumShadowContentTextures( ) ) << std::endl;
                std::cout << "\t\t\tnum texture unit states: " << int( p->getNumTextureUnitStates( ) ) << std::endl;
                std::cout << "\t\t\tpass iteration count: " << int( p->getPassIterationCount( ) ) << std::endl;
                std::cout << "\t\t\tpoint attenuation constant: " << p->getPointAttenuationConstant( ) << std::endl;
                std::cout << "\t\t\tpoint attenuation linear: " << p->getPointAttenuationLinear( ) << std::endl;
                std::cout << "\t\t\tpoint attenuation quadratic: " << p->getPointAttenuationQuadratic( ) << std::endl;
                std::cout << "\t\t\tpoint max size: " << p->getPointMaxSize( ) << std::endl;
                std::cout << "\t\t\tpoint min size: " << p->getPointMinSize( ) << std::endl;
                std::cout << "\t\t\tpoint size: " << p->getPointSize( ) << std::endl;
                std::cout << "\t\t\tshininess: " << p->getShininess( ) << std::endl;
                std::cout << "\t\t\tstart light: " << int( p->getStartLight( ) ) << std::endl;

                std::cout << "\t\t** booleans:" << std::endl;
                std::cout << "\t\t\talpha coverage enabled: " << p->isAlphaToCoverageEnabled( ) << std::endl;
                std::cout << "\t\t\tambient light only: " << p->isAmbientOnly( ) << std::endl;
                std::cout << "\t\t\tcolour write enabled: " << p->getColourWriteEnabled( ) << std::endl;
                std::cout << "\t\t\tdepth check enabled: " << p->getDepthCheckEnabled( ) << std::endl;
                std::cout << "\t\t\tdepth write enabled: " << p->getDepthWriteEnabled( ) << std::endl;
                std::cout << "\t\t\tfog override: " << p->getFogOverride( ) << std::endl;
                std::cout << "\t\t\titerate per light: " << p->getIteratePerLight( ) << std::endl;
                std::cout << "\t\t\tlight clip plane enabled: " << p->getLightClipPlanesEnabled( ) << std::endl;
                std::cout << "\t\t\tlight scissoring enabled: " << p->getLightScissoringEnabled( ) << std::endl;
                std::cout << "\t\t\tlighting enabled: " << p->getLightingEnabled( ) << std::endl;
                std::cout << "\t\t\tloaded: " << p->isLoaded( ) << std::endl;
                std::cout << "\t\t\tnormalised normals: " << p->getNormaliseNormals( ) << std::endl;
                std::cout << "\t\t\tpoint attenuation enabled: " << p->isPointAttenuationEnabled( ) << std::endl;
                std::cout << "\t\t\tpoint sprite enabled: " << p->getPointSpritesEnabled( ) << std::endl;
                std::cout << "\t\t\tpolygon mode overrideable: " << p->getPolygonModeOverrideable( ) << std::endl;
                std::cout << "\t\t\tprogrammable: " << p->isProgrammable( ) << std::endl;
                std::cout << "\t\t\trun only for one light type: " << p->getRunOnlyForOneLightType( ) << std::endl;
                std::cout << "\t\t\ttransparent: " << p->isTransparent( ) << std::endl;
                std::cout << "\t\t\ttransparent sorting enabled: " << p->getTransparentSortingEnabled( ) << std::endl;
                std::cout << "\t\t\ttransparent sorting forced: " << p->getTransparentSortingForced( ) << std::endl;

                std::cout << "\t\t** enums:" << std::endl;
                switch ( p->getAlphaRejectFunction( ) ) {
                    case Ogre::CMPF_ALWAYS_FAIL:
                        std::cout << "\t\t\talpha reject function: CMPF_ALWAYS_FAIL" << std::endl;
                        break;
                    case Ogre::CMPF_ALWAYS_PASS:
                        std::cout << "\t\t\talpha reject function: CMPF_ALWAYS_PASS" << std::endl;
                        break;
                    case Ogre::CMPF_LESS:
                        std::cout << "\t\t\talpha reject function: CMPF_LESS" << std::endl;
                        break;
                    case Ogre::CMPF_LESS_EQUAL:
                        std::cout << "\t\t\talpha reject function: CMPF_LESS_EQUAL" << std::endl;
                        break;
                    case Ogre::CMPF_EQUAL:
                        std::cout << "\t\t\talpha reject function: CMPF_EQUAL" << std::endl;
                        break;
                    case Ogre::CMPF_NOT_EQUAL:
                        std::cout << "\t\t\talpha reject function: CMPF_NOT_EQUAL" << std::endl;
                        break;
                    case Ogre::CMPF_GREATER_EQUAL:
                        std::cout << "\t\t\talpha reject function: CMPF_GREATER_EQUAL" << std::endl;
                        break;
                    case Ogre::CMPF_GREATER:
                        std::cout << "\t\t\talpha reject function: CMPF_GREATER" << std::endl;
                        break;
                    default:
                        std::cout << "\t\t\talpha reject function: UNDEFINED" << std::endl;
                        break;
                }
                switch ( p->getCullingMode( ) ) {
                    case Ogre::CULL_NONE:
                        std::cout << "\t\t\tculling mode: CULL_NONE" << std::endl;
                        break;
                    case Ogre::CULL_CLOCKWISE:
                        std::cout << "\t\t\tculling mode: CULL_CLOCKWISE" << std::endl;
                        break;
                    case Ogre::CULL_ANTICLOCKWISE:
                        std::cout << "\t\t\tculling mode: CULL_ANTICLOCKWISE" << std::endl;
                        break;
                    default:
                        std::cout << "\t\t\tculling mode: UNDEFINED" << std::endl;
                        break;
                }

                switch ( p->getDepthFunction( ) ) {
                    case Ogre::CMPF_ALWAYS_FAIL:
                        std::cout << "\t\t\tdepth function: CMPF_ALWAYS_FAIL" << std::endl;
                        break;
                    case Ogre::CMPF_ALWAYS_PASS:
                        std::cout << "\t\t\tdepth function: CMPF_ALWAYS_PASS" << std::endl;
                        break;
                    case Ogre::CMPF_LESS:
                        std::cout << "\t\t\tdepth function: CMPF_LESS" << std::endl;
                        break;
                    case Ogre::CMPF_LESS_EQUAL:
                        std::cout << "\t\t\tdepth function: CMPF_LESS_EQUAL" << std::endl;
                        break;
                    case Ogre::CMPF_EQUAL:
                        std::cout << "\t\t\tdepth function: CMPF_EQUAL" << std::endl;
                        break;
                    case Ogre::CMPF_NOT_EQUAL:
                        std::cout << "\t\t\tdepth function: CMPF_NOT_EQUAL" << std::endl;
                        break;
                    case Ogre::CMPF_GREATER_EQUAL:
                        std::cout << "\t\t\tdepth function: CMPF_GREATER_EQUAL" << std::endl;
                        break;
                    case Ogre::CMPF_GREATER:
                        std::cout << "\t\t\tdepth function: CMPF_GREATER" << std::endl;
                        break;
                    default:
                        std::cout << "\t\t\tdepth function: UNDEFINED" << std::endl;
                        break;
                }

                switch ( p->getDestBlendFactor( ) ) {
                    case Ogre::SBF_ONE:
                        std::cout << "\t\t\tdest blend factor: SBF_ONE" << std::endl;
                        break;
                    case Ogre::SBF_ZERO:
                        std::cout << "\t\t\tdest blend factor: SBF_ZERO" << std::endl;
                        break;
                    case Ogre::SBF_DEST_COLOUR:
                        std::cout << "\t\t\tdest blend factor: SBF_DEST_COLOUR" << std::endl;
                        break;
                    case Ogre::SBF_SOURCE_COLOUR:
                        std::cout << "\t\t\tdest blend factor: SBF_SOURCE_COLOUR" << std::endl;
                        break;
                    case Ogre::SBF_ONE_MINUS_DEST_COLOUR:
                        std::cout << "\t\t\tdest blend factor: SBF_ONE_MINUS_DEST_COLOUR" << std::endl;
                        break;
                    case Ogre::SBF_ONE_MINUS_SOURCE_COLOUR:
                        std::cout << "\t\t\tdest blend factor: SBF_ONE_MINUS_SOURCE_COLOUR" << std::endl;
                        break;
                    case Ogre::SBF_DEST_ALPHA:
                        std::cout << "\t\t\tdest blend factor: SBF_DEST_ALPHA" << std::endl;
                        break;
                    case Ogre::SBF_SOURCE_ALPHA:
                        std::cout << "\t\t\tdest blend factor: SBF_SOURCE_ALPHA" << std::endl;
                        break;
                    case Ogre::SBF_ONE_MINUS_DEST_ALPHA:
                        std::cout << "\t\t\tdest blend factor: SBF_ONE_MINUS_DEST_ALPHA" << std::endl;
                        break;
                    case Ogre::SBF_ONE_MINUS_SOURCE_ALPHA:
                        std::cout << "\t\t\tdest blend factor: SBF_ONE_MINUS_SOURCE_ALPHA" << std::endl;
                        break;
                    default:
                        std::cout << "\t\t\tdest blend factor: UNDEFINED" << std::endl;
                        break;
                }

                switch ( p->getDestBlendFactorAlpha( ) ) {
                    case Ogre::SBF_ONE:
                        std::cout << "\t\t\tdest blend factor alpha: SBF_ONE" << std::endl;
                        break;
                    case Ogre::SBF_ZERO:
                        std::cout << "\t\t\tdest blend factor alpha: SBF_ZERO" << std::endl;
                        break;
                    case Ogre::SBF_DEST_COLOUR:
                        std::cout << "\t\t\tdest blend factor alpha: SBF_DEST_COLOUR" << std::endl;
                        break;
                    case Ogre::SBF_SOURCE_COLOUR:
                        std::cout << "\t\t\tdest blend factor alpha: SBF_SOURCE_COLOUR" << std::endl;
                        break;
                    case Ogre::SBF_ONE_MINUS_DEST_COLOUR:
                        std::cout << "\t\t\tdest blend factor alpha: SBF_ONE_MINUS_DEST_COLOUR" << std::endl;
                        break;
                    case Ogre::SBF_ONE_MINUS_SOURCE_COLOUR:
                        std::cout << "\t\t\tdest blend factor alpha: SBF_ONE_MINUS_SOURCE_COLOUR" << std::endl;
                        break;
                    case Ogre::SBF_DEST_ALPHA:
                        std::cout << "\t\t\tdest blend factor alpha: SBF_DEST_ALPHA" << std::endl;
                        break;
                    case Ogre::SBF_SOURCE_ALPHA:
                        std::cout << "\t\t\tdest blend factor alpha: SBF_SOURCE_ALPHA" << std::endl;
                        break;
                    case Ogre::SBF_ONE_MINUS_DEST_ALPHA:
                        std::cout << "\t\t\tdest blend factor alpha: SBF_ONE_MINUS_DEST_ALPHA" << std::endl;
                        break;
                    case Ogre::SBF_ONE_MINUS_SOURCE_ALPHA:
                        std::cout << "\t\t\tdest blend factor alpha: SBF_ONE_MINUS_SOURCE_ALPHA" << std::endl;
                        break;
                    default:
                        std::cout << "\t\t\tdest blend factor alpha: UNDEFINED" << std::endl;
                        break;
                }

                switch ( p->getFogMode( ) ) {
                    case Ogre::FOG_NONE:
                        std::cout << "\t\t\tfog mode: FOG_NONE" << std::endl;
                        break;
                    case Ogre::FOG_EXP:
                        std::cout << "\t\t\tfog mode: FOG_EXP" << std::endl;
                        break;
                    case Ogre::FOG_EXP2:
                        std::cout << "\t\t\tfog mode: FOG_EXP2" << std::endl;
                        break;
                    case Ogre::FOG_LINEAR:
                        std::cout << "\t\t\tfog mode: FOG_LINEAR" << std::endl;
                        break;
                    default:
                        std::cout << "\t\t\tfog mode: UNDEFINED" << std::endl;
                        break;
                }

                switch ( p->getIlluminationStage( ) ) {
                    case Ogre::IS_AMBIENT:
                        std::cout << "\t\t\tillumination stage: IS_AMBIENT" << std::endl;
                        break;
                    case Ogre::IS_PER_LIGHT:
                        std::cout << "\t\t\tillumination stage: IS_PER_LIGHT" << std::endl;
                        break;
                    case Ogre::IS_DECAL:
                        std::cout << "\t\t\tillumination stage: IS_DECAL" << std::endl;
                        break;
                    case Ogre::IS_UNKNOWN:
                        std::cout << "\t\t\tillumination stage: IS_UNKNOWN" << std::endl;
                        break;
                    default:
                        std::cout << "\t\t\tillumination stage: UNDEFINED" << std::endl;
                        break;
                }

                switch ( p->getManualCullingMode( ) ) {
                    case Ogre::MANUAL_CULL_NONE:
                        std::cout << "\t\t\tmanual culling mode: MANUAL_CULL_NONE" << std::endl;
                        break;
                    case Ogre::MANUAL_CULL_BACK:
                        std::cout << "\t\t\tmanual culling mode: MANUAL_CULL_BACK" << std::endl;
                        break;
                    case Ogre::MANUAL_CULL_FRONT:
                        std::cout << "\t\t\tmanual culling mode: MANUAL_CULL_FRONT" << std::endl;
                        break;
                    default:
                        std::cout << "\t\t\tmanual culling mode: UNDEFINED" << std::endl;
                        break;
                }

                switch ( p->getOnlyLightType( ) ) {
                    case Ogre::Light::LT_POINT:
                        std::cout << "\t\t\tonly light type: LT_POINT" << std::endl;
                        break;
                    case Ogre::Light::LT_DIRECTIONAL:
                        std::cout << "\t\t\tonly light type: LT_DIRECTIONAL" << std::endl;
                        break;
                    case Ogre::Light::LT_SPOTLIGHT:
                        std::cout << "\t\t\tonly light type: LT_SPOTLIGHT" << std::endl;
                        break;
                    default:
                        std::cout << "\t\t\tonly light type: UNDEFINED" << std::endl;
                        break;
                }

                switch ( p->getPolygonMode( ) ) {
                    case Ogre::PM_POINTS:
                        std::cout << "\t\t\tpolygon mode: PM_POINTS" << std::endl;
                        break;
                    case Ogre::PM_SOLID:
                        std::cout << "\t\t\tpolygon mode: PM_SOLID" << std::endl;
                        break;
                    case Ogre::PM_WIREFRAME:
                        std::cout << "\t\t\tpolygon mode: PM_WIREFRAME" << std::endl;
                        break;
                    default:
                        std::cout << "\t\t\tpolygon mode: UNDEFINED" << std::endl;
                        break;
                }

                switch ( p->getSceneBlendingOperation( ) ) {
                    case Ogre::SBO_ADD:
                        std::cout << "\t\t\tscene blending operation: SBO_ADD" << std::endl;
                        break;
                    case Ogre::SBO_MAX:
                        std::cout << "\t\t\tscene blending operation: SBO_MAX" << std::endl;
                        break;
                    case Ogre::SBO_MIN:
                        std::cout << "\t\t\tscene blending operation: SBO_MIN" << std::endl;
                        break;
                    case Ogre::SBO_REVERSE_SUBTRACT:
                        std::cout << "\t\t\tscene blending operation: SBO_REVERSE_SUBTRACT" << std::endl;
                        break;
                    case Ogre::SBO_SUBTRACT:
                        std::cout << "\t\t\tscene blending operation: SBO_SUBTRACT" << std::endl;
                        break;
                    default:
                        std::cout << "\t\t\tscene blending operation: UNDEFINED" << std::endl;
                        break;
                }

                switch ( p->getSceneBlendingOperationAlpha( ) ) {
                    case Ogre::SBO_ADD:
                        std::cout << "\t\t\tscene blending operation alpha: SBO_ADD" << std::endl;
                        break;
                    case Ogre::SBO_MAX:
                        std::cout << "\t\t\tscene blending operation alpha: SBO_MAX" << std::endl;
                        break;
                    case Ogre::SBO_MIN:
                        std::cout << "\t\t\tscene blending operation alpha: SBO_MIN" << std::endl;
                        break;
                    case Ogre::SBO_REVERSE_SUBTRACT:
                        std::cout << "\t\t\tscene blending operation alpha: SBO_REVERSE_SUBTRACT" << std::endl;
                        break;
                    case Ogre::SBO_SUBTRACT:
                        std::cout << "\t\t\tscene blending operation alpha: SBO_SUBTRACT" << std::endl;
                        break;
                    default:
                        std::cout << "\t\t\tscene blending operation alpha: UNDEFINED" << std::endl;
                        break;
                }

                switch ( p->getShadingMode( ) ) {
                    case Ogre::SO_FLAT:
                        std::cout << "\t\t\tshading mode: SO_FLAT" << std::endl;
                        break;
                    case Ogre::SO_GOURAUD:
                        std::cout << "\t\t\tshading mode: SO_GOURAUD" << std::endl;
                        break;
                    case Ogre::SO_PHONG:
                        std::cout << "\t\t\tshading mode: SO_PHONG" << std::endl;
                        break;
                    default:
                        std::cout << "\t\t\tshading mode: UNDEFINED" << std::endl;
                        break;
                }

                switch ( p->getSourceBlendFactor( ) ) {
                    case Ogre::SBF_ONE:
                        std::cout << "\t\t\tsource blend factor: SBF_ONE" << std::endl;
                        break;
                    case Ogre::SBF_ZERO:
                        std::cout << "\t\t\tsource blend factor: SBF_ZERO" << std::endl;
                        break;
                    case Ogre::SBF_DEST_COLOUR:
                        std::cout << "\t\t\tsource blend factor: SBF_DEST_COLOUR" << std::endl;
                        break;
                    case Ogre::SBF_SOURCE_COLOUR:
                        std::cout << "\t\t\tsource blend factor: SBF_SOURCE_COLOUR" << std::endl;
                        break;
                    case Ogre::SBF_ONE_MINUS_DEST_COLOUR:
                        std::cout << "\t\t\tsource blend factor: SBF_ONE_MINUS_DEST_COLOUR" << std::endl;
                        break;
                    case Ogre::SBF_ONE_MINUS_SOURCE_COLOUR:
                        std::cout << "\t\t\tsource blend factor: SBF_ONE_MINUS_SOURCE_COLOUR" << std::endl;
                        break;
                    case Ogre::SBF_DEST_ALPHA:
                        std::cout << "\t\t\tsource blend factor: SBF_DEST_ALPHA" << std::endl;
                        break;
                    case Ogre::SBF_SOURCE_ALPHA:
                        std::cout << "\t\t\tsource blend factor: SBF_SOURCE_ALPHA" << std::endl;
                        break;
                    case Ogre::SBF_ONE_MINUS_DEST_ALPHA:
                        std::cout << "\t\t\tsource blend factor: SBF_ONE_MINUS_DEST_ALPHA" << std::endl;
                        break;
                    case Ogre::SBF_ONE_MINUS_SOURCE_ALPHA:
                        std::cout << "\t\t\tsource blend factor: SBF_ONE_MINUS_SOURCE_ALPHA" << std::endl;
                        break;
                    default:
                        std::cout << "\t\t\tsource blend factor: UNDEFINED" << std::endl;
                        break;
                }

                switch ( p->getSourceBlendFactorAlpha( ) ) {
                    case Ogre::SBF_ONE:
                        std::cout << "\t\t\tsource blend factor alpha: SBF_ONE" << std::endl;
                        break;
                    case Ogre::SBF_ZERO:
                        std::cout << "\t\t\tsource blend factor alpha: SBF_ZERO" << std::endl;
                        break;
                    case Ogre::SBF_DEST_COLOUR:
                        std::cout << "\t\t\tsource blend factor alpha: SBF_DEST_COLOUR" << std::endl;
                        break;
                    case Ogre::SBF_SOURCE_COLOUR:
                        std::cout << "\t\t\tsource blend factor alpha: SBF_SOURCE_COLOUR" << std::endl;
                        break;
                    case Ogre::SBF_ONE_MINUS_DEST_COLOUR:
                        std::cout << "\t\t\tsource blend factor alpha: SBF_ONE_MINUS_DEST_COLOUR" << std::endl;
                        break;
                    case Ogre::SBF_ONE_MINUS_SOURCE_COLOUR:
                        std::cout << "\t\t\tsource blend factor alpha: SBF_ONE_MINUS_SOURCE_COLOUR" << std::endl;
                        break;
                    case Ogre::SBF_DEST_ALPHA:
                        std::cout << "\t\t\tsource blend factor alpha: SBF_DEST_ALPHA" << std::endl;
                        break;
                    case Ogre::SBF_SOURCE_ALPHA:
                        std::cout << "\t\t\tsource blend factor alpha: SBF_SOURCE_ALPHA" << std::endl;
                        break;
                    case Ogre::SBF_ONE_MINUS_DEST_ALPHA:
                        std::cout << "\t\t\tsource blend factor alpha: SBF_ONE_MINUS_DEST_ALPHA" << std::endl;
                        break;
                    case Ogre::SBF_ONE_MINUS_SOURCE_ALPHA:
                        std::cout << "\t\t\tsource blend factor alpha: SBF_ONE_MINUS_SOURCE_ALPHA" << std::endl;
                        break;
                    default:
                        std::cout << "\t\t\tsource blend factor alpha: UNDEFINED" << std::endl;
                        break;

                }


                std::cout << "TEXTURE UNIT COUNT = " << getTextureUnitCount( ) << std::endl;
                std::cout << "NUM TEXTURE UNIT STATES = " << p->getNumTextureUnitStates( ) << std::endl;

                //OLD WAY WITH TEXTURE UNIT COUNT
                //             VS.
                //NEW WAY WITH NUM TEXTURE UNIT STATES
                /*
                Ogre::Pass::TextureUnitStateIterator it = p->getTextureUnitStateIterator( );
                if ( !it.hasMoreElements( ) ) {
                    std::cout << "\t\ttextures: 0" << std::endl;
                } else {
                    uint tusi = 0;
                    while ( it.peekNext( ) ) {
                        std::cout << "ITERATOR PEEK NEXT : " << it.peekNext( ) << std::endl;
                        it.moveNext( );
                        ++tusi;
                    }
                }
                 */

                uint tusi = p->getNumTextureUnitStates( ); //WORKS BETTER

                std::cout << "\t\ttextures: " << tusi << std::endl;
                for ( uint tusj = 0; tusj < tusi; ++tusj ) {
                    Ogre::TextureUnitState* tus = p->getTextureUnitState( tusj );
                    std::cout << "\t\t\ttexture[" << tusj << "]" << std::endl;

                    //std::cout << "\t\t\t\talpha blend mode: " << tus->getAlphaBlendMode( ) << std::endl;
                    std::cout << "\t\t\t\tanimation duration: " << tus->getAnimationDuration( ) << std::endl;
                    std::cout << "\t\t\t\tbinding type: " << tus->getBindingType( ) << std::endl;
                    std::cout << "\t\t\t\tcolour blend fallback dest: " << tus->getColourBlendFallbackDest( ) << std::endl;
                    std::cout << "\t\t\t\tcolour blend fallback src: " << tus->getColourBlendFallbackSrc( ) << std::endl;
                    //std::cout << "\t\t\t\tcolour blend mode: " << tus->getColourBlendMode( ) << std::endl;
                    std::cout << "\t\t\t\tcontent type: " << tus->getContentType( ) << std::endl;
                    std::cout << "\t\t\t\tcurrent frame: " << tus->getCurrentFrame( ) << std::endl;
                    std::cout << "\t\t\t\tdesired format: " << tus->getDesiredFormat( ) << std::endl;
                    //std::cout << "\t\t\t\teffects: " << tus->getEffects( ) << std::endl;
                    //std::cout << "\t\t\t\tframe texture name: " << tus->getFrameTextureName( ) << std::endl;
                    std::cout << "\t\t\t\tgamma: " << tus->getGamma( ) << std::endl;
                    std::cout << "\t\t\t\talpha: " << tus->getIsAlpha( ) << std::endl;
                    std::cout << "\t\t\t\tname: " << tus->getName( ) << std::endl;
                    std::cout << "\t\t\t\tnum frames: " << tus->getNumFrames( ) << std::endl;
                    std::cout << "\t\t\t\tnum mipmaps: " << tus->getNumMipmaps( ) << std::endl;
                    std::cout << "\t\t\t\tparent: " << tus->getParent( ) << std::endl;
                    std::cout << "\t\t\t\treferenced MRT index: " << tus->getReferencedMRTIndex( ) << std::endl;
                    //std::cout << "\t\t\t\treferenced texture name: " << tus->getReferencedTextureName( ) << std::endl;
                    //std::cout << "\t\t\t\ttexture addressing mode: " << tus->getTextureAddressingMode( ) << std::endl;
                    std::cout << "\t\t\t\ttexture anisotropy: " << tus->getTextureAnisotropy( ) << std::endl;
                    std::cout << "\t\t\t\ttexture border colour: " << tus->getTextureBorderColour( ) << std::endl;
                    std::cout << "\t\t\t\ttexture compare enabled: " << tus->getTextureCompareEnabled( ) << std::endl;
                    std::cout << "\t\t\t\ttexture compare function: " << tus->getTextureCompareFunction( ) << std::endl;
                    std::cout << "\t\t\t\ttexture coord set: " << tus->getTextureCoordSet( ) << std::endl;
                    //std::cout << "\t\t\t\ttexture dimensions: " << tus->getTextureDimensions( ) << std::endl;
                    //std::cout << "\t\t\t\ttexture filtering: " << tus->getTextureFiltering( ) << std::endl;
                    std::cout << "\t\t\t\ttexture mipmap bias: " << tus->getTextureMipmapBias( ) << std::endl;
                    std::cout << "\t\t\t\ttexture name: " << tus->getTextureName( ) << std::endl;
                    std::cout << "\t\t\t\ttexture name alias: " << tus->getTextureNameAlias( ) << std::endl;
                    std::cout << "\t\t\t\ttexture rotate: " << tus->getTextureRotate( ) << std::endl;
                    std::cout << "\t\t\t\ttexture transform: " << tus->getTextureTransform( ) << std::endl;
                    std::cout << "\t\t\t\ttexture type: " << tus->getTextureType( ) << std::endl;
                    std::cout << "\t\t\t\ttexture U scale: " << tus->getTextureUScale( ) << std::endl;
                    std::cout << "\t\t\t\ttexture U scroll: " << tus->getTextureUScroll( ) << std::endl;
                    std::cout << "\t\t\t\ttexture V scale: " << tus->getTextureVScale( ) << std::endl;
                    std::cout << "\t\t\t\ttexture V scroll: " << tus->getTextureVScroll( ) << std::endl;
                }

            }

        }

        const Ogre::ParameterList& params = _mat->getParameters( );

        if ( params.size( ) > 0 ) {
            std::cout <<
                    "\tparams : " << params.size( ) << std::endl;
        }

        Ogre::vector<Ogre::ParameterDef>::const_iterator it = params.begin( );
        Ogre::vector<Ogre::ParameterDef>::const_iterator ite = params.end( );

        for (; it != ite; ++it ) {

            std::cout <<
                    "\t\tparam[" << ( *it ).name << "]:" << std::endl <<
                    "\t\t\tdescription: " << ( *it ).description << std::endl <<
                    "\t\t\ttype: ";
            switch ( ( *it ).paramType ) {
                case Ogre::PT_BOOL:
                    std::cout << "boolean" << std::endl;
                    break;
                case Ogre::PT_REAL:
                    std::cout << "real" << std::endl;
                    break;
                case Ogre::PT_INT:
                    std::cout << "int" << std::endl;
                    break;
                case Ogre::PT_UNSIGNED_INT:
                    std::cout << "uint" << std::endl;
                    break;
                case Ogre::PT_SHORT:
                    std::cout << "short" << std::endl;
                    break;
                case Ogre::PT_UNSIGNED_SHORT:
                    std::cout << "ushort" << std::endl;
                    break;
                case Ogre::PT_LONG:
                    std::cout << "long" << std::endl;
                    break;
                case Ogre::PT_UNSIGNED_LONG:
                    std::cout << "unsigned long" << std::endl;
                    break;
                case Ogre::PT_STRING:
                    std::cout << "string" << std::endl;
                    break;
                case Ogre::PT_VECTOR3:
                    std::cout << "vec3" << std::endl;
                    break;
                case Ogre::PT_MATRIX3:
                    std::cout << "mat3x3" << std::endl;
                    break;
                case Ogre::PT_MATRIX4:
                    std::cout << "mat4x4" << std::endl;
                    break;
                case Ogre::PT_QUATERNION:
                    std::cout << "quaternion" << std::endl;
                    break;
                case Ogre::PT_COLOURVALUE:
                    std::cout << "colour" << std::endl;
                    break;
                default:
                    std::cout << "undefined" << std::endl;
                    break;
            }

        }

    }
}

void PMaterial::makeNameUnique( ) {
    std::size_t found = _name.find( "|cloned|" );
    if ( found != std::string::npos ) {
        _name = _name.substr( 0, found );
    }
    std::stringstream ss;
    ss << _name << "|cloned|" << this;
    _name = ss.str();
}