/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2017 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
 */

/* 
 * File:   Puredata.cpp
 * Author: frankiezafe
 * 
 * Created on April 5, 2017, 4:43 PM
 */

#include "Puredata.h"

using namespace std;
using namespace polymorph;

Puredata::Puredata( ) :
PuredataInOut( ),
_runs( false ),
_verbosed( false ),
_sound_enabled( false ),
_pulseaudio_enabled( false ),
_edition( false ) {
    // loading default configuration
    _channels = 2;
    _sample_rate = 44100;
    _samples = 128;
#ifdef PD_SDL_LINUX
    _audio_driver = "pulseaudio";
#endif
}

Puredata::~Puredata( ) {

    stop( );
    patchAllUnloaded( );

}

bool Puredata::configure( PDData& data ) {

    _configuration = data;
    _edition = data.edit_mode;
    _channels = data.channels;
    _sample_rate = data.sample_rate;
    _samples = data.samples;
    _audio_driver = data.audio_driver;
    
    return true;

}

bool Puredata::configure(
        int output_cannels,
        int sample_rate,
        int samples, std::string audio_driver
        ) {
    if ( _pd_enabled ) return false;
    this->_channels = output_cannels;
    this->_sample_rate = sample_rate;
    this->_samples = samples;
    this->_audio_driver = audio_driver;
    return true;
}

bool Puredata::init( ) {

    if ( _edition ) {
        if ( _verbosed ) {
            PUtil::logCritical( "Puredata: ERROR: quit edit mode "
                    "by calling edition(false)" );
        }
        return false;
    }

    bool success = false;
    _pd_enabled = false;

    if ( _pd.init( 0, _channels, _sample_rate, true ) ) {
        if ( _verbosed ) {
            PUtil::logTrivial( "Puredata: pd started successfully" );
        }
        _pd_enabled = true;
        for (
                vector<string>::iterator its = _search_paths.begin( );
                its != _search_paths.end( );
                ++its ) {
            _pd.addToSearchPath( ( *its ) );
        }
        // set receivers
        _pd.setReceiver( this );
        _pd.setMidiReceiver( this );
        _pd.computeAudio( true );

#ifdef PD_SDL_LINUX    
        // http://rerwarwar.weebly.com/sdl2-audio-init.html

        if ( SDL_Init( SDL_INIT_AUDIO ) < 0 ) {

            if ( _verbosed ) {
                PUtil::logTrivial( "Puredata: Could not init SDL_INIT_AUDIO" );
            }
            return false;

        } else {

            // searching pulseaudio
            int numAudioDrivers = SDL_GetNumAudioDrivers( );
            for ( int i = 0; i < numAudioDrivers; i++ ) {
                if ( string( SDL_GetAudioDriver( i ) ).compare( _audio_driver ) == 0 ) {
                    _pulseaudio_enabled = true;
                    break;
                }
            }

            if ( _pulseaudio_enabled ) {

                SDL_AudioInit( _audio_driver.c_str( ) );
                SDL_AudioSpec want, have;
                SDL_zero( want );
                want.freq = _sample_rate;
                want.format = AUDIO_F32;
                want.channels = _channels;
                want.samples = _samples;
                want.callback = Puredata::forwardCallback;
                want.userdata = this;

                _audio_device_id = SDL_OpenAudioDevice(
                        NULL, 0, &want, &have, SDL_AUDIO_ALLOW_ANY_CHANGE );

                if ( _verbosed ) {

                    if ( want.channels != have.channels ) {
                        std::stringstream ss;
                        ss << "Puredata: SDL_INIT_AUDIO: " <<
                                "requested channels " << want.channels <<
                                ", received channels " << have.channels;
                        PUtil::logCritical( ss.str( ) );
                    }

                    if ( want.format != have.format ) {
                        std::stringstream ss;
                        ss << "Puredata: SDL_INIT_AUDIO: " <<
                                "requested format " << want.format <<
                                ", received format " << have.format;
                        PUtil::logCritical( ss.str( ) );
                    }

                    if ( want.freq != have.freq ) {
                        std::stringstream ss;
                        ss << "Puredata: SDL_INIT_AUDIO: " <<
                                "requested freq " << want.format <<
                                ", received freq " << have.format;
                        PUtil::logCritical( ss.str( ) );
                    }

                    if ( want.samples != have.samples ) {
                        std::stringstream ss;
                        ss << "Puredata: SDL_INIT_AUDIO: " <<
                                "requested samples " << want.format <<
                                ", received samples " << have.format;
                        PUtil::logCritical( ss.str( ) );
                    }

                }

                SDL_PauseAudioDevice( _audio_device_id, 1 );

                if ( _verbosed ) {

                    std::stringstream ss;
                    ss << "Puredata: SDL_INIT_AUDIO w. " <<
                            _audio_driver <<
                            ", channels: " << _channels <<
                            ", sample rate: " << _sample_rate <<
                            ", samples: " << _samples <<
                            " successfully started" << endl;
                    PUtil::logNormal( ss.str( ) );

                }

                _sound_enabled = true;
                success = true;

            } else {

                if ( _verbosed ) {
                    std::stringstream ss;
                    ss << "Puredata: SDL_INIT_AUDIO: Failed to load " <<
                            _audio_driver;
                    PUtil::logCritical( ss.str( ) );
                }
            }

        }
#endif

    } else {

        _pd.clear( );
        if ( _verbosed ) {
            PUtil::logCritical( "Puredata: pd started FAILED" );
        }

    }

    if ( !success ) {

        if ( _pd_enabled ) _pd.computeAudio( false );
        return false;

    }

    return true;

}

bool Puredata::start( ) {

    if ( !_pd_enabled ) return false;

    if ( _edition ) {

        POSDataListIter its = _configuration.edition_senders.begin( );
        POSDataListIter itse = _configuration.edition_senders.end( );
        for (; its != itse; ++its ) {
            POscSender* sender = new POscSender( ( *its ) );
            _edition_senders.push_back( sender );
        }

        PORDataListIter itr = _configuration.edition_receivers.begin( );
        PORDataListIter itre = _configuration.edition_receivers.end( );
        for (; itr != itre; ++itr ) {
            POscReceiver* rcv = new POscReceiver( ( *itr ) );
            _edition_receivers.push_back( rcv );
        }

        if ( _verbosed ) {
            PUtil::logTrivial( "Puredata: starting, in edit mode" );
        }

    } else {

        std::vector<std::string>::iterator itsp = _configuration.search_paths.begin( );
        std::vector<std::string>::iterator itspe = _configuration.search_paths.end( );
        for (; itsp != itspe; ++itsp ) {
            _pd.addToSearchPath( ( *itsp ) );
            if ( _verbosed ) {
                PUtil::logTrivial( "Puredata: adding search path: " + ( *itsp ) );
            }
        }

#ifdef PD_SDL_LINUX          
        SDL_PauseAudioDevice( _audio_device_id, 0 );
#endif
        
        assert( !_bthread );
        _runs = true;

        _bthread = boost::shared_ptr<boost::thread>(
                new boost::thread( boost::bind( &Puredata::runReleaseThread, this ) ) );

        if ( _verbosed ) {
            PUtil::logTrivial( "Puredata: starting, in release mode" );
        }

    }


    return true;
}

void Puredata::stop( ) {

    if ( _edition ) {

        POscSenderListIter its = _edition_senders.begin( );
        POscSenderListIter itse = _edition_senders.end( );
        for (; its != itse; ++its ) {
            delete (*its );
        }
        _edition_senders.clear( );

        POscRcvrListIter itr = _edition_receivers.begin( );
        POscRcvrListIter itre = _edition_receivers.end( );
        for (; itr != itre; ++itr ) {
            delete (*itr );
        }
        _edition_receivers.clear( );

    } else {

#ifdef PD_SDL_LINUX
        SDL_PauseAudioDevice( _audio_device_id, 0 );
#endif
        
        _pd.clearSearchPath( );
        _pd.unsubscribeAll( );
        patchAllUnloaded( );

        if ( !_runs ) return;
        assert( _bthread );
        _runs = false;
        _bthread->join( );

    }

}

bool Puredata::subscribe( const std::string& source ) {

    if ( _pd_enabled ) {
        _pd.subscribe( source );
        return true;
    }
    return false;

}

bool Puredata::unsubscribe( const std::string& source ) {

    if ( _pd_enabled ) {
        _pd.unsubscribe( source );
        return true;
    }
    return false;

}

bool Puredata::unsubscribeAll( ) {

    if ( _pd_enabled ) {
        _pd.unsubscribeAll( );
        return true;
    }
    return false;

}

void Puredata::runReleaseThread( ) {

    while ( _runs ) {

        boost::unique_lock<boost::mutex>* lock =
                new boost::unique_lock<boost::mutex>( _bmutex );

        if ( _pd_enabled ) {
            _pd.receiveMessages( );
            _pd.receiveMidi( );
        }

        delete lock;

        boost::this_thread::sleep( boost::posix_time::milliseconds( 10 ) );

    }

}

void Puredata::runEditionThread( ) {

}

void Puredata::audioCallback( unsigned char* stream, int len ) {

    // Pd magic
    int ticks = len / ( sizeof (float ) * _channels * 64 );
    _pd.processFloat( ticks, ( float* ) stream, ( float* ) stream );

}

pd::Patch* Puredata::loadPatch( std::string patch_path ) {

    if ( !_pd_enabled ) {
        return 0;
    }
    boost::unique_lock<boost::mutex>* lock =
            new boost::unique_lock<boost::mutex>( _bmutex );

    pd::Patch p = _pd.openPatch( patch_path, "." );
    pd::Patch* p_ptr = new pd::Patch( p );
    addPatch( p_ptr );

    delete lock;
    return p_ptr;

}

void Puredata::unloadPatch( pd::Patch* p ) {

    if ( !_pd_enabled ) {
        return;
    }

    boost::unique_lock<boost::mutex>* lock =
            new boost::unique_lock<boost::mutex>( _bmutex );

    _pd.closePatch( *p );
    removePatch( p );

    delete lock;

}

bool Puredata::containsPatch( pd::Patch* p ) {

    return find(
            _loaded_patchs.begin( ),
            _loaded_patchs.end( ),
            p
            ) != _loaded_patchs.end( );

}

void Puredata::addPatch( pd::Patch* p ) {

    if ( containsPatch( p ) ) {
        return;
    }
    _loaded_patchs.push_back( p );
    patchLoaded( p );

}

void Puredata::removePatch( pd::Patch* p ) {

    pdPListIter it = find( _loaded_patchs.begin( ), _loaded_patchs.end( ), p );
    if ( it != _loaded_patchs.end( ) ) {
        patchUnloaded( ( *it ) );
        delete (*it );
        _loaded_patchs.erase( it );
    }

}