/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2017 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
*/

/* 
 * File:   PPatch.cpp
 * Author: frankiezafe
 * 
 * Created on April 5, 2017, 5:15 PM
 */

#include "PPatch.h"

using namespace std;
using namespace polymorph;
using namespace Ogre;

PPatch::PPatch() :
_uid(0),
_patch(0),
_pd(0) {
}

PPatch::~PPatch() {

    if (_pd) {
    
        if (_patch) {
            _pd->unloadPatch(_patch);
            _patch = 0;
        }
        _pd->removePatchListener(this);
    
    }

}

bool PPatch::load(
                   Puredata* pd,
                   std::string resource_group,
                   std::string resource_name
                   ) {

    std::string localpath = PUtil::decompressPatch( resource_group, resource_name );
    if (localpath.empty()) {
        stringstream ss;
        ss << "PPatch::load, unable to decompress resource: " <<
                resource_group << ":" << resource_name;
        PUtil::logCritical(ss.str());
        return false;\
    }
    
    return load( pd, localpath );
    
}

bool PPatch::load(Puredata* pd, std::string path) {

    if (_patch) {
        PUtil::logCritical("PPatch::load, a patch is already setup!");
        return false;
    }

    if (path.empty() && _path.empty()) {
        PUtil::logCritical("PPatch::load, no path given!");
        return false;
    }

    if (!path.empty()) {
        _path = path;
    }

    _patch = pd->loadPatch(_path);

    if (!_patch) {
        _uid = 0;
        PUtil::logCritical("PPatch::load, patch not loaded!");
        return false;
    }

    if (!_patch->isValid()) {
        pd->unloadPatch(_patch);
        _patch = 0;
        _uid = 0;
        PUtil::logCritical("PPatch::load, patch is invalid!");
        return false;
    }

    if (_pd && _pd != pd) {
        _pd->removePatchListener(this);
    }

    _pd = pd;
    _uid = _patch->dollarZero();

    // subscribing to patch event in puredata
    _pd->addPatchListener(static_cast<PatchEventListerner*> (this));

    PUtil::logCritical("PPatch::load, " + _path + " sucess!");
    return true;

}

bool PPatch::unload() {

    if (!_patch) {
        return false;
    }

    pd::Patch* tmp = _patch;
    _patch = 0;
    _uid = 0;
    _pd->unloadPatch(tmp);

    return true;

}

void PPatch::patchLoaded(pd::Patch* p) {

    if (p == _patch) {
//        PUtil::logTrivial("PPatch::patchLoaded, confirmation of patch creation");
    }

}

void PPatch::patchUnloaded(pd::Patch* p) {


    if (p == _patch) {
//        PUtil::logTrivial("PPatch::patchUnloaded, confirmation of patch deletion");
        _patch = 0;
        _uid = 0;
    }

}

void PPatch::patchAllUnloaded() {


    // puredata stopped!
    if (_patch) {
//        PUtil::logTrivial("PPatch::patchAllUnloaded");
        _patch = 0;
    }
    _pd = 0;
    _uid = 0;

}