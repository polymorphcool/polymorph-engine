/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2017 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
*/

/* 
 * File:   PuredataInOut.cpp
 * Author: frankiezafe
 * 
 * Created on April 6, 2017, 1:59 PM
 */

#include "PuredataInOut.h"

using namespace std;
using namespace polymorph;

PuredataInOut::PuredataInOut():
_pd_enabled(0)
{
    
}

PuredataInOut::~PuredataInOut() {

}

bool PuredataInOut::subscribe( const std::string& source ) {
    if ( _pd_enabled ) {
        _pd.subscribe( source );
        return true;
    }
    return false;
}

bool PuredataInOut::unsubscribe( const std::string& source ) {
    if ( _pd_enabled ) {
        _pd.unsubscribe( source );
        return true;
    }
    return false;
}

bool PuredataInOut::unsubscribeAll( ) {
    if ( _pd_enabled ) {
        _pd.unsubscribeAll( );
        return true;
    }
    return false;
}

void PuredataInOut::print( const std::string& message ) {
    cout << message << endl;
}

void PuredataInOut::receiveBang(const std::string& dest) {
    cout << "CPP: bang " << dest << endl;
}

void PuredataInOut::receiveFloat(const std::string& dest, float num) {
    cout << "CPP: float " << dest << ": " << num << endl;
}

void PuredataInOut::receiveSymbol(
        const std::string& dest,
        const std::string& symbol
        ) {
    cout << "CPP: symbol " << dest << ": " << symbol << endl;
}

void PuredataInOut::receiveList(const std::string& dest, const pd::List& list) {


    //    size_t ll = list.len( );
    //
    //    if ( dest.compare( PD_INTERNAL_OBJECT_OUT ) == 0 ) {
    //        switch ( ll ) {
    //            case 5:
    //                if ( list.isSymbol( 0 ) ) {
    //                    if ( list.getSymbol( 0 ).compare( PD_INTERNAL_OBJECT_TRANS ) == 0 ) {
    //                        receivedObjectTrans( list );
    //                    } else if ( list.getSymbol( 0 ).compare( PD_INTERNAL_OBJECT_ROT ) == 0 ) {
    //                        receivedObjectRot( list );
    //                    } else if ( list.getSymbol( 0 ).compare( PD_INTERNAL_OBJECT_SCALE ) == 0 ) {
    //                        receivedObjectScale( list );
    //                    }
    //                }
    //                return;
    //            case 11:
    //                if ( list.isSymbol( 0 ) ) {
    //                    if ( list.getSymbol( 0 ).compare( PD_INTERNAL_OBJECT_ALL ) == 0 ) {
    //                        receivedObjectAll( list );
    //                    }
    //                }
    //                return;
    //            default:
    //                break;
    //        }
    //    }

    //    if ( !verbosed ) return;

    cout << "CPP: list " << dest << ": ";
    // step through the list
    for (uint i = 0; i < list.len(); ++i) {
        if (list.isFloat(i))
            cout << list.getFloat(i) << " ";
        else if (list.isSymbol(i))
            cout << list.getSymbol(i) << " ";
    }
    // you can also use the built in toString function or simply stream it out
    cout << list.toString();
    cout << list;
    // print an OSC-style type string
    cout << list.types() << endl;

}

void PuredataInOut::receiveMessage(
        const std::string& dest,
        const std::string& msg,
        const pd::List& list) {
    cout << "CPP: message " <<
            dest << ": " << msg << " " <<
            list.toString() << list.types() << endl;
}

void PuredataInOut::receiveNoteOn(
        const int channel,
        const int pitch,
        const int velocity) {
    cout << "CPP MIDI: note on: " <<
            channel << " " << pitch << " " << velocity << endl;
}

void PuredataInOut::receiveControlChange(
        const int channel,
        const int controller,
        const int value) {
    cout << "CPP MIDI: control change: " <<
            channel << " " << controller << " " << value << endl;
}

void PuredataInOut::receiveProgramChange(const int channel, const int value) {
    cout << "CPP MIDI: program change: " <<
            channel << " " << value << endl;
}

void PuredataInOut::receivePitchBend(const int channel, const int value) {
    cout << "CPP MIDI: pitch bend: " <<
            channel << " " << value << endl;
}

void PuredataInOut::receiveAftertouch(const int channel, const int value) {
    cout << "CPP MIDI: aftertouch: " <<
            channel << " " << value << endl;
}

void PuredataInOut::receivePolyAftertouch(
        const int channel,
        const int pitch,
        const int value) {
    cout << "CPP MIDI: poly aftertouch: " <<
            channel << " " << pitch << " " << value << endl;
}

void PuredataInOut::receiveMidiByte(const int port, const int byte) {
    cout << "CPP MIDI: midi byte: " <<
            port << " " << byte << endl;
}

bool PuredataInOut::addReceiver(pd::PdReceiver* _pdr) {
    bool success = false;
    std::set<pd::PdReceiver*>::iterator iter;
    iter = receivers.find(_pdr);
    if (iter == receivers.end()) {
        receivers.insert(_pdr);
        success = true;
    }
    return success;
}

bool PuredataInOut::removeReceiver(pd::PdReceiver* _pdr) {
    bool success = false;
    std::set<pd::PdReceiver*>::iterator iter;
    iter = receivers.find(_pdr);
    if (iter != receivers.end()) {
        receivers.erase(_pdr);
        success = true;
    }
    return success;
}

void PuredataInOut::removeAllReceivers() {
    receivers.clear();
}

bool PuredataInOut::addMidiReceiver(pd::PdMidiReceiver* _pdr) {
    bool success = false;
    std::set<pd::PdMidiReceiver*>::iterator iter;
    iter = midi_receivers.find(_pdr);
    if (iter == midi_receivers.end()) {
        midi_receivers.insert(_pdr);
        success = true;
    }
    return success;
}

bool PuredataInOut::removeMidiReceiver(pd::PdMidiReceiver* _pdr) {
    bool success = false;
    std::set<pd::PdMidiReceiver*>::iterator iter;
    iter = midi_receivers.find(_pdr);
    if (iter != midi_receivers.end()) {
        midi_receivers.erase(_pdr);
        success = true;
    }
    return success;
}

void PuredataInOut::removeAllMidiReceivers() {
    midi_receivers.clear();
}

bool PuredataInOut::sendBang(const std::string& dest) {
    if (!_pd_enabled) return false;
    _pd.sendBang(dest);
    return true;
}

bool PuredataInOut::sendFloat(const std::string& dest, float value) {
    if (!_pd_enabled) return false;
    _pd.sendFloat(dest, value);
    return true;
}

bool PuredataInOut::sendSymbol(const std::string& dest, const std::string& symbol) {
    if (!_pd_enabled) return false;
    _pd.sendSymbol(dest, symbol);
    return true;
}

bool PuredataInOut::startMessage() {
    if (!_pd_enabled) return false;
    _pd.startMessage();
    return true;
}

void PuredataInOut::addFloat(const float value) {
    if (!_pd_enabled) return;
    _pd.addFloat(value);
}

void PuredataInOut::addSymbol(const std::string& symbol) {
    if (!_pd_enabled) return;
    _pd.addSymbol(symbol);
}

bool PuredataInOut::finishList(const std::string& dest) {
    if (!_pd_enabled) return false;
    _pd.finishList(dest);
    return true;
}

bool PuredataInOut::finishMessage(const std::string& dest, const std::string& msg) {
    if (!_pd_enabled) return false;
    _pd.finishMessage(dest, msg);
    return true;
}

bool PuredataInOut::sendList(const std::string& dest, const pd::List& list) {
    if (!_pd_enabled) return false;
    _pd.sendList(dest, list);
    return true;
}

bool PuredataInOut::sendMessage(
        const std::string& dest,
        const std::string& msg,
        const pd::List& list) {
    if (!_pd_enabled) return false;
    _pd.sendMessage(dest, msg, list);
    return true;
}

bool PuredataInOut::sendNoteOn(
        const int channel,
        const int pitch,
        const int velocity) {
    if (!_pd_enabled) return false;
    _pd.sendNoteOn(channel - 1, pitch, velocity);
    return true;
}

bool PuredataInOut::sendControlChange(
        const int channel,
        const int control,
        const int value) {
    if (!_pd_enabled) return false;
    _pd.sendControlChange(channel - 1, control, value);
    return true;
}

bool PuredataInOut::sendProgramChange(const int channel, int program) {
    if (!_pd_enabled) return false;
    _pd.sendProgramChange(channel - 1, program - 1);
    return true;
}

bool PuredataInOut::sendPitchBend(const int channel, const int value) {
    if (!_pd_enabled) return false;
    _pd.sendPitchBend(channel - 1, value);
    return true;
}

bool PuredataInOut::sendAftertouch(const int channel, const int value) {
    if (!_pd_enabled) return false;
    _pd.sendAftertouch(channel - 1, value);
    return true;
}

bool PuredataInOut::sendPolyAftertouch(const int channel, int pitch, int value) {
    if (!_pd_enabled) return false;
    _pd.sendPolyAftertouch(channel - 1, pitch, value);
    return true;
}

bool PuredataInOut::sendMidiByte(const int port, const int value) {
    if (!_pd_enabled) return false;
    _pd.sendMidiByte(port, value);
    return true;
}

bool PuredataInOut::sendSysex(const int port, const int value) {
    if (!_pd_enabled) return false;
    _pd.sendSysex(port, value);
    return true;
}

bool PuredataInOut::sendSysRealTime(const int port, const int value) {
    if (!_pd_enabled) return false;
    _pd.sendSysRealTime(port, value);
    return true;
}

int PuredataInOut::arraySize(const std::string& arrayName) {
    if (!_pd_enabled) return -1;
    int len = _pd.arraySize(arrayName);
    return len;
}

bool PuredataInOut::readArray(
        const std::string& arrayName,
        std::vector<float>& dest,
        int readLen, int offset) {
    if (!_pd_enabled) return false;
    bool ret = _pd.readArray(arrayName, dest, readLen, offset);
    return ret;
}

bool PuredataInOut::writeArray(
        const std::string& arrayName,
        std::vector<float>& source,
        int writeLen, int offset) {
    if (!_pd_enabled) return false;
    bool ret = _pd.writeArray(arrayName, source, writeLen, offset);
    return ret;
}

bool PuredataInOut::clearArray(const std::string& arrayName, int value) {
    if (!_pd_enabled) return false;
    _pd.clearArray(arrayName, value);
    return true;
}