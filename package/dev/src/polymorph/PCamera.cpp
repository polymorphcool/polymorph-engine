/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2017 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
 */

/* 
 * File:   PCamera.cpp
 * Author: frankiezafe
 * 
 * Created on January 21, 2017, 10:38 PM
 */

#include "PCamera.h"

using namespace std;
using namespace Ogre;
using namespace polymorph;

PCamera::PCamera( ) :
PObject<Ogre::Camera*>( ),
_scale( Ogre::Vector3::UNIT_SCALE ),
_pivot( 0 ),
_look_at( 0 ),
_debug_node( 0 ),
_debug_entity( 0 ),
_aspect_ratio( 1 ),
_fov_y( 0.7 ),
_request_lookat( false ),
_mat_world( Matrix4::IDENTITY ),
_mat_worldi( Matrix4::IDENTITY.inverse( ) ) {
    _bt_exchange = static_cast<PBulletExchange*> (&_cam_exchange);
}

PCamera::~PCamera( ) {
    clear( );
}

void PCamera::clearDebug( ) {

    if ( _sceneMgr ) {
        if ( _debug_node )
            _sceneMgr->destroySceneNode( _debug_node );
        if ( _debug_entity )
            _sceneMgr->destroyEntity( _debug_entity );
        if ( _axis.sm ) {
            PUtil::destroyXYZ( _axis );
        }
    }

    _debug_node = 0;
    _debug_entity = 0;

}

void PCamera::clear( ) {

    clearDebug( );
    if ( _object ) PObject<Ogre::Camera*>::clear( );

}

void PCamera::init( Ogre::SceneManager* sceneMgr, std::string name ) {

    _sceneMgr = sceneMgr;
    _name = name;

    _origin = _sceneMgr->getRootSceneNode( )->createChildSceneNode( );
    _pivot = _origin->createChildSceneNode( );
    _pivot->setInheritOrientation( true );

    _object = _sceneMgr->createCamera( name );
    _object->setPosition( Ogre::Vector3( 0, 0, 0 ) );
    _object->setNearClipDistance( 0.1 );
    _object->setFarClipDistance( 100000 );
    _object->setFOVy( Radian( 0.7 ) );
    _object->setAutoAspectRatio( true );
    _object->addListener( this );

    _object->detachFromParent( );
    _pivot->attachObject( _object );

    _aspect_ratio = _object->getAspectRatio( );
    
    //    updateMatrices();

}

void PCamera::visible( bool enable ) {

}

void PCamera::debug( bool enable ) {

    if (
            !_sceneMgr ||
            ( enable && _debug_node ) ||
            ( !enable && !_debug_node )
            ) return;

    _debugging = enable;

    if ( !enable ) {
        clearDebug( );
    }

    //    _object->setDebugDisplayEnabled( true );

    _debug_entity = _sceneMgr->createEntity( PUtil::cameraDebugMesh( ) );
    _debug_entity->setMaterial( PUtil::materialDebugNode( ) );

    _debug_node = _pivot->createChildSceneNode( );
    _debug_node->setName( _name + "_debug_node" );
    _debug_node->attachObject( _debug_entity );
    _debug_node->scale(
            POLYMORPH_XYZAXIS_LENGTH,
            POLYMORPH_XYZAXIS_LENGTH,
            POLYMORPH_XYZAXIS_LENGTH );

    //    Ogre::Vector3 sc( 
    //        POLYMORPH_XYZAXIS_LENGTH, 
    //        POLYMORPH_XYZAXIS_LENGTH / _aspect_ratio,
    //        POLYMORPH_XYZAXIS_LENGTH * _object->getFOVy() );
    //    _debug_node->scale( sc );

}

void PCamera::showAxis( bool enabled ) {

    if ( enabled && !_axis.sm ) {
        PUtil::createXYZ(
                _axis, _sceneMgr, _origin,
                POLYMORPH_XYZAXIS_LENGTH,
                POLYMORPH_XYZAXIS_THICKNESS
                );
    } else if ( !enabled && _axis.sm ) {
        PUtil::destroyXYZ( _axis );
    }

}

void PCamera::lookAt( Ogre::Vector3 p, bool force ) {

    _request_lookat = true;
    _request_lookat_v3 = p;

    if ( force ) {
        applyLookat( );
    }

}

void PCamera::renderUp( ) {

    // required to avoid using world up when lookat is rendered!
    Vector3 local_up( 0, 1, 0 );
    Quaternion q = _origin->_getDerivedOrientation();
    local_up = q * local_up;
    _object->setFixedYawAxis( false, local_up );

}

void PCamera::applyLookat( ) {
    

    if ( _request_lookat ) {
        
        renderUp( );
        _request_lookat = false;
        _object->lookAt( _request_lookat_v3 );
        
        if ( _debugging ) {
            _debug_node->setOrientation( _object->getOrientation( ) );
        }
        
    } else if ( _look_at ) {
        
        renderUp( );
        _object->lookAt( _look_at->getTrans( ) );
        
        if ( _debugging ) {
            _debug_node->setOrientation( _object->getOrientation( ) );
        }
        
    } else {
    
        Vector3 local_up( 0, 1, 0 );
        Quaternion q = _pivot->getOrientation();
        local_up = q * local_up;
        _object->setFixedYawAxis( false, local_up );
        
    }

}

//void PCamera::updateMatrices() {
//    
//    if ( !_object ) return;
//    
//    _object->getWorldTransforms( &_mat_projection );
//    _mat_worldi = _mat_world.inverse();
//    
//    _mat_world = Matrix4( _object->getRealOrientation() );
//    _mat_world.setTrans( _object->getRealPosition() );
//    cout << "PCamera matrices updated" << endl;
//    PUtil::print( _mat_projection );
//    cout << "****************************" << endl;
//    PUtil::print( _mat_world );
//    
//}

void PCamera::fov( Ogre::Real degree ) {
    
    fov( Radian( Ogre::Degree( degree ).valueRadians() ) );
    
}

void PCamera::fov( Ogre::Radian r ) {
    if ( !_object ) return;
    _object->setFOVy( r );
    if ( !_object->isWindowSet( ) && _debugging ) {
        updateDebug( );
    }
}

void PCamera::updateDebug( ) {
    
    _fov_y = _object->getFOVy( );
    Ogre::Real zmult = Math::Tan( _fov_y * 0.5 );
    Ogre::Vector3 sc;
    
    if ( zmult > 0 ) {
        sc = Ogre::Vector3(
                POLYMORPH_XYZAXIS_LENGTH * 2,
                POLYMORPH_XYZAXIS_LENGTH * 2 / _object->getAspectRatio( ),
                POLYMORPH_XYZAXIS_LENGTH / zmult );
    } else {
        sc = Ogre::Vector3(
                POLYMORPH_XYZAXIS_LENGTH * 2,
                POLYMORPH_XYZAXIS_LENGTH * 2 / _object->getAspectRatio( ),
                POLYMORPH_XYZAXIS_LENGTH );
    }
    
    _debug_node->scale( sc / _debug_node->getScale( ) );
    _aspect_ratio = _object->getAspectRatio( );
    
}

void PCamera::cameraPreRenderScene( Ogre::Camera* cam ) {

    applyLookat( );

    if ( _debugging ) {

        if (
                _aspect_ratio != _object->getAspectRatio( ) ||
                _fov_y != _object->getFOVy( )
                ) {
            updateDebug( );
        }

    }

}

Ogre::Vector3 PCamera::worldToCam( Ogre::Vector3 v ) {
    
    if ( !_object ) return Ogre::Vector3::ZERO;
    
    _object->getWorldTransforms( &_mat_world );
    _mat_worldi = _mat_world.inverse();
    Ogre::Matrix4 local = Ogre::Matrix4::IDENTITY;
    local.setTrans( v );
    return Ogre::Matrix4( _mat_worldi * local * _mat_world ).getTrans();

}
        
Ogre::Quaternion PCamera::worldToCam( Ogre::Quaternion q ) {
    
    if ( !_object ) return Ogre::Quaternion::IDENTITY;
    
    _object->getWorldTransforms( &_mat_world );
    _mat_worldi = _mat_world.inverse();
    Ogre::Matrix4 local(q);
    return Ogre::Matrix4( _mat_worldi * local ).extractQuaternion();
    
}