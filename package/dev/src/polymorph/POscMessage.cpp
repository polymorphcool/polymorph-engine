/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2017 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
 */

/* 
 * File:   POscMessage.cpp
 * Author: frankiezafe
 * 
 * Created on October 15, 2017, 5:41 PM
 */

#include "POscMessage.h"

using namespace polymorph;
using namespace osc;

POscMessage::POscMessage() :
packet(0),
packed(false),
max_32bits(false)
{
}

POscMessage::POscMessage(const POscMessage& orig) :
packet(0),
packed(false) {
}

POscMessage::~POscMessage() {
    purgeMessage();
}

void POscMessage::purgeMessage() {
    if (packet) {
        delete packet;
        packet = 0;
        packed = false;
    }
}

void POscMessage::startMessage(const std::string& marker) {
    purgeMessage();
    packet = new OutboundPacketStream(1024);
    (*packet) << osc::BeginBundleImmediate << osc::BeginMessage(marker.c_str());
}

void POscMessage::add(char* cs) {
    if (!packet) return;
    (*packet) << cs;
}

void POscMessage::add(const std::string& str) {
    if (!packet) return;
    (*packet) << str.c_str();
}

void POscMessage::add(const int8_t& i) {
    if (!packet) return;
    (*packet) << (char) i;
}

void POscMessage::add(const uint8_t& i) {
    if (!packet) return;
    (*packet) << (char) i;
}

void POscMessage::add(const int32_t& i) {
    if (!packet) return;
    (*packet) << i;
}

void POscMessage::add(const uint32_t& i) {
    if (!packet) return;
    (*packet) << (long) i;
}

void POscMessage::add(const int64_t& i) {
    if (!packet) return;
    if (!max_32bits) (*packet) << (double) i;
    else (*packet) << (int32_t) i;
}

void POscMessage::add(const uint64_t& i) {
    if (!packet) return;
    if (!max_32bits) (*packet) << (double) i;
    else (*packet) << (int32_t) i;
}

void POscMessage::add(const float& f) {
    if (!packet) return;
    (*packet) << f;
}

void POscMessage::add(const double& d) {
    if (!packet) return;
    if (!max_32bits) (*packet) << d;
    else (*packet) << (float) d;
}

void POscMessage::add(const bool& b) {
    if (!packet) return;
    (*packet) << b;
}

void POscMessage::add(const Ogre::Vector2& v) {
    if (!packet) return;
    for (int i = 0; i < 2; ++i) {
        add(v[i]);
    }
}

void POscMessage::add(const Ogre::Vector3& v) {
    if (!packet) return;
    for (int i = 0; i < 3; ++i) {
        add(v[i]);
    }
}

void POscMessage::add(const Ogre::Vector4& v) {
    if (!packet) return;
    for (int i = 0; i < 4; ++i) {
        add(v[i]);
    }
}

void POscMessage::add(const Ogre::Quaternion& q) {
    if (!packet) return;
    for (int i = 0; i < 4; ++i) {
        add(q[i]);
    }
}

void POscMessage::add(const Ogre::Matrix3& m) {
    if (!packet) return;
    for (int r = 0; r < 3; ++r) {
        for (int c = 0; c < 3; ++c) {
            add(m[r][c]);
        }
    }
}

void POscMessage::add(const Ogre::Matrix4& m) {
    if (!packet) return;
    for (int r = 0; r < 4; ++r) {
        for (int c = 0; c < 4; ++c) {
            add(m[r][c]);
        }
    }
}

void POscMessage::add(const Ogre::Euler& e) {
    if (!packet) return;
    add(e.pitch());
    add(e.yaw());
    add(e.roll());
}

void POscMessage::packMessage() {
    if (!packet) return;
    (*packet) << osc::EndMessage << osc::EndBundle;
    packed = true;
}