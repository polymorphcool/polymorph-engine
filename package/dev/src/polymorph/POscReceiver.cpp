/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ___________________________________( ^3^)_____________________________________
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 */

/* 
 * File:   POscReceiver.cpp
 * Author: frankiezafe
 * 
 * Created on September 16, 2016, 10:31 AM
 */

#include "POscReceiver.h"
#include "serialisation/POscData.h"

using namespace osc;
using namespace polymorph;
using namespace Ogre;

POscReceiver::POscReceiver( ) :
_socket( 0 ),
port( 0 ),
buffer_capacity( MAX_QUEUE_SIZE ),
autostart(false),
write_index(0),
write_last_index(0),
written(false),
looped(false),
write_buffer(0),
read_buffer(0),
read_msg(0)
{}

POscReceiver::POscReceiver(POscReceiverData& data) :
_socket( 0 ),
port( data.port ),
buffer_capacity( data.max_queue_size ),
autostart( data.autostart ),
write_index(0),
write_last_index(0),
written(false),
looped(false),
write_buffer(0),
read_buffer(0),
read_msg(0)
{
    if ( autostart ) {
        openSocket();
        start();
    }
}

POscReceiver::~POscReceiver( ) {

    stop( );

}

void POscReceiver::configure(POscReceiverData& data) {

    if ( _runs ) return;
    if ( port == data.port ) return;
    if ( port != data.port ) {
        delete _socket;
        _socket = 0;
    }
    port = data.port;
    buffer_capacity = data.max_queue_size;
    autostart = data.autostart;
    openSocket();
    if ( autostart ) start();
    
}

void POscReceiver::setup( polymorph::uint port ) {

    if ( _runs ) return;
    if ( this->port == port ) return;
    if ( this->port != port ) {
        delete _socket;
        _socket = 0;
    }
    this->port = port;
    openSocket();
//    std::cout << port << ", " << _socket << std::endl;

}

void POscReceiver::openSocket() {
    _socket = new UdpListeningReceiveSocket(
            IpEndpointName( IpEndpointName::ANY_ADDRESS, port ),
            this );
    _runs = false;
}

bool POscReceiver::start( ) {

//    std::cout << "POscReceiver::start" << std::endl;
    
    assert( !bthread );
    
    _runs = true;
    written = false;
    looped = false;
    
    // impossible to change the queue size after this point
    buffer01 = new PMessageData[ buffer_capacity ];
    buffer02 = new PMessageData[ buffer_capacity ];
    
    // creation of an array of pointers
    write_buffer = buffer01;
    read_buffer = buffer02;
    
    // resetting indexes
    write_index = 0;
    write_last_index = 0;
    read_index = 0;
    read_last_index = 0;
    read_msg = 0;
    
    bthread = boost::shared_ptr<boost::thread>( new boost::thread( boost::bind( &POscReceiver::runThread, this ) ) );
    
    return true;

}

void POscReceiver::stop( ) {
    
    if ( !_runs ) return;
    
    _socket->Break( );
    assert( bthread );
    bthread->detach( );
        
    // locking all mutex!
    write_mutex.lock();
    read_mutex.lock();
    
    // cleaning the queue
    delete [] buffer01;
    delete [] buffer02;
    write_buffer = 0;
    read_buffer = 0;
    
    _runs = false;
    written = false;
    looped = false;
    
    write_mutex.unlock();
    read_mutex.unlock();
    
    // finished

}

void POscReceiver::runThread( ) {
    _socket->Run( );
}

void POscReceiver::ProcessMessage(
        const osc::ReceivedMessage& m,
        const IpEndpointName& remoteEndpoint
        ) {

    ( void ) remoteEndpoint; // suppress unused parameter warning
    
    bthread->yield();
    
    write_mutex.lock();
    
    written = true;
    
    write_buffer[ write_index ].load( m, remoteEndpoint );
    ++write_index;
    if ( write_index >= buffer_capacity ) {
        write_index = 0;
        looped = true;
    }
    if ( looped ) {
        write_last_index = ( write_index + 1 ) % buffer_capacity;
    }
    
    write_mutex.unlock();
        
        
}

void POscReceiver::setMaximumQueueSize( polymorph::uint max_queue ) {
    
    if ( _runs ) return;
    buffer_capacity = max_queue;
    
}


bool POscReceiver::hasPending() {
    
    if ( !_runs ) return false;
    
    write_mutex.lock();
    
    if ( !written ) {
//        std::cout << "POscReceiver::hasPending, nothing received" << std::endl;
        write_mutex.unlock();
        return false;
    }
    
    // swapping buffers
    PMessageData* tmp = write_buffer;
    write_buffer = read_buffer;
    read_buffer = tmp;
    read_msg = 0;
    
    read_index = write_index;
    read_last_index = write_last_index;
    read_start_index = write_last_index;
    
    // restarting write buffer
    written = false;
    looped = false;
    write_index = 0;
    write_last_index = 0;
    
    write_mutex.unlock();
    
    return true;
    
}

PMessageData* POscReceiver::getNext() {
    
    read_mutex.lock();
        
    if ( read_msg != 0 ) {
        read_msg->clear();
        read_msg = 0;
    }
    
    if ( read_last_index == read_index ) {
        read_mutex.unlock();
        return read_msg;
    }
    
    read_msg = &read_buffer[ read_last_index ];
    read_last_index = ( read_last_index + 1 ) % buffer_capacity;
    
    read_mutex.unlock();
    
    return read_msg;
    
}