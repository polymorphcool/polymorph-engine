/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
 */

/* 
 * File:   PBullet.cpp
 * Author: frankiezafe
 * 
 * Created on September 22, 2016, 8:30 PM
 */

#include "PBullet.h"

using namespace polymorph;
using namespace Ogre;
using namespace std;

// engine management
static PBullet* _engine = 0;
static PBulletEngineData _serialised_data;

// contact events management
static bool _callback_enabled = false;
static bool _callback_toggle = false;

static Ogre::Timer contactTimer;
static unsigned long nowMicros = 0;
static unsigned long lastContactMicros = 0;

static PBCSmap contact_solos;
static PBCPmap contact_pairs;
static PBCPLlist contact_pair_listeners;
static PBCPLiterator itb_cpl;
static PBCPLiterator ite_cpl;

static bool _contactsolo_enabled = false;
static bool _contactpair_enabled = false;

PBullet* PBullet::getBullet() {

    if (!_engine) {
        _engine = new PBullet();
    }
    return _engine;

}

void PBullet::enableContactEvents(const bool& activate) {

    if (_serialised_data.contact_events == activate) {
        return;
    } else {
        _serialised_data.contact_events = activate;
    }

    if (_serialised_data.contact_events != _callback_enabled) {
        _callback_toggle = true;
    }

}

void PBullet::configure(PBulletEngineData& data) {
    _serialised_data = data;
}

void PBullet::max_substeps(Ogre::uint32 mss) {
    _serialised_data.max_substeps = mss;
}

void PBullet::gravity(const Ogre::Vector3& g) {
    gravity(PUtil::convert(g));
}

void PBullet::gravity(const btVector3& g) {
    _serialised_data.gravity = g;
    if (!_engine) return;
    _engine->world->setGravity(g);
}

void PBullet::time_multiplier(btScalar tm) {
    _serialised_data.time_multiplier = tm;
}

void PBullet::verbose(bool v) {
    _serialised_data.verbose = v;
}

//void PBullet::start(Ogre::SceneManager* sm) {
//
//    getBullet();
//
//    if (sm) {
//        _engine->setSceneManager(sm);
//    }
//
//    if (_engine->sceneMgr) {
//        _engine->sceneMgr->addRenderQueueListener(_engine);
//        _engine->internal_clock = new Timer();
//    }
//    _engine->world->setGravity(_serialised_data.gravity);
//
//    enableContactEvents(_serialised_data.contact_events);
//
//    if (_serialised_data.verbose) cout << "PBullet started" << endl;
//
//}

void PBullet::start(Ogre::Root* r) {

    getBullet();

    if (r) {
        _engine->setRoot(r);
    }
    
    _engine->world->setGravity(_serialised_data.gravity);

    enableContactEvents(_serialised_data.contact_events);

    if (_serialised_data.verbose) cout << "PBullet started" << endl;

}

void PBullet::stop() {

    if (_engine && _engine->sceneMgr) {
    
        _engine->sceneMgr->removeRenderQueueListener(_engine);
        delete _engine->internal_clock;
        _engine->sceneMgr = 0;
        _engine->internal_clock = 0;
    
    } else if (_engine && _engine->root) {
        
        _engine->setRoot(0);
        delete _engine->internal_clock;
        _engine->internal_clock = 0;
        
    }

    if (_serialised_data.verbose) cout << "PBullet started" << endl;

}

void PBullet::update(
        const btScalar& delatime,
        Ogre::uint32 maxsteps
        ) {

    if (!_engine) {
        return;
    }

    if (maxsteps != _serialised_data.max_substeps) {
        _serialised_data.max_substeps = maxsteps;
    }

    _engine->_update(delatime);

}

btGeneric6DofSpringConstraint* PBullet::spring(
        PBulletExchange& e1,
        PBulletExchange& e2,
        PBulletSpringData* conf
        ) {

    //http://bulletphysics.org/Bullet/phpBB3/viewtopic.php?f=9&t=4785&view=previous

    if (!_engine) return 0;

    e1.body->setActivationState(DISABLE_DEACTIVATION);
    e2.body->setActivationState(DISABLE_DEACTIVATION);

    btTransform t1;
    btTransform t2;
    t1.setIdentity();
    t2.setIdentity();

    if (conf && conf->pos1_enabled) {
        t1.setOrigin(conf->pos1);
    }
    if (conf && conf->pos2_enabled) {
        t2.setOrigin(conf->pos2);
    }

    //    btScalar springRestLen(t1.getOrigin().distance(t2.getOrigin()));

    btGeneric6DofSpringConstraint* spring =
            new btGeneric6DofSpringConstraint(
            *e1.body, *e2.body,
            t1, t2, true);

    if (conf) {
        if (conf->limit_linear.enabled) {
            spring->setLinearLowerLimit(conf->limit_linear.min);
            spring->setLinearUpperLimit(conf->limit_linear.max);
        }
        if (conf->limit_angular.enabled) {
            spring->setAngularLowerLimit(conf->limit_angular.min);
            spring->setAngularUpperLimit(conf->limit_angular.max);
        }
    }

    // spring->setDamping(0, 0.1f); // add some damping
    // spring->setStiffness(0, 9.478f); // period 1 sec for !kG body
    // spring->setLinearUpperLimit(btVector3(springRestLen + springRange, 0., 0.));
    // spring->setLinearLowerLimit(btVector3(springRestLen - springRange, 0., 0.));

    _engine->world->addConstraint(spring, true);

    if (conf) {

        if (conf->limit_linear.enabled) {
            for (int i = 0; i < 3; ++i) {
                spring->enableSpring(i, true);
                spring->setParam(BT_CONSTRAINT_STOP_CFM, conf->stop_cfm, i);
                spring->setParam(BT_CONSTRAINT_STOP_ERP, conf->stop_erp, i);
                spring->setStiffness(i, conf->stiffness_linear);
                spring->setDamping(i, conf->damping_linear);
            }
        }

        if (conf->limit_angular.enabled) {
            for (int i = 3; i < 6; ++i) {
                spring->enableSpring(i, true);
                spring->setParam(BT_CONSTRAINT_STOP_CFM, conf->stop_cfm, i);
                spring->setParam(BT_CONSTRAINT_STOP_ERP, conf->stop_erp, i);
                spring->setStiffness(i, conf->stiffness_angular);
                spring->setDamping(i, conf->damping_angular);
            }
        }

        if (conf->enableEquilibrium) {
            spring->setEquilibriumPoint();
        }

    }


    return spring;

}

void PBullet::removeSpring(btGeneric6DofSpringConstraint* spring) {

    if (!_engine) return;

    _engine->world->removeConstraint(spring);

}

void PBullet::deactivation(PBulletExchange& e, bool enable) {

    if (!_engine) return;

    if (enable) {
        e.body->setActivationState(WANTS_DEACTIVATION);
    } else {
        e.body->setActivationState(DISABLE_DEACTIVATION);
    }

}

bool PBullet::remove(PBulletExchange& e) {

    if (!_engine) return false;

    if (e.callable) {
        if (
                _engine->callable_bodies.find(e.body) !=
                _engine->callable_bodies.end()
                ) {
            _engine->callable_bodies.erase(e.body);
        }
        _engine->collisionShapes.remove(e.collisionshape);
    }

    if (
            _engine->callable_bodies.find(e.body) !=
            _engine->callable_bodies.end()
            ) {
        _engine->callable_bodies.erase(e.body);
    }

    // removing rigidbody
    _engine->world->removeRigidBody(e.body);

    if (e.collisionshape) delete e.collisionshape;
    if (e.mstate) delete e.mstate;
    if (e.body) delete e.body;

    return false;

}

bool PBullet::create(PBulletExchange& e, PBulletRigidbodyUser* l) {

    if (!_engine) return false;
    return _engine->_create(e, l);

}

bool PBullet::isVerbose() {
    return _serialised_data.verbose;
}

Ogre::Vector3 PBullet::getGravity() {
    return PUtil::convert(_engine->world->getGravity());
}

void PBullet::rays(
        const Ogre::Camera* cam,
        const Ogre::Vector2 screenpos,
        PBulletRay*& res,
        uint16_t& res_count) {

    if (!_engine) return;

    Ogre::Ray r;
    cam->getCameraToViewportRay(screenpos.x, screenpos.y, &r);
    rays(
            cam->getDerivedPosition(),
            r.getPoint(cam->getFarClipDistance() / 2),
            res, res_count);
}

void PBullet::rays(
        const Ogre::Vector3 start,
        const Ogre::Vector3 end,
        PBulletRay*& res,
        uint16_t& res_count) {

    btVector3 from = PUtil::convert(start);
    btVector3 to = PUtil::convert(end);
    btCollisionWorld::AllHitsRayResultCallback cb(from, to);
    _engine->world->rayTest(from, to, cb);

    res_count = cb.m_collisionObjects.size();
    if (res_count == 0) {
        return;
    }

    std::map< float, PBulletRay > ordered_map;

    for (uint16_t i = 0; i < res_count; ++i) {
        Ogre::Vector3 hitp;
        PUtil::copy(cb.m_hitPointWorld[i], &hitp);
        float d = start.distance(hitp);
        ordered_map[ d ].point = hitp;
        if (
                _engine->callable_bodies.find(cb.m_collisionObjects[i]) !=
                _engine->callable_bodies.end()
                ) {
            _engine->callable_bodies[ cb.m_collisionObjects[i] ]->ray(&ordered_map[ d ]);
        }
        ordered_map[ d ].hit = true;
    }

    std::map< float, PBulletRay >::iterator itr = ordered_map.begin();
    std::map< float, PBulletRay >::iterator itre = ordered_map.end();
    res = new PBulletRay[ res_count ];
    for (uint16_t i = 0; itr != itre; ++itr, ++i) {
        res[i] = itr->second;
    }

}

void PBullet::ray(
        const Ogre::Camera* cam,
        const Ogre::Vector2 screenpos,
        PBulletRay* res
        ) {

    if (!_engine) return;

    Ogre::Ray r;
    cam->getCameraToViewportRay(screenpos.x, screenpos.y, &r);
    ray(
            cam->getDerivedPosition(),
            r.getPoint(cam->getFarClipDistance() / 2),
            res);

}

void PBullet::ray(
        const Ogre::Vector3 start,
        const Ogre::Vector3 end,
        PBulletRay* res
        ) {

    if (!_engine) return;

    btVector3 from = PUtil::convert(start);
    btVector3 to = PUtil::convert(end);
    btCollisionWorld::ClosestRayResultCallback cb(from, to);
    _engine->world->rayTest(from, to, cb);


    if (cb.hasHit()) {

        PUtil::copy(cb.m_hitPointWorld, &(res->point));
        if (
                _engine->callable_bodies.find(cb.m_collisionObject) !=
                _engine->callable_bodies.end()
                ) {
            _engine->callable_bodies[ cb.m_collisionObject ]->ray(res);
        }
        res->hit = true;
        return;

    }
    res->hit = false;

    return;

}

/*
 not static methods
 */

PBullet::PBullet() :
sceneMgr(0),
root(0)
{

    collisionConfiguration = new btDefaultCollisionConfiguration();
    dispatcher = new btCollisionDispatcher(collisionConfiguration);
    overlappingPairCache = new btDbvtBroadphase();
    solver = new btSequentialImpulseConstraintSolver;
    world = new btDiscreteDynamicsWorld(dispatcher, overlappingPairCache, solver, collisionConfiguration);
    world->setGravity(_serialised_data.gravity);

    // see http://www.bulletphysics.org/mediawiki-1.5.8/index.php?title=Collision_Callbacks_and_Triggers
    //    ghostObject = new btPairCachingGhostObject();
    world->getPairCache()->setInternalGhostPairCallback(new btGhostPairCallback());

    // registration of GImpact
    btCollisionDispatcher * dispatcher = static_cast<btCollisionDispatcher *> (world->getDispatcher());
    btGImpactCollisionAlgorithm::registerAlgorithm(dispatcher);

    if (_serialised_data.verbose) cout << "PBullet instanciated" << endl;

}

PBullet::~PBullet() {

    stop();

}

void PBullet::setSceneManager(Ogre::SceneManager* sm) {

    if (sceneMgr && sceneMgr != sm) {
        sceneMgr->removeRenderQueueListener(this);
    }
    sceneMgr = sm;

}

void PBullet::setRoot(Ogre::Root* r) {

    if ( root == r ) {
        return;
    }
    
    if (root && root != r) {
        root->removeFrameListener(this);
    }
    
    root = r;
    
    if (root) {
        root->addFrameListener(this);
        internal_clock = new Timer();
    }
    
}

bool PBullet::_create(PBulletExchange& e, PBulletRigidbodyUser* l) {

    bool bghost = PUtil::isGhost(e);
    bool bcube = PUtil::isBox(e);
    bool bsphere = PUtil::isSphere(e);
    bool bdynamic = PUtil::isDynamic(e);
    bool bstatic = PUtil::isStatic(e);
    bool bconvex = PUtil::isConvex(e);
    bool bconcave = PUtil::isConcave(e);
    bool bcompound = PUtil::isCompound(e);

    bool apply_scaling = false;

    if (bcube) {

        e.collisionshape = new btBoxShape(btVector3(0.5, 0.5, 0.5));
        e.collisionshape->setLocalScaling(e.size);
        e.volume = e.size.x() * e.size.y() * e.size.z();

    } else if (bsphere) {

        e.collisionshape = new btSphereShape(0.5);
        e.collisionshape->setLocalScaling(e.size);
        e.volume = Math::PI * 4.f / 3.f * e.size.x() * e.size.y() * e.size.z();

    } else {

        if (bconvex) {

            if (!loadConvexHull(e)) {
                if (_serialised_data.verbose) cerr << "PBullet::doCreate, failed to load Convex hull for dynamic" << endl;
                return false;
            }
            e.volume *= e.scale.length();
            apply_scaling = true;

        } else if (bcompound) {

            if (!loadCompound(e)) {
                if (_serialised_data.verbose) cerr << "PBullet::doCreate, failed to load Compound for dynamic" << endl;
                return false;
            }
            e.volume *= e.scale.length();
            apply_scaling = true;

        } else if (bconcave) {

            if (!loadGImpactMesh(e)) {
                if (_serialised_data.verbose) cerr << "PBullet::doCreate, failed to load GImpact mesh for dynamic" << endl;
                return false;
            }
            e.volume *= e.scale.length();
            apply_scaling = true;

        } else {

            if (_serialised_data.verbose) cerr << "PBullet::doCreate, failed to load dynamic : " << PUtil::str(e.type) << endl;
            return false;

        }

        //        } else if (bstatic) {
        //
        //            if (!loadTriangleMesh(e)) {
        //                if (_serialised_data.verbose) cerr << "PBullet::doCreate, failed to load triangle mesh for static" << endl;
        //                return false;
        //            }
        ////            e.collisionshape->setLocalScaling(e.scale);
        //            apply_scaling = true;
        //
        //        } else {
        //
        //            if (_serialised_data.verbose) cerr << "PBullet::doCreate, nothing prepared for this type... " << PUtil::str(e.type) << endl;
        //            return false;
        //
        //        }

    }

    collisionShapes.push_back(e.collisionshape);

    e.transform.setIdentity();
    e.transform.setOrigin(e.position);
    e.transform.setRotation(e.orientation);

    //rigidbody is dynamic if and only if mass is non zero, otherwise static
    if (bdynamic) {
        if (e.volume == 0) {
            e.volume = e.size.x() * e.size.y() * e.size.z();
        }
        e.mass = e.volume * e.density;
        e.collisionshape->calculateLocalInertia(e.mass, e.inertia);
        if (_serialised_data.verbose) cout << "PBullet::instanciate, dynamic body created, " << PUtil::str(e.type) << endl;
    } else {
        e.mass = 0;
        e.inertia.setValue(0, 0, 0);
        if (_serialised_data.verbose) cout << "PBullet::instanciate, static body created, " << PUtil::str(e.type) << endl;
    }

    e.mstate = new btDefaultMotionState(e.transform);

    //using motionstate is optional, it provides interpolation capabilities, and only synchronizes 'active' objects    
    btRigidBody::btRigidBodyConstructionInfo rbInfo(
            e.mass, e.mstate, e.collisionshape, e.inertia);
    e.body = new btRigidBody(rbInfo);
    e.body->setFriction(e.friction);
    e.body->setRestitution(e.restitution);

    if (bghost) {
        e.body->setCollisionFlags(
                e.body->getCollisionFlags() |
                btCollisionObject::CF_NO_CONTACT_RESPONSE
                );
    } else {
        e.body->setCollisionFlags(
                e.body->getCollisionFlags() |
                btCollisionObject::CF_CUSTOM_MATERIAL_CALLBACK);
    }

    // setting damping
    e.body->setDamping(e.damping_linear, e.damping_angular);

    // registering the listener
    e.body->setUserPointer(l);

    //add the body to the dynamics world
    world->addRigidBody(e.body);

    if (apply_scaling) {
        e.body->getCollisionShape()->setLocalScaling(e.scale);
        world->updateSingleAabb(e.body);
    }

    if (e.callable) {
        if (_serialised_data.verbose) cout << "PBullet::doCreate, btExchange is callable!, " << e.caller() << endl;
        callable_bodies[ e.body ] = &e;
    }

    return true;

}

void PBullet::applyTransforms() {

    btTransform trans;

    // updating body_listeners 
    for (int j = world->getNumCollisionObjects() - 1; j >= 0; j--) {
        btCollisionObject* obj = world->getCollisionObjectArray()[j];
        btRigidBody* body = btRigidBody::upcast(obj);
        if (body && body->getMotionState() && body->getUserPointer()) {
            body->getMotionState()->getWorldTransform(trans);
            ((PBulletRigidbodyUser*) body->getUserPointer())->bulletTransform(trans);
        }
    }

}

void PBullet::_update(const btScalar& delatime) {

    nowMicros = contactTimer.getMicroseconds();

    if (_callback_toggle && !_callback_enabled) {

        itb_cpl = contact_pair_listeners.begin();
        ite_cpl = contact_pair_listeners.end();
        world->setInternalTickCallback(tickCallback);
        _callback_enabled = true;
        _callback_toggle = false;

    } else if (_callback_toggle && _callback_enabled) {

        world->setInternalTickCallback(0);
        _callback_enabled = false;
        _callback_toggle = false;

    }

    world->stepSimulation(
            delatime * _serialised_data.time_multiplier,
            _serialised_data.max_substeps
            );

    applyTransforms();

    if (_serialised_data.contact_events) {
        brodcastContacts();
    }

}

void PBullet::preRenderQueues() {
    double deltatime = internal_clock->getMicroseconds() * 0.000001;
    internal_clock->reset();
    _update(deltatime);
}

bool PBullet::frameStarted(const Ogre::FrameEvent& evt) {
    double deltatime = internal_clock->getMicroseconds() * 0.000001;
    internal_clock->reset();
    _update(deltatime);
    return true;
}

bool PBullet::loadMeshData(const PBulletExchange& e) {

    if (!PUtil::meshExists(e.meshgroup, e.meshname)) return false;

    MeshPtr mp = MeshManager::getSingleton().load(e.meshname, e.meshgroup);

    if (!mp.get()) {
        if (_serialised_data.verbose) cout << "PBullet::loadMeshData, no '" << e.meshgroup << ":" << e.meshname << "'" << endl;
        return false;
    }

    btOgreMeshData* md = new btOgreMeshData();
    PUtil::meshData(mp.get(), (*md));

    //    mp->unload( );

    meshdata[ getMeshDataId(e) ] = md;

    return true;

}

btOgreMeshData* PBullet::getData(const PBulletExchange& e) {

    Ogre::IdString is = getMeshDataId(e);
    if (meshdata.find(is) == meshdata.end()) {
        if (!loadMeshData(e)) {
            return 0;
        }
    }
    return meshdata[ is ];

}

bool PBullet::loadCompound(PBulletExchange& e) {

    if (!PUtil::meshExists(e.meshgroup, e.meshname)) return false;

    btOgreMeshData* md = getData(e);
    if (!md) return false;

    btCompoundShape* cs = new btCompoundShape();

    //    if ( _serialised_data.verbose ) cout << "PBullet::loadCompound sub mesh num: " << md->mesh_num << endl;

    for (uint i = 0; i < md->mesh_num; ++i) {

        if (PUtil::isBtCube(md->mat_names[ i ])) {

            btBoxShape* box = 0;
            btTransform t;
            PUtil::meshToBtBox(md, i, box, t, e.volume);
            cs->addChildShape(t, box);

        } else if (PUtil::isBtSphere(md->mat_names[ i ])) {

            btSphereShape* sphere = 0;
            btTransform t;
            PUtil::meshToBtSphere(md, i, sphere, t, e.volume);
            cs->addChildShape(t, sphere);

        } else if (PUtil::isBtCylinder(md->mat_names[ i ])) {

            btCylinderShape* cylinder = 0;
            btTransform t;
            PUtil::meshToBtCylinder(md, i, cylinder, t, e.volume);
            cs->addChildShape(t, cylinder);

        } else if (PUtil::isBtCapsule(md->mat_names[ i ])) {

            btCapsuleShape* capsule = 0;
            btTransform t;
            PUtil::meshToBtCapsule(md, i, capsule, t, e.volume);
            cs->addChildShape(t, capsule);

        } else if (PUtil::isBtCone(md->mat_names[ i ])) {

            btConeShape* cone = 0;
            btTransform t;
            PUtil::meshToBtCone(md, i, cone, t, e.volume);
            cs->addChildShape(t, cone);

        } else {

            btConvexHullShape* hull = new btConvexHullShape();

            uint start = 0;
            uint limit = md->indices_count;

            if (i > 0) {
                start = md->indices_start_at[ i ];
            }
            if (i < md->mesh_num - 1) {
                limit = md->indices_start_at[ i + 1 ];
            }

            btVector3 v3;
            bool calcaabb = false;
            for (uint j = start; j < limit; j++) {
                if (j == limit - 1) calcaabb = true;
                v3.setValue(
                        md->vertices[j][0],
                        md->vertices[j][1],
                        md->vertices[j][2]
                        );
                hull->addPoint(v3, calcaabb);
            }
            hull->setUserPointer(0);

            btTransform t;
            t.setIdentity();
            cs->addChildShape(t, hull);

            btVector3 min;
            btVector3 max;
            hull->getAabb(t, min, max);

            btVector3 size = max - min;
            e.volume += size.x() * size.y() * size.z();

        }

    }

    e.collisionshape = cs;

    return true;

}

bool PBullet::loadConvexHull(PBulletExchange& e) {

    if (!PUtil::meshExists(e.meshgroup, e.meshname)) return false;

    btOgreMeshData* md = getData(e);
    if (!md) return false;

    btConvexHullShape* hull = new btConvexHullShape();
    btVector3 v3;
    bool calcaabb = false;
    for (int i = 0; i < md->vertices_count; i++) {
        if (i == md->vertices_count - 1) calcaabb = true;
        v3.setValue(
                md->vertices[i][0],
                md->vertices[i][1],
                md->vertices[i][2]
                );
        hull->addPoint(v3, calcaabb);
    }
    hull->setUserPointer(0);

    e.collisionshape = hull;

    return true;

}

bool PBullet::loadGImpactMesh(PBulletExchange& e) {

    if (!PUtil::meshExists(e.meshgroup, e.meshname)) return false;

    btOgreMeshData* md = getData(e);
    if (!md) return false;

    float* vertpos = new float[ md->vertices_count * 3 ];
    for (int i = 0; i < md->vertices_count; ++i) {
        for (int j = 0; j < 3; ++j) {
            vertpos[ (i * 3) + j ] = md->vertices[ i ][ j ];
        }
    }

    btIndexedMesh mesh;
    mesh.m_numVertices = md->vertices_count;
    mesh.m_numTriangles = md->indices_count / 3;
    mesh.m_triangleIndexBase = (const unsigned char *) md->indices;
    mesh.m_triangleIndexStride = sizeof (int) * 3;
    mesh.m_vertexBase = (const unsigned char *) vertpos;
    mesh.m_vertexStride = sizeof (float);

    // http://gimpact.sourceforge.net/reference_html/programmers_guide.html

    btTriangleIndexVertexArray* triav = 0;
    triav = new btTriangleIndexVertexArray();
    triav->addIndexedMesh(mesh);

    btGImpactMeshShape* trimesh = new btGImpactMeshShape(triav);
    trimesh->setLocalScaling(btVector3(1, 1, 1));
    trimesh->setMargin(0.1);
    trimesh->setUserPointer(0);
    trimesh->updateBound();

    btTransform t;
    btVector3 aabbmin;
    btVector3 aabbmax;
    trimesh->getAabb(t, aabbmin, aabbmax);

    e.collisionshape = trimesh;

    return true;

}

bool PBullet::loadTriangleMesh(PBulletExchange& e) {

    if (!PUtil::meshExists(e.meshgroup, e.meshname)) return false;

    btOgreMeshData* md = getData(e);
    if (!md) return false;

    // https://www.raywenderlich.com/53077/bullet-physics-tutorial-getting-started

    btTriangleMesh* trimesh = new btTriangleMesh();
    btVector3 vertexPos[3];

    int* ind = md->indices;

    for (int i = 0; i < md->indices_count; i++) {

        for (unsigned int i = 0; i < 3; ++i) {
            const Vector3 &vec = md->vertices[*ind];
            vertexPos[i][0] = vec.x;
            vertexPos[i][1] = vec.y;
            vertexPos[i][2] = vec.z;
            (*ind)++;
        }

        trimesh->addTriangle(vertexPos[0], vertexPos[1], vertexPos[2]);

    }

    //    btConvexShape* tmpshape = new btConvexTriangleMeshShape( trimesh );

    return true;

}

void PBullet::tickCallback(btDynamicsWorld *w, btScalar ts) {

    if (!_contactsolo_enabled && !_contactpair_enabled) {
        // nobody is listening...
        return;
    }

    int numManifolds = w->getDispatcher()->getNumManifolds();

    for (int i = 0; i < numManifolds; i++) {

        btPersistentManifold* manifold =
                w->getDispatcher()->getManifoldByIndexInternal(i);

        const btRigidBody* obA = btRigidBody::upcast(manifold->getBody0());
        const btRigidBody* obB = btRigidBody::upcast(manifold->getBody1());

        size_t i0 = (size_t) obA;
        size_t i1 = (size_t) obB;

        if (i0 > i1) {
            size_t tmp = i0;
            i0 = i1;
            i1 = tmp;
            const btRigidBody* objT = obA;
            obA = obB;
            obB = objT;
        }

        PBCPkey pair(i0, i1);
        PBCSkey p0 = static_cast<PBCSkey> (obA->getUserPointer());
        PBCSkey p1 = static_cast<PBCSkey> (obB->getUserPointer());

        int count = manifold->getNumContacts();

        for (int j = 0; j < count; j++) {

            btManifoldPoint& pt = manifold->getContactPoint(j);

            if (_contactpair_enabled) {
                if (!contact_pairs[pair].initialised()) {
                    //std::cout << "new contact: " << i0 << ":" << i1 << std::endl;
                    contact_pairs[pair].init(nowMicros, pair, p0, p1, pt);
                } else {
                    //std::cout << "persistent contact: " << i0 << ":" << i1 << std::endl;
                    contact_pairs[pair].add(nowMicros, pt);
                }
            }

        }

    }

}

void PBullet::brodcastContacts() {

    // SOLO events

    if (_contactsolo_enabled) {

        PBCSiterator itso = contact_solos.begin();
        PBCSiterator itsoe = contact_solos.end();

        std::vector< PBCSkey > zombie_solos;
        zombie_solos.reserve(contact_solos.size());

        for (; itso != itsoe; ++itso) {
            if (itso->second.last_update < lastContactMicros) {
                zombie_solos.push_back(itso->first);
            } else {
                bool first_time = itso->second.first_time;
                itso->second.render();
                if (first_time) {
                    //                std::cout << "new collision on "
                    //                        << _instance->getName(itso->second->object)
                    //                        << std::endl;
                } else {
                    //                std::cout
                    //                        << itso->second->object
                    //                        << " in collision since "
                    //                        << (itso->second->last_update - itso->second->start)
                    //                        << std::endl;
                }

                // emit event

                // and resetting it
                itso->second.reset();

            }
        }

        std::vector< PBCSkey >::iterator itz = zombie_solos.begin();
        std::vector< PBCSkey >::iterator itze = zombie_solos.end();
        for (; itz != itze; ++itz) {
            if (!(*itz)) {
                break;
            }
            contact_solos.erase((*itz));
        }

    }

    // PAIR events

    if (_contactpair_enabled) {

        PBCPiterator itpa = contact_pairs.begin();
        PBCPiterator itpae = contact_pairs.end();

        std::vector< PBCPkey > zombie_pairs;
        zombie_pairs.reserve(contact_pairs.size());

        PBCPLiterator looper;

        for (; itpa != itpae; ++itpa) {

            looper = contact_pair_listeners.begin();

            if (itpa->second.last_update() < nowMicros) {
                itpa->second.destroy(nowMicros);
            }

            if (itpa->second.destroyed()) {

                for (; looper != ite_cpl; ++looper) {
                    (*looper)->contactPairEnd(itpa->second);
                }
                //cout << "zombie!! " << itpa->first.first << ":" << itpa->first.second << std::endl;
                zombie_pairs.push_back(itpa->first);

            } else {

                itpa->second.render(nowMicros);

                if (itpa->second.first_time()) {

                    for (; looper != ite_cpl; ++looper) {
                        (*looper)->contactPairStart(itpa->second);
                    }

                } else {

                    for (; looper != ite_cpl; ++looper) {
                        (*looper)->contactPairUpdate(itpa->second);
                    }

                }

                // resetting for next run
                itpa->second.reset();

            }
        }

        std::vector< PBCPkey >::iterator itzp = zombie_pairs.begin();
        std::vector< PBCPkey >::iterator itzpe = zombie_pairs.end();
        for (; itzp != itzpe; ++itzp) {
            contact_pairs.erase((*itzp));
        }

    }

    lastContactMicros = contactTimer.getMicroseconds();

}

bool PBullet::addContactPairListener(PBulletContactPairListener* l) {

    if (!_callback_enabled) {
        enableContactEvents(true);
    }

    if (std::find(itb_cpl, ite_cpl, l) != ite_cpl) {
        return false;
    }
    contact_pair_listeners.push_back(l);
    itb_cpl = contact_pair_listeners.begin();
    ite_cpl = contact_pair_listeners.end();

    _contactpair_enabled = contact_pair_listeners.size() != 0;

    return true;

}

bool PBullet::removeContactPairListener(PBulletContactPairListener* l) {

    PBCPLiterator f =
            std::find(itb_cpl, ite_cpl, l);

    if (f == ite_cpl) {
        return false;
    }
    contact_pair_listeners.erase(f);
    itb_cpl = contact_pair_listeners.begin();
    ite_cpl = contact_pair_listeners.end();

    _contactpair_enabled = contact_pair_listeners.size() != 0;

    return true;

}