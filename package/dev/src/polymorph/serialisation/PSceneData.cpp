/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
 */

#include "PSceneData.h"

using namespace std;
using namespace Ogre;
using namespace polymorph;

PSceneData::PSceneData(uint32_t id) :
UID(_UID),
name(_name),
bullet(_bullet),
_UID(id) {
}

PSceneData::PSceneData(uint32_t id,  std::string& n ) :
UID(_UID),
name(_name),
bullet(_bullet),
_UID(id),
_name(n)
{
    
}

PSceneData::~PSceneData() {
    clear();
}

void PSceneData::clear() {

    nmap.clear();
    lmap.clear();
    //    smap.clear( );
    nodes.clear();
    lights.clear();
    //    sounds.clear( );
    PObjDataMapIter it = objmap.begin();
    PObjDataMapIter ite = objmap.end();
    for (; it != ite; ++it) {
        delete it->second;
    }
    objmap.clear();
    
    _bullet.reset();

}

bool PSceneData::push(const PNodeData& src) {

    if (objmap.find(src.name) != objmap.end()) {
        std::cout <<
                "PSceneData::push PNodeData, " <<
                "duplicated name! " <<
                src.name << std::endl;
        return false;
    }
    PNodeData* pnd = new PNodeData();
    (*pnd) = src;
    nodes.push_back(pnd);
    nmap[ src.name ] = pnd;
    objmap[ src.name ] = static_cast<PObjectData*> (pnd);

    if (pnd->repeat) {

        Vector3 pos = pnd->origin_pos;
        Euler ori = pnd->origin_orientation;
        Vector3 sca = pnd->origin_scale;

        Vector3 off_pos = pnd->repeat_pos;
        Euler off_ori = pnd->repeat_orientation;
        Vector3 off_sca = pnd->repeat_scale;


        for (uint32_t i = 1; i <= pnd->repeat_count; ++i) {

            PNodeData* cp = new PNodeData();
            (*cp) = src;

            std::stringstream ss;
            ss << ".";
            uint32_t max = 1e5;
            while (max > i * 10) {
                ss << "0";
                max /= 10;
            }
            ss << i;
            cp->name += ss.str();
            cp->origin_pos = pos + (off_pos * i);
            cp->origin_orientation = ori + (off_ori * i);
            cp->origin_scale = sca + (off_sca * i);
            cp->repeat = false; // avoiding infinite loop!
            cp->repeat_count = 0;
            cp->repeat_pos = Ogre::Vector3::ZERO;
            cp->repeat_scale = Ogre::Vector3::ZERO;
            cp->generated = true;

            nodes.push_back(cp);
            nmap[ cp->name ] = cp;
            objmap[ cp->name ] = static_cast<PObjectData*> (cp);

        }

    }

    return true;

}

bool PSceneData::push(const PLightData& src) {

    if (objmap.find(src.name) != objmap.end()) {
        std::cout <<
                "PSceneData::push PLightData, " <<
                "duplicated name! " <<
                src.name << std::endl;
        return false;
    }
    PLightData* pld = new PLightData();
    (*pld) = src;
    lights.push_back(pld);
    lmap[ src.name ] = pld;
    objmap[ src.name ] = static_cast<PObjectData*> (pld);
    return true;

}

//bool PSceneData::push( const PSoundData& src ) {
//
//    if ( objmap.find( src.name ) != objmap.end( ) ) {
//        std::cout <<
//                "PSceneData::push PSoundData, " <<
//                "duplicated name! " <<
//                src.name << std::endl;
//        return false;
//    }
//    PSoundData* psd = new PSoundData( );
//    ( *psd ) = src;
//    sounds.push_back( psd );
//    smap[ src.name ] = psd;
//    objmap[ src.name ] = static_cast < PObjectData* > ( psd );
//    return true;
//
//}

bool PSceneData::set(const PBulletEngineData& src) {
    
    _bullet = src;
    _bullet.overwrite = true;
    return true;
    
}