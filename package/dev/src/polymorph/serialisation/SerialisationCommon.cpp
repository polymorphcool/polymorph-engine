/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
*/

#include "SerialisationCommon.h"
#include "PUtil.h"

using namespace polymorph;

void SerialisationUtil::splitstring(
        const char* v,
        std::vector<std::string>& elems
        ) {
    std::stringstream ss;
    ss.str(v);
    std::string item;
    while (std::getline(ss, item, ',')) {
        elems.push_back(item);
    }
}

bool SerialisationUtil::decode( TiXmlElement* e, const char* attr, Ogre::Quaternion& dst) {

    if ( !exists( e, attr ) ) {
        dst = Ogre::Quaternion::IDENTITY;
        return false;
    }

    std::vector<std::string> elems;
    splitstring( e->Attribute( attr ), elems);

    if (elems.size() == 4) {

        dst = Ogre::Quaternion(
                Ogre::StringConverter::parseReal(elems[0]),
                Ogre::StringConverter::parseReal(elems[1]),
                Ogre::StringConverter::parseReal(elems[2]),
                Ogre::StringConverter::parseReal(elems[3])
                );
        return true;

    } else if (elems.size() == 3) {

        // a serie of 3 degree rotation has been given
        Ogre::Quaternion qx(
                Ogre::Radian( Ogre::Degree(
                Ogre::StringConverter::parseReal(elems[0])
                ).valueRadians() ),
                Ogre::Vector3::UNIT_X
                );
        Ogre::Quaternion qy(
                Ogre::Radian( Ogre::Degree(
                Ogre::StringConverter::parseReal(elems[1])
                ).valueRadians() ),
                Ogre::Vector3::UNIT_Y
                );
        Ogre::Quaternion qz(
                Ogre::Radian( Ogre::Degree(
                Ogre::StringConverter::parseReal(elems[2])
                ).valueRadians() ),
                Ogre::Vector3::UNIT_Z
                );
        dst = qx * qy * qz;
        return true;

    } else {

        std::cerr << "SerialisationUtil::decode Quaternion, invalid entry" << std::endl;
        dst = Ogre::Quaternion::IDENTITY;
        return false;

    }

}

bool SerialisationUtil::decode( TiXmlElement* e, const char* attr, Ogre::Vector4& dst) {

    if ( !exists( e, attr ) ) {
        dst = Ogre::Vector4::ZERO;
        return false;
    }

    std::vector<std::string> elems;
    splitstring( e->Attribute( attr ), elems);

    if (elems.size() != 4) {

        std::cerr << "SerialisationUtil::decode Vector4, invalid entry" << std::endl;
        dst = Ogre::Vector4::ZERO;
        return false;

    } else {

        dst = Ogre::Vector4(
                Ogre::StringConverter::parseReal(elems[0]),
                Ogre::StringConverter::parseReal(elems[1]),
                Ogre::StringConverter::parseReal(elems[2]),
                Ogre::StringConverter::parseReal(elems[3])
                );
        return true;

    }

}

bool SerialisationUtil::decode( TiXmlElement* e, const char* attr, Ogre::ColourValue& dst) {

    if ( !exists( e, attr ) ) {
        dst = Ogre::ColourValue::ZERO;
        return false;
    }

    std::vector<std::string> elems;
    splitstring( e->Attribute( attr ), elems);

    if (elems.size() == 4) {

        dst = Ogre::ColourValue(
                Ogre::StringConverter::parseReal(elems[0]),
                Ogre::StringConverter::parseReal(elems[1]),
                Ogre::StringConverter::parseReal(elems[2]),
                Ogre::StringConverter::parseReal(elems[3])
                );
        return true;

    } else if (elems.size() == 3) {
        
        dst = Ogre::ColourValue(
                Ogre::StringConverter::parseReal(elems[0]),
                Ogre::StringConverter::parseReal(elems[1]),
                Ogre::StringConverter::parseReal(elems[2]),
                1.0
                );
        return true;

    } else if (elems.size() == 2) {

        Ogre::Real w = Ogre::StringConverter::parseReal(elems[0]);
        dst = Ogre::ColourValue(
                w, w, w,
                Ogre::StringConverter::parseReal(elems[1])
                );
        return true;

    } else if (elems.size() == 1) {

        Ogre::Real w = Ogre::StringConverter::parseReal(elems[0]);
        dst = Ogre::ColourValue(w, w, w, 1.0);
        return true;

    } else {

        std::cerr << "SerialisationUtil::ColourValue, invalid entry" << std::endl;
        dst = Ogre::ColourValue::ZERO;
        return false;

    }

}

bool SerialisationUtil::decode( TiXmlElement* e, const char* attr, Ogre::Euler& dst) {

    if ( !exists( e, attr ) ) {
        dst = Ogre::Euler();
        return false;
    }

    std::vector<std::string> elems;
    splitstring( e->Attribute( attr ), elems);

    if (elems.size() != 3) {

        std::cerr << "SerialisationUtil::decode Euler, invalid entry" << std::endl;
        dst = Ogre::Euler();
        return false;

    } else {

        dst = Ogre::Euler(
                Ogre::Degree( Ogre::StringConverter::parseReal(elems[1]) ).valueRadians(),
                Ogre::Degree( Ogre::StringConverter::parseReal(elems[0]) ).valueRadians(),
                Ogre::Degree( Ogre::StringConverter::parseReal(elems[2]) ).valueRadians()
                );
        
        return true;

    }

}

bool SerialisationUtil::decode( TiXmlElement* e, const char* attr, Ogre::Vector3& dst) {

    if ( !exists( e, attr ) ) {
        dst = Ogre::Vector3::ZERO;
        return false;
    }

    std::vector<std::string> elems;
    splitstring( e->Attribute( attr ), elems);

    if (elems.size() != 3) {

        std::cerr << "SerialisationUtil::decode Vector3, invalid entry" << std::endl;
        dst = Ogre::Vector3::ZERO;
        return false;

    } else {

        dst = Ogre::Vector3(
                Ogre::StringConverter::parseReal(elems[0]),
                Ogre::StringConverter::parseReal(elems[1]),
                Ogre::StringConverter::parseReal(elems[2])
                );
        return true;

    }

}

bool SerialisationUtil::decode( TiXmlElement* e, const char* attr, Ogre::Vector2& dst) {

    if ( !exists( e, attr ) ) {
        dst = Ogre::Vector2::ZERO;
        return false;
    }

    std::vector<std::string> elems;
    splitstring( e->Attribute( attr ), elems);

    if (elems.size() != 2) {

        std::cerr << "SerialisationUtil::decode Vector2, invalid entry" << std::endl;
        dst = Ogre::Vector2::ZERO;
        return false;

    } else {

        dst = Ogre::Vector2(
                Ogre::StringConverter::parseReal(elems[0]),
                Ogre::StringConverter::parseReal(elems[1])
                );
        return true;

    }

}

bool SerialisationUtil::decode( TiXmlElement* e, const char* attr, bool& dst) {

    if ( !exists( e, attr ) ) {
        dst = false;
        return false;
    }

    std::string s = e->Attribute( attr );
    std::transform(s.begin(), s.end(), s.begin(), ::tolower);

    if (s.compare("1") == 0) {
        dst = true;
        return true;
    }

    if (s.compare("true") == 0) {
        dst = true;
        return true;
    }

    dst = false;
    return true;
    

}

bool SerialisationUtil::decode( TiXmlElement* e, const char* attr, uint32_t& dst) {

    if ( !exists( e, attr ) ) {
        dst = 0;
        return false;
    }
    dst = Ogre::StringConverter::parseReal( e->Attribute( attr ) );
    return true;
    
}

bool SerialisationUtil::decode( TiXmlElement* e, const char* attr, Ogre::Real& dst) {

    if ( !exists( e, attr ) ) {
        dst = 0;
        return false;
    }
    dst = Ogre::StringConverter::parseReal( e->Attribute( attr ) );
    return true;
    
}

bool SerialisationUtil::decode( TiXmlElement* e, const char* attr, std::string& dst) {

    if ( !exists( e, attr ) ) {
        return false;
    }
    dst = e->Attribute( attr );
    return true;
    
}

std::string SerialisationUtil::encode(const Ogre::Quaternion& v) {
    
    std::stringstream ss;
    ss << 
            v.getPitch(true).valueDegrees() << "," << 
            v.getYaw(true).valueDegrees() << "," << 
            v.getRoll(true).valueDegrees();
    return ss.str();
    
}

std::string SerialisationUtil::encode(const Ogre::Vector4& v) {
    std::stringstream ss;
    ss << v.x << "," << v.y << "," << v.z << "," << v.w;
    return ss.str();
}

std::string SerialisationUtil::encode(const Ogre::ColourValue& v) {
    std::stringstream ss;
    ss << v.r << "," << v.g << "," << v.b << "," << v.a;
    return ss.str();
}

std::string SerialisationUtil::encode(const Ogre::Vector3& v) {
    std::stringstream ss;
    ss << v.x << "," << v.y << "," << v.z;
    return ss.str();
}

std::string SerialisationUtil::encode(const Ogre::Vector2& v) {
    std::stringstream ss;
    ss << v.x << "," << v.y;
    return ss.str();
}

std::string SerialisationUtil::encode(const bool& v) {
    if (v) return "1";
    else return "0";
}