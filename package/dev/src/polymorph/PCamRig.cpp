/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
 */

/* 
 * File:   PCamRig.cpp
 * Author: frankiezafe
 * 
 * Created on October 11, 2016, 7:32 PM
 */

#include "PCamRig.h"

using namespace Ogre;
using namespace std;
using namespace polymorph;

static Vector3 local_up(0, 1, 0);

PCamRig::PCamRig() :
status(CRS_IDLE),
fallback_status(CRS_IDLE),
_sm(0),
_cam(0),
_origin(0),
_pivot(0),
world2origin_orientation(Matrix4::IDENTITY),
cam_mat(Matrix4::IDENTITY),
cam_mat_inverse(Matrix4::IDENTITY),
origin_eulers(Vector3::ZERO),
pivot_eulers(Vector3::ZERO),
origin_move_duration(0),
origin_move_time(0),
ori_pos_previous(Vector3::ZERO),
ori_pos_current(Vector3::ZERO),
ori_pos_target(Vector3::ZERO),
ori_dir_previous(Quaternion::IDENTITY),
ori_dir_current(Quaternion::IDENTITY),
ori_dir_target(Quaternion::IDENTITY),
cam_dist_duration(0),
cam_dist_time(0),
cam_dist_previous(0),
cam_dist_target(0),
_axis_x(Vector3::ZERO),
_axis_y(Vector3::ZERO),
_axis_z(Vector3::ZERO),
_node_x(0),
_node_y(0),
_node_z(0),
_ball_x(0),
_ball_y(0),
_ball_z(0) {
}

PCamRig::~PCamRig() {
}

void PCamRig::adaptfov() {
    // required to avoid using world up when lookat is rendered!
    local_up = _origin->getOrientation() * local_up;
    _cam->setFixedYawAxis(false, local_up);
    if (conf.dist.enabled && conf.fov.enabled) {
        float r = (_cam->getPosition().length() - conf.dist.min) / conf.dist.delta;
        float fy = conf.fov.min + (1 - r) * conf.fov.delta;
        _cam->setFOVy(Radian(fy));
    }
    _cam->lookAt(_origin->getPosition());
}

void PCamRig::updateWorldToOrign(void) {

    Vector3 cx = _cam->getRealRight();
    Vector3 cy = _cam->getRealUp();
    Vector3 cz = _cam->getRealDirection() * -1;

    world2origin_orientation[0][0] = cx.x;
    world2origin_orientation[1][0] = cx.y;
    world2origin_orientation[2][0] = cx.z;
    world2origin_orientation[0][1] = cy.x;
    world2origin_orientation[1][1] = cy.y;
    world2origin_orientation[2][1] = cy.z;
    world2origin_orientation[0][2] = cz.x;
    world2origin_orientation[1][2] = cz.y;
    world2origin_orientation[2][2] = cz.z;

    cam_mat = Matrix4(_cam->getDerivedOrientation());
    cam_mat.setTrans(_cam->getDerivedPosition());
    cam_mat_inverse = cam_mat.inverse();

}

void PCamRig::init(Ogre::SceneManager* sm, Ogre::Camera * cam) {

    if (_sm) return;

    _sm = sm;

    _origin = _sm->getRootSceneNode()->createChildSceneNode();
    _pivot = _origin->createChildSceneNode();
    _pivot->setInheritOrientation(true);

    if (cam) {
        _cam = cam;
        Vector3 cp = _cam->getDerivedPosition();
        _origin->setPosition(Vector3(0, cp.y, cp.z));
        _cam->setPosition(Ogre::Vector3(cp.x, 0, 0));
    } else {
        _cam = _sm->createCamera(conf.name);
        _cam->setPosition(Ogre::Vector3(conf.dist.init, 0, 0));
        _cam->setNearClipDistance(conf.nearfar.min);
        _cam->setFarClipDistance(conf.nearfar.max);
        _cam->setFOVy(Radian(conf.fov.init));
    }

    _cam->detachFromParent();
    _pivot->attachObject(_cam);

    // applying orientation
    pitch(conf.origin_pitch_range.init);
    yaw(conf.origin_yaw_range.init);
    roll(conf.origin_roll_range.init);
    pitch(conf.pivot_pitch_range.init, true);
    yaw(conf.pivot_yaw_range.init, true);
    roll(conf.pivot_roll_range.init, true);

    //    adaptfov();

    status = CRS_FREE;
    fallback_status = CRS_FREE;

    //build3DAxis();

}

void PCamRig::fit(Ogre::RenderWindow * rw) {
    if (!rw) return;
    _cam->setAspectRatio(Ogre::Real(rw->getWidth()) / Ogre::Real(rw->getHeight()));
    adaptfov();
}

void PCamRig::fit(const Ogre::Vector2& window_size) {
    _cam->setAspectRatio(window_size.x / window_size.y);
    adaptfov();
}

//void PCamRig::moveGapDecoy( std::vector<Ogre::Vector3>& vs ) {
//    if ( vs.empty() ) return;
//    Vector3 v = vs[0];
//    gap_decoy->setPosition( _origin->getPosition() + v );
//}

void PCamRig::update(void) {

    if (!_sm) return;

    updateWorldToOrign();
    update3DAxis();

    //    if (gcf && _cam_pivot) {
    //        if (
    //                gcf->axis_status[ input::GCB_LEFTSTICK ] == input::IS_ACTIVATED ||
    //                gcf->axis_status[ input::GCB_LEFTSTICK ] == input::IS_MODIFIED ||
    //                gcf->axis_status[ input::GCB_LEFTSTICK ] == input::IS_ACTIVE
    //                ) {
    //            Vector3 lfs = Vector3(
    //                    gcf->axis[ input::GCB_LEFTSTICK ].x * 5,
    //                    gcf->axis[ input::GCB_LEFTSTICK ].y * -5,
    //                    0);
    //            lfs = world2origin_orientation * lfs;
    //            leftstick_decoy->setPosition(_origin->_getDerivedPosition() + lfs);
    //        } else {
    //            leftstick_decoy->setPosition(_origin->_getDerivedPosition());
    //        }
    //    }


}


//void PCamRig::frame( input::GameControllerFrame* gcf, Ogre::FrameEvent& evt ) {
//
//    if ( !_sm ) return;
//    
//    if ( gcf && _cam_pivot ) {
//        if (
//            gcf->axis_status[ input::GCB_RIGHTSTICK ] == input::IS_ACTIVATED || 
//            gcf->axis_status[ input::GCB_RIGHTSTICK ] == input::IS_MODIFIED || 
//            gcf->axis_status[ input::GCB_RIGHTSTICK ] == input::IS_ACTIVE || 
//            gcf->axis_status[ input::GCB_TRIGGER ] == input::IS_ACTIVATED || 
//            gcf->axis_status[ input::GCB_TRIGGER ] == input::IS_MODIFIED ||
//            gcf->axis_status[ input::GCB_TRIGGER ] == input::IS_ACTIVE 
//        ) {
//            
//            Quaternion q( Radian( gcf->axis[ input::GCB_RIGHTSTICK ].x * evt.timeSinceLastFrame * 5 ), Vector3( 0,1,0 ) );
//            q = _cam_pivot->getOrientation() * q;
//            _cam_pivot->setOrientation( q );
//            
//            //_cam_pivot->yaw( Radian( gcf->axis[ input::GCB_RIGHTSTICK ].x * evt.timeSinceLastFrame * 5 ) );
//            
//            Vector3 cp = cam->getPosition();
//            
//            // zoom is deactivated when cam rig status is not stable => network should be in status NS_GAP (puzzle solving)
//            if ( status == CRS_FREE ) {
//                if ( gcf->axis[ input::GCB_TRIGGER ].x != 0 && gcf->axis[ input::GCB_TRIGGER ].y == 0 ) {            
//                    cp = cam->getPosition();
//                    cp *= 1 + gcf->axis[ input::GCB_TRIGGER ].x * evt.timeSinceLastFrame;
//                    if ( cp.length() > conf.cam_distance.y ) {
//                        cp = cp.normalisedCopy() * conf.cam_distance.y;
//                    }
//                } else if ( gcf->axis[ input::GCB_TRIGGER ].x == 0 && gcf->axis[ input::GCB_TRIGGER ].y != 0 ) {            
//                    cp = cam->getPosition();
//                    cp *= 1 - gcf->axis[ input::GCB_TRIGGER ].y * evt.timeSinceLastFrame;
//                    if ( cp.length() < conf.cam_distance.x ) {
//                        cp = cp.normalisedCopy() * conf.cam_distance.x;
//                    }
//                }
//            }
//            
//            cp.y += -gcf->axis[ input::GCB_RIGHTSTICK ].y * 20 * evt.timeSinceLastFrame;
//            if ( cp.y > conf.cam_max_y ) cp.y = conf.cam_max_y;
//            else if ( cp.y < -conf.cam_max_y ) cp.y = -conf.cam_max_y;
//            cam->setPosition( cp );
//            adaptfov();
//            
//        }
//        
//    }
//    
//    updateWorldToOrign();
//    update3DAxis();
//
//    if ( gcf && _cam_pivot ) {
//        if (
//            gcf->axis_status[ input::GCB_LEFTSTICK ] == input::IS_ACTIVATED || 
//            gcf->axis_status[ input::GCB_LEFTSTICK ] == input::IS_MODIFIED ||
//            gcf->axis_status[ input::GCB_LEFTSTICK ] == input::IS_ACTIVE
//        ) {
//            Vector3 lfs = Vector3( 
//                    gcf->axis[ input::GCB_LEFTSTICK ].x * 5, 
//                    gcf->axis[ input::GCB_LEFTSTICK ].y * -5,
//                    0 );
//            lfs = world2origin_orientation * lfs;
//            leftstick_decoy->setPosition( _origin->_getDerivedPosition() + lfs );
//        } else {
//            leftstick_decoy->setPosition( _origin->_getDerivedPosition() );
//        }
//    }
//    
//// ************** TEMP **************
////    cam_decoy->setPosition( cam->getDerivedPosition() );
////    cam_decoy->setOrientation( cam->getDerivedOrientation() );
////    Vector3 rel = Vector3( 5,20,0 ) - cam->getDerivedPosition();
////    Matrix4 relm = Matrix4::IDENTITY;
////    relm.setTrans( rel );
////    relm = cam_mat_inverse * relm * cam_mat;
////    rel = relm.getTrans();
////    fixed_decoy->setPosition( rel );
//    
//    // animations
//    if ( status == CRS_TRANSITION ) {
//        
//        bool alldone = true;
//        
//        if ( _origin_move_time > 0 ) {
//            alldone = false;
//            _origin_move_time -= evt.timeSinceLastFrame;
//            if ( _origin_move_time <= 0 ) {
//                ori_pos_current = ori_pos_target;
//                ori_dir_current = ori_dir_target;
//                alldone = true;
//            } else {
//                Real pc = sinInterpolation( _origin_move_duration, _origin_move_time );
//                ori_pos_current = ori_pos_previous + ( ori_pos_target - ori_pos_previous ) * pc;
//                ori_dir_current = Quaternion::Slerp( pc, ori_dir_previous, ori_dir_target );
//            }
//            _origin->setPosition( ori_pos_current );
//            _origin->setOrientation( ori_dir_current );
//        }
//        
//        if ( cam_dist_time > 0 ) {
//            alldone = false;
//            Vector3 cp = cam->getPosition();
//            Real cam_dist = cp.length();
//            cam_dist_time -= evt.timeSinceLastFrame;
//            if ( cam_dist_time <= 0 ) {
//                cam_dist = cam_dist_target;
//                alldone = true;
//            } else {
//                cam_dist = cam_dist_previous + ( cam_dist_target - cam_dist_previous ) * sinInterpolation( cam_dist_duration, cam_dist_time );
//            }
//            cp.normalise();
//            cp *= cam_dist;
//            cam->setPosition( cp );
//            adaptfov();
//        }
//        
//        if ( alldone ) {
//            status = fallback_status;        
//        }
//    
//    }
//    
//}

void PCamRig::update3DAxis() {

    if (!_ball_x) {
        return;
    }

    Vector3 p;
    p = world2origin_orientation * _axis_x;
    _ball_x->setPosition(_origin->getPosition() + p * 3);
    p = world2origin_orientation * _axis_y;
    _ball_y->setPosition(_origin->getPosition() + p * 3);
    p = world2origin_orientation * _axis_z;
    _ball_z->setPosition(_origin->getPosition() + p * 3);
    return;

}

void PCamRig::debug(const bool& enable) {

    if (!_node_x && enable) {
        build3DAxis();
    } else if (_node_x && !enable) {
        delete _node_x;
        delete _node_y;
        delete _node_z;
        delete _ball_x;
        delete _ball_y;
        delete _ball_z;
    }

}

void PCamRig::build3DAxis(void) {

    _axis_x = Vector3(1, 0, 0);
    _axis_y = Vector3(0, 1, 0);
    _axis_z = Vector3(0, 0, 1);

    // X, Y & Z materials
    MaterialManager& matMgr = MaterialManager::getSingleton();
    Technique* mtech;
    Pass* mpass;

    MaterialPtr mat = matMgr.create("mat_axis_X", "General");
    mtech = mat->getTechnique(0);
    mpass = mtech->getPass(0);
    mpass->setLightingEnabled(true);
    mpass->setDiffuse(1, 0, 0, 1);
    mpass->setEmissive(1, 0, 0);
    mat = matMgr.create("mat_axis_Y", "General");
    mtech = mat->getTechnique(0);
    mpass = mtech->getPass(0);
    mpass->setLightingEnabled(true);
    mpass->setDiffuse(0, 1, 0, 1);
    mpass->setEmissive(0, 1, 0);
    mat = matMgr.create("mat_axis_Z", "General");
    mtech = mat->getTechnique(0);
    mpass = mtech->getPass(0);
    mpass->setLightingEnabled(true);
    mpass->setDiffuse(0, 0, 1, 1);
    mpass->setEmissive(0, 0, 1);

    Ogre::Entity * entity;
    entity = _sm->createEntity("Prefab_Cube");
    entity->setMaterialName("mat_axis_X");
    _node_x = _origin->createChildSceneNode(SCENE_DYNAMIC, Vector3(0, 0, 0));
    _node_x->setScale(0.0002, 0.06, 0.0002);
    _node_x->attachObject(entity);
    _node_x->setName("axis_X");
    _node_x->setInheritOrientation(false);
    _node_x->setOrientation(Quaternion(Radian(Math::HALF_PI), Vector3(1, 0, 0)));
    entity = _sm->createEntity("Prefab_Sphere");
    entity->setMaterialName("mat_axis_X");
    _ball_x = _sm->getRootSceneNode()->createChildSceneNode(SCENE_DYNAMIC, Vector3(0, 0, 0));
    _ball_x->setScale(0.001, 0.001, 0.001);
    _ball_x->attachObject(entity);
    _ball_x->setName("ball_X");
    _ball_x->setInheritOrientation(false);

    entity = _sm->createEntity("Prefab_Cube");
    entity->setMaterialName("mat_axis_Y");
    _node_y = _origin->createChildSceneNode(SCENE_DYNAMIC, Vector3(0, 0, 0));
    _node_y->setScale(0.0002, 0.06, 0.0002);
    _node_y->attachObject(entity);
    _node_y->setName("axis_Y");
    _node_y->setInheritOrientation(false);
    _node_y->setOrientation(Quaternion(Radian(Math::HALF_PI), Vector3(0, 1, 0)));
    entity = _sm->createEntity("Prefab_Sphere");
    entity->setMaterialName("mat_axis_Y");
    _ball_y = _sm->getRootSceneNode()->createChildSceneNode(SCENE_DYNAMIC, Vector3(0, 0, 0));
    _ball_y->setScale(0.001, 0.001, 0.001);
    _ball_y->attachObject(entity);
    _ball_y->setName("ball_Y");
    _ball_y->setInheritOrientation(false);

    entity = _sm->createEntity("Prefab_Cube");
    entity->setMaterialName("mat_axis_Z");
    _node_z = _origin->createChildSceneNode(SCENE_DYNAMIC, Vector3(0, 0, 0));
    _node_z->setScale(0.0002, 0.06, .0002);
    _node_z->attachObject(entity);
    _node_z->setName("axis_Z");
    _node_z->setInheritOrientation(false);
    _node_z->setOrientation(Quaternion(Radian(Math::HALF_PI), Vector3(0, 0, 1)));
    entity = _sm->createEntity("Prefab_Sphere");
    entity->setMaterialName("mat_axis_Z");
    _ball_z = _sm->getRootSceneNode()->createChildSceneNode(SCENE_DYNAMIC, Vector3(0, 0, 0));
    _ball_z->setScale(0.001, 0.001, 0.001);
    _ball_z->attachObject(entity);
    _ball_z->setName("ball_Z");
    _ball_z->setInheritOrientation(false);

    mat = matMgr.create("mat_decoy", "General");
    mtech = mat->getTechnique(0);
    mpass = mtech->getPass(0);
    mpass->setLightingEnabled(true);
    //    mpass->setPolygonMode( PM_WIREFRAME );
    mpass->setDepthCheckEnabled(false);
    mpass->setDiffuse(0, 0, 1, 1);
    mpass->setEmissive(1, 0, 1);
    entity = _sm->createEntity("Prefab_Sphere");
    entity->setMaterialName("mat_decoy");
    //    leftstick_decoy = _sm->getRootSceneNode()->createChildSceneNode( SCENE_DYNAMIC, Vector3( 0,0,0 ) );
    //    leftstick_decoy->setScale( 0.005, 0.005, 0.005 );
    //    leftstick_decoy->attachObject( entity );
    //    leftstick_decoy->setName( "leftstick_decoy" );
    //    leftstick_decoy->setInheritOrientation( false );

    //    entity = _sm->createEntity( "Prefab_Sphere" );
    //    entity->setMaterialName( "mat_axis_X" );
    //    cam_decoy = _sm->getRootSceneNode()->createChildSceneNode( SCENE_DYNAMIC, Vector3( 0,0,0 ) );
    //    fixed_decoy = cam_decoy->createChildSceneNode( SCENE_DYNAMIC, Vector3( 0,0,0 ) );
    //    fixed_decoy->setScale( 0.015, 0.015, 0.015 );
    //    fixed_decoy->attachObject( entity );

}

void PCamRig::move(Ogre::Vector3 p, bool campivot) {

    if (!_origin) return;
    if (campivot) _pivot->setPosition(p);
    else _origin->setPosition(p);

}

void PCamRig::pitchRange(Ogre::Real pc, bool campivot) {
    if (campivot) {
        pitch(conf.pivot_pitch_range.lerp(pc, false), campivot);
    } else {
        pitch(conf.origin_pitch_range.lerp(pc, false), campivot);
    }
}

void PCamRig::yawRange(Ogre::Real pc, bool campivot) {
    if (campivot) {
        yaw(conf.pivot_yaw_range.lerp(pc, false), campivot);
    } else {
        yaw(conf.origin_yaw_range.lerp(pc, false), campivot);
    }
}

void PCamRig::rollRange(Ogre::Real pc, bool campivot) {
    if (campivot) {
        roll(conf.pivot_roll_range.lerp(pc, false), campivot);
    } else {
        roll(conf.origin_roll_range.lerp(pc, false), campivot);
    }
}

void PCamRig::pitch(Ogre::Real a, bool campivot) {
    if (!_origin) return;
    orient(a, true, false, false, campivot);
}

void PCamRig::yaw(Ogre::Real a, bool campivot) {
    if (!_origin) return;
    orient(a, false, true, false, campivot);
}

void PCamRig::roll(Ogre::Real a, bool campivot) {
    if (!_origin) return;
    orient(a, false, false, true, campivot);
}

void PCamRig::orient(Ogre::Real a, bool x, bool y, bool z, bool campivot) {


    Ogre::SceneNode* target;
    Ogre::Vector3* eulers;

    if (campivot) {
        eulers = &pivot_eulers;
        target = _pivot;
    } else {
        eulers = &origin_eulers;
        target = _origin;
    }

    if (x) eulers->x = Ogre::Degree(a).valueRadians();
    if (y) eulers->y = Ogre::Degree(a).valueRadians();
    if (z) eulers->z = Ogre::Degree(a).valueRadians();

    //    polymorph::PUtil::print( *eulers );

    Ogre::Quaternion qx(Ogre::Radian(eulers->x), Ogre::Vector3::UNIT_X);
    Ogre::Quaternion qy(Ogre::Radian(eulers->y), Ogre::Vector3::UNIT_Y);
    Ogre::Quaternion qz(Ogre::Radian(eulers->z), Ogre::Vector3::UNIT_Z);

    target->setOrientation(qx * qy * qz);

    adaptfov();

}