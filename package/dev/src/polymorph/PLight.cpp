/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ___________________________________( ^3^)_____________________________________
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 */

/* 
 * File:   PLight.cpp
 * Author: frankiezafe
 * 
 * Created on November 20, 2016, 8:08 PM
 */

#include "PLight.h"

using namespace std;
using namespace Ogre;
using namespace polymorph;

PLight::PLight( ) :
PObject<Ogre::Light*>( ),
_pivot( 0 ),
_debug_node( 0 ),
_debug_entity( 0 ),
_debug_node_dir( 0 ),
_debug_entity_dir( 0 ),
_ltype( -1 ),
_shadows( false ) {
    
    _bt_exchange = static_cast<PBulletExchange*> (&_light_exchange);
    
}

PLight::PLight(
        Ogre::SceneManager* sceneMgr,
        const PLightData* src ) :
PObject<Ogre::Light*>( ),
_pivot( 0 ),
_debug_node( 0 ),
_debug_entity( 0 ),
_debug_node_dir( 0 ),
_debug_entity_dir( 0 ),
_ltype( -1 ),
_shadows( false ) {
    
    UID = 0;
    _world_mat = Matrix4::IDENTITY;
    _world_scale = Vector3::UNIT_SCALE;
    
    _name = src->name;
    
    switch( src->type ) {
        case Light::LT_DIRECTIONAL:
            sun( sceneMgr );
            break;
        case Light::LT_POINT:
            point( sceneMgr );
            break;
        case Light::LT_SPOTLIGHT:
            spot( sceneMgr );
            break;
        default:
            return;
    }
    
    visible(src->visible);
    debug(src->debug);
    move(src->origin_pos, false);
    scale(src->origin_scale, false);
    orientation(src->origin_orientation, false);

    diffuse( src->diffuse );
    specular( src->specular );
    
    _shadows = src->shadows;
    range( src->range );
    
    if (src->bullet_type != BT_NONE) {
        
        _light_exchange.density = src->bullet_density;
        _light_exchange.friction = src->bullet_friction;
        _light_exchange.restitution = src->bullet_restitution;
        _light_exchange.damping_linear = src->bullet_damping_linear;
        _light_exchange.damping_angular = src->bullet_damping_angular;
        _light_exchange.set(this);
        _light_exchange.set(this);

        if (PUtil::isComplex(src->bullet_type)) {
            physics(src->bullet_type, src->bullet_group, src->bullet_file);
        } else {
            physics(src->bullet_type);
        }
        if (PUtil::isDynamic(src->bullet_type) && src->bullet_restless) {
            restless(true);
        }

    }
    
    _bt_exchange = static_cast<PBulletExchange*> (&_light_exchange);
    
}

PLight::~PLight( ) {

    clear( );

}

void PLight::clearDebug( ) {

    if ( _sceneMgr ) {
        if ( _debug_node )
            _sceneMgr->destroySceneNode( _debug_node );
        if ( _debug_entity )
            _sceneMgr->destroyEntity( _debug_entity );
        if ( _debug_node_dir )
            _sceneMgr->destroySceneNode( _debug_node_dir );
        if ( _debug_entity_dir )
            _sceneMgr->destroyEntity( _debug_entity_dir );
    }
    _debug_node = 0;
    _debug_entity = 0;

}

void PLight::clear( ) {

    clearDebug( );

    // this must be done before deleting Ogre objects!
    // unlinking all childrens
    clearChildren();
    
    if ( _sceneMgr ) {
        if ( _object )
            _sceneMgr->destroyLight( _object );
        if ( _pivot )
            _sceneMgr->destroySceneNode( _pivot );
    }

    _ltype = -1;
    _shadows = false;
    _bt_exchange = 0;
    
    // backfire protection on deletion
    if ( _origin ) PObject<Ogre::Light*>::clear( );

}

void PLight::createLight( ) {

    _origin = _sceneMgr->getRootSceneNode( )->createChildSceneNode( );
    _pivot = _origin->createChildSceneNode( );
    _object = _sceneMgr->createLight( );
    _object->detachFromParent( );
    _pivot->attachObject( _object );
    _idobject = static_cast < Ogre::IdObject* > ( _object );

    naming( );

    _pivot->setName( _name + "_pivot" );
    
    _visible = true;

}

void PLight::diffuse( Ogre::Real red, Ogre::Real green, Ogre::Real blue ) {
    diffuse( Ogre::ColourValue( red, green, blue ) );
}

void PLight::diffuse( const Ogre::ColourValue& color ) {
    if ( !_object ) return;
    _object->setDiffuseColour( color );
}

void PLight::specular( Ogre::Real red, Ogre::Real green, Ogre::Real blue ) {
    specular( Ogre::ColourValue( red, green, blue ) );
}

void PLight::specular( const Ogre::ColourValue& color ) {
    if ( !_object ) return;
    _object->setSpecularColour( color );
}

void PLight::range( Ogre::Real r ) {

    _object->setAttenuation(
            r,
            _object->getAttenuationConstant( ),
            _object->getAttenuationLinear( ),
            _object->getAttenuationQuadric( )
            );

    if ( _debug_node_dir ) {
        _debug_node_dir->setScale( Ogre::Vector3( 0.001, r * 0.01f, 0.001 ) );
        _debug_node_dir->setPosition( 0, r * -0.5f, 0 );
    }

}

bool PLight::point( Ogre::SceneManager* sceneMgr ) {

    if ( _origin ) return false;
    _sceneMgr = sceneMgr;
    createLight( );
    _object->setType( Light::LT_POINT );
    _ltype = ( int ) _object->getType( );
    return true;

}

bool PLight::sun( Ogre::SceneManager* sceneMgr ) {

    if ( _origin ) return false;
    _sceneMgr = sceneMgr;
    createLight( );
    _object->setType( Light::LT_DIRECTIONAL );
    _object->setDirection( Ogre::Vector3::NEGATIVE_UNIT_Y );
    _ltype = ( int ) _object->getType( );
    return true;

}

bool PLight::spot( Ogre::SceneManager* sceneMgr ) {

    if ( _origin ) return false;
    _sceneMgr = sceneMgr;
    createLight( );
    _object->setType( Light::LT_SPOTLIGHT );
    _object->setDirection( Ogre::Vector3::NEGATIVE_UNIT_Y );
    _shadows = true;
    _ltype = ( int ) _object->getType( );
    return true;

}

void PLight::visible( bool enable ) {
    
    if ( !_origin ) return;
    if (_visible != enable) {
        _origin->setVisible(enable, true);
        _visible = enable;
    }
}

void PLight::debug( bool enable ) {

    if (
            !_sceneMgr ||
            ( enable && _debug_node ) ||
            ( !enable && !_debug_node )
            ) return;

    _debugging = enable;
    
    if ( !enable ) {
        clearDebug( );
        return;
    }

    _debug_node = _origin->createChildSceneNode( );
    _debug_node->setName( _name + "_debug_node" );

    _debug_entity = _sceneMgr->createEntity( "Prefab_Sphere" );
    _debug_entity->setName( _name + "_debug_entity" );
    _debug_entity->setMaterial( PUtil::materialDebugLight( ) );

    _debug_node->attachObject( _debug_entity );
    _debug_node->setScale( ONE_PERCENT * 2 );

    if (
            _ltype == Light::LT_DIRECTIONAL ||
            _ltype == Light::LT_SPOTLIGHT
            ) {

        _debug_node_dir = _origin->createChildSceneNode( );
        _debug_node_dir->setName( _name + "_debug_node_dir" );

        _debug_entity_dir = _sceneMgr->createEntity( "Prefab_Cube" );
        _debug_entity_dir->setName( _name + "_debug_entity_dir" );
        _debug_entity_dir->setMaterial( PUtil::materialDebugLight( ) );

        _debug_node_dir->attachObject( _debug_entity_dir );
        _debug_node_dir->setScale( Ogre::Vector3( 0.001, _object->getAttenuationRange( ) * 0.01f, 0.001 ) );
        _debug_node_dir->setPosition( 0, _object->getAttenuationRange( ) * -0.5f, 0 );

    }

}