/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ___________________________________( ^3^)_____________________________________
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 */

/* 
 * File:   PNode.cpp
 * Author: frankiezafe
 * 
 * Created on September 20, 2016, 7:38 PM
 */

#include "PNode.h"

using namespace std;
using namespace Ogre;
using namespace polymorph;

PNode::PNode() :
PObject<Ogre::SceneNode*>(),
_internal(false),
_cube(false),
_sphere(false),
_plane(false),
_empty(false),
_entity_num(0),
_entities(0),
_skeleton(0),
_mat_num(0),
_default_mat(0),
_materials(0),
_debug_node(0),
_debug_entity(0),
_debug_skeleton_root(0) {

    UID = 0;
    _world_mat = Matrix4::IDENTITY;
    _world_scale = Vector3::UNIT_SCALE;
    _bt_exchange = static_cast<PBulletExchange*> (&_node_exchange);

}

PNode::PNode(PNode& src) :
PObject<Ogre::SceneNode*>(),
_internal(false),
_cube(false),
_sphere(false),
_plane(false),
_empty(false),
_entity_num(0),
_entities(0),
_skeleton(0),
_mat_num(0),
_default_mat(0),
_materials(0),
_debug_node(0),
_debug_entity(0),
_debug_skeleton_root(0) {

    cout << "PNode::PNode( PNode& src ), IMPLEMENT!!!!" << endl;
    _bt_exchange = static_cast<PBulletExchange*> (&_node_exchange);
    _generated = src.generated();
}

PNode::PNode(
        Ogre::SceneManager* sceneMgr,
        const PNodeData* src) :
PObject<Ogre::SceneNode*>(),
_internal(false),
_cube(false),
_sphere(false),
_plane(false),
_empty(false),
_entity_num(0),
_entities(0),
_skeleton(0),
_mat_num(0),
_default_mat(0),
_materials(0),
_debug_node(0),
_debug_entity(0),
_debug_skeleton_root(0) {

    UID = 0;
    _world_mat = Matrix4::IDENTITY;
    _world_scale = Vector3::UNIT_SCALE;
    _bt_exchange = static_cast<PBulletExchange*> (&_node_exchange);
    _generated = src->generated;

    _name = src->name;

    _internal = src->internal;
    if (_internal) {
        if (src->cube) {
            cube(sceneMgr);
        } else if (src->sphere) {
            sphere(sceneMgr);
        } else if (src->plane) {
            plane(sceneMgr);
        } else if (src->empty) {
            empty(sceneMgr);
        }
    } else {
        load(sceneMgr, src->mesh_group, src->mesh_file);
    }

    visible(src->visible);
    debug(src->debug);
    move(src->origin_pos, false);
    move(src->node_pos, true);
    scale(src->origin_scale, false);
    scale(src->node_scale, true);
    orientation(src->origin_orientation, false);
    orientation(src->node_orientation, true);

    if (!src->mat_group.empty() && !src->mat_name.empty()) {
        material(src->mat_group, src->mat_name, true);
    }

    if (src->bullet_type != BT_NONE) {
        
        _node_exchange.density = src->bullet_density;
        _node_exchange.friction = src->bullet_friction;
        _node_exchange.restitution = src->bullet_restitution;
        _node_exchange.damping_linear = src->bullet_damping_linear;
        _node_exchange.damping_angular = src->bullet_damping_angular;
        _node_exchange.set(this);

        if (PUtil::isComplex(src->bullet_type)) {
            physics(src->bullet_type, src->bullet_group, src->bullet_file);
        } else {
            physics(src->bullet_type);
        }
        if (PUtil::isDynamic(src->bullet_type) && src->bullet_restless) {
            restless(true);
        }

    }

}

PNode::~PNode() {

    clear();

}

void PNode::clearDebugMesh() {

    if (_sceneMgr) {
        if (_debug_node)
            _sceneMgr->destroySceneNode(_debug_node);
        if (_debug_entity)
            _sceneMgr->destroyEntity(_debug_entity);
        if (_axis.sm) {
            PUtil::destroyXYZ(_axis);
        }
    }

    _debug_node = 0;
    _debug_entity = 0;

}

void PNode::clearDebugSkeleton() {

    if (_debug_skeleton_root) {

        PBDebugMapIter it = _debug_skeleton.begin();
        PBDebugMapIter ite = _debug_skeleton.end();
        for (; it != ite; ++it) {
            delete( it->second);
        }
        _debug_skeleton.clear();
        _debug_skeleton_root = 0;

    }

}

void PNode::clear() {

    clearDebugMesh();
    clearDebugSkeleton();

    // this must be done before deleting Ogre objects!
    // unlinking all childrens
//    clearChildren();

    if (_sceneMgr) {

        if (_entities) {
            for (size_t i = 0; i < _entity_num; ++i) {
                _sceneMgr->destroyEntity(_entities[i]);
            }
            delete[] _entities;
        }

        if (_object) {
            PObject<Ogre::SceneNode*>::clear();
            _object = 0;
        }

        if (_mat_num > 0) {
            delete[] _default_mat;
            for (size_t i = 0; i < _mat_num; ++i) {
                if (_materials[i]) delete _materials[i];
            }
            delete[] _materials;
        }

    }

    _internal = false;
    _cube = false;
    _sphere = false;
    _plane = false;
    _empty = false;
    _entity_num = 0;
    _entities = 0;
    _default_mat = 0;
    _materials = 0;
    _mat_num = 0;

    // backfire protection on deletion
    if (_object) PObject<Ogre::SceneNode*>::clear();

}

void PNode::appendEntitySlot() {

    if (!_entities) {

        _entity_num = 1;
        _entities = new Ogre::Entity*[_entity_num];
        _entities[ 0 ] = 0;

    } else {

        Ogre::Entity** tmp = new Ogre::Entity*[_entity_num + 1];
        for (size_t i = 0; i < _entity_num; ++i) {
            tmp[ i ] = _entities[ i ];
        }
        tmp[ _entity_num ] = 0;
        delete[] _entities;
        _entities = tmp;
        _entity_num++;

    }

}

void PNode::appendMaterialSlot(size_t add) {

    if (!_default_mat) {

        _mat_num = add;
        _default_mat = new MaterialInfo[_mat_num];
        _materials = new PMaterial*[_mat_num];
        for (size_t i = 0; i < _mat_num; ++i) {
            _materials[ i ] = 0;
        }

    } else {


        MaterialInfo* tmp = new MaterialInfo[_mat_num + add];
        PMaterial** tmpp = new PMaterial*[_mat_num + add];
        for (size_t i = 0; i < _mat_num + add; ++i) {
            if (i < _mat_num) {
                tmp[ i ] = _default_mat[ i ];
                tmpp[ i ] = _materials[ i ];
            } else {
                tmpp[ i ] = 0;
            }
        }
        delete[] _default_mat;
        _default_mat = tmp;
        delete[] _materials;
        _materials = tmpp;
        _mat_num += add;

    }

}

bool PNode::load(
        Ogre::SceneManager* sceneMgr,
        string meshgroup,
        string meshname,
        std::string name
        ) {

    if (_origin) return false;
    if (!_internal && !PUtil::meshExists(meshgroup, meshname)) {
        return false;
    }

    if (!name.empty()) {
        _name = name;
    }

    _sceneMgr = sceneMgr;
    _group = meshgroup;
    _mesh_name = meshname;

    appendEntitySlot();
    if (!_internal) {
        _entities[0] = _sceneMgr->createEntity(_mesh_name, _group);
    } else {
        _entities[0] = _sceneMgr->createEntity(_mesh_name);
    }

    _origin = _sceneMgr->getRootSceneNode()->createChildSceneNode();

    _object = _origin->createChildSceneNode();
    _object->attachObject(_entities[0]);

    _idobject = static_cast<Ogre::IdObject*> (_object);

    _visible = true;

    naming();

    _entities[0]->setName(_name + "_entity");

    // skeleton loading
    loadSkeleton();

    // materials loading
    loadMaterials();

    if (!_internal) loadAabb();

    return true;

}

void PNode::material(
        Ogre::MaterialPtr ptr,
        bool overwrite_default,
        size_t entityid,
        int submesh
        ) {

    if (!_sceneMgr) return;
    if (_empty) return;

    if (entityid >= _entity_num) {
        Ogre::LogManager::getSingleton().logMessage(
                Ogre::LML_NORMAL,
                "PNode::material, entity does not exists.");
        return;
    }

    size_t sm = submesh;
    if (submesh < 0) {
        sm = 0;
    }

    // getting entity
    Ogre::Entity* e = _entities[ entityid ];
    size_t se_num = e->getNumSubEntities();
    if (sm >= se_num) {
        Ogre::LogManager::getSingleton().logMessage(
                Ogre::LML_NORMAL,
                "PNode::material, sub-entity does not exists.");
        return;
    }

    if (submesh < 0) {

        for (size_t i = 0; i < se_num; ++i) {
            e->getSubEntity(i)->setMaterial(ptr);
        }

    } else {

        e->getSubEntity(sm)->setMaterial(ptr);

    }

    MaterialInfo* mi = 0;
    size_t mi_id = 0;
    while (!mi && mi_id < _mat_num) {
        if (_default_mat[ mi_id ].entity == entityid) {
            mi = &_default_mat[ mi_id ];
            break;
        }
        ++mi_id;
    }

    if (!mi) {
        Ogre::LogManager::getSingleton().logMessage(
                Ogre::LML_NORMAL,
                "PNode::material, impossible to locate default material.");
        return;
    }

    if (submesh < 0) {

        while (mi->entity == entityid) {

            if (overwrite_default) {
                mi->group = ptr.get()->getGroup();
                mi->name = ptr.get()->getName();
            }

            if (_materials[ mi_id ]) {
                delete _materials[ mi_id ];
                _materials[ mi_id ] = 0;
            }

            ++mi_id;

            if (mi_id == _mat_num) {
                break;
            }

            mi = &_default_mat[ mi_id ];

        }

    } else {

        mi_id += sm;

        if (overwrite_default) {
            mi = &_default_mat[ mi_id ];
            mi->group = ptr.get()->getGroup();
            mi->name = ptr.get()->getName();
        }

        if (_materials[ mi_id ]) {
            delete _materials[ mi_id ];
            _materials[ mi_id ] = 0;
        }

    }

}

void PNode::material(
        PMaterial* mat,
        bool overwrite_default,
        size_t entityid,
        int submesh
        ) {

    if (!mat || !mat->getMaterial().get()) {
        Ogre::LogManager::getSingleton().logMessage(
                Ogre::LML_NORMAL,
                "PNode::material, invalid PMaterial!");
        return;
    }

    material(
            mat->getMaterial(),
            overwrite_default,
            entityid,
            submesh
            );

}

void PNode::material(
        std::string matgroup,
        std::string matname,
        bool overwrite_default,
        size_t entityid,
        int submesh
        ) {

    if (!PUtil::materialExists(matgroup, matname)) {
        Ogre::LogManager::getSingleton().logMessage(
                Ogre::LML_NORMAL,
                "PNode::material, invalid group and material!");
        return;

    }

    material(
            Ogre::MaterialManager::getSingleton().getByName(
            matname, matgroup),
            overwrite_default,
            entityid,
            submesh
            );
}

PMaterial* PNode::getMaterial(
        size_t entityid,
        int submesh,
        bool autocreate) {

    if (_empty) return 0;

    if (!_entities) {
        Ogre::LogManager::getSingleton().logMessage(
                Ogre::LML_NORMAL,
                "PNode::getMaterial, node is not loaded!");
        return 0;

    }

    if (entityid >= _entity_num) {
        Ogre::LogManager::getSingleton().logMessage(
                Ogre::LML_NORMAL,
                "PNode::getMaterial, entity does not exist!");
        return 0;
    }

    size_t sm = submesh;
    if (submesh < 0) {
        sm = 0;
    }

    MaterialInfo* mi = 0;
    size_t mi_id = 0;
    while (!mi) {
        if (
                _default_mat[ mi_id ].entity == entityid &&
                _default_mat[ mi_id ].sub_entity == sm
                ) {
            mi = &_default_mat[ mi_id ];
            break;
        }
        ++mi_id;
    }

    if (sm >= _mat_num) {
        Ogre::LogManager::getSingleton().logMessage(
                Ogre::LML_NORMAL,
                "PNode::getMaterial, submesh does not exist!");
        return 0;
    }

    PMaterial* m = _materials[ mi_id ];

    if (!m && autocreate) {

        // creation of a new material on the fly
        _materials[ mi_id ] = new PMaterial();
        _materials[ mi_id ]->load(
                _entities[ entityid ]->getSubEntity(sm)->getMaterial()
                );
        m = _materials[ mi_id ];

        if (!m->getMaterial().get()) {

            //            cout << "creation of a new Ogre::Material in " << _name << endl;

            stringstream ss;
            ss << _name << "_mat_" << _object << "::" << entityid << "." << sm;

            string matname = ss.str();
            m->create(_group, matname);

            // apply on all sub entites with no mat
            if (submesh < 0) {

                for (size_t i = 0; i < _mat_num; ++i) {
                    if (_default_mat[ i ].entity == entityid) {
                        _entities[entityid]->getSubEntity(
                                _default_mat[ i ].sub_entity
                                )->setMaterial(m->getMaterial());
                        _default_mat[ i ].group = _group;
                        _default_mat[ i ].name = matname;
                    }
                }

            } else {

                _entities[entityid]->getSubEntity(
                        mi->sub_entity
                        )->setMaterial(m->getMaterial());
                mi->group = _group;
                mi->name = matname;

            }

        }

    }

    return m;

}

void PNode::resetMaterial() {

    if (_empty) return;

    if (!_default_mat) {

        Ogre::LogManager::getSingleton().logMessage(
                Ogre::LML_NORMAL,
                "PNode::resetMaterial, materials not set on " +
                _group + ":" + _name + "', mesh: '" + _mesh_name + "'");

        return;
    }

    for (size_t i = 0; i < _mat_num; ++i) {

        MaterialInfo& mi = _default_mat[i];
        _entities[mi.entity]->getSubEntity(mi.sub_entity)->setMaterialName(
                mi.name, mi.group);
    }

    // clean up of PMaterials
    for (size_t i = 0; i < _mat_num; ++i) {

        if (_materials[ i ]) {
            delete _materials[ i ];
            _materials[ i ] = 0;
        }

    }

}

bool PNode::sphere(
        Ogre::SceneManager* sceneMgr,
        std::string name
        ) {

    if (_origin) return false;
    _internal = true;
    _sphere = true;
    load(sceneMgr, "Internal", "Prefab_Sphere", name);
    scale(Vector3(1, 1, 1), true);
    loadAabb();
    return true;

}

bool PNode::cube(
        Ogre::SceneManager* sceneMgr,
        std::string name
        ) {

    if (_origin) return false;
    _internal = true;
    _cube = true;
    load(sceneMgr, "Internal", "Prefab_Cube", name);
    scale(Vector3(1, 1, 1), true);
    loadAabb();
    return true;

}

bool PNode::plane(
        Ogre::SceneManager* sceneMgr,
        std::string name
        ) {

    if (_origin) return false;
    _internal = true;
    _plane = true;
    load(sceneMgr, "Internal", "Prefab_Plane", name);
    scale(Vector3(1, 1, 1), true);
    loadAabb();
    return true;

}

bool PNode::empty(
        Ogre::SceneManager* sceneMgr,
        std::string name
        ) {

    if (_origin) return false;
    _internal = true;
    _empty = true;
    _sceneMgr = sceneMgr;
    _origin = _sceneMgr->getRootSceneNode()->createChildSceneNode();
    _object = _origin->createChildSceneNode();
    _idobject = static_cast<Ogre::IdObject*> (_object);
    _visible = true;
    scale(Vector3(1, 1, 1), true);
    loadAabb();
    return true;

}

bool PNode::append(
        std::string meshgroup,
        std::string meshname
        ) {

    if (!_sceneMgr || _empty || !PUtil::meshExists(meshgroup, meshname)) {
        return false;
    }

    appendEntitySlot();
    size_t eid = _entity_num - 1;
    _entities[ eid ] = _sceneMgr->createEntity(meshname, meshgroup);
    _object->attachObject(_entities[ eid ]);

    size_t subei = _entities[ eid ]->getNumSubEntities();
    size_t dm_offset = _mat_num;

    appendMaterialSlot(subei);

    std::string mg;
    std::string mn;

    for (size_t i = 0; i < subei; ++i) {

        PUtil::subentityMaterial(
                _entities[ eid ]->getSubEntity(i),
                mg, mn);

        _default_mat[ dm_offset + i ].entity = eid;
        _default_mat[ dm_offset + i ].sub_entity = i;
        _default_mat[ dm_offset + i ].group = mg;
        _default_mat[ dm_offset + i ].name = mn;

    }

    return true;

}

void PNode::visible(bool enable) {

    if (!_sceneMgr) return;
    if (_visible != enable) {

        _origin->setVisible(enable, true);
        _visible = enable;
    }

}

void PNode::debug(PNodeDebugMode mode, bool enable) {

    switch (mode) {
        case PDM_ALL:
            debugMesh(enable);
            debugSkeleton(enable);
            break;
        case PDM_MESH:
            debugMesh(enable);
            break;
        case PDM_SKELETON:
            debugSkeleton(enable);

            break;
        default:
            break;
    }

}

void PNode::showAxis(bool enabled) {

    if (enabled && !_axis.sm) {
        PUtil::createXYZ(
                _axis, _sceneMgr, _object,
                POLYMORPH_XYZAXIS_LENGTH,
                POLYMORPH_XYZAXIS_THICKNESS
                );
    } else if (!enabled && _axis.sm) {

        PUtil::destroyXYZ(_axis);
    }

}

PNodeDebugMode PNode::getDebugMode() {

    if (_debug_node && _debug_skeleton_root) {
        return PDM_ALL;
    } else if (_debug_node) {
        return PDM_MESH;
    } else if (_debug_skeleton_root) {

        return PDM_SKELETON;
    }
    return PDM_NONE;

}

bool PNode::isDebugMode(PNodeDebugMode mode) {

    PNodeDebugMode _mode = getDebugMode();

    if (
            mode == PDM_ALL &&
            _mode == PDM_ALL
            ) {
        return true;
    }

    if (
            mode == PDM_MESH &&
            (_mode == PDM_ALL || _mode == PDM_MESH)
            ) {
        return true;
    }

    if (
            mode == PDM_SKELETON &&
            (_mode == PDM_ALL || _mode == PDM_SKELETON)
            ) {

        return true;
    }

    return false;
}

void PNode::debugMesh(bool enable) {

    // nothing else to do for empties
    if (_empty) {
        showAxis(enable);
        _debugging = enable;
        return;
    }

    // other kinds
    if (
            !_sceneMgr ||
            (enable && _debug_node) ||
            (!enable && !_debug_node)
            ) return;

    _debugging = enable;

    if (!enable) {
        // verification of skeleton debugging
        if (_debug_skeleton_root) _debugging = true;
        clearDebugMesh();
        return;
    }

    if (_origin->getScale().length() == 0) return;


    _debug_node = _object->createChildSceneNode();
    _debug_node->setName(_name + "_debug_node");

    if (PUtil::isComplex(_node_exchange)) {
        _debug_entity = _sceneMgr->createEntity(_node_exchange.meshname, _node_exchange.meshgroup);
    } else if (PUtil::isSphere(_node_exchange)) {
        _debug_entity = _sceneMgr->createEntity("Prefab_Sphere");
    } else {
        _debug_entity = _sceneMgr->createEntity("Prefab_Cube");
    }

    _debug_entity->setName(_name + "_debug_entity");
    _debug_entity->setMaterial(PUtil::materialDebugNode());
    _debug_node->attachObject(_debug_entity);

    if (!PUtil::isComplex(_node_exchange)) {
        Vector3 sc = _bounding_box.mHalfSize * 2;

        if (!_internal) sc *= ONE_PERCENT;
        _debug_node->setScale(sc);
    }

    _debug_node->translate(_bounding_box.mCenter);

}

void PNode::debugSkeleton(bool enable) {

    if (
            !_sceneMgr ||
            !_skeleton ||
            (enable && _debug_skeleton_root) ||
            (!enable && !_debug_skeleton_root)
            ) return;

    _debugging = enable;

    if (!enable) {
        // verification of skeleton debugging
        if (_debug_node) _debugging = true;
        clearDebugSkeleton();
        return;
    }

    // loading bones debugging
    PSkeleton::BoneIterator it = _skeleton->getBoneIterator();
    while (it.current() != it.end()) {

        PBone* b = it.peekNext();
        debugBone(b);
        it.moveNext();
    }

}

void PNode::debugBone(PBone* b) {

    if (!_skeleton) return;

    // creation of a data holder
    PBoneDebug * bdebug = new PBoneDebug(_sceneMgr);

    // getting parent debug bone
    Ogre::SceneNode * parentnode;
    if (b->getParent()) {
        parentnode = _debug_skeleton[
                POLYMORPH_DEBUG_BONE_PREFIX + b->getParent()->getName()
                ]->main;
    } else {
        parentnode = _object;
    }

    bdebug->main = parentnode->createChildSceneNode();
    bdebug->main->setName(POLYMORPH_DEBUG_BONE_PREFIX + b->getName());
    bdebug->main->setScale(b->getScale());
    bdebug->main->setOrientation(b->getOrientation());
    bdebug->main->translate(b->getPosition());

    // storage
    _debug_skeleton[ bdebug->main->getName() ] = bdebug;

    bdebug->head = bdebug->main->createChildSceneNode();
    bdebug->head_e = _sceneMgr->createEntity("Prefab_Sphere");
    bdebug->head_e->setMaterial(PUtil::materialDebugBone());
    bdebug->head->attachObject(bdebug->head_e);

    // no need to draw this bone, it's the root one
    if (!b->getParent()) {
        bdebug->head->setScale(ONE_PERCENT * 0.04);
        _debug_skeleton_root = bdebug->main;

        return;
    }

    bdebug->head->setScale(ONE_PERCENT * 0.02);

    Ogre::Vector3 t = b->getPosition();

    bdebug->toparent = bdebug->main->createChildSceneNode();
    bdebug->toparent_display = bdebug->toparent->createChildSceneNode();

    bdebug->toparent_display_e = _sceneMgr->createEntity("Prefab_Cube");
    bdebug->toparent_display_e->setMaterial(PUtil::materialDebugBone());
    bdebug->toparent_display->attachObject(bdebug->toparent_display_e);
    Ogre::Vector3 sc(0.00002, 1, 0.00002);
    sc.y = t.length() / 100;
    bdebug->toparent_display->scale(sc);
    bdebug->toparent_display->translate(Ogre::Vector3(0, -sc.y * 50, 0));

    // direction to parent:
    Ogre::Quaternion q = PUtil::fromTwoVectors(Ogre::Vector3::UNIT_Y, t);
    bdebug->toparent->setOrientation(b->getOrientation().Inverse() * q);

    // AXIS
    Ogre::Real BIG = 0.0008;
    Ogre::Real SMALL = 0.00003;

    bdebug->x_display = bdebug->main->createChildSceneNode();
    bdebug->x_display_e = _sceneMgr->createEntity("Prefab_Cube");
    bdebug->x_display_e->setMaterial(PUtil::materialAxisX());
    bdebug->x_display->attachObject(bdebug->x_display_e);
    sc = Ogre::Vector3(BIG, SMALL, SMALL);
    bdebug->x_display->scale(sc);
    bdebug->x_display->translate(Ogre::Vector3(sc.x * 50, 0, 0));

    bdebug->y_display = bdebug->main->createChildSceneNode();
    bdebug->y_display_e = _sceneMgr->createEntity("Prefab_Cube");
    bdebug->y_display_e->setMaterial(PUtil::materialAxisY());
    bdebug->y_display->attachObject(bdebug->y_display_e);
    sc = Ogre::Vector3(SMALL, BIG, SMALL);
    bdebug->y_display->scale(sc);
    bdebug->y_display->translate(Ogre::Vector3(0, sc.y * 50, 0));

    bdebug->z_display = bdebug->main->createChildSceneNode();
    bdebug->z_display_e = _sceneMgr->createEntity("Prefab_Cube");
    bdebug->z_display_e->setMaterial(PUtil::materialAxisZ());
    bdebug->z_display->attachObject(bdebug->z_display_e);
    sc = Ogre::Vector3(SMALL, SMALL, BIG);
    bdebug->z_display->scale(sc);
    bdebug->z_display->translate(Ogre::Vector3(0, 0, sc.z * 50));

}

void PNode::loadAabb() {

    if (!_origin) return;
    if (!_entities) {
        _bounding_box = Ogre::Aabb::BOX_ZERO;

        return;
    }
    _bounding_box = _entities[0]->getWorldAabbUpdated();

}

bool PNode::loadSkeleton() {

    if (_skeleton) return false;
    if (!_entities) return false;
    if (!_entities[0]->hasSkeleton()) return false;

    _skeleton = _entities[0]->getSkeleton();

    return true;

}

bool PNode::loadMaterials() {

    if (_empty) return false;

    // backup of default materials
    size_t submi = _entities[0]->getNumSubEntities();
    appendMaterialSlot(submi);
    std::string mg;
    std::string mn;
    for (size_t i = 0; i < submi; ++i) {
        PUtil::subentityMaterial(
                _entities[0]->getSubEntity(i),
                mg, mn);
        _default_mat[ i ].entity = 0;
        _default_mat[ i ].sub_entity = i;
        _default_mat[ i ].group = mg;
        _default_mat[ i ].name = mn;
    }

    return true;

}

bool PNode::physics(
        BT_Type type,
        std::string meshgroup,
        std::string meshname) {

    if (PObject<Ogre::SceneNode*>::physics(type, meshgroup, meshname)) {
        
        if (_debug_entity && PUtil::isComplex(type)) {
            _debug_node->detachObject(_debug_entity);
            OGRE_DELETE _debug_entity;
            _debug_entity = _sceneMgr->createEntity(meshname, meshgroup);
            _debug_entity->setName("pnode_debug_entity_" + StringConverter::toString(UID));
            _debug_node->attachObject(_debug_entity);
            _debug_node->setScale(Vector3::UNIT_SCALE);
        }
        return true;
        
    }
    return false;

}

void PNode::getBoneNames(size_t& bnum, std::string*& bnames) {

    if (!_skeleton) {
        bnum = 0;
        bnames = 0;
        return;
    }

    bnum = _skeleton->getNumBones();
    bnames = new string[ bnum ];
    PSkeleton::BoneIterator it = _skeleton->getBoneIterator();
    int i = 0;
    while (it.current() != it.end()) {

        PBone* b = it.peekNext();
        bnames[i] = b->getName();
        it.moveNext();
        ++i;
    }

}

void PNode::getBones(size_t& bnum, PBone**& bones) {

    if (!_skeleton) {
        bnum = 0;
        bones = 0;
        return;
    }

    bnum = _skeleton->getNumBones();
    bones = new PBone*[ bnum ];
    PSkeleton::BoneIterator it = _skeleton->getBoneIterator();
    int i = 0;
    while (it.current() != it.end()) {

        PBone* b = it.peekNext();
        bones[i] = b;
        it.moveNext();
        ++i;
    }

}

PBone* PNode::bone(std::string name) {
    if (!_skeleton) return 0;
    if (_skeleton->hasBone(name)) {
        return _skeleton->getBone(name);
    } else {

        return 0;
    }
}

PBone* PNode::bone(uint id) {
    if (!_skeleton) return 0;
    if (id < _skeleton->getNumBones()) {
        return _skeleton->getBone(id);
    } else {

        return 0;
    }
}

void PNode::debugBoneUpdate(PBone* b) {

    Ogre::Vector3 t = b->getPosition();
    Ogre::SceneNode* sn;
    sn = _debug_skeleton[
            POLYMORPH_DEBUG_BONE_PREFIX + b->getName()
            ]->main;
    sn->setPosition(t);
    sn->setOrientation(b->getOrientation());

    if (_debug_skeleton[
            POLYMORPH_DEBUG_BONE_PREFIX + b->getName()
            ]->toparent == 0
            ) return;

    sn = _debug_skeleton[
            POLYMORPH_DEBUG_BONE_PREFIX + b->getName()
            ]->toparent;
    Ogre::Quaternion q = PUtil::fromTwoVectors(Ogre::Vector3::UNIT_Y, t);
    sn->setOrientation(b->getOrientation().Inverse() * q);

}

bool PNode::translateBone(
        std::string name, Ogre::Vector3 v,
        PBoneSpace ts) {

    return translateBone(bone(name), v, ts);
}

bool PNode::translateBone(
        uint id, Ogre::Vector3 v,
        PBoneSpace ts) {

    return translateBone(bone(id), v, ts);
}

bool PNode::translateBone(
        PBone* b, Ogre::Vector3 v,
        PBoneSpace ts) {

    if (!b) return false;
    if (!b->isManuallyControlled()) b->setManuallyControlled(true);
    b->translate(v, ts);
    if (_debug_skeleton_root) debugBoneUpdate(b);

    return true;

}

bool PNode::moveBone(
        std::string name, Ogre::Vector3 v,
        PBoneSpace ts) {

    return moveBone(bone(name), v, ts);
}

bool PNode::moveBone(
        uint id, Ogre::Vector3 v,
        PBoneSpace ts) {

    return moveBone(bone(id), v, ts);
}

bool PNode::moveBone(
        PBone* b, Ogre::Vector3 v,
        PBoneSpace ts) {

    if (!b) return false;
    if (!b->isManuallyControlled()) b->setManuallyControlled(true);
    switch (ts) {
        case PBone::TS_LOCAL:
            b->setPosition(v);
            break;
        case PBone::TS_PARENT:
            b->setPosition(v);
            break;
        case PBone::TS_WORLD:
            b->setPosition(v);
            break;
        default:
            break;
    }
    if (_debug_skeleton_root) debugBoneUpdate(b);

    return true;

}

bool PNode::pitchBone(
        std::string name, Ogre::Radian r,
        PBoneSpace ts) {

    return pitchBone(bone(name), r, ts);
}

bool PNode::pitchBone(
        uint id, Ogre::Radian r,
        PBoneSpace ts) {

    return pitchBone(bone(id), r, ts);
}

bool PNode::pitchBone(
        PBone* b, Ogre::Radian r,
        PBoneSpace ts) {

    if (!b) return false;
    if (!b->isManuallyControlled()) b->setManuallyControlled(true);
    b->pitch(r, ts);
    if (_debug_skeleton_root) debugBoneUpdate(b);

    return true;

}

bool PNode::yawBone(
        std::string name, Ogre::Radian r,
        PBoneSpace ts) {

    return yawBone(bone(name), r, ts);
}

bool PNode::yawBone(
        uint id, Ogre::Radian r,
        PBoneSpace ts) {

    return yawBone(bone(id), r, ts);
}

bool PNode::yawBone(
        PBone* b, Ogre::Radian r,
        PBoneSpace ts) {

    if (!b) return false;
    if (!b->isManuallyControlled()) b->setManuallyControlled(true);
    b->yaw(r, ts);
    if (_debug_skeleton_root) debugBoneUpdate(b);

    return true;

}

bool PNode::rollBone(
        std::string name, Ogre::Radian r,
        PBoneSpace ts) {

    return rollBone(bone(name), r, ts);
}

bool PNode::rollBone(
        uint id, Ogre::Radian r,
        PBoneSpace ts) {

    return rollBone(bone(id), r, ts);
}

bool PNode::rollBone(
        PBone* b, Ogre::Radian r,
        PBoneSpace ts) {

    if (!b) return false;
    if (!b->isManuallyControlled()) b->setManuallyControlled(true);
    b->roll(r, ts);
    if (_debug_skeleton_root) debugBoneUpdate(b);

    return true;

}

bool PNode::orientBone(
        std::string name,
        const Ogre::Quaternion& q,
        PBoneSpace ts) {

    return orientBone(bone(name), q, ts);
}

bool PNode::orientBone(
        uint id,
        const Ogre::Quaternion& q,
        PBoneSpace ts) {

    return orientBone(bone(id), q, ts);
}

bool PNode::orientBone(
        PBone* b,
        const Ogre::Quaternion& q,
        PBoneSpace ts) {

    if (!b) return false;
    if (!b->isManuallyControlled()) b->setManuallyControlled(true);
    switch (ts) {
        case PBone::TS_LOCAL:
            b->setOrientation(q);
            break;
        case PBone::TS_PARENT:
            if (b->getParent() && b->getInheritOrientation()) {
                Ogre::Quaternion pq = b->getParent()->getOrientation();
                b->setOrientation(pq.Inverse() * q);
            } else {
                b->setOrientation(q);
            }
            break;
        case PBone::TS_WORLD:
            if (b->getParent() && b->getInheritOrientation()) {
                Ogre::Quaternion pq = b->getParent()->_getDerivedOrientation();
                b->setOrientation(pq.Inverse() * q);
            } else {
                b->setOrientation(q);
            };
            break;
        default:
            break;
    }
    if (_debug_skeleton_root) debugBoneUpdate(b);

    return true;

}

bool PNode::scaleBone(
        std::string name,
        const Ogre::Vector3& v,
        PBoneSpace ts) {

    return scaleBone(bone(name), v, ts);
}

bool PNode::scaleBone(
        uint id,
        const Ogre::Vector3& v,
        PBoneSpace ts) {

    return scaleBone(bone(id), v, ts);
}

bool PNode::scaleBone(
        PBone* b,
        const Ogre::Vector3& v,
        PBoneSpace ts) {

    if (!b) return false;
    if (!b->isManuallyControlled()) b->setManuallyControlled(true);
    switch (ts) {
        case PBone::TS_LOCAL:
            b->setScale(v);
            break;
        case PBone::TS_PARENT:
            if (b->getParent() && b->getInheritScale()) {
                Ogre::Vector3 ps = b->getParent()->getScale();
                b->setScale(ps * v);
            } else {
                b->setScale(v);
            }
            break;
        case PBone::TS_WORLD:
            if (b->getParent() && b->getInheritScale()) {
                Ogre::Vector3 ps = b->getParent()->_getDerivedScale();
                b->setScale(ps * v);
            } else {
                b->setScale(v);
            }
            break;
        default:
            break;
    }
    if (_debug_skeleton_root) debugBoneUpdate(b);

    return true;

}

bool PNode::getBonePosition(
        std::string name,
        Ogre::Vector3& v,
        PBoneSpace ts) {

    return getBonePosition(bone(name), v, ts);
}

bool PNode::getBonePosition(
        uint id,
        Ogre::Vector3& v,
        PBoneSpace ts) {

    return getBonePosition(bone(id), v, ts);
}

bool PNode::getBonePosition(
        PBone* b,
        Ogre::Vector3& v,
        PBoneSpace ts) {

    if (!b) return false;
    switch (ts) {
        case PBone::TS_LOCAL:
            v = b->getPosition();
            break;
        case PBone::TS_PARENT:
            if (b->getParent()) {
                v = b->getPosition() + b->getParent()->getPosition();
            } else {
                v = b->getPosition();
            }
            break;
        case PBone::TS_WORLD:
            v = b->_getDerivedPosition() * _object->_getDerivedScale();
            v = _object->_getDerivedOrientation() * v;
            v += _object->_getDerivedPosition();

            break;
        default:
            break;
    }
    return true;

}

bool PNode::getBoneOrientation(
        std::string name,
        Ogre::Quaternion& q,
        PBoneSpace ts) {

    return getBoneOrientation(bone(name), q, ts);
}

bool PNode::getBoneOrientation(
        uint id,
        Ogre::Quaternion& q,
        PBoneSpace ts) {

    return getBoneOrientation(bone(id), q, ts);
}

bool PNode::getBoneOrientation(
        PBone* b,
        Ogre::Quaternion& q,
        PBoneSpace ts) {

    if (!b) return false;
    switch (ts) {
        case PBone::TS_LOCAL:
            q = b->getOrientation();
            break;
        case PBone::TS_PARENT:
            if (b->getParent() && b->getInheritOrientation()) {
                q = b->getParent()->getOrientation() * b->getOrientation();
            } else {
                q = b->getOrientation();
            }
            break;
        case PBone::TS_WORLD:
            q = b->_getDerivedOrientation();

            break;
        default:
            break;
    }
    return true;

}

bool PNode::getBoneScale(
        std::string name,
        Ogre::Vector3& s,
        PBoneSpace ts) {

    return getBoneScale(bone(name), s, ts);
}

bool PNode::getBoneScale(
        uint id,
        Ogre::Vector3& s,
        PBoneSpace ts) {

    return getBoneScale(bone(id), s, ts);
}

bool PNode::getBoneScale(
        PBone* b,
        Ogre::Vector3& s,
        PBoneSpace ts) {

    if (!b) return false;
    switch (ts) {
        case PBone::TS_LOCAL:
            s = b->getScale();
            break;
        case PBone::TS_PARENT:
            if (b->getParent() && b->getInheritScale()) {
                s = b->getParent()->getScale() * b->getScale();
            } else {
                s = b->getScale();
            }
            break;
        case PBone::TS_WORLD:
            s = b->_getDerivedScale();
            break;
        default:
            break;
    }
    return true;

}