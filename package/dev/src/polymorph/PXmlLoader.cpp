/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ___________________________________( ^3^)_____________________________________
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 */

/* 
 * File:   PXmlLoader.cpp
 * Author: frankiezafe
 * 
 * Created on October 21, 2016, 11:51 AM
 */

#include "PXmlLoader.h"

using namespace std;
using namespace polymorph;
using namespace Ogre;

PXmlLoader::PXmlLoader() :
verbosed(false),
current_scene(0) {
}

PXmlLoader::~PXmlLoader() {
}

bool PXmlLoader::parse() {

    // copied from OgreConfigFile, void ConfigFile::loadDirect
    std::string path = "configuration.xml";
    std::ifstream fp;
    // Always open in binary mode
    fp.open(path.c_str(), std::ios::in | std::ios::binary);
    if (!fp) {
        OGRE_EXCEPT(
                Exception::ERR_FILE_NOT_FOUND,
                "'" + path + "' file not found!",
                "PXmlLoader::parse");
        return false;
    }

    // Wrap as a stream
    DataStreamPtr stream(
            OGRE_NEW FileStreamDataStream(path.c_str(), &fp, false)
            );

    bool success = parse(stream);

    if (success) {
        pdata.path = path;

        if (verbosed) cout <<
                "path " << path <<
                ", version: " << pdata.version <<
                "." << pdata.subversion <<
                ", date: " << boost::gregorian::to_iso_string(pdata.date) <<
            endl;
    }

    return success;

}

bool PXmlLoader::parse(Ogre::String group, Ogre::String file) {

    Ogre::ResourceGroupManager& rgm =
            Ogre::ResourceGroupManager::getSingleton();
    if (!rgm.resourceExists(group, file)) {
        if (verbosed) cerr << "NO " << group_name << ":" << xml_name << endl;
        return false;
    }

    Ogre::DataStreamPtr stream = rgm.openResource(file, group);

    bool success = parse(stream);

    if (success) {

        pdata.group = group;
        pdata.file = file;

        if (verbosed) cout <<
                "file " << group << ":" << file <<
                ", version: " << pdata.version <<
                "." << pdata.subversion <<
                ", date: " << boost::gregorian::to_iso_string(pdata.date) <<
            endl;
    }

    return success;

}

bool PXmlLoader::parse(Ogre::DataStreamPtr & stream) {

    size_t s = stream->size();
    if (s == 0) {
        OGRE_EXCEPT(
                Exception::ERR_FILE_NOT_FOUND,
                "XML is empty!",
                "PXmlLoader::parse");
        return false;
    }

    char* buffer = new char[ s ];
    stream->read(buffer, s);

    TiXmlDocument xml;
    xml.Parse(buffer);

    // entering in dom
    TiXmlNode* root = xml.FirstChild("polymorph");
    if (!root) return false;
    TiXmlElement* root_e = root->ToElement();

    // configuration information
    Ogre::Real real;
    uint16_t polym_version;
    uint16_t polym_subversion;

    if (decode(root_e, "version", real)) {

        polym_version = uint16_t(real);
        real -= polym_version;
        polym_subversion = uint16_t(real * 10);
        if (
                polym_version > POLYMORPH_VERSION ||
                polym_subversion > POLYMORPH_SUBVERSION
                ) {

            std::stringstream ss;
            ss << "Project version is too recent! " <<
                    "Engine is: " <<
                    POLYMORPH_VERSION << "." <<
                    POLYMORPH_SUBVERSION << ", "
                    "configuraton is: " <<
                    polym_version << "." <<
                    polym_subversion;


            OGRE_EXCEPT(
                    Exception::ERR_NOT_IMPLEMENTED,
                    ss.str(),
                    "PXmlLoader::parse");
        }

    } else {

        std::stringstream ss;
        ss << "Project version is not specified! " <<
                "Engine is: " <<
                POLYMORPH_VERSION << "." <<
                POLYMORPH_SUBVERSION;

        OGRE_EXCEPT(
                Exception::ERR_NOT_IMPLEMENTED,
                ss.str(),
                "PXmlLoader::parse");

    }

    // ok, seems to be cool, let's clean the project
    pdata.clear();


    if (decode(root_e, "version", real)) {
        pdata.version = uint16_t(real);
        real -= pdata.version;
        pdata.subversion = uint16_t(real * 10);
    }

    // file info will be retrieved by caller methods
    if (!exists(root_e, "date")) {
        boost::posix_time::ptime pt =
                boost::posix_time::second_clock::local_time();
        pdata.date = boost::gregorian::date(
                pt.date().year(),
                pt.date().month(),
                pt.date().day()
                );
    } else {
        pdata.date = boost::gregorian::from_undelimited_string(
                root_e->Attribute("date")
                );
    }


    // loading resources
    TiXmlNode* resources = root->FirstChild("resources");

    if (resources) {

        TiXmlElement* resources_e = resources->ToElement();
        parseResources(resources_e);

    } else {

        if (verbosed) cerr << "PProjectLoader::load, "
                "error: no resources node!" << endl;

    }

    // loading compositor
    TiXmlNode* compositor = root->FirstChild("compositor");

    if (compositor) {

        TiXmlElement* compositor_e = compositor->ToElement();
        parseCompositor(compositor_e);

        if (verbosed) {
            std::cout << "Compositor, workspaces: " <<
                    pdata.compositor_workspaces.size() << std::endl;
            PWSListIter itw = pdata.compositor_workspaces.begin();
            PWSListIter itwe = pdata.compositor_workspaces.end();
            for (; itw != itwe; ++itw) {
                PCompositorWSData* wsd = (*itw);
                wsd->print();
            }
        }

    } else {

        if (verbosed) cerr << "PProjectLoader::load, "
                "error: no compositor node!" << endl;

    }

    // loading scenes
    TiXmlNode* scenes = root->FirstChild("scenes");

    if (scenes) {

        TiXmlElement* scenes_e = scenes->ToElement();
        TiXmlNode* scene;
        for (
                scene = scenes_e->FirstChild("scene");
                scene;
                scene = scene->NextSibling("scene")) {
            //            cout << "loading scene" << endl;
            TiXmlElement* scene_e = scene->ToElement();
            parseScene(scene_e);
        }

    } else {

        if (verbosed) cerr << "PProjectLoader::load, "
                "error: no scenes node" << endl;

    }

    // loading bullet configuration
    TiXmlNode* bullet = root->FirstChild("bullet");
    if (bullet) {
        parseBullet(bullet->ToElement());
    }

    // loading network configuration
    TiXmlNode* network = root->FirstChild("network");
    if (network) {
        parseNetwork(network->ToElement());
    }

    // loading pureadata configuration
    TiXmlNode* puredata = root->FirstChild("puredata");
    if (puredata) {
        parsePuredata(puredata->ToElement());
    }

    // calling custom method
    parseCustom(root_e);

    delete[] buffer;

    return true;

}

bool PXmlLoader::parseCustom(TiXmlElement* e) {
    return true;
}

bool PXmlLoader::parseResources(TiXmlElement* e) {

    TiXmlNode* group;
    bool b;
    std::string gname;
    std::vector< PResourceData* >* rvec;
    for (
            group = e->FirstChild("group");
            group;
            group = group->NextSibling("group")) {

        TiXmlElement* group_e = group->ToElement();
        if (exists(group_e, "name")) {
            gname = group_e->Attribute("name");
            rvec = &(pdata.resources[ gname ]);
        } else {
            continue;
        }

        TiXmlNode* res;
        for (
                res = group_e->FirstChild("resource");
                res;
                res = res->NextSibling("resource")) {
            TiXmlElement* res_e = res->ToElement();
            if (
                    exists(res_e, "path") &&
                    exists(res_e, "type")
                    ) {
                PResourceData * rd = new PResourceData();
                rd->group = gname;
                rd->path = res_e->Attribute("path");
                rd->type = res_e->Attribute("type");
                // avoiding horrible segmentation faults at loading
                if (rd->type.compare("Zip") == 0) {
                    rd->recursive = false;
                    rd->readonly = true;
                } else {
                    if (decode(res_e, "recursive", b)) {
                        rd->recursive = b;
                    }
                    if (decode(res_e, "readonly", b)) {
                        rd->readonly = b;
                    }
                }
                rvec->push_back(rd);
                if (verbosed) {
                    rd->print();
                }
            } else {

                if (verbosed) cerr << "PProjectLoader::parseResources, "
                        "resource in group '" <<
                        gname <<
                        "'is not correctly configured!" << endl;

            }

        }

    }

    return true;

}

PCompositorWSValue<Ogre::ColourValue> PXmlLoader::parseCompositorColor(TiXmlElement* e) {

    PCompositorWSValue<Ogre::ColourValue> color;
    if (exists(e, "name")) decode(e, "name", color.name);
    if (exists(e, "node")) decode(e, "node", color.node);
    if (exists(e, "pass")) decode(e, "pass", color.pass);
    if (exists(e, "index")) decode(e, "index", color.index);
    if (exists(e, "value")) decode(e, "value", color.value);
    return color;

}

PCompositorWSValue<float> PXmlLoader::parseCompositorVariable(TiXmlElement* e) {

    PCompositorWSValue<float> var;
    if (exists(e, "name")) decode(e, "name", var.name);
    if (exists(e, "node")) decode(e, "node", var.node);
    if (exists(e, "pass")) decode(e, "pass", var.pass);
    if (exists(e, "index")) decode(e, "index", var.index);
    if (exists(e, "value")) decode(e, "value", var.value);
    return var;

}

bool PXmlLoader::parseCompositor(TiXmlElement* e) {

    TiXmlNode* ws;
    std::string ws_name;
    std::string cl_name;
    std::string var_name;
    Ogre::ColourValue cl_value;

    for (
            ws = e->FirstChild("workspace");
            ws;
            ws = ws->NextSibling("workspace")) {

        TiXmlElement* ws_e = ws->ToElement();

        if (exists(ws_e, "name")) {
            ws_name = ws_e->Attribute("name");
        } else {
            continue;
        }

        PCompositorWSData* ws_data = new PCompositorWSData();
        ws_data->name = ws_name;

        TiXmlNode* script = ws_e->FirstChild("script");

        if (script) {
            TiXmlElement* script_e = script->ToElement();
            if (
                    exists(script_e, "group") ||
                    exists(script_e, "workspace")) {
                decode(script_e, "group", ws_data->group);
                decode(script_e, "workspace", ws_data->workspace);
                ws_data->basic = false;
            } else {
                continue;
            }
        }

        TiXmlNode* cls = ws_e->FirstChild("colors");

        if (cls) {

            TiXmlElement* cls_e = cls->ToElement();
            TiXmlNode* cl;

            for (
                    cl = cls_e->FirstChild("color");
                    cl;
                    cl = cl->NextSibling("color")) {

                TiXmlElement* cl_e = cl->ToElement();

                PCompositorWSValue<Ogre::ColourValue> cv;
                cv = parseCompositorColor(cl_e);

                if (!ws_data->basic && !cv.isCustom()) {

                    std::stringstream ss;
                    ss << "PXmlLoader::parseCompositor, wrong color assignment,";
                    ss << " color name: " << cv.name;
                    PUtil::logCritical(ss.str());

                } else {

                    ws_data->colors[ cv.name ] = cv;

                }

            }

        }

        TiXmlNode* vars = ws_e->FirstChild("variables");

        if (vars) {

            TiXmlElement* vars_e = vars->ToElement();
            TiXmlNode* var;

            for (
                    var = vars_e->FirstChild("variable");
                    var;
                    var = var->NextSibling("variable")) {

                TiXmlElement* var_e = var->ToElement();

                PCompositorWSValue<float> cv;
                cv = parseCompositorVariable(var_e);

                if (!ws_data->basic && !cv.isCustom()) {

                    std::stringstream ss;
                    ss << "PXmlLoader::parseCompositor, wrong variable assignment,";
                    ss << " variable name: " << cv.name;
                    PUtil::logCritical(ss.str());

                } else {

                    ws_data->variables[ cv.name ] = cv;

                }

            }

        }

        pdata.compositor_workspaces.push_back(ws_data);
        pdata.compositor_workspace_map[ ws_name ] = ws_data;

    }

    return true;

}

bool PXmlLoader::parseScene(TiXmlElement* e) {

    if (!exists(e, "id")) {
        return false;
    }

    uint32_t scID = Ogre::StringConverter::parseReal(e->Attribute("id"));
    std::string name;
    if (decode(e, "name", name)) {
        current_scene = pdata.create(scID, name);
    } else {
        current_scene = pdata.create(scID);
    }

    if (current_scene == 0) {
        if (verbosed) cerr << "PProjectLoader::loadScene, "
                "error: impossible to create scene " << scID << endl;
        return false;
    }

    TiXmlNode* bullet = e->FirstChild("bullet");
    if (bullet) {
        TiXmlElement* bullet_e = bullet->ToElement();
        TiXmlElement* bconf_e = bullet_e->FirstChild("config")->ToElement();
        Ogre::Vector3 g;
        bool persistent;
        PBulletEngineData bulletd;
        if (decode(bconf_e, "gravity", g)) {
            bulletd.gravity = PUtil::convert(g);
        }
        if (decode(bconf_e, "persistent", persistent)) {
            bulletd.persistent = persistent;
        }
        current_scene->set(bulletd);

    }

    TiXmlNode* pobjects = e->FirstChild("pobjects");
    if (!pobjects) {
        if (verbosed) cerr << "PProjectLoader::loadScene, "
                "error: no pnodes node" << endl;
        return false;
    }

    TiXmlElement* pobjects_e = pobjects->ToElement();
    TiXmlNode* pobject = 0;
    for (
            pobject = pobjects_e->FirstChild("pnode");
            pobject;
            pobject = pobject->NextSibling("pnode")) {
        TiXmlElement* pnode_e = pobject->ToElement();
        parseNode(pnode_e);
    }

    pobject = 0;

    for (
            pobject = pobjects_e->FirstChild("plight");
            pobject;
            pobject = pobject->NextSibling("plight")) {
        TiXmlElement* pnode_e = pobject->ToElement();
        parseLight(pnode_e);
    }

    pobject = 0;

    for (
            pobject = pobjects_e->FirstChild("psound");
            pobject;
            pobject = pobject->NextSibling("psound")) {
        // TiXmlElement* pnode_e = pobject->ToElement( );
        // parseSound(pnode_e);
    }

    return true;

}

bool PXmlLoader::parseObjectData(
        TiXmlElement* e,
        PObjectData& d) {

    bool b;
    Ogre::Real real;
    Ogre::Vector3 v;
    Ogre::Euler elrs;

    if (exists(e, "name")) d.name = e->Attribute("name");

    if (decode(e, "visible", b)) d.visible = b;
    if (decode(e, "debug", b)) d.debug = b;

    TiXmlNode* n;
    n = e->FirstChild("origin");
    if (n) {

        TiXmlElement* sub_e = n->ToElement();

        if (decode(sub_e, "pos", v)) d.origin_pos = v;
        if (decode(sub_e, "size", v)) d.origin_scale = v;
        if (decode(sub_e, "dir", elrs)) d.origin_orientation = elrs;

    }

    n = e->FirstChild("repeat");

    if (n) {
        TiXmlElement* sub_e = n->ToElement();
        if (decode(sub_e, "count", real)) {
            if (real >= 0) {
                d.repeat = true;
                d.repeat_count = real;
            }

        }
        if (decode(sub_e, "offset_pos", v)) d.repeat_pos = v;
        if (decode(sub_e, "offset_scale", v)) d.repeat_scale = v;
        if (decode(sub_e, "offset_dir", elrs)) d.repeat_orientation = elrs;
    }

    return true;

}

bool PXmlLoader::parseObjectBulletData(
        TiXmlNode* n,
        PObjectBulletData& d
        ) {

    // bullet configuration
    bool b;
    Ogre::Real real;

    if (n) {

        TiXmlElement* sub_e = n->ToElement();
        std::string btype;
        std::string bshape;
        if (exists(sub_e, "type")) btype = sub_e->Attribute("type");
        if (exists(sub_e, "shape")) bshape = sub_e->Attribute("shape");

        if (btype.compare("dynamic") == 0) {

            if (bshape.compare("box") == 0) {
                d.bullet_type = BT_DYNAMIC_BOX;
            } else if (bshape.compare("sphere") == 0) {
                d.bullet_type = BT_DYNAMIC_SPHERE;
            } else if (bshape.compare("compound") == 0) {
                d.bullet_type = BT_DYNAMIC_COMPOUND;
            } else if (bshape.compare("convex") == 0) {
                d.bullet_type = BT_DYNAMIC_CONVEX;
            } else if (bshape.compare("concave") == 0) {
                d.bullet_type = BT_DYNAMIC_CONCAVE;
            } else if (verbosed) {
                cout << "PProjectLoader::loadNode, unsupported bullet type "
                        "'" << btype << ":" << bshape << "'" << endl;
            }

        } else if (btype.compare("static") == 0) {

            if (bshape.compare("box") == 0) {
                d.bullet_type = BT_STATIC_BOX;
            } else if (bshape.compare("sphere") == 0) {
                d.bullet_type = BT_STATIC_SPHERE;
            } else if (bshape.compare("compound") == 0) {
                d.bullet_type = BT_STATIC_COMPOUND;
            } else if (bshape.compare("convex") == 0) {
                d.bullet_type = BT_STATIC_CONVEX;
            } else if (bshape.compare("concave") == 0) {
                d.bullet_type = BT_STATIC_CONCAVE;
            } else if (verbosed) {
                cout << "PProjectLoader::loadNode, unsupported bullet type "
                        "'" << btype << ":" << bshape << "'" << endl;
            }

        } else if (btype.compare("ghost") == 0) {

            if (bshape.compare("box") == 0) {
                d.bullet_type = BT_GHOST_BOX;
            } else if (bshape.compare("sphere") == 0) {
                d.bullet_type = BT_GHOST_SPHERE;
            } else if (bshape.compare("compound") == 0) {
                d.bullet_type = BT_GHOST_COMPOUND;
            } else if (bshape.compare("convex") == 0) {
                d.bullet_type = BT_GHOST_CONVEX;
            } else if (bshape.compare("concave") == 0) {
                d.bullet_type = BT_GHOST_CONCAVE;
            } else if (verbosed) {
                cout << "PProjectLoader::loadNode, unsupported bullet type "
                        "'" << btype << ":" << bshape << "'" << endl;
            }

        }

        if (d.bullet_type != BT_NONE) {

            if (PUtil::isComplex(d.bullet_type)) {

                // a mesh MUST be provided
                n = sub_e->FirstChild("mesh");
                if (n) {
                    TiXmlElement* sub_e2 = n->ToElement();
                    if (exists(sub_e2, "group"))
                        d.bullet_group = sub_e2->Attribute("group");
                    if (exists(sub_e2, "file"))
                        d.bullet_file = sub_e2->Attribute("file");
                }

                if (d.bullet_group.empty() || d.bullet_file.empty()) {

                    if (verbosed) {
                        cerr << "PProjectLoader::loadNode, "
                                "bullet configuration is not correct" <<
                                endl;
                    }
                    return false;

                }

            }

            n = sub_e->FirstChild("config");
            if (n) {
                TiXmlElement* sub_e2 = n->ToElement();
                if (decode(sub_e2, "density", real))
                    d.bullet_density = real;
                if (decode(sub_e2, "friction", real))
                    d.bullet_friction = real;
                if (decode(sub_e2, "restitution", real))
                    d.bullet_restitution = real;
                if (decode(sub_e2, "restless", b))
                    d.bullet_restless = b;
            }
            n = sub_e->FirstChild("damping");
            if (n) {
                TiXmlElement* sub_e2 = n->ToElement();
                if (decode(sub_e2, "linear", real))
                    d.bullet_damping_linear = real;
                if (decode(sub_e2, "angular", real))
                    d.bullet_damping_angular = real;
            }

            n = sub_e->FirstChild("data");
            if (n) {
                //TiXmlElement* sub_e2 = n->ToElement( );
                //cout << "PProjectLoader::loadNode, "
                //  "serialised data for bullet, todo" << endl;
            }

        }

    }

    return true;

}

bool PXmlLoader::parseNode(TiXmlElement* e) {

    assert(current_scene);

    Ogre::Vector3 v;
    Ogre::Euler elrs;
    TiXmlNode* n;

    PNodeData d;

    // object data
    parseObjectData(e, (PObjectData&) d);

    // bullet data
    parseObjectBulletData(
            e->FirstChild("bullet"),
            (PObjectBulletData&) d);

    // custom data
    n = e->FirstChild("node");
    if (n) {
        TiXmlElement* sub_e = n->ToElement();
        if (decode(sub_e, "pos", v)) d.node_pos = v;
        if (decode(sub_e, "size", v)) d.node_scale = v;
        if (decode(sub_e, "dir", elrs)) d.node_orientation = elrs;
    }

    // loading mesh
    n = e->FirstChild("mesh");
    if (n) {
        TiXmlElement* sub_e = n->ToElement();
        if (exists(sub_e, "group"))
            d.mesh_group = sub_e->Attribute("group");
        if (exists(sub_e, "file"))
            d.mesh_file = sub_e->Attribute("file");
    } else {
        //  a pimitive MUST be provided
        n = e->FirstChild("primitive");
        if (n) {
            TiXmlElement* sub_e = n->ToElement();
            std::string s;
            if (exists(sub_e, "type")) s = sub_e->Attribute("type");
            if (s.compare("cube") == 0) {
                d.internal = true;
                d.cube = true;
            } else if (s.compare("sphere") == 0) {
                d.internal = true;
                d.sphere = true;
            } else if (s.compare("plane") == 0) {
                d.internal = true;
                d.plane = true;
            } else if (s.compare("empty") == 0) {
                d.internal = true;
                d.empty = true;
            } else if (verbosed) {
                cout << "PProjectLoader::loadNode, unknown primitive '"
                        << s << "'" << endl;
            }
        }
    }
    // checking mesh
    if (
            !d.internal &&
            (
            d.mesh_group.empty() ||
            d.mesh_file.empty()
            )) {
        if (verbosed) {
            cerr << "PProjectLoader::loadNode, primitive and/or mesh "
                    " not correctly set!" << endl;
        }
        return false;
    }
    // loading skeleton
    n = e->FirstChild("skeleton");
    if (n) {
        TiXmlElement* sub_e = n->ToElement();
        if (exists(sub_e, "group"))
            d.skeleton_group = sub_e->Attribute("group");
        if (exists(sub_e, "file"))
            d.skeleton_file = sub_e->Attribute("file");
    }
    // checking skeleton
    if (
            !(d.skeleton_group.empty() && d.skeleton_file.empty()) &&
            (d.skeleton_group.empty() || d.skeleton_file.empty())
            ) {
        if (verbosed) {
            cerr << "PProjectLoader::loadNode, skeleton "
                    " not correctly set!" << endl;
        }
    }
    // loading material
    n = e->FirstChild("material");
    if (n) {

        TiXmlElement* sub_e = n->ToElement();
        if (exists(sub_e, "group"))
            d.mat_group = sub_e->Attribute("group");
        if (exists(sub_e, "name"))
            d.mat_name = sub_e->Attribute("name");

    }

    if (!current_scene->push(d)) {
        if (verbosed) {
            cerr << "PProjectLoader::loadNode, "
                    "failed to store the node in the scene" <<
                    endl;
        }
        return false;
    } else {
        if (verbosed) {
            cout << "PProjectLoader::loadNode, "
                    "polymorph node '" <<
                    current_scene->lastNode()->name <<
                    "' " <<
                    "successfully loaded in scene " << current_scene->UID <<
                    endl;
            current_scene->lastNode()->print();
        }
    }

    return true;

}

bool PXmlLoader::parseLight(TiXmlElement* e) {

    assert(current_scene);

    bool b;
    Ogre::ColourValue colour;
    TiXmlNode* n;

    PLightData d;

    // object data
    parseObjectData(e, (PObjectData&) d);

    // bullet data
    parseObjectBulletData(e->FirstChild("bullet"), (PObjectBulletData&) d);

    // custom
    // general config
    n = e->FirstChild("config");
    if (n) {
        TiXmlElement* sub_e = n->ToElement();
        std::string s;
        if (exists(sub_e, "type")) s = sub_e->Attribute("type");
        if (s.compare("sun") == 0) {
            d.type = (int) Ogre::Light::LT_DIRECTIONAL;
        } else if (s.compare("spot") == 0) {
            d.type = (int) Ogre::Light::LT_SPOTLIGHT;
        } else if (s.compare("point") == 0) {
            d.type = (int) Ogre::Light::LT_POINT;
        } else if (verbosed) {
            cout << "PProjectLoader::loadLight, unknown light type '" <<
                    s << "'" << endl;
        }
    }
    // colors
    n = e->FirstChild("color");
    if (n) {
        TiXmlElement* sub_e = n->ToElement();
        if (decode(sub_e, "shadows", b)) d.shadows = b;
        if (decode(sub_e, "diffuse", colour)) d.diffuse = colour;
        if (decode(sub_e, "specular", colour)) d.specular = colour;
    }

    if (!current_scene->push(d)) {
        if (verbosed) {
            cerr << "PProjectLoader::loadLight, "
                    "failed to store the node in the scene" <<
                    endl;
        }
        return false;
    } else {
        if (verbosed) {
            cout << "PProjectLoader::loadLight, "
                    "polymorph node '" <<
                    current_scene->lastLight()->name <<
                    "' " <<
                    "successfully loaded in scene " << current_scene->UID <<
                    endl;
            current_scene->lastLight()->print();
        }
    }

    return true;

}

//bool PXmlLoader::parseSound(TiXmlElement* e) {
//
//    assert(current_scene);
//
//    TiXmlNode* n;
//
//    PSoundData d;
//
//    // object data
//    parseObjectData(e, (PObjectData&) d);
//
//    // bullet data
//    parseObjectBulletData(e->FirstChild("bullet"), (PObjectBulletData&) d);
//
//    // custom
//    // general config
//    n = e->FirstChild("config");
//    if (n) {
//        TiXmlElement* sub_e = n->ToElement();
//        if (exists(sub_e, "sample"))
//            d.sample = sub_e->Attribute("sample");
//        std::string s;
//        if (exists(sub_e, "type"))
//            s = sub_e->Attribute("type");
//        if (s.compare("point") == 0) {
//            d.type = (int) libpd::PST_POINT;
//        } else if (s.compare("ambient") == 0) {
//            d.type = (int) libpd::PST_AMBIENT;
//        } else if (verbosed) {
//            cout << "PProjectLoader::loadSound, unknown sound type '" <<
//                    s << "'" << endl;
//        }
//    }
//
//    if (!current_scene->push(d)) {
//        if (verbosed) {
//            cerr << "PProjectLoader::loadSound, "
//                    "failed to store the node in the scene" <<
//                    endl;
//        }
//        return false;
//    } else {
//        if (verbosed) {
//            cout << "PProjectLoader::loadSound, "
//                    "polymorph node '" <<
//                    current_scene->lastSound()->name <<
//                    "' " <<
//                    "successfully loaded in scene " << current_scene->UID <<
//                    endl;
//            current_scene->lastSound()->print();
//        }
//    }
//
//    return true;
//
//}

bool PXmlLoader::parseBullet(TiXmlElement* e) {

    TiXmlNode* conf = e->FirstChild("config");
    if (conf) {
        TiXmlElement* conf_e = conf->ToElement();
        Ogre::Vector3 g;
        if (decode(conf_e, "gravity", g))
            pdata.bullet.gravity = PUtil::convert(g);
        Ogre::Real mss;
        if (decode(conf_e, "maxsubsteps", mss))
            pdata.bullet.max_substeps = mss;
        bool verb;
        if (decode(conf_e, "verbose", verb))
            pdata.bullet.verbose = verb;
    }

    if (verbosed) pdata.bullet.print();

    return true;

}

bool PXmlLoader::parseNetwork(TiXmlElement* e) {

    TiXmlNode* osc = e->FirstChild("osc");

    if (!osc) return false;

    if (osc) {

        TiXmlElement* osc_e = osc->ToElement();

        TiXmlNode* senders = osc_e->FirstChild("senders");
        if (senders) {
            TiXmlElement* senders_e = senders->ToElement();
            TiXmlNode* sender;
            for (
                    sender = senders_e->FirstChild("sender");
                    sender;
                    sender = sender->NextSibling("sender")) {

                parseOscSender(
                        sender->ToElement(),
                        pdata.network_osc_senders);

            }
        }

        TiXmlNode* rcvrs = osc_e->FirstChild("receivers");
        if (rcvrs) {
            TiXmlElement* rcvrs_e = rcvrs->ToElement();
            TiXmlNode* rcvr;
            for (
                    rcvr = rcvrs_e->FirstChild("receiver");
                    rcvr;
                    rcvr = rcvr->NextSibling("receiver")) {

                parseOscReceiver(
                        rcvr->ToElement(),
                        pdata.network_osc_receivers);

            }
        }

    }

    if (verbosed) {
        POSDataListIter it = pdata.network_osc_senders.begin();
        POSDataListIter it_end = pdata.network_osc_senders.end();
        for (; it != it_end; ++it) {
            (*it).print();
        }
        PORDataListIter its = pdata.network_osc_receivers.begin();
        PORDataListIter its_end = pdata.network_osc_receivers.end();
        for (; its != its_end; ++its) {
            (*its).print();
        }
    }

    return true;

}

bool PXmlLoader::parseOscSender(TiXmlElement* e, POSDataList& list) {

    bool success = true;
    bool autostart = false;
    bool max32bits = false;
    std::string address;
    uint32_t integer;

    if (exists(e, "ip")) {

        address = e->Attribute("ip");

        if (
                !address.empty() &&
                decode(e, "port", integer)
                ) {

            POscSenderData pos;
            pos.address = address;
            pos.port = integer;

            if (decode(e, "autostart", autostart)) {
                pos.autostart = autostart;
            }

            if (decode(e, "max_32bits", max32bits)) {
                pos.max_32bits = max32bits;
            }

            list.push_back(pos);

        } else {

            success = false;

        }

    } else {

        success = false;

    }

    if (!success) {

        std::stringstream ss;
        ss << "PXmlLoader::parseOscSender, parsing failed, data retrieved: ";
        ss << "address [mandatory]: '" << address << "', ";
        ss << "port [mandatory]: '" << integer << "', ";
        ss << "auto start [optional]: " << autostart << ", ";
        ss << "max 32bits [optional]: " << max32bits;
        PUtil::logCritical(ss.str());

    }

    return success;

}

bool PXmlLoader::parseOscReceiver(TiXmlElement* e, PORDataList& list) {

    bool success = true;
    bool autostart = false;
    std::string address;
    uint32_t port;
    uint32_t maxqueue;

    if (decode(e, "port", port)) {

        POscReceiverData por;
        por.port = port;

        if (decode(e, "autostart", autostart)) {
            por.autostart = autostart;
        }

        if (decode(e, "max_queue_size", maxqueue)) {
            por.max_queue_size = maxqueue;
        }

        list.push_back(por);

    } else {

        success = false;

    }

    if (!success) {

        std::stringstream ss;
        ss << "PXmlLoader::parseOscReceiver, parsing failed, data retrieved: ";
        ss << "port [mandatory]: '" << port << "', ";
        ss << "auto start [optional]: " << autostart << ", ";
        ss << "max queue size [optional]: " << maxqueue;
        PUtil::logCritical(ss.str());

    }

    return success;
}

bool PXmlLoader::parsePuredata(TiXmlElement* e) {

    bool success = true;
    bool editmode = false;
    uint32_t channels = 0;
    uint32_t samplerate = 0;
    uint32_t samples = 0;
    std::string audiodriver = "undefined";
    std::string sp;

    if (decode(e, "edit_mode", editmode)) {
        pdata.pd.edit_mode = editmode;
    }

    TiXmlNode* output = e->FirstChild("output");
    if (output) {
        TiXmlElement* output_e = output->ToElement();

        if (decode(output_e, "channels", channels)) {
            pdata.pd.channels = channels;
        }

        if (decode(output_e, "sample_rate", samplerate)) {
            pdata.pd.sample_rate = samplerate;
        }

        if (decode(output_e, "samples", samples)) {
            pdata.pd.samples = samples;
        }

        if (decode(output_e, "audio_driver", audiodriver)) {
            pdata.pd.audio_driver = audiodriver;
        }

    }

    TiXmlNode* searchpaths = e->FirstChild("searchpaths");
    if (searchpaths) {

        TiXmlElement* searchpaths_e = searchpaths->ToElement();
        TiXmlNode* searchpath;
        for (
                searchpath = searchpaths_e->FirstChild("searchpath");
                searchpath;
                searchpath = searchpath->NextSibling("searchpath")) {

            TiXmlElement* searchpath_e = searchpath->ToElement();

            if (decode(searchpath_e, "value", sp)) {
                pdata.pd.search_paths.push_back(sp);
            }

        }
    }

    TiXmlNode* edition = e->FirstChild("edition");
    if (edition) {

        TiXmlElement* edition_e = edition->ToElement();
        TiXmlNode* senders = edition_e->FirstChild("senders");

        if (senders) {
            TiXmlElement* senders_e = senders->ToElement();
            TiXmlNode* sender;
            for (
                    sender = senders_e->FirstChild("sender");
                    sender;
                    sender = sender->NextSibling("sender")) {

                parseOscSender(
                        sender->ToElement(),
                        pdata.pd.edition_senders);

            }
        }

        TiXmlNode* rcvrs = edition_e->FirstChild("receivers");
        if (rcvrs) {
            TiXmlElement* rcvrs_e = rcvrs->ToElement();
            TiXmlNode* rcvr;
            for (
                    rcvr = rcvrs_e->FirstChild("receiver");
                    rcvr;
                    rcvr = rcvr->NextSibling("receiver")) {

                parseOscReceiver(
                        rcvr->ToElement(),
                        pdata.pd.edition_receivers);

            }
        }

    }

    if (verbosed) {
        pdata.pd.print();
    }

    return success;

}

bool PXmlLoader::parseBulletSpring(
        TiXmlElement* e,
        PBulletSpringData& spring_data
        ) {

    float f;
    bool b;

    Ogre::Vector3 min;
    Ogre::Vector3 max;

    if (decode(e, "stiffness_linear", f)) {
        spring_data.stiffness_linear = f;
    }

    if (decode(e, "damping_linear", f)) {
        spring_data.damping_linear = f;
    }

    if (decode(e, "stiffness_angular", f)) {
        spring_data.stiffness_angular = f;
    }

    if (decode(e, "damping_angular", f)) {
        spring_data.damping_angular = f;
    }

    if (decode(e, "enableEquilibrium", b)) {
        spring_data.enableEquilibrium = b;
    }

    if (decode(e, "stop_cfm", f)) {
        spring_data.stop_cfm = f;
    }

    if (decode(e, "stop_erp", f)) {
        spring_data.stop_erp = f;
    }

    if (
            decode(e, "limit_linear_min", min) &&
            decode(e, "limit_linear_max", max)
            ) {
        spring_data.limit_linear.set(
                PUtil::convert(min),
                PUtil::convert(max));
    }

    if (
            decode(e, "limit_angular_min", min) &&
            decode(e, "limit_angular_max", max)
            ) {
        spring_data.limit_angular.set(
                PUtil::convert(min),
                PUtil::convert(max));
    }

    return false;

}