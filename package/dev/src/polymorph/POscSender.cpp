/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
 */

/* 
 * File:   POscSender.cpp
 * Author: frankiezafe
 * 
 * Created on August 16, 2016, 2:09 PM
 */

#include "POscSender.h"

using namespace osc;
using namespace polymorph;
using namespace Ogre;

POscSender::POscSender() :
_socket(0),
_address(""),
_port(0),
max_32bits(false),
autostart(false),
packet(0) {
}

POscSender::POscSender(POscSenderData& data) :
_socket(0),
_address(data.address),
_port(data.port),
max_32bits(data.max_32bits),
autostart(data.autostart),
packet(0) {
    if (autostart) startEmission();
}

POscSender::~POscSender() {

    stop();

}

void POscSender::configure(POscSenderData& data) {
    if (_address.compare(data.address) == 0 && _port == data.port) return;
    if (_socket) {
        delete _socket;
        _socket = 0;
    }
    _address = data.address;
    _port = data.port;
    max_32bits = data.max_32bits;
    autostart = data.autostart;
    if (autostart) startEmission();
}

void POscSender::setup(String address, polymorph::uint port, bool autostart) {

    if (_address.compare(address) == 0 && _port == port) {
        Ogre::LogManager::getSingleton().logMessage(
                LML_NORMAL,
                "POscSender::setup, "
                "configuration is similar to current one.");
        return;
    }

    stop();

    _address = address;
    _port = port;
    std::string addr = address;

    if (!autostart) {
        return;
    }

    startEmission();
}

bool POscSender::start() {

    if (_address.empty() || _port == 0) {
        Ogre::LogManager::getSingleton().logMessage(
                LML_NORMAL,
                "POscSender::start, "
                "sender is not configured!");
        return false;
    }
    if (_socket) {
        Ogre::LogManager::getSingleton().logMessage(
                LML_NORMAL,
                "POscSender::start, "
                "sender is already started!");
        return false;
    }
    startEmission();
    return true;

}

void POscSender::stop() {

    purgeMessage();

    if (_socket) {
        delete _socket;
        _socket = 0;
    }

}

void POscSender::startEmission() {
    _socket = new UdpTransmitSocket(IpEndpointName(_address.c_str(), _port));
}

void POscSender::sendMessage(bool purge) {

    if (!_socket) {
        Ogre::LogManager::getSingleton().logMessage(
                LML_NORMAL,
                "POscSender::sendMessage, "
                "sender is not started!");
        return;
    }
    if (!packet) {
        Ogre::LogManager::getSingleton().logMessage(
                LML_NORMAL,
                "POscSender::sendMessage, "
                "internal message is not ready, "
                "call startMessage() at least!");
        return;
    }
    
    if (!packed) {
        packMessage();
    }
    
    (*packet) << osc::EndMessage << osc::EndBundle;
    _socket->Send(packet->Data(), packet->Size());
    
    if (!purge) {
        return;
    }
    
    purgeMessage();
    
}

void POscSender::sendMessage(POscMessage& msg) {
    
    if (!_socket) {
        Ogre::LogManager::getSingleton().logMessage(
                LML_NORMAL,
                "POscSender::sendMessage, "
                "sender is not started!");
        return;
    }
    
    if (!msg.ready()) {
        Ogre::LogManager::getSingleton().logMessage(
                LML_NORMAL,
                "POscSender::sendMessage, "
                "external message is not ready!");
        return;
    }
    
    _socket->Send(msg.data(), msg.size());
    
}