/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ___________________________________( ^3^)_____________________________________
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 */

/* 
 * File:   PObjectManager.cpp
 * Author: frankiezafe
 * 
 * Created on November 25, 2016, 2:43 PM
 */

#include "PObjectManager.h"

using namespace std;
using namespace Ogre;
using namespace polymorph;

PObjectManager::PObjectManager( ) :
PXmlLoader( ),
scene_manager( 0 ),
//pd_engine( 0 ),
bullet_engine( 0 ),
loaded_scene( 0 ),
scenenum( 0 ),
sceneids( 0 ) {
}

PObjectManager::~PObjectManager( ) {

    clear( );
    
    if ( scenenum > 0 ) {
        delete[] sceneids;
        sceneids = 0;
        scenenum = 0;
    }

}

bool PObjectManager::parse() {

    if ( scenenum > 0 ) {
        delete[] sceneids;
        sceneids = 0;
        scenenum = 0;
    }

    bool success = PXmlLoader::parse();

    pdata.getSceneIds( sceneids, scenenum );

    return success;
    
}

bool PObjectManager::parse( Ogre::String group, Ogre::String xml ) {

    if ( scenenum > 0 ) {
        delete[] sceneids;
        sceneids = 0;
        scenenum = 0;
    }

    bool success = PXmlLoader::parse( group, xml );

    pdata.getSceneIds( sceneids, scenenum );

    return success;
    
}

void PObjectManager::clear( ) {

    destroyScene( );

    // from PProjectLoader
    pdata.clear( );

}

bool PObjectManager::setSceneManager(
        Ogre::SceneManager* sm,
        bool force
        ) {

    if ( scene_manager && !force ) {
        if ( verbosed ) cout << "PObjectManager::setSceneManager, error: "
                "scene manager is already setup" << endl;
        return false;
    }

    if ( scene_manager && loaded_scene ) {
        destroyScene( );
    }

    scene_manager = sm;

    return true;

}

//bool PObjectManager::setPdEngine(
//        libpd::PdEngine* pe,
//        bool force
//        ) {
//
//    if ( pd_engine && !force ) {
//        if ( verbosed ) cout << "PObjectManager::setPdEngine, error: "
//                "puredata engine is already setup" << endl;
//        return false;
//    }
//
//    if ( pd_engine && loaded_scene ) {
//        destroyScene( );
//    }
//
//    pd_engine = pe;
//
//    return true;
//
//}

bool PObjectManager::setBulletEngine(
        PBullet* be,
        bool force
        ) {

    if ( bullet_engine && !force ) {
        if ( verbosed ) cout << "PObjectManager::setBulletEngine, error: "
                "bullet engine is already setup" << endl;
        return false;
    }

    if ( bullet_engine && loaded_scene ) {
        destroyScene( );
    }

    bullet_engine = be;

    return true;

}

bool PObjectManager::destroyScene( ) {

    if ( !loaded_scene ) {
        return false;
    }

    nodes.clear( );
    lights.clear( );
//    sounds.clear( );
    
    omap.clear( );
    nmap.clear( );
    lmap.clear( );
//    smap.clear( );

    PObjectListIter it = objects.begin( );
    PObjectListIter ite = objects.end( );
    for (; it != ite; ++it ) {
        delete (*it );
    }
    objects.clear( );

    loaded_scene = 0;

    return true;

}

bool PObjectManager::loadScene(
        uint32_t& UID,
        bool force
        ) {

    if ( !scene_manager ) {
        if ( verbosed ) cout << "PObjectManager::loadScene, error: "
                "no scene manager setup" << endl;
        return false;
    }

    PSceneData* psd = pdata.scene( UID );
    if ( !psd ) {
        if ( verbosed ) cout << "PObjectManager::loadScene, error: "
                "no scene with UID " << UID << endl;
        return false;
    }

    return loadScene( psd, force );

}

bool PObjectManager::loadScene(
        PSceneData* psd,
        bool force
        ) {

    if ( !scene_manager ) {
        if ( verbosed ) cout << "PObjectManager::loadScene, error: "
                "no scene manager setup" << endl;
        return false;
    }

    if ( !psd ) {
        if ( verbosed ) cout << "PObjectManager::loadScene, error: "
                "not a valid scene" << endl;
        return false;
    }

    if ( loaded_scene && !force ) {
        if ( verbosed ) cout << "PObjectManager::loadScene, error: "
                "another scene is already setup" << endl;
        return false;
    }

    bool reset_gravity = false;
    if ( loaded_scene ) {
        if ( !loaded_scene->bullet.persistent ) {
            reset_gravity = true;
        }
        destroyScene( );
    }

    size_t nnum = psd->nodeCount( );
    for ( size_t i = 0; i < nnum; ++i ) {
        loadNode( psd->node( i ), true );
    }
    
    size_t lnum = psd->lightCount( );
    for ( size_t i = 0; i < lnum; ++i ) {
        loadLight( psd->light( i ), true );
    }

    if ( psd->bullet.overwrite ) {
        PBullet::gravity( psd->bullet.gravity );
        std::cout << "Custom gravity loading: " << PUtil::str( PUtil::convert(psd->bullet.gravity) ) << std::endl;
    } else if ( reset_gravity ) {
        PBullet::gravity( btVector3( PBULLET_GRAVITY ) );
    }
    
    loaded_scene = psd;

    if ( verbosed ) cout << "PObjectManager::loadLight, success: "
            "scene " << loaded_scene->UID << " loaded" << endl;

    return true;

}

bool PObjectManager::nextScene( ) {

    if ( !scene_manager ) {
        if ( verbosed ) cout << "PObjectManager::nextScene, error: "
                "no scene manager setup" << endl;
        return false;
    }
    if ( scenenum == 0 ) {
        if ( verbosed ) cout << "PObjectManager::nextScene, error: "
                "no scene available" << endl;
        return false;
    }

    if ( !loaded_scene ) {
        loadScene( pdata.scene( sceneids[ 0 ] ) );
        return true;
    }

    for ( size_t i = 0; i < scenenum; ++i ) {
        if ( sceneids[ i ] == loaded_scene->UID ) {
            ++i;
            if ( i >= scenenum ) i = 0;
            loadScene( pdata.scene( sceneids[ i ] ), true );
            return true;
        }
    }

    if ( verbosed ) cout << "PObjectManager::nextScene, error: "
            "something went really wrong!" << endl;

    return false;


}



bool PObjectManager::loadNode(
        uint32_t& sceneUID,
        std::string name,
        bool force
        ) {

    if ( !scene_manager ) {
        if ( verbosed ) cout << "PObjectManager::loadNode, error: "
                "no scene manager setup" << endl;
        return false;
    }

    PSceneData* psd = pdata.scene( sceneUID );
    if ( psd == 0 ) {
        if ( verbosed ) cout << "PObjectManager::loadNode, error: "
                "no scene with UID " << sceneUID << endl;
        return false;
    }

    PNodeData* d = psd->node( name );
    if ( d == 0 ) {
        if ( verbosed ) cout << "PObjectManager::loadNode, error: "
                "no node with name '" << name << "'" << endl;
        return false;
    }

    return loadNode( d, force );

}

bool PObjectManager::loadNode(
        PNodeData* d,
        bool force
        ) {

    if ( !scene_manager ) {
        if ( verbosed ) cout << "PObjectManager::loadNode, error: "
                "no scene manager setup" << endl;
        return false;
    }

    if ( !force ) {
        if ( nmap.find( d->name ) != nmap.end( ) ) {
            if ( verbosed ) cout << "PObjectManager::loadNode, error: "
                    "node '" << d->name << "' already loaded" << endl;
            return false;
        }
    }

    PNode* pn = new PNode( scene_manager, d );
    omap[ d->name ] = static_cast < PObjectAbstract* > ( pn );
    nmap[ d->name ] = pn;

    objects.push_back( pn );
    nodes.push_back( pn );
    
    if ( verbosed ) cout << "PObjectManager::loadNode, success: "
            "node '" << pn->getName( ) << "' loaded" << endl;

//    if ( d->repeat ) {
//
//        Vector3 pos = d->origin_pos;
//        Vector3 ori = d->origin_orientation;
//        Vector3 sca = d->origin_scale;
//
//        Vector3 off_pos = d->repeat_pos;
//        Vector3 off_ori = d->repeat_orientation;
//        Vector3 off_sca = d->repeat_scale;
//
//        for ( uint32_t i = 1; i <= d->repeat_count; ++i ) {
//            polymorph::PNodeData tmpd;
//            tmpd = (*d);
//            stringstream ss;
//            ss << ".";
//            uint32_t max = 1e5;
//            while ( max > i * 10 ) {
//                ss << "0";
//                max /= 10;
//            }
//            ss << i;
//            tmpd.name += ss.str( );
//            tmpd.origin_pos = pos + ( off_pos * i );
//            tmpd.origin_orientation = ori + ( off_ori * i );
//            tmpd.origin_scale = sca + ( off_sca * i );
//            tmpd.repeat = false; // avoiding infinite loop!
//            
//            PNode* pn = new PNode( scene_manager, &tmpd );
//            omap[ d->name ] = static_cast < PObjectAbstract* > ( pn );
//            nmap[ d->name ] = pn;
//
//            objects.push_back( pn );
//            nodes.push_back( pn );
//            
//            if ( verbosed ) cout << "PObjectManager::loadNode, success: "
//                "node '" << pn->getName( ) << "' loaded" << endl;
//            
//        }
//
//    }

    return true;

}

bool PObjectManager::loadLight(
        uint32_t& sceneUID,
        std::string name,
        bool force
        ) {

    if ( !scene_manager ) {
        if ( verbosed ) cout << "PObjectManager::loadLight, error: "
                "no scene manager setup" << endl;
        return false;
    }

    PSceneData* psd = pdata.scene( sceneUID );
    if ( psd == 0 ) {
        if ( verbosed ) cout << "PObjectManager::loadLight, error: "
                "no scene with UID " << sceneUID << endl;
        return false;
    }

    PLightData* d = psd->light( name );
    if ( d == 0 ) {
        if ( verbosed ) cout << "PObjectManager::loadLight, error: "
                "no light with name '" << name << "'" << endl;
        return false;
    }

    return loadLight( d, force );

}

bool PObjectManager::loadLight(
        PLightData* d,
        bool force
        ) {

    if ( !scene_manager ) {
        if ( verbosed ) cout << "PObjectManager::loadLight, error: "
                "no scene manager setup" << endl;
        return false;
    }

    if ( !force ) {
        if ( lmap.find( d->name ) != lmap.end( ) ) {
            if ( verbosed ) cout << "PObjectManager::loadLight, error: "
                    "light '" << d->name << "' already loaded" << endl;
            return false;
        }
    }

    PLight* pl = new PLight( scene_manager, d );
    omap[ d->name ] = static_cast < PObjectAbstract* > ( pl );
    lmap[ d->name ] = pl;

    objects.push_back( pl );
    lights.push_back( pl );

    if ( verbosed ) cout << "PObjectManager::loadLight, success: "
            "light '" << pl->getName( ) << "' loaded" << endl;

    return true;

}

//bool PObjectManager::loadSound(
//        uint32_t& sceneUID,
//        std::string name,
//        bool force
//        ) {
//
//    if ( !scene_manager ) {
//        if ( verbosed ) cout << "PObjectManager::loadSound, error: "
//                "no scene manager setup" << endl;
//        return false;
//    }
//    
//    if ( !pd_engine ) {
//        if ( verbosed ) cout << "PObjectManager::loadSound, error: "
//                "no puredata engine setup" << endl;
//        return false;
//    }
//
//    PSceneData* psd = pdata.scene( sceneUID );
//    if ( psd == 0 ) {
//        if ( verbosed ) cout << "PObjectManager::loadSound, error: "
//                "no scene with UID " << sceneUID << endl;
//        return false;
//    }
//
//    PSoundData* d = psd->sound( name );
//    if ( d == 0 ) {
//        if ( verbosed ) cout << "PObjectManager::loadSound, error: "
//                "no sound with name '" << name << "'" << endl;
//        return false;
//    }
//
//    return loadSound( d, force );
//
//}

//bool PObjectManager::loadSound(
//        PSoundData* d,
//        bool force
//        ) {
//
//    if ( !scene_manager ) {
//        if ( verbosed ) cout << "PObjectManager::loadSound, error: "
//                "no scene manager setup" << endl;
//        return false;
//    }
//    
//    if ( !pd_engine ) {
//        if ( verbosed ) cout << "PObjectManager::loadSound, error: "
//                "no puredata engine setup" << endl;
//        return false;
//    }
//
//    if ( !force ) {
//        if ( smap.find( d->name ) != smap.end( ) ) {
//            if ( verbosed ) cout << "PObjectManager::loadSound, error: "
//                    "sound '" << d->name << "' already loaded" << endl;
//            return false;
//        }
//    }
//
//    PSound* ps = new PSound( scene_manager, pd_engine, d );
//    omap[ d->name ] = static_cast < PObjectAbstract* > ( ps );
//    smap[ d->name ] = ps;
//
//    objects.push_back( ps );
//    sounds.push_back( ps );
//
//    if ( verbosed ) cout << "PObjectManager::loadSound, success: "
//            "sound '" << ps->getName( ) << "' loaded" << endl;
//
//    return true;
//
//}


PNode* PObjectManager::node( std::string name ) {
    
    if ( nmap.find( name ) == nmap.end() ) return 0;
    return nmap[ name ];
    
}

PNode* PObjectManager::node( uint32_t i ) {
    
    if ( i >= nodes.size() ) return 0;
    return nodes[ i ];
    
}

PLight* PObjectManager::light( std::string name ) {
    
    if ( lmap.find( name ) == lmap.end() ) return 0;
    return lmap[ name ];
    
}

PLight* PObjectManager::light( uint32_t i ) {
    
    if ( i >= lights.size() ) return 0;
    return lights[ i ];
    
}

//PSound* PObjectManager::sound( std::string name ) {
//    
//    if ( smap.find( name ) == smap.end() ) return 0;
//    return smap[ name ];
//    
//}

//PSound* PObjectManager::sound( uint32_t i ) {
//    
//    if ( i >= sounds.size() ) return 0;
//    return sounds[ i ];
//    
//}

//void PObjectManager::setupPdEngine( libpd::PdEngine* pde, std::string groupname ) {
//
//    PRDMapIter it = pdata.resources.begin();
//    PRDMapIter ite = pdata.resources.end();
//    std::vector< polymorph::PResourceData* >::iterator vit;
//    std::vector< polymorph::PResourceData* >::iterator vite;
//    for ( ; it != ite; ++it ) {
//        if ( !groupname.empty() && groupname.compare( it->first ) != 0 ) {
//            continue;
//        }
//        vit = it->second.begin();
//        vite = it->second.end();
//        for ( ; vit != vite; ++vit ) {
//            polymorph::PResourceData* prd = (*vit);
//            pde->addSearchPath( prd->path );
//            cout << "pd search path added: " << prd->path << endl;
//        }
//    }
//    
//}