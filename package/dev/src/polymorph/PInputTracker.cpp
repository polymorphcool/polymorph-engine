/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2017 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
 */

/* 
 * File:   PInputTracker.cpp
 * Author: frankiezafe
 * 
 * Created on September 27, 2017, 6:15 PM
 */

#include "PInputTracker.h"

using namespace std;
using namespace Ogre;
using namespace polymorph;

PInputTracker::PInputTracker() :
window(0),
wdimension(Vector2::ZERO),
wmult(Vector2::ZERO),
wratio(0),
mouse_moved(false),
mouse_absolute_position(0, 0),
mouse_relative_position(0, 0),
mouse_cumulative(false) {

    mbuttons = new PMouseButton*[ PMOUSEBUTTON_COUNT ];
    for (uint16_t i = 0; i < PMOUSEBUTTON_COUNT; ++i) {
        mbuttons[i] = 0;
    }

    keys = new PKeyboardKey*[ PKEY_COUNT ];
    for (uint16_t i = 0; i < PKEY_COUNT; ++i) {
        keys[i] = 0;
    }

}

PInputTracker::~PInputTracker() {

    for (uint16_t i = 0; i < PMOUSEBUTTON_COUNT; ++i) {
        if (mbuttons[i]) {
            delete mbuttons[i];
        }
    }

    for (uint16_t i = 0; i < PKEY_COUNT; ++i) {
        if (keys[i]) {
            delete keys[i];
        }
    }

    delete [] mbuttons;
    delete [] keys;
}

void PInputTracker::init(Ogre::RenderWindow* w) {

    window = w;

    windowResized(w);

}

void PInputTracker::purge() {

    for (uint16_t i = 0; i < PMOUSEBUTTON_COUNT; ++i) {
        if (mbuttons[i]) {
            mbuttons[i]->purge();
        }
    }

    for (uint16_t i = 0; i < PKEY_COUNT; ++i) {
        if (keys[i]) {
            keys[i]->purge();
        }
    }

}

void PInputTracker::reset() {

    for (uint16_t i = 0; i < PMOUSEBUTTON_COUNT; ++i) {
        if (mbuttons[i]) {
            mbuttons[i]->changed = false;
        }
    }

    for (uint16_t i = 0; i < PKEY_COUNT; ++i) {
        if (keys[i]) {
            keys[i]->changed = false;
        }
    }

    mouse_moved = false;

}

bool PInputTracker::addMouseButton(OIS::MouseButtonID id) {
    if (mbuttons[id]) {
        return false;
    }
    mbuttons[id] = new PMouseButton();
    return true;
}

bool PInputTracker::removeMouseButton(OIS::MouseButtonID id) {
    if (!mbuttons[id]) {
        return false;
    }
    delete mbuttons[id];
    mbuttons[id] = 0;
    return true;
}

bool PInputTracker::addKey(
        OIS::KeyCode k,
        OIS::KeyCode opposite_key,
        bool prioriy_to_new) {

    if (
            keys[k] ||
            (
            opposite_key != OIS::KC_UNASSIGNED &&
            keys[opposite_key]
            )
            ) {
        return false;
    }

    keys[k] = new PKeyboardKey(k, opposite_key, prioriy_to_new);

    if (opposite_key != OIS::KC_UNASSIGNED) {
        keys[opposite_key] = new PKeyboardKey(opposite_key, k, prioriy_to_new);
    }

    return true;

}

bool PInputTracker::removeKey(
        OIS::KeyCode k,
        bool remove_opposite) {

    if (!keys[k]) {
        return false;
    }

    if (remove_opposite && keys[keys[k]->opposite]) {
        delete keys[keys[k]->opposite];
        keys[keys[k]->opposite] = 0;
    } else if (keys[keys[k]->opposite]) {
        keys[keys[k]->opposite]->opposite = OIS::KC_UNASSIGNED;
    }
    delete keys[k];
    keys[k] = 0;
    return true;

}

bool PInputTracker::keyPressed(const OIS::KeyEvent &arg) {

    if (!keys[arg.key]) {
        return false;
    }

    PKeyboardKey* k = keys[arg.key];
    k->changed = true;

    if (
            k->hasOpposite() &&
            keys[k->opposite]->isPressed()
            ) {

        if (k->new_first) {

            k->status = KS_PRESSED;
            keys[k->opposite]->status = KS_CONCURRENT;
            keys[k->opposite]->changed = true;

        } else {

            k->status = KS_CONCURRENT;

        }

    } else {

        k->status = KS_PRESSED;

    }

    return true;

}

bool PInputTracker::keyReleased(const OIS::KeyEvent &arg) {

    if (!keys[arg.key]) {
        return false;
    }

    PKeyboardKey* k = keys[arg.key];
    k->changed = true;
    k->status = KS_RELEASED;

    if (
            k->hasOpposite() &&
            keys[k->opposite]->isConcurrent()
            ) {

        keys[k->opposite]->changed = true;
        keys[k->opposite]->status = KS_PRESSED;

    }

    return true;

}

void PInputTracker::updateMouse(const OIS::MouseEvent &arg) {

    mouse_relative_position =
            Ogre::Vector2(arg.state.X.rel, arg.state.Y.rel) * wmult;


    if (!mouse_cumulative) {
        mouse_absolute_position =
                Ogre::Vector2(arg.state.X.abs, arg.state.Y.abs) * wmult;
    } else {
        mouse_absolute_position += mouse_relative_position;
    }

    if (mouse_relative_position.length() != 0) {
        mouse_moved = true;
    }

}

bool PInputTracker::mouseMoved(const OIS::MouseEvent &arg) {

    updateMouse(arg);

    return true;

}

bool PInputTracker::mousePressed(const OIS::MouseEvent &arg, OIS::MouseButtonID id) {

    updateMouse(arg);

    if (!mbuttons[ id ] || mbuttons[ id ]->pressed) {
        return false;
    }

    mbuttons[ id ]->changed = true;
    mbuttons[ id ]->pressed = true;
    mbuttons[ id ]->pressed_pos = mouse_absolute_position;

    return true;

}

bool PInputTracker::mouseReleased(const OIS::MouseEvent &arg, OIS::MouseButtonID id) {

    updateMouse(arg);

    if (!mbuttons[ id ] || !mbuttons[ id ]->pressed) {
        return false;
    }

    mbuttons[ id ]->changed = true;
    mbuttons[ id ]->pressed = false;
    mbuttons[ id ]->released_pos = mouse_absolute_position;

    return true;

}

bool PInputTracker::windowResized(Ogre::RenderWindow* rw) {

    if (rw && rw == window) {

        wdimension = Ogre::Vector2(
                window->getWidth(),
                window->getHeight());

        wmult = Ogre::Vector2(
                1.f / window->getWidth(),
                1.f / window->getHeight());

        wratio = window->getWidth() * 1.f / window->getHeight();

        return true;

    }

    return false;

}