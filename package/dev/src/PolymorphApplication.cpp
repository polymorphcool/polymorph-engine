/*
-----------------------------------------------------------------------------
Filename:    CMakeLists.txt
-----------------------------------------------------------------------------

 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      
 

 art & game engine
 
____________________________________  ?   ____________________________________
                                    (._.)
 
 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
___________________________________( ^3^)_____________________________________
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 */

#include "PolymorphApplication.h"

using namespace std;
using namespace Ogre;
using namespace OgreBites;

PolymorphApplication::PolymorphApplication() :
shutdownRequested(false) {

    root = 0;
    window = 0;
    mouse = 0;
    keyboard = 0;
    sceneMgr = 0;
    inputMgr = 0;
    overlaySys = 0;
    trayMgr = 0;
    cam = 0;
    compositorwp = 0;
    overed = 0;

#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
    resourcePath = macBundlePath() + "/Contents/Resources/";
#else
    resourcePath = "";
#endif

}

PolymorphApplication::~PolymorphApplication() {
    if (trayMgr) delete trayMgr;
    if (root) OGRE_DELETE root;
}

/*********** SETUP ************/

void PolymorphApplication::go(void) {
    if (!setup()) {
        return;
    }
    root->startRendering();
    destroyScene();
}

bool PolymorphApplication::setup(void) {

    String pluginsPath;
    // only use plugins.cfg if not static
#ifndef OGRE_STATIC_LIB
    pluginsPath = resourcePath + "plugins.cfg";
#endif
	

    root = OGRE_NEW Root(pluginsPath, resourcePath + "polymorph.cfg", resourcePath + "polymorph.log");
    overlaySys = OGRE_NEW Ogre::OverlaySystem();

    setupResources();

    bool carryOn = configure();
    if (!carryOn) return false;

    loadResources();

    createSceneManager();
    createCamera();
    createCompositor();
    createResourceListener();
    createInputListener();
    createFrameListener();
    createTray();
    createScene();

    return true;

}

/*********** SETUP SUB TASKS, BY CALL ORDER ************/

// see SampleContext::locateResources() for a cross-platform resources setup

void PolymorphApplication::setupResources(void) {

    std::string path = "resources.cfg";
    std::ifstream fp;
    fp.open(path.c_str(), std::ios::in | std::ios::binary);

    if (fp) {

        // Load resource paths from config file
        ConfigFile cf;
        cf.load(resourcePath + "resources.cfg");
        // Go through all sections & settings in the file
        ConfigFile::SectionIterator seci = cf.getSectionIterator();
        String secName, typeName, archName;
        //    cout << "\n** setupResources **" << endl;
        while (seci.hasMoreElements()) {
            secName = seci.peekNextKey();
            ConfigFile::SettingsMultiMap *settings = seci.getNext();
            ConfigFile::SettingsMultiMap::iterator i;
            for (i = settings->begin(); i != settings->end(); ++i) {
                typeName = i->first;
                archName = i->second;
#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
                // OS X does not set the working directory relative to the app,
                // In order to make things portable on OS X we need to provide
                // the loading with it's own bundle path location
                ResourceGroupManager::getSingleton().addResourceLocation(
                        String(macBundlePath() + "/" + archName), typeName, secName);
#else
                ResourceGroupManager::getSingleton().addResourceLocation(
                        archName, typeName, secName);
#endif
                //            std::cout << "\t" << archName << " > " << typeName << " > " << secName << std::endl;
            }
        }

    } else {

        OGRE_EXCEPT(
                Exception::ERR_FILE_NOT_FOUND,
                "no 'resources.cfg' file not found! "
                "Overload this method in your class to remove this error",
                "PolymorphApplication::setupResources");

    }

}

void PolymorphApplication::setupResources(
        polymorph::PProjectData& ppd
        ) {

    std::vector< polymorph::PResourceData* >::iterator vit;
    std::vector< polymorph::PResourceData* >::iterator vite;
    polymorph::PRDMapIter it = ppd.resources.begin();
    polymorph::PRDMapIter ite = ppd.resources.end();
    for (; it != ite; ++it) {

        vit = it->second.begin();
        vite = it->second.end();

        for (; vit != vite; ++vit) {

            polymorph::PResourceData* prd = (*vit);

#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
            // OS X does not set the working directory relative to the app,
            // In order to make things portable on OS X we need to provide
            // the loading with it's own bundle path location
            ResourceGroupManager::getSingleton().addResourceLocation(
                    String(macBundlePath() + "/" + prd->path),
                    prd->type,
                    prd->group,
                    prd->recursive,
                    prd->readonly
                    );
#else
            ResourceGroupManager::getSingleton().addResourceLocation(
                    prd->path,
                    prd->type,
                    prd->group,
                    prd->recursive,
                    prd->readonly
                    );
#endif
        }

    }

}

bool PolymorphApplication::configure(void) {

    // Show the configuration dialog and initialise the system
    // You can skip this and use root.restoreConfig() to load configuration
    // settings if you were sure there are valid ones saved in ogre.cfg
    if (root->showConfigDialog()) {
        // If returned true, user clicked OK so initialise
        // Here we choose to let the system create a default rendering window by passing 'true'
        if (window_title.empty()) {

            std::stringstream ss;
            ss << "Polymorph engine v" <<
                    POLYMORPH_VERSION << "." <<
                    POLYMORPH_SUBVERSION;
            window = root->initialise(true, ss.str());

        } else {

            window = root->initialise(true, window_title);

        }

        return true;

    } else {
        return false;
    }

}

void PolymorphApplication::loadResources(void) {

    ResourceGroupManager::getSingleton().initialiseAllResourceGroups();

    // print all available resources
    //
    //    getFileList( "General", true );
    //    getFileList( "Essential", true );
    //
    //    ResourceGroupManager::ResourceManagerIterator rgmi = ResourceGroupManager::getSingleton( ).getResourceManagerIterator( );
    //    while ( rgmi.current( ) != rgmi.end( ) ) {
    //        std::cout << "\nResourceGroupManager: " << rgmi.peekNextKey( ) << std::endl;
    //        ResourceManager* rm = rgmi.peekNextValue( );
    //        ResourceManager::ResourceMapIterator rmi = rm->getResourceIterator( );
    //        while ( rmi.current( ) != rmi.end( ) ) {
    //            std::cout << "\t" << rmi.peekNextKey( ) << std::endl;
    //            std::cout << "\t[" << rmi.peekNextValue( )->getGroup( ) << "] " << rmi.peekNextValue( )->getName( ) << std::endl;
    //            rmi.moveNext( );
    //        }
    //        rgmi.moveNext( );
    //    }

}

void PolymorphApplication::createSceneManager(void) {

#if OGRE_DEBUG_MODE
    //Debugging multithreaded code is a PITA, disable it.
    const size_t numThreads = 1;
    Ogre::InstancingThreadedCullingMethod threadedCullingMethod = Ogre::INSTANCING_CULLING_SINGLETHREAD;
#else
    //getNumLogicalCores() may return 0 if couldn't detect
    const size_t numThreads = std::max<size_t>(1, Ogre::PlatformInformation::getNumLogicalCores());
    Ogre::InstancingThreadedCullingMethod threadedCullingMethod = Ogre::INSTANCING_CULLING_SINGLETHREAD;
    if (numThreads > 1) threadedCullingMethod = Ogre::INSTANCING_CULLING_THREADED;
#endif

    sceneMgr = Ogre::Root::getSingleton().createSceneManager(Ogre::ST_GENERIC, numThreads, threadedCullingMethod);
    sceneMgr->addRenderQueueListener(overlaySys);

#ifdef INCLUDE_RTSHADER_SYSTEM
    mShaderGenerator->addSceneManager(sceneMgr);

#endif


}

void PolymorphApplication::createCamera(void) {

    cam = sceneMgr->createCamera("PlayerCam");
    cam->setPosition(Vector3(0, 0, 500));
    cam->lookAt(Vector3(0, 0, 0));
    cam->setNearClipDistance(5);
    cam->setFarClipDistance(10000);

}

void PolymorphApplication::createCompositor(void) {

    compositorMgr = root->getCompositorManager2();
    compositorwpname = "CompositorBaseWorkspace";
    const Ogre::IdString workspaceName(compositorwpname);
    if (!compositorMgr->hasWorkspaceDefinition(workspaceName)) {
        compositorMgr->createBasicWorkspaceDef(
                "CompositorBaseWorkspace",
                Ogre::ColourValue(0.4f, 0.4f, 0.4f),
                Ogre::IdString()
                );
    }
    compositorwp = compositorMgr->addWorkspace(sceneMgr, window, cam, workspaceName, true);

}

bool PolymorphApplication::createCompositor(
        polymorph::PCompositorWSData* compdata,
        Ogre::SceneManager* sm,
        Ogre::RenderWindow* win,
        Ogre::Camera* cam
        ) {

    if (!compositorMgr) {
        compositorMgr = root->getCompositorManager2();
    }

    if (compdata->basic) {

        const Ogre::IdString workspaceName(compdata->name);
        if (!compositorMgr->hasWorkspaceDefinition(workspaceName)) {

            Ogre::ColourValue bgcolor(0.4f, 0.4f, 0.4f);
            if (compdata->containsColor("background")) {
                bgcolor = compdata->color("background");
            }

            compositorMgr->createBasicWorkspaceDef(
                    compdata->name, bgcolor, Ogre::IdString());

            compositorwp = compositorMgr->addWorkspace(
                    sm, win, cam, workspaceName, true);

            return true;

        } else {

            std::stringstream ss;
            ss << "PolymorphApplication::createCompositor, a workspace named '"
                    << compdata->name << "' already exists!";
            polymorph::PUtil::logCritical(ss.str());
            return false;

        }

    } else {

        const Ogre::IdString workspaceName(compdata->workspace);
        compositorwp = compositorMgr->addWorkspace(
                sm, win, cam, workspaceName, true);

        // loading variables and colors
        CompositorPassQuad* cpd;
        GpuProgramParametersSharedPtr gpp;

        polymorph::PWSColorMapIter itc = compdata->colors.begin();
        polymorph::PWSColorMapIter itce = compdata->colors.end();
        for (; itc != itce; ++itc) {
            cpd =
                    (CompositorPassQuad*)
                    compositorwp->getNodeSequence()[ itc->second.node ]->
                    _getPasses()[ itc->second.pass ];
            gpp = cpd->getPass()->getFragmentProgramParameters();
            gpp->getFloatPointer(itc->second.index)[0] = itc->second.value[0];
            gpp->getFloatPointer(itc->second.index)[1] = itc->second.value[1];
            gpp->getFloatPointer(itc->second.index)[2] = itc->second.value[2];
        }

        polymorph::PWSVarsMapIter itv = compdata->variables.begin();
        polymorph::PWSVarsMapIter itve = compdata->variables.end();
        for (; itv != itve; ++itv) {
            cpd =
                    (CompositorPassQuad*)
                    compositorwp->getNodeSequence()[ itv->second.node ]->
                    _getPasses()[ itv->second.pass ];
            gpp = cpd->getPass()->getFragmentProgramParameters();
            gpp->getFloatPointer(itv->second.index)[0] = itv->second.value;
        }

        return true;

    }

}

void PolymorphApplication::rebuildCompositorConnections() {

    CompositorWorkspaceDef* workspaceDef = compositorMgr->getWorkspaceDefinition(Ogre::IdString(compositorwpname));
    workspaceDef->clearAllInterNodeConnections();
    IdString finalCompositionId = "FinalComposition";
    const CompositorNodeVec &nodes = compositorwp->getNodeSequence();
    IdString lastInNode;
    CompositorNodeVec::const_iterator it = nodes.begin();
    CompositorNodeVec::const_iterator en = nodes.end();
    while (it != en) {
        CompositorNode *outNode = *it;
        if (outNode->getEnabled() && outNode->getName() != finalCompositionId) {
            CompositorNodeVec::const_iterator it2 = it + 1;
            while (it2 != en && (!(*it2)->getEnabled() || (*it2)->getName() == finalCompositionId))
                ++it2;
            if (it2 != en) {
                lastInNode = (*it2)->getName();
                workspaceDef->connect(outNode->getName(), lastInNode);
            }
            it = it2 - 1;
        }
        ++it;
    }
    if (lastInNode == IdString())
        lastInNode = "CompositorSampleStdRenderer";
    workspaceDef->connect(lastInNode, 0, "FinalComposition", 1);
    compositorwp->reconnectAllNodes();

}

void PolymorphApplication::createResourceListener(void) {

}

void PolymorphApplication::createInputListener(void) {

    OIS::ParamList pl;
    size_t windowHnd = 0;
    std::ostringstream windowHndStr;
    window->getCustomAttribute("WINDOW", &windowHnd);
    windowHndStr << windowHnd;
    pl.insert(std::make_pair(std::string("WINDOW"), windowHndStr.str()));

    appConfig.addParams(pl);

    //#if defined OIS_WIN32_PLATFORM
    //    pl.insert( std::make_pair( std::string( "w32_mouse" ), std::string( "DISCL_FOREGROUND" ) ) );
    //    pl.insert( std::make_pair( std::string( "w32_mouse" ), std::string( "DISCL_NONEXCLUSIVE" ) ) );
    //    pl.insert( std::make_pair( std::string( "w32_keyboard" ), std::string( "DISCL_FOREGROUND" ) ) );
    //    pl.insert( std::make_pair( std::string( "w32_keyboard" ), std::string( "DISCL_NONEXCLUSIVE" ) ) );
    //#elif defined OIS_LINUX_PLATFORM
    //    // enable to capture mouse
    //    //    pl.insert( std::make_pair( std::string( "x11_mouse_grab" ), std::string( "true" ) ) );
    //    pl.insert( std::make_pair( std::string( "x11_mouse_grab" ), std::string( "false" ) ) );
    //    pl.insert( std::make_pair( std::string( "x11_mouse_hide" ), std::string( "true" ) ) );
    //    pl.insert( std::make_pair( std::string( "x11_keyboard_grab" ), std::string( "false" ) ) );
    //    pl.insert( std::make_pair( std::string( "XAutoRepeatOn" ), std::string( "true" ) ) );
    //#endif

    inputMgr = OIS::InputManager::createInputSystem(pl);
    if (appConfig.input_keyboard_enable) {
        keyboard = static_cast<OIS::Keyboard*> (inputMgr->createInputObject(OIS::OISKeyboard, true));
        if (appConfig.input_keyboard_callbacks) {
            keyboard->setEventCallback(this);
        }
    }
    if (appConfig.input_mouse_enable) {
        mouse = static_cast<OIS::Mouse*> (inputMgr->createInputObject(OIS::OISMouse, true));
        if (appConfig.input_mouse_callbacks) {
            mouse->setEventCallback(this);
        }
    }

    inputContext.mKeyboard = keyboard;
    inputContext.mMouse = mouse;

}

void PolymorphApplication::createFrameListener(void) {

    windowResized(window);
    Ogre::WindowEventUtilities::addWindowEventListener(window, this);

    root->addFrameListener(this);

}

void PolymorphApplication::createTray(void) {

    trayMgr = new SdkTrayManager("SampleControls", window, inputContext, this);
    trayMgr->showFrameStats(TL_BOTTOMLEFT);
    trayMgr->showLogo(TL_TOPRIGHT);

}

void PolymorphApplication::createScene(void) {
}

void PolymorphApplication::destroyScene(void) {
}

/*********** SDKTRAY EVENTS ************/

void PolymorphApplication::buttonHit(OgreBites::Button* button) {
}

void PolymorphApplication::itemSelected(OgreBites::SelectMenu* menu) {
}

void PolymorphApplication::labelHit(OgreBites::Label* label) {
}

void PolymorphApplication::sliderMoved(OgreBites::Slider* slider) {
}

void PolymorphApplication::checkBoxToggled(OgreBites::CheckBox* box) {
}

void PolymorphApplication::okDialogClosed(const Ogre::DisplayString& message) {
}

void PolymorphApplication::yesNoDialogClosed(const Ogre::DisplayString& question, bool yesHit) {
}

/*********** INPUT ************/

bool PolymorphApplication::keyPressed(const OIS::KeyEvent &arg) {
    switch (arg.key) {
        case OIS::KC_ESCAPE:
            shutdownRequested = true;
            break;
        case OIS::KC_TAB:
            window->writeContentsToTimestampedFile("screenshot", ".png");
            break;
        default:
            break;
    }
    return true;
}

bool PolymorphApplication::keyReleased(const OIS::KeyEvent &arg) {
    return true;
}

bool PolymorphApplication::mouseMoved(const OIS::MouseEvent &arg) {
    if (trayMgr->injectPointerMove(arg)) return true;
    return true;
}

bool PolymorphApplication::mousePressed(const OIS::MouseEvent &arg, OIS::MouseButtonID id) {
    if (trayMgr->injectPointerDown(arg, id)) return true;
    return true;
}

bool PolymorphApplication::mouseReleased(const OIS::MouseEvent &arg, OIS::MouseButtonID id) {
    if (trayMgr->injectPointerUp(arg, id)) return true;
    return true;
}

#if OIS_WITH_MULTITOUCH

bool PolymorphApplication::touchMoved(const OIS::MultiTouchEvent& evt) {
    return true;
}

bool PolymorphApplication::touchPressed(const OIS::MultiTouchEvent& evt) {
    return true;
}

bool PolymorphApplication::touchReleased(const OIS::MultiTouchEvent& evt) {
    return true;
}

bool PolymorphApplication::touchCancelled(const OIS::MultiTouchEvent &evt) {
    return true;
}

#endif

void PolymorphApplication::windowResized(Ogre::RenderWindow* rw) {

    unsigned int width, height, depth;
    int left, top;
    rw->getMetrics(width, height, depth, left, top);

    const OIS::MouseState &ms = mouse->getMouseState();
    ms.width = width;
    ms.height = height;

    fit(cam, window);

}

void PolymorphApplication::windowClosed(Ogre::RenderWindow* rw) {
    if (rw == window) {
        if (inputMgr) {
            inputMgr->destroyInputObject(mouse);
            inputMgr->destroyInputObject(keyboard);
            OIS::InputManager::destroyInputSystem(inputMgr);
            inputMgr = 0;
        }
    }
}

void PolymorphApplication::deselectObject() {
    if (overed) {
        ((Entity*) overed->getAttachedObject(0))->setMaterialName(overed_mat_name);
        overed = 0;
    }
}

bool PolymorphApplication::pickObject(const OIS::MouseEvent &arg, OIS::MouseButtonID id) {

    Real srcx = Real(arg.state.X.abs) / window->getWidth();
    Real srcy = Real(arg.state.Y.abs) / window->getHeight();
    if (srcx < 0 || srcx > 1 || srcy < 0 || srcy > 1) {
        return false;
    }
    Ogre::Ray r = cam->getCameraToViewportRay(srcx, srcy);
    Ogre::RaySceneQuery* rquery = sceneMgr->createRayQuery(r);
    rquery->setSortByDistance(true);
    Ogre::RaySceneQueryResult rresults = rquery->execute();
    Ogre::SceneNode* newovered = 0;
    if (rresults.size() > 0) {
        newovered = rresults[0].movable->getParentSceneNode();
    }
    if (overed == newovered) {
        return true;
    } else if (overed) {
        deselectObject();
    }
    overed = newovered;
    bool success = false;
    if (overed != 0) {
        overed_mat_name = ((Entity*) overed->getAttachedObject(0))->getSubEntity(0)->getMaterialName();
        ((Entity*) overed->getAttachedObject(0))->setMaterialName(picked_mat_name);
        success = true;
    }
    sceneMgr->destroyQuery(rquery);
    return success;

}

/*********** FRAMES ************/

bool PolymorphApplication::frameStarted(const FrameEvent& evt) {
    before_draw();
    return true;
}

bool PolymorphApplication::frameRenderingQueued(const FrameEvent& evt) {

    if (window->isClosed()) return false;
    if (shutdownRequested) return false;

    if (trayMgr) trayMgr->frameRenderingQueued(evt);

#ifdef SDL_AVAILABLE
    input::GameController::capture(evt);
#endif

    keyboard->capture();
    mouse->capture();

    // animation
    draw_event.timeSinceLastEvent = evt.timeSinceLastEvent;
    draw_event.timeSinceLastFrame = evt.timeSinceLastFrame;
    draw();

    return true;

}

bool PolymorphApplication::frameEnded(const FrameEvent& evt) {
    after_draw();
    return true;
}

/*********** UTILS ************/

void PolymorphApplication::fit(Ogre::Camera * c, Ogre::Viewport * vp) {
    if (!c || !vp) return;
    c->setAspectRatio(Ogre::Real(vp->getActualWidth()) / Ogre::Real(vp->getActualHeight()));
}

void PolymorphApplication::fit(Ogre::Camera * c, Ogre::RenderWindow * rw) {
    if (!c || !rw) return;
    c->setAspectRatio(Ogre::Real(rw->getWidth()) / Ogre::Real(rw->getHeight()));
}

FileInfoList PolymorphApplication::getFileList(Ogre::String group, bool print_to_console) {

    FileInfoListPtr finfos = ResourceGroupManager::getSingleton().listResourceFileInfo(group);
    if (print_to_console) {
        std::cout << "\n** all files in group: '" << group << "' **" << std::endl;
        for (unsigned int i = 0; i < finfos->size(); ++i) {
            std::cout << "\t" << finfos->at(i).filename << std::endl;
        }
    }
    FileInfoList output(finfos->size());
    copy(finfos->begin(), finfos->end(), std::back_inserter(output));
    return output;
}

StringVector PolymorphApplication::getResourceList(Ogre::String group, bool print_to_console) {

    StringVectorPtr names = ResourceGroupManager::getSingleton().listResourceNames(group);
    if (print_to_console) {
        std::cout << "\n** all resources in group: '" << group << "' **" << std::endl;
        for (unsigned int i = 0; i < names->size(); ++i) {
            std::cout << "\t" << names->at(i) << std::endl;
        }
    }
    StringVector output(names->size());
    copy(names->begin(), names->end(), std::back_inserter(output));
    return output;
}

/*********** SIMPLIFIED ************/

void PolymorphApplication::before_draw() {
}

void PolymorphApplication::draw() {
}

void PolymorphApplication::after_draw() {
}