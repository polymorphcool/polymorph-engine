/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
*/

/* 
 * File:   GameController.cpp
 * Author: frankiezafe
 * 
 * Created on August 25, 2016, 2:32 PM
 */

#include "GameController.h"

using namespace Ogre;
using namespace std;
using namespace input;

GameController::GameController( SDL_GameController* gc, polymorph::uint id ):
_gcconf(0),
_active( false ),
_init( false ),
_modified( false ),
leftstick_move( false ),
rightstick_move( false )
{
    _gc = gc;
    _id = id;
    int i_end = (int) GCB_MAX;
    for ( int i = 0; i < i_end; ++i ) {
        axis[ (GameControllerAxis) i ] = Vector2::ZERO;
        norm_axis[ (GameControllerAxis) i ] = Vector2::ZERO;
    }
    i_end = (int) SDL_CONTROLLER_BUTTON_MAX;
    for ( int i = 0; i < i_end; ++i ) {
        buttons[ (SDL_GameControllerButton) i ] = false;
    }
    
}

GameController::~GameController( ) {
    SDL_GameControllerClose( _gc );
}

bool GameController::update( GameControllerFrame* data ) {
    
    _modified = false;
    
    int i_end = (int) GCB_MAX;
    for ( int i = 0; i < i_end; ++i ) {
        updateAxis( (GameControllerAxis) i );
    }
    _init = true;
    i_end = (int) SDL_CONTROLLER_BUTTON_MAX;
    for ( int i = 0; i < i_end; ++i ) {
        bool b = SDL_GameControllerGetButton( _gc, (SDL_GameControllerButton) i );
        if ( b != buttons[ (SDL_GameControllerButton) i ] ) {
            _modified = true;
            buttons[ (SDL_GameControllerButton) i ] = b;
        }
    }

    data->id = _id;
    data->axis = norm_axis;
    data->buttons = buttons;
    
    return _modified;
    
}

void GameController::updateAxis( GameControllerAxis gca ) {
    
    Vector2 prev = axis[ gca ];
    
    switch ( gca ) {
        case GCB_LEFTSTICK:
            axis[ gca ].x = SDL_GameControllerGetAxis( _gc, SDL_CONTROLLER_AXIS_LEFTX );
            axis[ gca ].y = SDL_GameControllerGetAxis( _gc, SDL_CONTROLLER_AXIS_LEFTY );
            if ( !_init ) dead_axis_previous[ gca ] = axis[ gca ];
            break;
        case GCB_RIGHTSTICK:
            axis[ gca ].x = SDL_GameControllerGetAxis( _gc, SDL_CONTROLLER_AXIS_RIGHTX );
            axis[ gca ].y = SDL_GameControllerGetAxis( _gc, SDL_CONTROLLER_AXIS_RIGHTY );
            if ( !_init ) dead_axis_previous[ gca ] = axis[ gca ];
            break;
        case GCB_TRIGGER:
            axis[ gca ].x = SDL_GameControllerGetAxis( _gc, SDL_CONTROLLER_AXIS_TRIGGERLEFT );
            axis[ gca ].y = SDL_GameControllerGetAxis( _gc, SDL_CONTROLLER_AXIS_TRIGGERRIGHT );
            break;
        default:
            break;
    }
    
    if ( prev != axis[ gca ] ) _modified = true;
    
    // normalised axis
    if ( !_gcconf ) return;
    switch ( gca ) {
        case GCB_LEFTSTICK:
        case GCB_RIGHTSTICK:
            normDeadAxis( gca );
            break;
        case GCB_TRIGGER:
            normAxis( gca );
            break;
        default:
            break;
    }
    
}

void GameController::normAxis( GameControllerAxis gca ) {
    for ( int i = 0; i < 2; ++i ) {
        norm_axis[gca][i] = axis[gca][i] / _gcconf->sticks_divider;
        if ( norm_axis[gca][i] > 1 ) norm_axis[gca][i] = 1;
        if ( norm_axis[gca][i] < -1 ) norm_axis[gca][i] = -1;
    }
}

void GameController::normDeadAxis( GameControllerAxis gca ) {
    
    Ogre::Vector2& v = axis[gca];
    Ogre::Vector2& norm_v = norm_axis[gca];
    Ogre::Vector2& prev_v = dead_axis_previous[gca];
    
    for ( int i = 0; i < 2; ++i ) {
        Real abs = Math::Abs( v[i] );
        if ( _gcconf->strict ) {
            if ( abs <= _gcconf->dead_zone_sticks ) {
                norm_v[i] = 0;
            } else {
                int16_t diff = _gcconf->dead_zone_sticks;
                if ( v[i] < 0 ) diff *= -1;
                norm_v[i] = ( v[i] - diff ) / _gcconf->dead_sticks_divider;
                if ( norm_v[i] > 1 ) norm_v[i] = 1;
                if ( norm_v[i] < -1 ) norm_v[i] = -1;
            }
        } else {
            if ( 
                abs <= _gcconf->dead_zone_sticks &&
                ( 
                    ( v[i] == prev_v[i] ) ||
                    Math::Abs( prev_v[i] - v[i] ) > _gcconf->sticks_min_delta
                )
            ) {
                // inside dead zone
                norm_v[i] = 0;
            } else {
                norm_v[i] = v[i] / _gcconf->sticks_divider;
                if ( norm_v[i] > 1 ) norm_v[i] = 1;
                if ( norm_v[i] < -1 ) norm_v[i] = -1;
            }
        }
    }
    
    prev_v = v;
    
}