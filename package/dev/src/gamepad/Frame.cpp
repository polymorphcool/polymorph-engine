/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
*/

#include "Frame.h"

using namespace std;
using namespace input;

void GameControllerFrame::compare( GameControllerFrame& previous ) {
    
    // comparaison of all axis and all buttons status
    if ( previous.axis.size() == 0 ) {
        init();
        return;
    }
    
    int i_end = (int) GCB_MAX;
    for ( int i = 0; i < i_end; ++i ) {
        GameControllerAxis a = (GameControllerAxis) i;
        Ogre::Vector2& local_v = axis[ a ];
        Ogre::Vector2& foreign_v = previous.axis[ a ];
        if ( local_v.x == 0 && local_v.y == 0 ) {
            if ( foreign_v.x == 0 && foreign_v.y == 0 ) {
                axis_status[ a ] = IS_INACTIVE;
            } else {
                axis_status[ a ] = IS_DEACTIVATED;
            }
        } else {
            if ( foreign_v.x == 0 && foreign_v.y == 0 ) {
                axis_status[ a ] = IS_ACTIVATED;
            } else {
                if ( local_v == foreign_v ) {                
                    axis_status[ a ] = IS_ACTIVE;
                } else {
                    axis_status[ a ] = IS_MODIFIED;
                }
            }
        }
        axis_delta[ a ] = foreign_v - local_v;
    }
    
    i_end = (int) SDL_CONTROLLER_BUTTON_MAX;
    for ( int i = 0; i < i_end; ++i ) {
        SDL_GameControllerButton b = (SDL_GameControllerButton) i;
        bool& local_b = buttons[ b ];
        bool& foreign_b = previous.buttons[ b ];
        if ( local_b ) {
            if ( foreign_b ) {
                button_status[ b ] = IS_ACTIVE;
            } else {
                button_status[ b ] = IS_ACTIVATED;
            }
        } else {
            if ( foreign_b ) {
                button_status[ b ] = IS_DEACTIVATED;
            } else {
                button_status[ b ] = IS_INACTIVE;
            }
        }
    }
    
}

void GameControllerFrame::init() {
    
    // first time a GameControllerFrame appears
    int i_end = (int) GCB_MAX;
    for ( int i = 0; i < i_end; ++i ) {
        GameControllerAxis a = (GameControllerAxis) i;
        Ogre::Vector2& local_v = axis[ a ];
        if ( local_v.x == 0 && local_v.y == 0 ) {
            axis_status[ a ] = IS_INACTIVE;
        } else {
            axis_status[ a ] = IS_ACTIVATED;
        }
        axis_delta[ a ] = Ogre::Vector2::ZERO;
    }
    
    i_end = (int) SDL_CONTROLLER_BUTTON_MAX;
    for ( int i = 0; i < i_end; ++i ) {
        SDL_GameControllerButton b = (SDL_GameControllerButton) i;
        if ( buttons[ b ] ) {
            button_status[ b ] = IS_ACTIVATED;
        } else {
            button_status[ b ] = IS_INACTIVE;
        }
    }
    
}

DataFrame::DataFrame( bool enable_history ) {
    use_history = enable_history;
}

void DataFrame::store() {
    
    if ( !use_history ) return;
    previous_gcframes.clear();
    GCFListIter it = gcframes.begin();
    GCFListIter it_end = gcframes.end();
    for ( ; it != it_end; ++it ) {
        // only storing active frames
        if ( (*it).status != IS_DEACTIVATED ) {
            previous_gcframes.push_back( (*it) );
        }
    }
    
}

void DataFrame::compute() {
    
    if ( !use_history ) return;
    // compare gcframes with previous_gcframes
    GCFListIter it = gcframes.begin();
    GCFListIter it_end = gcframes.end();
    for ( ; it != it_end; ++it ) {
        GCFListIter prev_it = previous_gcframes.begin();
        GCFListIter prev_it_end = previous_gcframes.end();
        bool found = false;
        for ( ; prev_it != prev_it_end; ++prev_it ) {
            if ( (*prev_it).id == (*it).id ) {
                found = true;
                break;
            }
        }
        if ( !found ) {
            (*it).status = IS_ACTIVATED;
            (*it).init();
        } else {
            previous_gcframes.erase( prev_it );
            (*it).status = IS_ACTIVE;
            (*it).compare( (*prev_it) );
        }
    }
    
    it = previous_gcframes.begin();
    it_end = previous_gcframes.end();
    for ( ; it != it_end; ++it ) {
        (*it).status = IS_DEACTIVATED;
        gcframes.push_back( (*it) );
        // + extra copy of last status
        int l = gcframes.size() - 1;
        gcframes[ l ].axis_status = (*it).axis_status;
        gcframes[ l ].button_status = (*it).button_status;
    }
    
}