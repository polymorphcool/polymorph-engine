/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
*/

/* 
 * File:   InputManager.cpp
 * Author: frankiezafe
 * 
 * Created on August 25, 2016, 2:07 PM
 */

#include "InputManager.h"

using namespace std;
using namespace input;

static InputManager* _im = 0;

/*****************************/
/*          STATIC           */
/*****************************/

void InputManager::setVebose( bool v ) {
    get();
    _im->verbose = v;
}

bool InputManager::isGameControllerAvailable( uint id ) {
    get();
    return ( 
        find( _im->available_gamecontrollers.begin(), _im->available_gamecontrollers.end(), id ) !=
        _im->available_gamecontrollers.end()
    );
}

void InputManager::openGameController( uint id ) {
    
    get();
    _im->request_lock();
    InputRequest ir;
    ir.type = IRT_CONNECTION;
    ir.gamecontroller = id;
    _im->request_new.push_back( ir );
    _im->request_unlock();
    
}

void InputManager::closeGameController( uint id ) {
    
    get();
    _im->request_lock();
    InputRequest ir;
    ir.type = IRT_DELETION;
    ir.gamecontroller = id;
    _im->request_new.push_back( ir );
    _im->request_unlock();
    
}

void InputManager::getInputFrame( input::DataFrame* dst ) {
    
    assert(_im);
    dst->store();
    _im->read_lock();
    (*dst) << *(_im->frame_external);
    _im->read_unlock();
    dst->compute();
    
}

std::string InputManager::toString( InputStatus s ) {
    
    switch( s ) {
        case IS_INACTIVE:
            return "inactive";
            break;
        case IS_ACTIVATED:
            return "activated!";
            break;
        case IS_ACTIVE:
            return "active";
            break;
        case IS_DEACTIVATED:
            return "deactivated!";
            break;
        case IS_MODIFIED:
            return "modified!";
            break;
        case IS_UNDEFINED:
        default:
            return "undefined!";
            break;
    }
    
}

InputManager* InputManager::get() {
    
    if ( !_im ) {
        _im = new InputManager( );
    }
    return _im;
    
}

void InputManager::before() {
    get();
    _im->lock();
}

void InputManager::after() {
    assert(_im);
    _im->read_lock();
    _im->syncFrame();
    _im->read_unlock();
    _im->unlock();
}

/*****************************/
/*       INSTANCIATED        */
/*****************************/

InputManager::InputManager( ) {
    
    SDL_Init( SDL_INIT_JOYSTICK | SDL_INIT_NOPARACHUTE );
    SDL_JoystickEventState( SDL_ENABLE );
    
    SDL_Init( SDL_INIT_GAMECONTROLLER | SDL_INIT_NOPARACHUTE );
    SDL_GameControllerEventState( SDL_ENABLE );
    
    gcconfiguration.sticks_divider = std::numeric_limits<int16_t>::max();
    gcconfiguration.sticks_min_delta = 1;
    gcconfiguration.dead_zone_sticks = 5000;
    gcconfiguration.dead_sticks_divider = std::numeric_limits<int16_t>::max() - gcconfiguration.dead_zone_sticks;
    gcconfiguration.strict = false;
    gcconf_ptr = &gcconfiguration;
    
    frame_external = new DataFrame( false );
    frame_internal = new DataFrame( false );
    
    _innertime = 0;
    _innertime_since_start = 0;
    _innertime_last = 0;
    _innertime_delta = 0;
    
    assert( !bthread );
    runs = true;
    bthread = boost::shared_ptr<boost::thread>( new boost::thread( boost::bind( &InputManager::runThread, this ) ) );
    
}

InputManager::~InputManager( ) {
    
    assert( bthread );
    runs = false;
    bthread->join( );
    
    std::map<int, GameController*>::iterator it = _im->gcs.begin();
    std::map<int, GameController*>::iterator it_end = _im->gcs.end();
    for ( ; it != it_end; ++it  ) {
        delete it->second;
    }
    
}

void InputManager::runThread( ) {
    
    while ( runs ) {
        
        now();
        
        // verifying requests stack
        bool sdl_event = false;
        while (SDL_PollEvent( &device_event ) ) {
            switch ( device_event.type ) {
                case SDL_CONTROLLERDEVICEADDED:
                    cout << "SDL_CONTROLLERDEVICEADDED " << device_event.cdevice.which << endl;
                    // pushing the device in available_gamecontrollers
                    {
                        Sint32 id = device_event.cdevice.which;
                        SDLListIter it = find( available_gamecontrollers.begin(), available_gamecontrollers.end(), id );
                        if ( it == available_gamecontrollers.end() ) {
                            available_gamecontrollers.push_back( id );
                            cout << "added to available_gamecontrollers" << endl;
                        }
                    }
                    sdl_event = true;
                    break;
                case SDL_CONTROLLERDEVICEREMOVED:
                    cout << "SDL_CONTROLLERDEVICEREMOVED " << device_event.cdevice.which << endl;
                    // removing the device from available_gamecontrollers
                    {
                        // id detection is NOT reliable!
                        request_lock();
                        SDLListIter it = available_gamecontrollers.begin();
                        SDLListIter it_end = available_gamecontrollers.end();
                        for ( ; it != it_end; ++it ) {
                            InputRequest ir;
                            ir.type = IRT_VALIDATION;
                            ir.gamecontroller = (*it);
                            request_new.push_back( ir );
                        }
                        request_unlock();
                        
                    }
                    sdl_event = true;
                    break;
                case SDL_CONTROLLERDEVICEREMAPPED:
                    cout << "SDL_CONTROLLERDEVICEREMAPPED " << device_event.cdevice.which << endl;
                    break;
                    
                case SDL_CONTROLLERAXISMOTION:
                case SDL_CONTROLLERBUTTONDOWN:
                case SDL_CONTROLLERBUTTONUP:
                    // trashy device numbers, does not equals ones from SDL_CONTROLLERDEVICEADDED!!!!!!!!
                    {
                        GCEventIter it = gce.begin();
                        GCEventIter it_end = gce.end();
                        for ( ; it != it_end; ++it ) (*it).second = true;
                    }
                    break;
                
                case SDL_JOYDEVICEADDED:
                    break;
                case  SDL_JOYDEVICEREMOVED:
                    break;
                default:
                    break;
            }
        }
        
        // push the failed request of previous frame in the idle stack
        if ( !request_failed.empty() ) {
            request_idle.insert( request_idle.begin(), request_failed.begin(), request_failed.end() );
            request_failed.clear();
        }
        
        // there might be no sdl_event but a new request
        request_lock();
        if ( !request_new.empty() ) {
            request_toexec.insert( request_toexec.end(), request_new.begin(), request_new.end() );
            request_new.clear();
        }
        request_unlock();
        
        // if there was an event, an idle request might be processed
        if ( sdl_event ) {
            request_toexec.insert( request_toexec.end(), request_idle.begin(), request_idle.end() );
            request_idle.clear();
        }
        
        SDL_GameControllerUpdate();
        
        // processing request and input updates, main job
        lock();
        requests_processor();
        // working with frame_internal
        GCListIter it = gc_list.begin();
        GCListIter it_end = gc_list.end();
        int i = 0;
        for ( ; it != it_end; ++it ) {
            uint id = (*it)->getID();
            if ( gce[ id ] ) {
                gce[ id ] = (*it)->update( &( frame_internal->gcframes[ i ] ) );
            }
            ++i;
        }
        unlock();
        
        // swapping frames
        read_lock();
        (*frame_external) << (*frame_internal);
        read_unlock();
        
        boost::this_thread::sleep( boost::posix_time::milliseconds( 10 ) );
        
    }
    
}

void InputManager::requests_processor() {
    
    if ( !request_toexec.empty() ) {
        IRListIter itr = request_toexec.begin();
        IRListIter itr_end = request_toexec.end();
        for ( ; itr != itr_end; ++itr ) {
            InputRequest& ir = (*itr);
            switch( ir.type ) {
                case IRT_CONNECTION:
                    if ( !requests_connection( ir ) ) {
                        request_failed.push_back( ir );
                    }
                    break;
                case IRT_DELETION:
                    if ( !requests_deletion( ir ) ) {
                        request_failed.push_back( ir );
                    }
                    break;
                case IRT_VALIDATION:
                    if ( !requests_validation( ir ) ) {
                        InputRequest nir;
                        nir = ir;
                        nir.type = IRT_CONNECTION;
                        request_failed.push_back( nir );
                    }
                    break;
                case IRT_UNDEFINED:
                default:
                    break;
            }
        }
        request_toexec.clear();
    }
    
}

bool InputManager::requests_connection( const InputRequest& ir ) {
    
    bool success = false;
    if ( ir.gamecontroller > -1 ) {
        if ( 
            find( available_gamecontrollers.begin(), available_gamecontrollers.end(), ir.gamecontroller ) ==
            available_gamecontrollers.end() ) { return false; }
        if ( gcs.find( ir.gamecontroller ) != gcs.end() ) { 
            // already done!
            return true;
        }
        GCMapIter it = gcs.find( ir.gamecontroller );
        if ( it == gcs.end() ) {
            SDL_GameController* gc_ptr = SDL_GameControllerOpen( ir.gamecontroller );
            if ( gc_ptr != NULL ) {
                GameController* gc = new GameController( gc_ptr, ir.gamecontroller );
                gc->setConfig( gcconf_ptr );
                gcs[ ir.gamecontroller ] = gc;
                gce[ ir.gamecontroller ] = true;
                gc_list.push_back( gc );
                success = true;
            }
        }
        if ( verbose ) { 
           if ( success ) cout << "InputManager::openGameController, input " << ir.gamecontroller << " opened successfully" << endl;
           else cerr << "InputManager::openGameController, failed to start input " << ir.gamecontroller << endl;
        }
    }
    if ( success ) {
        read_lock();
        syncFrame();
        read_unlock();
    }
    return success;
    
}

bool InputManager::requests_validation( const InputRequest& ir ) {
    
    if ( ir.gamecontroller > -1 ) {
        if ( gcs.find( ir.gamecontroller ) != gcs.end() ) {
            SDL_GameController* gc_ptr = SDL_GameControllerOpen( ir.gamecontroller );
            if ( gc_ptr == NULL ) {
                SDLListIter it = find( available_gamecontrollers.begin(), available_gamecontrollers.end(), ir.gamecontroller );
                if ( it != available_gamecontrollers.end() ) {
                    available_gamecontrollers.erase( it );
                    cout << "removed from available_gamecontrollers" << endl;
                }
                requests_deletion( ir );
                return false;
            }
        }
    }
    return true;
    
}

bool InputManager::requests_deletion( const InputRequest& ir ) {

    if ( ir.gamecontroller > -1 ) {
        bool success = false;
        GameController* gc = 0;
        // management of all maps and vectors referencing this input
        GCMapIter it = gcs.find( ir.gamecontroller );
        if ( it != gcs.end() ) {
            gc = it->second;
            gcs.erase( it );
        }
        GCEventIter ite = gce.find( ir.gamecontroller );
        if ( ite != gce.end() ) {
            gce.erase( ite );
        }
        if ( gc != 0 ) {
            GCListIter itl = find( gc_list.begin(), gc_list.end(), gc );
            if ( itl != gc_list.end() ) {
                gc_list.erase( itl );
            }
            delete gc;
            success = true;
        }
        if ( verbose ) { 
           if ( success ) cout << "InputManager::closeGameController, input " << ir.gamecontroller << " closed successfully" << endl;
           else cerr << "InputManager::closeGameController, failed to close input " << ir.gamecontroller << endl;
        }
        read_lock();
        syncFrame();
        read_unlock();
    }
    return true;
    
}

void InputManager::now() {
    
    if ( _timestart.is_not_a_date_time() ) {
        _timestart = boost::posix_time::microsec_clock::local_time( );
    }
    _timecurrent = boost::posix_time::microsec_clock::local_time( );
    boost::posix_time::time_duration absolute_delta = _timecurrent - _timestart;
    _innertime_last = _innertime_since_start;
    _innertime_since_start = (double) absolute_delta.total_microseconds( );
    _innertime_delta = _innertime_since_start - _innertime_last;
    _innertime += _innertime_delta;

}

void InputManager::lock() {
    _lock = new boost::unique_lock<boost::mutex>( bmutex );
}

void InputManager::unlock() {
    delete _lock;
}

void InputManager::read_lock() {
    _read_lock = new boost::unique_lock<boost::mutex>( read_mutex );
}

void InputManager::read_unlock() {
    delete _read_lock;
}

void InputManager::request_lock() {
    _request_lock = new boost::unique_lock<boost::mutex>( request_mutex );
}

void InputManager::request_unlock() {
    delete _request_lock;
}

void InputManager::syncFrame() {
    frame_internal->gcframes.clear();
    GCListIter it = _im->gc_list.begin();
    GCListIter it_end = _im->gc_list.end();
    for ( ; it != it_end; ++it ) {
        GameControllerFrame gcf;
        frame_internal->gcframes.push_back( gcf );
    }
}