import os
import sys
hfile = []
cppfile = []
rootdir = "."
for root, subFolders, files in os.walk(rootdir):
    for file in files:
		if file[-2:] == ".h":
			f = os.path.join(root,file)
			hfile.append(f[2:])
		if file[-4:] == ".cpp":
			f = os.path.join(root,file)
			cppfile.append(f[2:])

f = open( "file.list", 'w' )
            
f.write( "\n*** H ***\n" )
for h in hfile:
	f.write( h + "\n" )
	
f.write( "\n*** CPP ***\n" )
for cpp in cppfile:
	f.write( cpp + "\n" )

f.close()