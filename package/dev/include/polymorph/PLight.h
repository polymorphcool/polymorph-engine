/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
*/

/* 
 * File:   PLight.h
 * Author: frankiezafe
 *
 * Created on November 20, 2016, 8:08 PM
 */

#ifndef POLYMORPH_LIGHT_H
#define POLYMORPH_LIGHT_H

#include "PCommon.h"
#include "PUtil.h"
#include "PLightData.h"
#include "PObject.h"

namespace polymorph {
    
    class PLight;
    typedef std::vector< PLight* > PLightList;
    typedef std::vector< PLight* >::iterator PLightListIter;
    typedef std::map< std::string, PLight* > PLightMap;
    typedef std::map< std::string, PLight* >::iterator PLightMapIter;
    
    class PLight : public PObject<Ogre::Light*> {
    
    public:
        
        PLight();
        PLight( Ogre::SceneManager* sceneMgr, const PLightData* src );
        virtual ~PLight();
        
        bool point( Ogre::SceneManager* sceneMgr );
        bool sun( Ogre::SceneManager* sceneMgr );
        bool spot( Ogre::SceneManager* sceneMgr );
        
        void visible( bool enable );
        void debug( bool enable );
        
        // coulour controls
        void diffuse( Ogre::Real red, Ogre::Real green, Ogre::Real blue );
        void diffuse( const Ogre::ColourValue& color );
        void specular( Ogre::Real red, Ogre::Real green, Ogre::Real blue );
        void specular( const Ogre::ColourValue& color );
        void range( Ogre::Real r );
        
        // call this before deletion - will clear ALL hierarchy!
        void clear();
    
    protected:
        
        void clearDebug();
        
        void createLight();
        
        void local_position(Ogre::Vector3 v) {
            _pivot->setPosition(v);
        }

        void local_scale(Ogre::Vector3 v) {
            _pivot->setScale(v);
        }

        void local_orientation(Ogre::Quaternion q) {
            _pivot->setOrientation(q);
        }

        void local_pitch(
                const Ogre::Radian& angle,
                TransformSpace space = polymorph::TS_LOCAL) {
            _pivot->pitch(angle, PUtil::convert(space));
        }

        void local_yaw(
                const Ogre::Radian& angle,
                TransformSpace space = polymorph::TS_LOCAL) {
            _pivot->yaw(angle, PUtil::convert(space));
        }

        void local_roll(
                const Ogre::Radian& angle,
                TransformSpace space = polymorph::TS_LOCAL) {
            _pivot->roll(angle, PUtil::convert(space));
        }
                
        Ogre::Quaternion getObjectOrientation(bool absolute = true) {
            if ( absolute ) {
                return _pivot->_getDerivedOrientation();
            }
            return _pivot->getOrientation();
        };

        Ogre::Vector3 getObjectTrans(bool absolute = true) {
            if ( absolute ) {
                return _pivot->_getDerivedPosition();
            }
            return _pivot->getPosition();
        };

        Ogre::Vector3 getObjectScale(bool absolute = true) {
            if ( absolute ) {
                return _pivot->_getDerivedScale();
            }
            return _pivot->getScale();
        };
        
        Ogre::Vector3 getTranslationUpdated() {
            return _origin->_getDerivedPositionUpdated();
        }
        
        Ogre::Quaternion getOrientationUpdated() {
            return _origin->_getDerivedOrientationUpdated();
        }
        
    private:
        
        Ogre::SceneNode*            _pivot;
        Ogre::SceneNode*            _debug_node;
        Ogre::Entity*               _debug_entity;
        Ogre::SceneNode*            _debug_node_dir;
        Ogre::Entity*               _debug_entity_dir;
        
        // types
        int                         _ltype;
        
        // configure
        bool                        _shadows;
        
        // bullet
        PBulletExchangePLight       _light_exchange;
        
        
        
    };
    
};

#endif /* POLYMORPH_LIGHT_H */

