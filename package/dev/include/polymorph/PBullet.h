/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
 */

/* 
 * File:   PBullet.h
 * Author: frankiezafe
 *
 * Created on September 22, 2016, 8:30 PM
 */

#ifndef POLYMORPH_BULLET_H
#define POLYMORPH_BULLET_H

#include "PCommon.h"
#include "PUtil.h"
#include "DoublePtr.h"
#include "PBulletRigidbodyUser.h"
#include "PBulletEngineData.h"
#include "PBulletSpringData.h"
#include "PBulletContactListener.h"

namespace polymorph {

    class PBullet : 
    public Ogre::RenderQueueListener, // to be removed
    public Ogre::FrameListener
    {
    
    public:

        /*Retrieves the pointer to PBullet singleton, strongly unadviced.
         * @return
         *      pointer to active PBullet object.
         */
        static PBullet* getBullet();

        /*Configure the physical engine via the serialisable object.
         * An internal copy will be made and modified by setters methods.
         * @param
         *      reference to a PBulletEngineData object.
         * @remarks
         *      If called after start(), some of the data parameters will not 
         *      be applied.
         */
        static void configure(PBulletEngineData& data);

        /* Activation of contact events management.
         * @param
         *      true to activate events generation, false to disable.
         * @remarks
         *      false by default
         */
        static void enableContactEvents(const bool& activate);

//        /* Starts bullet. If a SceneManager pointer is not provided,
//         * bullet has to be updated manually via PBullet::update().
//         * PBullet::stop() will clean pointer to SceneManager. To restart
//         * bullet, specify SceneManager each time. 
//         * @param
//         *      Pointer to the SceneManager to sync with.
//         * @remark
//         *      Manual update might involve inconsistency in bullet 
//         *      and ogre!
//         */
//        static void start(Ogre::SceneManager* sm = 0);

        /* Starts bullet. If a Root pointer is not provided,
         * bullet has to be updated manually via PBullet::update().
         * PBullet::stop() will clean pointer to Root. To restart
         * bullet, specify Root each time. 
         * @param
         *      Pointer to the Root to sync with.
         * @remark
         *      Manual update might involve inconsistency in bullet 
         *      and ogre!
         */
        static void start(Ogre::Root* r = 0);

        /* Deactivate bullet and stops sync with the scene manager,
         * clean pointer to SceneManager and delete internal clock.
         */
        static void stop();

        /* Use this method only if you want to control bullet manually,
         * or force a physical world refresh. In any other case, prefer
         * calling PBullet::start( SceneManager ).
         * @param
         *      time passed since last update (in seconds)
         * @param
         *      maximum number of solving to process
         */
        static void update(
                const btScalar& delatime,
                Ogre::uint32 maxsteps = 10
                );

        // ray casting, returns the closest body touched
        static void ray(
                const Ogre::Camera* cam,
                const Ogre::Vector2 screenpos,
                PBulletRay* res = 0);

        static void ray(
                const Ogre::Vector3 start,
                const Ogre::Vector3 end,
                PBulletRay* res = 0);

        // ray casting, returns all body touched
        static void rays(
                const Ogre::Camera* cam,
                const Ogre::Vector2 screenpos,
                PBulletRay*& res,
                uint16_t& res_count);

        static void rays(
                const Ogre::Vector3 start,
                const Ogre::Vector3 end,
                PBulletRay*& res,
                uint16_t& res_count);

        //constraint, btGeneric6DofSpringConstraint
        static btGeneric6DofSpringConstraint* spring(
                PBulletExchange& e1,
                PBulletExchange& e2,
                PBulletSpringData* conf = 0
                );

        static void removeSpring(btGeneric6DofSpringConstraint*);

        // creation/destruction
        static bool create(PBulletExchange& e, PBulletRigidbodyUser* l = 0);
        static bool remove(PBulletExchange& e);

        // setters

        static void max_substeps(Ogre::uint32 mss);

        static void gravity(const Ogre::Vector3& g);

        static void gravity(const btVector3& g);

        static void time_multiplier(btScalar tm);

        static void verbose(bool v);

        static void deactivation(PBulletExchange& e, bool enable);

        // getters
        static bool isVerbose();
        
        static Ogre::Vector3 getGravity();

        // listeners
        
        /*Register your object as a contact pairs listener.
         *@param
         *      pointer to an object inheriting from PBulletContactPairListener
         * @return
         *      true if object has been successfully registered
         *      false if the object is already registered
         */
        static bool addContactPairListener(PBulletContactPairListener*);
        
        /*Remove your object from contact event listeners.
         *@param
         *      pointer to an object inheriting from PBulletContactPairListener
         * @return
         *      true if object has been successfully removed
         *      false if the object was not registered
         */
        static bool removeContactPairListener(PBulletContactPairListener*);

        /** Implementation of RenderQueueListener::preRenderQueues(), 
         * do not call manually!
         */
        void preRenderQueues();
        
        /** Implementation of FrameListener::frameStarted(), 
         * do not call manually!
         */
        bool frameStarted(const Ogre::FrameEvent& evt);

    protected:
        
        static void tickCallback(btDynamicsWorld *world, btScalar timeStep);
        
        static void brodcastContacts();
        
        // called by static methods or callbacks
        
        void _update(const btScalar& delatime);

        bool _create(PBulletExchange& e, PBulletRigidbodyUser* l = 0);

    private:

        PBullet();
        virtual ~PBullet();

        Ogre::SceneManager* sceneMgr;
        Ogre::Root* root;
        Ogre::Timer* internal_clock;

        btDefaultCollisionConfiguration* collisionConfiguration;
        btCollisionDispatcher* dispatcher;
        btBroadphaseInterface* overlappingPairCache;
        btSequentialImpulseConstraintSolver* solver;
        btDiscreteDynamicsWorld* world;

        btAlignedObjectArray< btCollisionShape* > collisionShapes;
        std::map< Ogre::IdString, btOgreMeshData* > meshdata;

        std::map< const btCollisionObject*, PBulletExchange* > callable_bodies;

        void applyTransforms();
        
        void setSceneManager(Ogre::SceneManager* sm);
        
        void setRoot(Ogre::Root* r);

        btOgreMeshData* getData(const PBulletExchange& e);

        bool loadMeshData(const PBulletExchange& e);

        bool loadCompound(PBulletExchange& e);

        bool loadConvexHull(PBulletExchange& e);

        bool loadGImpactMesh(PBulletExchange& e);

        bool loadTriangleMesh(PBulletExchange& e);

        inline Ogre::IdString getMeshDataId(const PBulletExchange& e) {
            std::stringstream ss;
            ss << "bt_" << e.meshgroup << ":" << e.meshname;
            return Ogre::IdString(ss.str().c_str());
        }


    };

};

#endif /* POLYMORPH_BULLET_H */

