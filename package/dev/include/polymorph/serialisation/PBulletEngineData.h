/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
*/

/* 
 * File:   PBulletData.h
 * Author: frankiezafe
 *
 * Created on October 28, 2016, 7:32 PM
 */

#ifndef POLYMORPH_BULLETWORLDDATA_H
#define POLYMORPH_BULLETWORLDDATA_H

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#include <iostream>
#endif

#include "Ogre.h"
#include "PBulletTypes.h"

namespace polymorph {
    
    #define PBULLET_MAXSUBSTEPS 10
    #define PBULLET_GRAVITY 0,-10,0
    #define PBULLET_TIME_MULTIPLIER 1
    
    class PBulletEngineData {

    public:
        
        Ogre::uint32 max_substeps;
        
        btVector3 gravity;
        
        /** 1 by default, influence the time of stepSimulation,
         * greater than 1 = faster than normal
         * less than 1 = slower than normal
         * 0 = stopped
         * less than 0 = quantic state...
         *  */
        btScalar time_multiplier;
        
        bool verbose;
        
        /** set to true in scenes for scene loader to replace the
            default configuration by this one - only gravity is affected,
            setting max_substeps requires a bullet reboot
         */
        bool overwrite;
        
        /** set to true to avoid falling back on default configuration 
         * if the new scene loaded doesn't have a bullet configuration 
         */
        bool persistent;
        
        /** set to true to enable contact events management, same as calling
         * PBullet::enableContactEvents().
         */
        bool contact_events;
        
        PBulletEngineData():
        max_substeps( PBULLET_MAXSUBSTEPS ),
        gravity( PBULLET_GRAVITY ),
        time_multiplier( PBULLET_TIME_MULTIPLIER ),
        verbose( false ),
        overwrite( false ),
        persistent( false ),
        contact_events( false )
        {}
        
        inline void reset() {
            max_substeps = PBULLET_MAXSUBSTEPS;
            gravity = btVector3( PBULLET_GRAVITY );
            time_multiplier = PBULLET_TIME_MULTIPLIER;
            verbose = false;
            overwrite = false;
            persistent = false;
            contact_events = false;
        }
        
        inline void print() {
            
            std::cout << "PBulletData: " << std::endl <<
                "\t" << "max_substeps: " << max_substeps << std::endl <<
                "\t" << "gravity: " 
                    << gravity.x() << ", " 
                    << gravity.y() << ", " 
                    << gravity.z() << std::endl <<
                "\t" << "time_multiplier: " << time_multiplier << std::endl <<
                "\t" << "verbose: " << verbose << std::endl <<
                "\t" << "overwrite: " << overwrite << std::endl <<
                "\t" << "persistent: " << persistent << 
                "\t" << "contact_events: " << contact_events << 
                    std::endl;
            
        }
        
        inline void operator = ( const PBulletEngineData& src ) {
            max_substeps = src.max_substeps;
            gravity = src.gravity;
            time_multiplier = src.time_multiplier;
            verbose = src.verbose;
            overwrite = src.overwrite;
            persistent = src.persistent;
            contact_events = src.contact_events;
        }
        
    };
    
}

#endif /* POLYMORPH_BULLETWORLDDATA_H */

