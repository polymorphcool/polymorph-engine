/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
*/

/* 
 * File:   PLightData.h
 * Author: frankiezafe
 *
 * Created on November 20, 2016, 8:17 PM
 */

#ifndef POLYMORPH_LIGHTDATA_H
#define POLYMORPH_LIGHTDATA_H

#include "PObjectData.h"
#include "PObjectBulletData.h"

namespace polymorph {
    
    class PLightData : public PObjectData, public PObjectBulletData {

    public:
        
        // colors
        Ogre::ColourValue           diffuse;
        Ogre::ColourValue           specular;
        // configuration
        bool                        shadows;
        int                         type;
        Ogre::Real                  range;
        
        PLightData():
        PObjectData(),
        PObjectBulletData(),
        diffuse( Ogre::ColourValue::White ),
        specular( Ogre::ColourValue::White ),
        shadows( false ),
        type( -1 ),
        range( 10000 )
        {}
        
        inline void operator = ( const PLightData& src ) {
            (PObjectData&) (*this) = (const PObjectData&) src;
            (PObjectBulletData&) (*this) = (const PObjectBulletData&) src;
            diffuse = src.diffuse;
            specular = src.specular;
            shadows = src.shadows;
            type = src.type;
            range = src.range;
        }
        
        inline void print() {
            
            std::cout << 
                "polymorph::PLightData" << std::endl;
            
            PObjectData::print();
            PObjectBulletData::print();
                 
            // custom   
            std::cout << 
                "\t" << "** custom ** " << std::endl <<
                "\t" << "diffuse: " << 
                    diffuse.r << ", " <<
                    diffuse.g << ", " <<
                    diffuse.b << ", " << std::endl <<
                "\t" << "specular: " << 
                    specular.r << ", " <<
                    specular.g << ", " <<
                    specular.b << ", " << std::endl <<
                "\t" << "shadows: " << shadows << std::endl <<
                "\t" << "type: " << 
                    PUtil::lightTypeToString( type ) << std::endl;
        }
        
    };
    
    typedef std::vector< PLightData* > PLDataList;
    typedef std::vector< PLightData* >::iterator PLDataListIter;
    
    typedef std::map< std::string, PLightData* > PLDataMap;
    typedef std::map< std::string, PLightData* >::iterator PLDataMapIter;
    
};

#endif /* POLYMORPH_LIGHTDATA_H */

