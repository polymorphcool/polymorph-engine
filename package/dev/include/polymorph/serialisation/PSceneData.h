/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ___________________________________( ^3^)_____________________________________
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 */

/* 
 * File:   PSceneData.h
 * Author: frankiezafe
 *
 * Created on October 21, 2016, 3:42 PM
 */

#ifndef POLYMORPH_SCENEDATA_H
#define POLYMORPH_SCENEDATA_H

#include "PCamRigData.h"
#include "PNodeData.h"
#include "PLightData.h"
#include "PBulletEngineData.h"
//#include "PSoundData.h"

namespace polymorph {

    class PSceneData {
    public:

        /**
         * read only UID
         */
        const uint32_t& UID;
        
        /**
         * read only name, empty by default
         */
        const std::string& name;
        
        /**
         * read only bullet configuration
         */
        const PBulletEngineData& bullet;

        /**
         * class constructor
         * @param id: unique identifier, readable via UID
         */
        PSceneData(uint32_t id);
        
        /**
         * class constructor
         * @param id: unique identifier, readable via UID
         * @param name: name of the scene
         */
        PSceneData(uint32_t id,  std::string& name );

        ~PSceneData();

        /**
         * Hard reset of the object
         */
        void clear();

        /**
         * Add a new PNodeData to the scene via a copy of the given source
         * @param src
         *      data to copy
         * @return 
         *      true if the name is unique in this scene, 
         *      see xml field pnode:name
         */
        bool push(const PNodeData& src);

        /**
         * Add a new PLightData to the scene via a copy of the given source
         * @param src
         *      data to copy
         * @return 
         *      true if the name is unique in this scene, 
         *      see xml field plight:name
         */
        bool push(const PLightData& src);
        
        /**
         * Set the bulet configuration for this scene
         * @param src
         *      PBulletData to copy
         * @return 
         *      true
         */
        bool set(const PBulletEngineData& src);
        
        /**
         * Add a new PsoundData to the scene via a copy of the given source
         * @param src
         *      data to copy
         * @return 
         *      true if the name is unique in this scene, 
         *      see xml field psound:name
         */
//        bool push(const PSoundData& src);

        // getters

        /**
         * Returns a sorted array of object's names
         * @param names
         *      a null pointer of type std::string, 
         *      it is turned into an array by the method, 
         *      stays as it is if no scene
         * @param num
         *      the number of objects in the project
         * @note
         *      do not forget to delete names once used!
         */
        void getObjectNames(std::string*& names, size_t& num) {
            num = objectCount();
            if (num > 0) {
                names = new std::string[ num ];
                PObjDataMapIter it = objmap.begin();
                PObjDataMapIter ite = objmap.end();
                for (int i = 0; it != ite; ++it, ++i) {
                    names[ i ] = it->first;
                }

            }
        }

        /**
         * Returns a sorted array of node's names
         * @param names
         *      a null pointer of type std::string, 
         *      it is turned into an array by the method, 
         *      stays as it is if no scene
         * @param num
         *      the number of nodes in the project
         * @note
         *      do not forget to delete names once used!
         */
        void getNodeNames(std::string*& names, size_t& num) {
            num = nodeCount();
            if (num > 0) {
                names = new std::string[ num ];
                PNDataMapIter it = nmap.begin();
                PNDataMapIter ite = nmap.end();
                for (int i = 0; it != ite; ++it, ++i) {
                    names[ i ] = it->first;
                }
            }
        }

        /**
         * Returns a sorted array of light's names
         * @param names
         *      a null pointer of type std::string, 
         *      it is turned into an array by the method, 
         *      stays as it is if no scene
         * @param num
         *      the number of lights in the project
         * @note
         *      do not forget to delete names once used!
         */
        void getLightNames(std::string*& names, size_t& num) {
            num = lightCount();
            if (num > 0) {
                names = new std::string[ num ];
                PLDataMapIter it = lmap.begin();
                PLDataMapIter ite = lmap.end();
                for (int i = 0; it != ite; ++it, ++i) {
                    names[ i ] = it->first;
                }

            }
        }

        /**
         * Returns a sorted array of sound's names
         * @param names
         *      a null pointer of type std::string, 
         *      it is turned into an array by the method, 
         *      stays as it is if no scene
         * @param num
         *      the number of lights in the project
         * @note
         *      do not forget to delete names once used!
         */
//        void getSoundNames(std::string*& names, size_t& num) {
//            num = soundCount();
//            if (num > 0) {
//                names = new std::string[ num ];
//                PSoDataMapIter it = smap.begin();
//                PSoDataMapIter ite = smap.end();
//                for (int i = 0; it != ite; ++it, ++i) {
//                    names[ i ] = it->first;
//                }
//
//            }
//        }

        /**
         * get object data loaded in scene by its name
         * @param name
         *      name of the object, based on field plight:name or pnode:name
         *      in the xml
         * @return 
         *      pointer to PObjectData instance, 0 if name does not exist
         */
        PObjectData* object(const std::string& name) {
            if (!containsObject(name)) return 0;
            return objmap[ name ];
        }

        /**
         * get node data loaded in scene by its name
         * @param name
         *      name of the node, based on field pnode:name in the xml
         * @return 
         *      pointer to PNodeData instance, 0 if name does not exist
         */
        PNodeData* node(const std::string& name) {
            if (!containsNode(name)) return 0;
            return nmap[ name ];
        }

        /**
         * get light data loaded in scene by its name
         * @param name
         *      name of the light, based on field plight:name in the xml
         * @return 
         *      pointer to PLightData instance, 0 if name does not exist
         */
        PLightData* light(const std::string& name) {
            if (!containsLight(name)) return 0;
            return lmap[ name ];
        }

        /**
         * get sound data loaded in scene by its name
         * @param name
         *      name of the sound, based on field psound:name in the xml
         * @return 
         *      pointer to PSoundData instance, 0 if name does not exist
         */
//        PSoundData* sound(const std::string& name) {
//            if (!containsSound(name)) return 0;
//            return smap[ name ];
//        }

        /**
         * get object loaded in scene by its alphabetical position,
         * based by plight:name or pnode:name
         * @param i
         *      position of the object in the map
         * @return 
         *      pointer to PObjectData instance, 0 if name does not exist
         * @note
         *      use objectCount() to get the number of objects in the scene
         */
        PObjectData* object(const uint32_t& i) {
            if (i >= objmap.size()) return 0;
            PObjDataMapIter it = objmap.begin();
            for (uint32_t j = 0; j < i; ++j, ++it) {
            }
            return it->second;
        }

        /**
         * get node loaded in scene by loading order
         * @param i
         *      position of the node in list
         * @return 
         *      pointer to PNodeData instance, 0 if too far
         * @note
         *      use nodeCount() to get the number of nodes in the scene
         */
        PNodeData* node(const uint32_t& i) {
            if (i >= nodes.size()) return 0;
            return nodes[ i ];
        }

        /**
         * get light loaded in scene by loading order
         * @param i
         *      position of the light in list
         * @return 
         *      pointer to PLightData instance, 0 if too far
         * @note
         *      use lightCount() to get the number of lights in the scene
         */
        PLightData* light(const uint32_t& i) {
            if (i >= lights.size()) return 0;
            return lights[ i ];
        }
        
        /**
         * get sound loaded in scene by loading order
         * @param i
         *      position of the sound in list
         * @return 
         *      pointer to PSoundData instance, 0 if too far
         * @note
         *      use soundCount() to get the number of sounds in the scene
         */
//        PSoundData* sound(const uint32_t& i) {
//            if (i >= sounds.size()) return 0;
//            return sounds[ i ];
//        }

        // inline

        /**
         * get number of objects loaded in the scene
         * @return 
         *      unsigned int
         */
        inline size_t objectCount() {
            return objmap.size();
        }

        /**
         * get number of nodes loaded in the scene
         * @return 
         *      unsigned int
         */
        inline size_t nodeCount() {
            return nodes.size();
        }

        /**
         * get number of lights loaded in the scene
         * @return 
         *      unsigned int
         */
        inline size_t lightCount() {
            return lights.size();
        }

        /**
         * get number of sounds loaded in the scene
         * @return 
         *      unsigned int
         */
//        inline size_t soundCount() {
//            return sounds.size();
//        }

        /**
         * check if an object with given name exists in the scene
         * @param name
         *      name of the objects, based on field plight:name or pnode:name
         *      in the xml
         * @return
         *      true if present
         */
        inline bool containsObject(const std::string& name) {
            return objmap.find(name) != objmap.end();
        }
        
        /**
         * check if a node with given name exists in the scene
         * @param name
         *      name of the node, based on field pnode:name
         *      in the xml
         * @return
         *      true if present
         */
        inline bool containsNode(const std::string& name) {
            return nmap.find(name) != nmap.end();
        }
        
        /**
         * check if a light with given name exists in the scene
         * @param name
         *      name of the light, based on field plight:name
         *      in the xml
         * @return
         *      true if present
         */
        inline bool containsLight(const std::string& name) {
            return lmap.find(name) != lmap.end();
        }
        
        /**
         * check if a sound with given name exists in the scene
         * @param name
         *      name of the sound, based on field psound:name
         *      in the xml
         * @return
         *      true if present
         */
//        inline bool containsSound(const std::string& name) {
//            return smap.find(name) != smap.end();
//        }

        /**
         * get the last added node
         * @return 
         *      a pointer to node data
         * @note
         *      used during XML parsing
         */
        inline PNodeData* lastNode() {
            return nodes.back();
        }

        /**
         * get the last added light
         * @return 
         *      a pointer to light data
         * @note
         *      used during XML parsing
         */
        inline PLightData* lastLight() {
            return lights.back();
        }

        /**
         * get the last added sound
         * @return 
         *      a pointer to sound data
         * @note
         *      used during XML parsing
         */
//        inline PSoundData* lastSound() {
//            return sounds.back();
//        }

        /**
         * copy source scene data to local
         * @param src
         *      other scene data
         * @note
         *      previous data is erased before copy, no safety net
         */
        void operator=(const PSceneData& src) {
            clear();
            _UID = src._UID;
            _bullet = src.bullet;
            objmap = src.objmap;
            nmap = src.nmap;
            lmap = src.lmap;
//            smap = src.smap;
            nodes = src.nodes;
            lights = src.lights;
//            sounds = src.sounds;
        }

    protected:

        uint32_t _UID;
        std::string _name;
        
        // maps
        PObjDataMap objmap;
        PNDataMap nmap;
        PLDataMap lmap;
        
        // lists
        PNDataList nodes;
        PLDataList lights;
        
        PBulletEngineData _bullet;
        
    };

    typedef std::vector< PSceneData* > PSceDataList;
    typedef std::vector< PSceneData* >::iterator PSceDataListIter;

    typedef std::map< uint32_t, PSceneData* > PSceDataMap;
    typedef std::map< uint32_t, PSceneData* >::iterator PSceDataMapIter;

};

#endif /* POLYMORPH_SCENEDATA_H */

