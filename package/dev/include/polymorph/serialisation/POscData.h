/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
*/

/* 
 * File:   POscData.h
 * Author: frankiezafe
 *
 * Created on October 28, 2016, 8:29 PM
 */

#ifndef POLYMORPH_OSCDATA_H
#define POLYMORPH_OSCDATA_H

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#include <iostream>
#endif

#include "Ogre.h"

namespace polymorph {

    class POscSenderData {
    public:
        
        bool autostart;
        bool max_32bits;
        std::string address;
        uint32_t port;
        
        POscSenderData():
        autostart(false),
        max_32bits(false),
        address(""),
        port(0)
        {}
        
        inline void operator = ( const POscSenderData& src ) {
            autostart = src.autostart;
            max_32bits = src.max_32bits;
            address = src.address;
            port = src.port;
        }
        
        inline void print() {
            std::cout << "POscSenderData:" << std::endl <<
                    "\t" << "address: " << address << ":" << port << std::endl <<
                    "\t" << "max_32bits: " << max_32bits << std::endl <<
                    "\t" << "autostart: " << autostart << std::endl;
        }
        
    };
    
    typedef std::vector< POscSenderData > POSDataList;
    typedef std::vector< POscSenderData >::iterator POSDataListIter;
    
    class POscReceiverData {
    public:
    
        bool autostart;
        uint32_t port;
        uint32_t max_queue_size;
        
        POscReceiverData():
        autostart(false),
        port(0),
        max_queue_size(0)
        {}
        
        inline void operator = ( const POscReceiverData& src ) {
            autostart = src.autostart;
            port = src.port;
            max_queue_size = src.max_queue_size;
        }
        
        inline void print() {
            std::cout << "POscReceiverData:" << std::endl <<
                    "\t" << "port: " << port << std::endl <<
                    "\t" << "autostart: " << autostart << std::endl <<
                    "\t" << "max_queue_size: " << max_queue_size << std::endl;
        }
        
    };
    
    typedef std::vector< POscReceiverData > PORDataList;
    typedef std::vector< POscReceiverData >::iterator PORDataListIter;
    
};

#endif /* POLYMORPH_OSCDATA_H */

