/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
*/

/* 
 * File:   PResourceInfo.h
 * Author: frankiezafe
 *
 * Created on January 3, 2017, 12:36 PM
 */

#ifndef POLYMORPH_RESOURCEINFO_H
#define POLYMORPH_RESOURCEINFO_H

namespace polymorph {

    class PResourceData {
    public:
        
        std::string path;
        std::string type; // tested: Zip, FileSystem
        std::string group;
        bool recursive;
        bool readonly;
        
        PResourceData():
        recursive(true),
        readonly(false)  
        {}
        
        inline void print() {
            
            std::cout << 
                "polymorph::PResourceData" << std::endl;
            
            // custom
            std::cout <<
                "\t" << "group: " << group << std::endl <<
                "\t" << "path: " << path << std::endl <<
                "\t" << "type: " << type << std::endl <<
                "\t" << "recursive: " << recursive << std::endl <<
                "\t" << "readonly: " << readonly << std::endl;
        }
        
    };
    
    typedef std::map< 
            std::string, 
            std::vector< PResourceData* > > PRDMap;
    
    typedef std::map< 
            std::string, 
            std::vector< PResourceData* > >::iterator PRDMapIter;
    
};

#endif /* POLYMORPH_RESOURCEINFO_H */

