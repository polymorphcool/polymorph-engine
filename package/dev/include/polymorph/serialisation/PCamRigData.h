/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
*/

/* 
 * File:   PCamRigData.h
 * Author: frankiezafe
 *
 * Created on October 11, 2016, 7:39 PM
 */

#ifndef PCAMRIGDATA_H
#define PCAMRIGDATA_H

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#include <iostream>
#endif

#include "Ogre.h"
#include "PRange.h"

namespace polymorph {

    class PCamRigData {
    public:
        
        Ogre::String name;
        Ogre::Vector3 origin_pos;
        Ogre::Quaternion origin_orientation;
        Ogre::Quaternion cam_pivot_orientation;
        PRange<Ogre::Real> nearfar;
        PRange<Ogre::Real> dist;
        PRange<Ogre::Real> fov;
        PRange<Ogre::Real> y_range;
        PRange<Ogre::Real> origin_pitch_range; // X rotation
        PRange<Ogre::Real> origin_yaw_range; // Y rotation
        PRange<Ogre::Real> origin_roll_range;  // Z rotation
        PRange<Ogre::Real> pivot_pitch_range; // X rotation
        PRange<Ogre::Real> pivot_yaw_range; // Y rotation
        PRange<Ogre::Real> pivot_roll_range;  // Z rotation
        
        PCamRigData() {
            identity();
        }
        
        void identity() {
            name.clear();
            origin_pos = Ogre::Vector3::ZERO;
            origin_orientation = Ogre::Quaternion::IDENTITY;
            cam_pivot_orientation = Ogre::Quaternion::IDENTITY;
            nearfar.set( 0.01, 0.01, 1000 );
            dist.set( 0 );
            fov.set( 0.7 );
            y_range.set( 0 );
            origin_pitch_range.set( 0 );
            origin_yaw_range.set( 0 );
            origin_roll_range.set( 0 );
            pivot_pitch_range.set( 0 );
            pivot_yaw_range.set( 0 );
            pivot_roll_range.set( 0 );
        }
        
        inline void operator = ( const PCamRigData& src ) {
            name = src.name;
            origin_pos = src.origin_pos;
            origin_orientation = src.origin_orientation;
            cam_pivot_orientation = src.cam_pivot_orientation;
            nearfar = src.nearfar;
            dist = src.dist;
            fov = src.fov;
            y_range = src.y_range;
            origin_pitch_range = src.origin_pitch_range;
            origin_yaw_range = src.origin_yaw_range;
            origin_roll_range = src.origin_roll_range;
            pivot_pitch_range = src.pivot_pitch_range;
            pivot_yaw_range = src.pivot_yaw_range;
            pivot_roll_range = src.pivot_roll_range;
        }
        
        void print() {
            std::cout << "\t" << "name: " << name << std::endl;
            std::cout << "\t" << "origin_pos: " << 
                    origin_pos.x << ", " << 
                    origin_pos.y << ", " << 
                    origin_pos.z << std::endl;
            std::cout << "\t" << "origin_orientation: " << 
                    origin_orientation.w << ", "  << 
                    origin_orientation.x << ", " << 
                    origin_orientation.y << ", " << 
                    origin_orientation.z << std::endl;
            std::cout << "\t" << "cam_pivot_orientation: " << 
                    cam_pivot_orientation.w << ", "  << 
                    cam_pivot_orientation.x << ", " << 
                    cam_pivot_orientation.y << ", " << 
                    cam_pivot_orientation.z << std::endl;
            std::cout << "\t" << "nearfar: " << nearfar << std::endl;
            std::cout << "\t" << "dist: " << dist << std::endl;
            std::cout << "\t" << "fov: " << fov << std::endl;
            std::cout << "\t" << "y_range: " << y_range << std::endl;
            std::cout << "\t" << "origin_pitch_range: " << origin_pitch_range << std::endl;
            std::cout << "\t" << "origin_yaw_range: " << origin_yaw_range << std::endl;
            std::cout << "\t" << "origin_roll_range: " << origin_roll_range << std::endl;
            std::cout << "\t" << "pivot_pitch_range: " << pivot_pitch_range << std::endl;
            std::cout << "\t" << "pivot_yaw_range: " << pivot_yaw_range << std::endl;
            std::cout << "\t" << "pivot_roll_range: " << pivot_roll_range << std::endl;
        }
        
    };
    
};

#endif /* PCAMRIGDATA_H */