/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2017 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
 */

/* 
 * File:   PDData.h
 * Author: frankiezafe
 *
 * Created on April 6, 2017, 10:38 PM
 */

#ifndef PDDATA_H
#define PDDATA_H

#include "POscData.h"

namespace polymorph {

    // default puredata configuration
#define PUREDATA_CHANNELS 2
#define PUREDATA_SAMPLE_RATE 44100
#define PUREDATA_SAMPLES 128
#if OGRE_PLATFORM == OGRE_PLATFORM_LINUX
#define PUREDATA_AUDIO_DRIVER "pulseaudio"
#else
#define PUREDATA_AUDIO_DRIVER "undefined"
#endif

    class PDData {
    
    public:
        
        bool edit_mode;
        uint8_t channels;
        uint16_t sample_rate;
        uint8_t samples;
        std::string audio_driver;
        std::vector<std::string> search_paths;
        POSDataList edition_senders;
        PORDataList edition_receivers;

        PDData( ) :
        edit_mode( false ),
        channels( PUREDATA_CHANNELS ),
        sample_rate( PUREDATA_SAMPLE_RATE ),
        samples( PUREDATA_SAMPLES ),
        audio_driver( PUREDATA_AUDIO_DRIVER ) {
        }

        inline void print( ) {

            std::cout << "PDData: " << std::endl <<
                    "\t" << "edit mode: " << edit_mode << std::endl <<
                    "\t" << "output channels: " << int( channels ) << 
                    std::endl <<
                    "\t" << "sample rate: " << sample_rate << std::endl <<
                    "\t" << "samples: " << int( samples ) << std::endl <<
                    "\t" << "audio driver: " << audio_driver << std::endl <<
                    "\t" << "search paths (" << search_paths.size( ) << ")" <<
                    std::endl;
            std::vector<std::string>::iterator it = search_paths.begin( );
            std::vector<std::string>::iterator ite = search_paths.end( );
            for (; it != ite; ++it ) {
                std::cout << "\t\t" << ( *it ) << std::endl;
            }
            std::cout <<
                    "PDData osc senders (" << edition_senders.size( ) << ")" 
                    << std::endl;
            POSDataListIter its = edition_senders.begin();
            POSDataListIter itse = edition_senders.end();
            for(; its != itse; ++its) {
                (*its).print();
            }
            std::cout <<
                    "PDData osc receivers (" << edition_receivers.size( ) << ")" 
                    << std::endl;
            PORDataListIter itr = edition_receivers.begin();
            PORDataListIter itre = edition_receivers.end();
            for(; itr != itre; ++itr) {
                (*itr).print();
            }

        }

        inline void operator=( const PDData& src ) {

            edit_mode = src.edit_mode;
            channels = src.channels;
            sample_rate = src.sample_rate;
            samples = src.samples;
            audio_driver = src.audio_driver;
            search_paths = src.search_paths;
            edition_senders = src.edition_senders;
            edition_receivers = src.edition_receivers;

        }

    };

};

#endif /* PDDATA_H */

