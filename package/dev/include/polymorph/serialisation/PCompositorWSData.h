/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2017 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
 */

/* 
 * File:   PCompositorData.h
 * Author: frankiezafe
 *
 * Created on March 24, 2017, 5:09 PM
 */

#ifndef POLYMORPH_WORKSPACEDATA_H
#define POLYMORPH_WORKSPACEDATA_H

#include "Ogre.h"
#include "PUtil.h"
#include "PCompositorWSValue.h"

namespace polymorph {
    
    typedef std::map< std::string, PCompositorWSValue<Ogre::ColourValue> > PWSColorMap;
    typedef std::map< std::string, PCompositorWSValue<Ogre::ColourValue> >::iterator PWSColorMapIter;
    typedef std::map< std::string, PCompositorWSValue<float> > PWSVarsMap;
    typedef std::map< std::string, PCompositorWSValue<float> >::iterator PWSVarsMapIter;

    class PCompositorWSData {
    public:

        bool basic;
        std::string name;
        std::string group;
        std::string workspace;
        
        PWSColorMap colors;
        PWSVarsMap variables;

        PCompositorWSData() :
        basic(true),
        name("PolymorphDefaultCompositorName") {
        }

        inline bool containsColor( std::string name ) {
            return (colors.find(name) != colors.end());
        }
        
        inline bool containsVariable( std::string name ) {
            return (variables.find(name) != variables.end());
        }
        
        inline Ogre::ColourValue color(std::string n) {
            if (colors.find(n) == colors.end()) {
                return Ogre::ColourValue();
            }
            return colors[ n ].value;
        }

        inline Ogre::ColourValue color(size_t i) {
            if (i >= colors.size()) {
                return Ogre::ColourValue();
            }
            PWSColorMapIter it = colors.begin();
            PWSColorMapIter ite = colors.end();
            size_t counter = 0;
            for (; it != ite; ++it) {
                if (counter == i) {
                    return it->second.value;
                }
                counter++;
            }
        }

        void print() {
            std::cout << "PCompositorWSData " << std::endl << 
                    "\tname " << name << std::endl << 
                    "\tbasic: " << basic << std::endl << 
                    "\tcolors: " << colors.size() << std::endl << 
                    "\tvariables: " << variables.size() << std::endl;
            if ( !basic ) {
                std::cout << "\tscript: " << group << ":" << workspace << std::endl;
            }
            PWSColorMapIter itc = colors.begin();
            PWSColorMapIter itce = colors.end();
            for (; itc != itce; ++itc) {
                std::cout << "\tcolor: " << itc->second.name << std::endl <<
                        "\t\tnode: " << itc->second.node << std::endl <<
                        "\t\tpass: " << itc->second.pass << std::endl <<
                        "\t\tindex: " << itc->second.index << std::endl <<
                        "\t\tcolor: " << PUtil::str( itc->second.value ) <<
                       std::endl;
            }
            PWSVarsMapIter itv = variables.begin();
            PWSVarsMapIter itve = variables.end();
            for (; itv != itve; ++itv) {
                std::cout << "\tvariable: " << itv->second.name << std::endl <<
                        "\t\tnode: " << itv->second.node << std::endl <<
                        "\t\tpass: " << itv->second.pass << std::endl <<
                        "\t\tindex: " << itv->second.index << std::endl <<
                        "\t\tvalue: " << itv->second.value <<
                       std::endl;
            }
        }

    };

    typedef std::vector< PCompositorWSData* > PWSList;
    typedef std::vector< PCompositorWSData* >::iterator PWSListIter;
    typedef std::map< std::string, PCompositorWSData* > PWSMap;
    typedef std::map< std::string, PCompositorWSData* >::iterator PWSMapIter;

};

#endif /* POLYMORPH_WORKSPACEDATA_H */

