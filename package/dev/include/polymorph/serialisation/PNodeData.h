/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
*/

/* 
 * File:   PNodeData.h
 * Author: frankiezafe
 *
 * Created on October 11, 2016, 9:37 PM
 */

#ifndef POLYMORPH_NODEDATA_H
#define POLYMORPH_NODEDATA_H

#include "PObjectData.h"
#include "PObjectBulletData.h"

namespace polymorph {
    
    class PNodeData : public PObjectData, public PObjectBulletData {
    public:
        
        bool                        internal;
        bool                        cube;
        bool                        sphere;
        bool                        plane;
        bool                        empty;
        Ogre::String                mesh_group;
        Ogre::String                mesh_file;
        Ogre::String                skeleton_group;
        Ogre::String                skeleton_file;
        Ogre::Vector3               node_pos;
        Ogre::Vector3               node_scale;
        Ogre::Euler                 node_orientation;
        
        // materials
        Ogre::String                mat_group;
        Ogre::String                mat_name;
        
        PNodeData():
        PObjectData(),
        PObjectBulletData(),
        internal( false ),
        cube( false ),
        sphere( false ),
        plane( false ),
        empty( false ),
        node_pos( Ogre::Vector3::ZERO ),
        node_scale( Ogre::Vector3::UNIT_SCALE )
        {}
        
        inline void operator = ( const PNodeData& src ) {
            (PObjectData&) (*this) = (const PObjectData&) src;
            (PObjectBulletData&) (*this) = (const PObjectBulletData&) src;
            internal = src.internal;
            cube = src.cube;
            sphere = src.sphere;
            plane = src.plane;
            empty = src.empty;
            mesh_group = src.mesh_group;
            mesh_file = src.mesh_file;
            skeleton_group = src.skeleton_group;
            skeleton_file = src.skeleton_file;
            node_pos = src.node_pos;
            node_scale = src.node_scale;
            node_orientation = src.node_orientation;
            mat_group = src.mat_group;
            mat_name = src.mat_name;
//            bullet_type = src.bullet_type;
//            bullet_group = src.bullet_group;
//            bullet_file = src.bullet_file;
//            bullet_restless = src.bullet_restless;
//            bullet_density = src.bullet_density;
//            bullet_friction = src.bullet_friction;
//            bullet_restitution = src.bullet_restitution;
//            repeat = src.repeat;
//            repeat_count = src.repeat_count;
//            repeat_pos = src.repeat_pos;
//            repeat_scale = src.repeat_scale;
//            repeat_orientation = src.repeat_orientation;
        }
        
        inline void print() {
            
            std::cout << 
                "polymorph::PNodeData" << std::endl;
            
            PObjectData::print();
            PObjectBulletData::print();
            
            // custom
            std::cout <<
                "\t" << "** custom ** " << std::endl <<
                "\t" << "internal: " << internal << std::endl <<
                "\t" << "cube: " << cube << std::endl <<
                "\t" << "sphere: " << sphere << std::endl <<
                "\t" << "plane: " << plane << std::endl <<
                "\t" << "empty: " << empty << std::endl <<
                "\t" << "mesh_group: " << mesh_group << std::endl <<
                "\t" << "mesh_file: " << mesh_file << std::endl <<
                "\t" << "skeleton_group: " << skeleton_group << std::endl <<
                "\t" << "skeleton_file: " << skeleton_file << std::endl <<
                "\t" << "node_pos: " << 
                    node_pos.x << ", " <<
                    node_pos.y << ", " <<
                    node_pos.z << std::endl <<
                "\t" << "node_scale: " << 
                    node_scale.x << ", " <<
                    node_scale.y << ", " <<
                    node_scale.z << std::endl <<
                "\t" << "node_orientation: " << 
                    node_orientation.pitch() << ", " <<
                    node_orientation.yaw() << ", " <<
                    node_orientation.roll() << std::endl <<
                "\t" << "mat_group: " << 
                    mat_group << std::endl <<
                "\t" << "mat_name: " << 
                    mat_name << std::endl;
        }
        
    };
    
    typedef std::vector< PNodeData* > PNDataList;
    typedef std::vector< PNodeData* >::iterator PNDataListIter;
    
    typedef std::map< std::string, PNodeData* > PNDataMap;
    typedef std::map< std::string, PNodeData* >::iterator PNDataMapIter;
    
};

#endif /* POLYMORPH_NODEDATA_H */

