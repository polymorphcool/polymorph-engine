/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
*/

/* 
 * File:   PObjectBulletData.h
 * Author: frankiezafe
 *
 * Created on November 24, 2016, 5:34 PM
 */

#ifndef POLYMORPH_OBJECTBULLETDATA_H
#define POLYMORPH_OBJECTBULLETDATA_H

#include "Ogre.h"
#include "PBulletTypes.h"
#include "PUtil.h"

namespace polymorph {
    
    class PObjectBulletData {
    public:
        
        // physics
        BT_Type                     bullet_type;
        Ogre::String                bullet_group;
        Ogre::String                bullet_file;
        bool                        bullet_restless;
        Ogre::Real                  bullet_density;
        Ogre::Real                  bullet_friction;
        Ogre::Real                  bullet_restitution;
        Ogre::Real                  bullet_damping_linear;
        Ogre::Real                  bullet_damping_angular;
        
        PObjectBulletData():
        bullet_type( BT_NONE ),
        bullet_restless( false ),
        bullet_density( 1 ),
        bullet_friction( 0.5 ),
        bullet_restitution( 0 ),
        bullet_damping_linear(0),
        bullet_damping_angular(0)
        {}
        
        inline void operator = ( const PObjectBulletData& src ) {
            bullet_type = src.bullet_type;
            bullet_group = src.bullet_group;
            bullet_file = src.bullet_file;
            bullet_restless = src.bullet_restless;
            bullet_density = src.bullet_density;
            bullet_friction = src.bullet_friction;
            bullet_restitution = src.bullet_restitution;
            bullet_damping_linear = src.bullet_damping_linear;
            bullet_damping_angular = src.bullet_damping_angular;
        }
        
        inline void print() {
            std::cout <<
                "\t" << "bullet_type: " << 
                    PUtil::str( bullet_type ) << 
                    std::endl <<
                "\t" << "bullet_group: " << 
                    bullet_group << std::endl <<
                "\t" << "bullet_file: " << 
                    bullet_file << std::endl <<
                "\t" << "bullet_restless: " << 
                    bullet_restless << std::endl <<
                "\t" << "bullet_density: " << 
                    bullet_density << std::endl <<
                "\t" << "bullet_friction: " << 
                    bullet_friction << std::endl <<
                "\t" << "bullet_restitution: " << 
                    bullet_friction << std::endl <<
                "\t" << "bullet_damping_linear: " << 
                    bullet_damping_linear << std::endl <<
                "\t" << "bullet_damping_angular: " << 
                    bullet_damping_angular << std::endl;
        }
        
    };
    
};

#endif /* POLYMORPH_OBJECTBULLETDATA_H */

