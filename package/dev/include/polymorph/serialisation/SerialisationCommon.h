/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
 */

/* 
 * File:   SerialisationCommon.h
 * Author: frankiezafe
 *
 * Created on October 11, 2016, 10:46 PM
 */

#ifndef POLYMORPH_SERIALISATIONCOMMON_H
#define POLYMORPH_SERIALISATIONCOMMON_H

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#include <iostream>
#endif

#include "Ogre.h"
#include "Euler.h"
#include "PRange.h"
#include "tinyxml.h"

namespace polymorph {

    class SerialisationUtil {
    public:

        static void splitstring(
                const char* attr,
                std::vector<std::string>& elems
                );

        static bool exists(TiXmlElement* e, const char* attr) {
            if (e == 0 || e == NULL || attr == 0 || attr == NULL) {
                return false;
            }
            if (e->Attribute(attr) == NULL) {
                return false;
            }
            return true;
        }

        static bool decode(TiXmlElement* e, const char* attr, Ogre::Quaternion& dst);

        static bool decode(TiXmlElement* e, const char* attr, Ogre::Euler& dst);

        static bool decode(TiXmlElement* e, const char* attr, Ogre::Vector4& dst);

        static bool decode(TiXmlElement* e, const char* attr, Ogre::ColourValue& dst);

        static bool decode(TiXmlElement* e, const char* attr, Ogre::Vector3& dst);

        static bool decode(TiXmlElement* e, const char* attr, Ogre::Vector2& dst);

        static bool decode(TiXmlElement* e, const char* attr, bool& dst);

        static bool decode(TiXmlElement* e, const char* attr, uint32_t& dst);

        static bool decode(TiXmlElement* e, const char* attr, Ogre::Real& dst);

        static bool decode(TiXmlElement* e, const char* attr, std::string& dst);

        template<typename T>
        static bool decode(TiXmlElement* e, const char* attr, PRange<T>& dst) {

            if (!exists(e, attr)) {
                return false;
            }
            std::vector<std::string> elems;
            splitstring(e->Attribute(attr), elems);
            if (elems.size() != 3) {
                std::cerr << "SerialisationUtil::decode PRange, invalid entry" << std::endl;
                return false;
            } else {
                dst.set(
                        Ogre::StringConverter::parseReal(elems[0]),
                        Ogre::StringConverter::parseReal(elems[1]),
                        Ogre::StringConverter::parseReal(elems[2])
                        );
                return true;
            }
        }

        static std::string encode(const Ogre::Quaternion& v);

        static std::string encode(const Ogre::Vector4& v);

        static std::string encode(const Ogre::ColourValue& v);

        static std::string encode(const Ogre::Vector3& v);

        static std::string encode(const Ogre::Vector2& v);

        static std::string encode(const bool& v);

    };

};

#endif /* POLYMORPH_SERIALISATIONCOMMON_H */