/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2017 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
*/

/* 
 * File:   PBulletSpringData.h
 * Author: frankiezafe
 *
 * Created on October 11, 2017, 8:49 PM
 */

#ifndef POLYMORPH_BULLETSPRINGDATA_H
#define POLYMORPH_BULLETSPRINGDATA_H

#include "Ogre.h"
#include "PRange.h"
#include "PBulletTypes.h"

namespace polymorph {
    
    class PBulletSpringData {
    public:

        btScalar stiffness_linear; // period 1 sec for !kG body
        btScalar stiffness_angular;
        btScalar damping_linear;
        btScalar damping_angular;
        bool enableEquilibrium;
        btScalar stop_cfm;
        btScalar stop_erp;

        bool pos1_enabled;
        btVector3 pos1;
        bool pos2_enabled;
        btVector3 pos2;

        PRange<btVector3> limit_linear;
        PRange<btVector3> limit_angular;

        PBulletSpringData() :
        stiffness_linear(0),
        stiffness_angular(0),
        damping_linear(1),
        damping_angular(1),
        enableEquilibrium(false),
        stop_cfm(1.e-5),
        stop_erp(1.e-5),
        pos1_enabled(false),
        pos2_enabled(false) {
        }

        inline void setPos1(const Ogre::Vector3& src) {
            pos1_enabled = true;
            pos1 = PUtil::convert(src);
        }

        inline void setPos1(const btVector3& src) {
            pos1_enabled = true;
            pos1 = src;
        }

        inline void setPos2(const Ogre::Vector3& src) {
            pos2_enabled = true;
            pos2 = PUtil::convert(src);
        }

        inline void setPos2(const btVector3& src) {
            pos2_enabled = true;
            pos2 = src;
        }
        
        friend std::ostream& operator<<(std::ostream& os, const PBulletSpringData& d) {
            os <<
                    "PBulletSpringData:" << std::endl << 
                    "\tlinear stiffness: " << d.stiffness_linear << std::endl <<  
                    "\tlinear damping: " << d.damping_linear << std::endl <<  
                    "\tangular stiffness: " << d.stiffness_angular << std::endl <<  
                    "\tangular damping: " << d.damping_angular << std::endl <<  
                    "\tenableEquilibrium: " << d.enableEquilibrium <<  std::endl <<  
                    "\tstop_cfm: " << d.stop_cfm <<  std::endl <<  
                    "\tstop_erp: " << d.stop_erp <<  std::endl <<  
                    "\tpos1_enabled: " << d.pos1_enabled << 
                    ", pos1: " << PUtil::str( PUtil::convert( d.pos1 ) ) << std::endl <<   
                    "\tpos2_enabled: " << d.pos2_enabled <<
                    ", pos2: " << PUtil::str( PUtil::convert( d.pos2 ) ) << std::endl <<   
                    "\tlimit_linear " << 
                    " min: " << PUtil::str( PUtil::convert( d.limit_linear.min ) ) <<
                    ", max: " << PUtil::str( PUtil::convert( d.limit_linear.max ) ) <<
                    ", enabled: " << d.limit_linear.enabled <<
                    std::endl <<
                    "\tlimit_angular: " << 
                    " min: " << PUtil::str( PUtil::convert( d.limit_angular.min ) ) <<
                    ", max: " << PUtil::str( PUtil::convert( d.limit_angular.max ) ) <<
                    ", enabled: " << d.limit_angular.enabled;
            return os;
        }
    };
    
};

#endif /* POLYMORPH_BULLETSPRINGDATA_H */

