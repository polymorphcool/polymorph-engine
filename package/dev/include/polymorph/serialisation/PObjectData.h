/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
*/

/* 
 * File:   PObjectData.h
 * Author: frankiezafe
 *
 * Created on November 24, 2016, 9:01 PM
 */

#ifndef POLYMORPH_OBJECTDATA_H
#define POLYMORPH_OBJECTDATA_H

#include "Ogre.h"
#include "PBulletTypes.h"
#include "PUtil.h"

namespace polymorph {
    
    class PObjectData {
    public:
        
        Ogre::uint32                UID;
        Ogre::uint32                pUID; // parent UID
        bool                        visible;
        bool                        debug;
        Ogre::String                name;
        Ogre::Vector3               origin_pos;
        Ogre::Vector3               origin_scale;
        Ogre::Euler                 origin_orientation;
        
        // repeat
        bool                        repeat;
        Ogre::uint32                repeat_count;
        Ogre::Vector3               repeat_pos;
        Ogre::Vector3               repeat_scale;
        Ogre::Euler                 repeat_orientation;
        
        /**
         * true if this data is a copy of an original
         * false by default
         */
        bool                        generated;
                
        PObjectData():
        UID( UINT32_MAX ),
        pUID( UINT32_MAX ),
        visible( false ),
        debug( false ),
        origin_pos( Ogre::Vector3::ZERO ),
        origin_scale( Ogre::Vector3::UNIT_SCALE ),
        repeat( false ),
        repeat_count( 0 ),
        repeat_pos( Ogre::Vector3::ZERO ),
        repeat_scale( Ogre::Vector3::ZERO ),
        generated( false )
        {}
        
        inline void operator = ( const PObjectData& src ) {
            UID = src.UID;
            pUID = src.pUID;
            visible = src.visible;
            debug = src.debug;
            name = src.name;
            origin_pos = src.origin_pos;
            origin_scale = src.origin_scale;
            origin_orientation = src.origin_orientation;
            repeat = src.repeat;
            repeat_count = src.repeat_count;
            repeat_pos = src.repeat_pos;
            repeat_scale = src.repeat_scale;
            repeat_orientation = src.repeat_orientation;
        }
        
        inline void print() {
            std::cout << 
                "\t" << "UID: " << UID << std::endl <<
                "\t" << "pUID: " << pUID << std::endl <<
                "\t" << "name: " << name << std::endl <<
                "\t" << "visible: " << visible << std::endl <<
                "\t" << "debug: " << debug << std::endl <<
                "\t" << "origin_pos: " << 
                    origin_pos.x << ", " <<
                    origin_pos.y << ", " <<
                    origin_pos.z << std::endl <<
                "\t" << "origin_scale: " << 
                    origin_scale.x << ", " <<
                    origin_scale.y << ", " <<
                    origin_scale.z << std::endl <<
                "\t" << "origin_orientation: " << 
                    origin_orientation.pitch() << ", " <<
                    origin_orientation.yaw() << ", " <<
                    origin_orientation.roll() <<
                "\t" << "generated: " << 
                    generated << std::endl <<
                "\t" << "repeat: " << 
                    repeat << std::endl <<
                "\t" << "repeat_count: " << 
                    repeat_count << std::endl <<
                "\t" << "repeat_pos: " << 
                    repeat_pos.x << ", " <<
                    repeat_pos.y << ", " <<
                    repeat_pos.z << std::endl <<
                "\t" << "repeat_scale: " << 
                    repeat_scale.x << ", " <<
                    repeat_scale.y << ", " <<
                    repeat_scale.z << std::endl <<
                "\t" << "repeat_orientation: " << 
                    repeat_orientation.pitch() << ", " <<
                    repeat_orientation.yaw() << ", " <<
                    repeat_orientation.roll() << std::endl;
        }
        
    };
    
    // used in PSceneData to check names consistency
    typedef std::map< std::string, PObjectData* > PObjDataMap;
    typedef std::map< std::string, PObjectData* >::iterator PObjDataMapIter;
    
};

#endif /* POLYMORPH_OBJECTDATA_H */

