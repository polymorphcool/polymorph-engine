/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2017 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
*/

/* 
 * File:   PCompositorWSValue.h
 * Author: frankiezafe
 *
 * Created on April 24, 2017, 6:23 PM
 */

#ifndef POLYMORPH_WORKSPACEDATAVALUE_H
#define POLYMORPH_WORKSPACEDATAVALUE_H

#include "Ogre.h"
#include "PUtil.h"

namespace polymorph {

    template< class T >
    class PCompositorWSValue {
    public:

        uint32_t node;
        uint32_t pass;
        uint32_t index;
        std::string name;
        T value;

        PCompositorWSValue() :
        node(-1),
        pass(-1),
        index(-1) {
        }

        inline bool isCustom() {
            uint32_t i(-1);
            return ( node != i && pass != i && index != i);
        }

        inline void operator=(const PCompositorWSValue<T>& src) {
            node = src.node;
            pass = src.pass;
            index = src.index;
            name = src.name;
            value = src.value;
        }

        inline void operator=(PCompositorWSValue<T>* src) {
            node = src->node;
            pass = src->pass;
            index = src->index;
            name = src.name;
            value = src->value;
        }

    };
    
};

#endif /* POLYMORPH_WORKSPACEDATAVALUE_H */

