/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ___________________________________( ^3^)_____________________________________
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 */

/* 
 * File:   PProject.h
 * Author: frankiezafe
 *
 * Created on October 21, 2016, 3:40 PM
 */

#ifndef POLYMORPH_PROJECT_H
#define POLYMORPH_PROJECT_H

#include "PResourceData.h"
#include "PCompositorWSData.h"
#include "PSceneData.h"
#include "PBulletEngineData.h"
#include "POscData.h"
#include "PDData.h"

#include <boost/timer.hpp>
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#include <boost/date_time/gregorian/gregorian.hpp>
#endif

namespace polymorph {

    class PProjectData {
    public:

        std::string group;
        std::string file;
        std::string path;
        uint16_t version;
        uint16_t subversion;
        boost::gregorian::date date;
        
        PWSList compositor_workspaces;
        PWSMap compositor_workspace_map;
        
        PSceDataMap scenes_map;
        PSceDataList scenes;
        
        PBulletEngineData bullet;
        
        PDData pd;
        
        POSDataList network_osc_senders;
        PORDataList network_osc_receivers;
        
        PRDMap resources;

        PProjectData() 
        {}
        
        ~PProjectData() {
            clear();
        }
        
        inline void clear() {
            
            scenes.clear();
            network_osc_senders.clear();
            network_osc_receivers.clear();
            PSceDataMapIter it = scenes_map.begin();
            PSceDataMapIter ite = scenes_map.end();
            for ( ; it != ite; ++it ) {
                delete it->second;
            }
            scenes_map.clear();
            
            // cleanup of resources: map of vector of objects
            PRDMapIter rit = resources.begin();
            PRDMapIter rite = resources.end();
            for ( ; rit != rite; ++rit ) {
                std::vector< PResourceData* >::iterator dit =
                        rit->second.begin();
                std::vector< PResourceData* >::iterator dite =
                        rit->second.end();
                for ( ; dit != dite; ++dit ) {
                    delete (*dit);
                }
            }
            resources.clear();
            
            PWSListIter wit = compositor_workspaces.begin();
            PWSListIter wite = compositor_workspaces.end();
            for (; wit != wite; ++wit) {
                delete (*wit);
            }
            compositor_workspaces.clear();
            compositor_workspace_map.clear();
            
            
        }
        
        /**
         * Returns a sorted array of scene's id
         * @param ids
         *      a null pointer of type uint32_t, 
         *      it is turned into an array by the  method, 
         *      stays as it is if no scene
         * @param num
         *      the number of scenes in the project
         * @note
         *      do not forget to delete ids once used!
         */
        void getSceneIds(uint32_t*& ids, size_t& num) {

            num = scenes.size();

            if (num == 0) return;

            ids = new uint32_t[ num ];
            PSceDataListIter it = scenes.begin();
            PSceDataListIter it_end = scenes.end();
            for (int i = 0; it != it_end; ++it, ++i) {
                ids[ i ] = (*it)->UID;
            }
            std::sort(ids, ids + num);

        }

        PSceneData* scene( uint32_t scene_id) {
            if ( scenes_map.find( scene_id ) == scenes_map.end() ) {
                return 0;
            }
            return scenes_map[ scene_id ];
        }

        PSceneData* create( uint32_t scene_id, std::string name = "" ) {

            if (scenes_map.find(scene_id) != scenes_map.end()) {
                std::cout <<
                        "PProjectData::scene, " <<
                        "duplicated scene ID! " <<
                        scene_id << std::endl;
                return 0;
            }
            
            PSceneData* sc = 0;
            if ( name.empty() ) {
                sc = new PSceneData( scene_id );
            } else {
                sc = new PSceneData( scene_id, name );
            }
            
            scenes.push_back(sc);
            scenes_map[ scene_id ] = sc;
            return sc;

        }

    };

};

#endif /* POLYMORPH_PROJECT_H */

