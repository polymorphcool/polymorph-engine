/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
*/

/* 
 * File:   Polymorph.h
 * Author: frankiezafe
 *
 * Created on September 22, 2016, 8:39 PM
 */

#ifndef POLYMORPH_H
#define POLYMORPH_H

// serialisation
#include "PBulletEngineData.h"
#include "PCamRigData.h"
#include "PResourceData.h"
#include "PNodeData.h"
#include "PLightData.h"
#include "PProjectData.h"
#include "PSceneData.h"
#include "SerialisationCommon.h"

// types
#include "PBulletTypes.h"
#include "POscArgPair.h"
#include "POscMessageData.h"
#include "PRange.h"
#include "PApplicationConfiguration.h"

// base
#include "PUtil.h"
#include "PBullet.h"
#include "PBulletRigidbodyUser.h"
#include "PCamRig.h"
#include "PCamera.h"
#include "PCommon.h"
#include "PObject.h"
#include "PMaterial.h"
#include "PNode.h"
#include "PLight.h"
#include "POscReceiver.h"
#include "POscSender.h"
#include "PXmlLoader.h"
#include "PObjectManager.h"

// inputs
#include "PInputTracker.h"

//#include "PSoundData.h"
//#include "PSound.h"

#endif /* POLYMORPH_H */

