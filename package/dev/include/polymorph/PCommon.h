/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
 */

/* 
 * File:   Common.h
 * Author: frankiezafe
 *
 * Created on September 22, 2016, 8:27 PM
 */

#ifndef POLYMORPH_COMMON_H
#define POLYMORPH_COMMON_H

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#include <iostream>
#endif

#include "Ogre.h"
#include "Euler.h"

#define POLYMORPH_VERSION       0
#define POLYMORPH_SUBVERSION    1

#define POLYMORPH_DEBUG_BONE_PREFIX "_debug_bnode_"

#define POLYMORPH_XYZAXIS_LENGTH       50
#define POLYMORPH_XYZAXIS_THICKNESS    1

namespace polymorph {

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    typedef unsigned int uint;
#else
    // no change on standard lib
    typedef uint uint;
#endif

//    typedef Ogre::Node::TransformSpace T_SPACE;

    enum TransformSpace {
        /// Transform is relative to the local space
        TS_LOCAL,
        /// Transform is relative to the space of the parent node
        TS_PARENT,
        /// Transform is relative to world space
        TS_WORLD
    };

    static const Ogre::Vector3 ONE_PERCENT(0.01, 0.01, 0.01);

    // !!! destroy manually !!!

    struct btOgreMeshData {
        int64_t vertices_count;
        Ogre::Vector3* vertices;
        int64_t indices_count;
        int* indices;
        unsigned int mesh_num;
        unsigned int* indices_start_at;
        std::string* mat_names; // used for compound!

    };

    struct XYZaxis {
        Ogre::SceneManager* sm;
        Ogre::SceneNode* parent;
        Ogre::SceneNode* node_X;
        Ogre::SceneNode* node_Y;
        Ogre::SceneNode* node_Z;
        Ogre::Entity* entity_X;
        Ogre::Entity* entity_Y;
        Ogre::Entity* entity_Z;
    };

};

#endif /* POLYMORPH_COMMON_H */

