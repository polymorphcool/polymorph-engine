/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2017 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
 */

/* 
 * File:   PInputTracker.h
 * Author: frankiezafe
 *
 * Created on September 27, 2017, 6:15 PM
 */

#ifndef POLYMORPH_INOUTTRACKER_H
#define POLYMORPH_INOUTTRACKER_H

#include "Ogre.h"

#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE

#include <OIS/OISEvents.h>
#include <OIS/OISInputManager.h>
#include <OIS/OISKeyboard.h>
#include <OIS/OISMouse.h>

#elif OGRE_PLATFORM == OGRE_PLATFORM_ANDROID

#include <android_native_app_glue.h>
#include "Android/OgreAPKFileSystemArchive.h"
#include "Android/OgreAPKZipArchive.h"
#include "OISEvents.h"
#include "OISInputManager.h"
#include "OISKeyboard.h"
#include "OISMouse.h"
#include "SdkTrays.h"

#else

#include "OISEvents.h"
#include "OISInputManager.h"
#include "OISKeyboard.h"
#include "OISMouse.h"

#endif

/* Correspond to the number of button detected in OIS::MouseButtonID enum 
 * in OISMouse.h.
 */
#define PMOUSEBUTTON_COUNT  8
/* Wider than the number of keys defined in OIS::KeyCode in OISKeyboard.h
 */
#define PKEY_COUNT          256

namespace polymorph {

    class PMouseButton {
    public:

        /* Absolute normalised position of the mouse in the window 
         * at the detection of the button pressed.
         * @remark
         *      see PInputTracker::mousePressed for calculation
         */
        Ogre::Vector2 pressed_pos;

        /* Absolute normalised position of the mouse in the window 
         * at the detection of the button released.
         * @remark
         *      see PInputTracker::mouseReleased for calculation
         */
        Ogre::Vector2 released_pos;

        /* Indicates if button is pressed.
         * @remark
         *      see PInputTracker::mousePressed & PInputTracker::mouseReleased
         */
        bool pressed;

        /* Indicates if pressed has changed, must be set to false manually.
         * @remark
         *      see PInputTracker::mousePressed, PInputTracker::mouseReleased
         *      & PInputTracker::reset
         */
        bool changed;

        /* Representation of a mouse button and the normalised position of 
         * the mouse in the window.
         * @remark
         *      All calculations are processed in PInputTracker.
         */
        PMouseButton() :
        pressed(false),
        changed(false) {
        }

        /* Hard reset of pressed and changed.
         */
        inline void purge() {
            pressed = false;
            changed = false;
        }
        
    };

    enum PKeyStatus {
        KS_RELEASED,
        KS_PRESSED,
        KS_CONCURRENT
    };

    class PKeyboardKey {
    public:

        /* Character of the key, see OIS::KeyCode in OISKeyboard.h
         * for the complete list.
         * Default is OIS::KC_UNASSIGNED
         */
        OIS::KeyCode code;

        /* Character of the key having the opposite "direction" 
         * to the current one, see OIS::KeyCode in OISKeyboard.h
         * for the complete list.
         * If set, the keys will communicate together and prevent 
         * inconsistent behavior.
         * Default is OIS::KC_UNASSIGNED
         */
        OIS::KeyCode opposite;

        /* Indicates the current position of the key:
         * KS_RELEASED is the default state;
         * KS_PRESSED indicates that key is currently pressed;
         * KS_CONCURRENT indicates that key is currently pressed but its 
         * opposite prevents it to go to KS_PRESSED state.
         * @remark
         *      see PInputTracker::keyPressed & PInputTracker::keyReleased
         */
        PKeyStatus status;

        /* Indicates if pressed has changed, must be set to false manually.
         * @remark
         *      see PInputTracker::keyPressed, PInputTracker::keyReleased &
         *      PInputTracker::reset
         */
        bool changed;

        /* If opposite is defined and already pressed, this indicates if 
         * the latest key pressed has priority to go state KS_PRESSED. 
         * By default to false, meaning that latest key pressed goes to KS_CONCURRENT state.
         * @remark
         *      see PInputTracker::keyPressed
         */
        bool new_first;

        /* Representation of a keyboard key.
         * @remark
         *      All calculations are processed in PInputTracker.
         */
        PKeyboardKey() :
        code(OIS::KC_UNASSIGNED),
        opposite(OIS::KC_UNASSIGNED),
        status(KS_RELEASED),
        changed(false),
        new_first(false) {
        }

        /* Representation of a keyboard key.
         * @param
         *      OIS::KeyCode of the key.
         * @param
         *      OIS::KeyCode of the opposite key, see opposite field doc.
         * @param
         *      Boolean, see new_first field doc.
         * @remark
         *      All calculations are processed in PInputTracker.
         */
        PKeyboardKey(
                OIS::KeyCode k,
                OIS::KeyCode opposite_key = OIS::KC_UNASSIGNED,
                bool prioriy_to_new = false
                ) :
        code(k),
        opposite(opposite_key),
        status(KS_RELEASED),
        changed(false),
        new_first(prioriy_to_new) {
        }

        /* Check if the key has an opposite key defined.
         */
        inline bool hasOpposite() const {
            return opposite != OIS::KC_UNASSIGNED;
        }

        /* Check if the key is pressed.
         */
        inline bool isPressed() const {
            return status == KS_PRESSED;
        }

        /* Check if the key has just changed to pressed.
         */
        inline bool justPressed() const {
            return (
                    status == KS_PRESSED || 
                    status == KS_CONCURRENT
                    ) && changed;
        }

        /* Check if the key has just changed to released.
         */
        inline bool justReleased() const {
            return status == KS_RELEASED && changed;
        }
        
        /* Check if the key is pressed but in conflict with its opposite.
         */
        inline bool isConcurrent() const {
            return status == KS_CONCURRENT;
        }

        /* Hard reset of status and changed.
         */
        inline void purge() {
            status = KS_RELEASED;
            changed = false;
        }

    };

    /* Manage mouse and keyboard keys.
     */
    class PInputTracker {
    public:

        /* Utility class to gather and prepare mouse and key events in a 
         * simplified way.
         * All methods have to be called manually by an object implementing
         * Ogre::WindowEventListener, OIS::KeyListener and OIS::MouseListener,
         * typically PolymorphApplication.
         * @remark
         *      This class has been prepared for inheritance, 
         *      not direct instantiation.
         */
        PInputTracker();

        virtual ~PInputTracker();

        /* If not set, the event windowResized will reject the call and all
         * mouse positions will be set to 0,0. Normalisation of the mouse 
         * requires an access to the window.
         * @param
         *      The current window.
         */
        virtual void init(Ogre::RenderWindow* w);

        /* Hard reset of all managed keys and mouse button.
         * @remark
         *      See PKeyboardKey::purge and PMouseButton::purge.
         */
        virtual void purge();

        /* Set all keys and buttons "changed" flags to false.
         * Must be called at the end of each frame manually, can be linked to
         * Ogre::RenderQueueListener::postRenderQueues() for efficiency.
         */
        virtual void reset();

        /* Enables tracking of a new button.
         * @param 
         *      OIS::MouseButtonID of the mouse button to monitor.
         * @return
         *      true if button tracking has been successfully enabled.
         */
        virtual bool addMouseButton(OIS::MouseButtonID id);

        /* Disables tracking of a new button.
         * @param 
         *      OIS::MouseButtonID of the mouse button.
         * @return
         *      true if button tracking has been successfully disabled.
         */
        virtual bool removeMouseButton(OIS::MouseButtonID id);

        /* Enables tracking of a key.
         * @param 
         *      OIS::KeyCode of the key to monitor.
         * @param 
         *      OIS::KeyCode of the opposite key. If set, the opposite key 
         *      is automatically generated, no need to call the method with 
         *      flipped keycodes. See PKeyboardKey::opposite field doc.
         * @param 
         *      Boolean to set pressed priority, see PKeyboardKey::new_first 
         *      field doc.  
         * @return
         *      true if key tracking has been successfully enabled.
         */
        virtual bool addKey(
                OIS::KeyCode k,
                OIS::KeyCode opposite_key = OIS::KC_UNASSIGNED,
                bool prioriy_to_new = false);

        /* Disables tracking of a new button.
         * @param 
         *      OIS::MouseButtonID of the mouse button.
         * @param 
         *      Set to true if you need to keep the opposite.
         * @return
         *      true if key tracking has been successfully disabled.
         */
        virtual bool removeKey(
                OIS::KeyCode k,
                bool remove_opposite = true);

        /* OIS::KeyListener callback.
         */
        virtual bool keyPressed(const OIS::KeyEvent &arg);

        /* OIS::KeyListener callback.
         */
        virtual bool keyReleased(const OIS::KeyEvent &arg);

        /* OIS::MouseListener callback.
         */
        virtual bool mouseMoved(const OIS::MouseEvent &arg);

        /* OIS::MouseListener callback.
         */
        virtual bool mousePressed(const OIS::MouseEvent &arg, OIS::MouseButtonID id);

        /* OIS::MouseListener callback.
         */
        virtual bool mouseReleased(const OIS::MouseEvent &arg, OIS::MouseButtonID id);

        /* OIS::WindowEventListener callback.
         */
        virtual bool windowResized(Ogre::RenderWindow* rw);

    protected:

        /*Array of pointers to PMouseButton, set to PMOUSEBUTTON_COUNT at
         * object instantiation and filled up with 0 (~nullptr). */
        PMouseButton** mbuttons;

        /*Array of pointers to PKeyboardKey, set to PKEY_COUNT at
         * object instantiation and filled up with 0 (~nullptr). */
        PKeyboardKey** keys;

        /*Ogre::RenderWindow pointer, set to 0 at object instantiation */
        Ogre::RenderWindow* window;

        /* Vector2 storing window dimensions, avoid calling window->getWidth()
         * & window->getHeight() each time you need to get dimensions.
         * x: width
         * y: height
         */
        Ogre::Vector2 wdimension;
        
        /* Vector2 used to normalise mouse position, used as a multiplier to 
         * apply on both dimensions.
         * x: multiplier of width (1/width)
         * y: multiplier of height (1/height)
         */
        Ogre::Vector2 wmult;

        /*Represents the window ration, width/height.*/
        Ogre::Real wratio;
        
        /* True if the mouse has moved since the last frame.
         * Set to true by PInputTracker::updateMouse, reset to false by
         * PInputTracker::reset();
         */
        bool mouse_moved;

        /* Absolute normalised position of the mouse in the window 
         * at current frame, only rendered if the button is pressed.
         * @remark
         *      see PInputTracker::updateMouse for calculation
         */
        Ogre::Vector2 mouse_absolute_position;

        /* Normalised distance travelled by the mouse in the window 
         * since last frame, only rendered if the button is pressed.
         * @remark
         *      see PInputTracker::updateMouse for calculation
         */
        Ogre::Vector2 mouse_relative_position;
        
        /* Indicates if the absolute position have to be retrieved
         * from the mouse event, or be an accumulation of relative
         * position.
         * @default
         *      false
         * @remark
         *      If the mouse is locked to the window 
         *      (PApplicationConfiguration.input_mouse_capture == true),
         *      set this to true.
         */
        bool mouse_cumulative;

    private:

        void updateMouse(const OIS::MouseEvent &arg);

    };


};

#endif /* POLYMORPH_INOUTTRACKER_H */

