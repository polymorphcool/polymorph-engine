/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
 */

/* 
 * File:   POscSender.h
 * Author: frankiezafe
 *
 * Created on August 16, 2016, 2:09 PM
 */


#ifndef POLYMORPH_OSCSENDER_H
#define POLYMORPH_OSCSENDER_H

#include "UdpSocket.h"
#include "POscMessage.h"

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#include <winsock2.h>
#include <windows.h>
#else
#include <netinet/in.h>
#endif

namespace polymorph {

    /* Class managing OSC message sending.
     * POscSender is able to constructs a POscMessage on its own, see
     * startMessage(), add() methods and sendMessage() methods.
     */
    class POscSender : public POscMessage {
    public:

        /* Standard constructor, use setup() or configure() to eanble
         * message sending.
         */
        POscSender();

        /* Efficient constructor: setup everything at once.
         * @param 
         *      a reference to a POscSenderData describing the configuration
         *      of the sender.
         */
        POscSender(POscSenderData& data);

        /* Standard destructor, nothing special here.
         */
        virtual ~POscSender();

        /* Loads the objects with a configuration. If the object is already 
         * up and running, this is the way to modify its configuration at once.
         * @param 
         *      a reference to a POscSenderData describing the configuration
         *      of the sender.
         */
        void configure(POscSenderData& data);

        /* Manual startup of a sender. By default, this method is starting the 
         * sender.
         * @param
         *      string representation of the IP to reach, "localhost" or 
         *      "192.168.1.100"
         * @param
         *      unsigned integer specifying the port to reach
         * @param
         *      set to false if you want to start sender manually
         */
        void setup(
            Ogre::String address,
            polymorph::uint port, 
            bool autostart = true);

        /* Manual startup of the sender. Same could be achieved by using
         * POscSenderData.autostart=true via the configure() method.
         * @return
         *      true if the port is successfully opened
         */
        bool start();
        
        /* Manual stop of the sender.
         */
        void stop();
 
        /* Sends the internal message constructed with startMessage() and
         * add() methods, and, by default, purges it. Choosing to not purge 
         * the message once sent means that you willing to send it repeatedly, 
         * like a constant "i'm alive" message.
         * @param
         *      set to false to avoid message to be purged after sending
         */
        void sendMessage( bool purge = true );
        
        /* Sends a message constructed outside of the crrent sender.
         * This does not invalidate inetrnal message, if some.
         * @param
         *      a reference to a POscMessage
         */
        void sendMessage( POscMessage& msg );

    private:

        UdpTransmitSocket* _socket;
        std::string _address;
        uint _port;
        bool max_32bits;
        bool autostart;

        osc::OutboundPacketStream* packet;

//        void purge();

        void startEmission();

    };

    typedef std::vector< polymorph::POscSender* > POscSenderList;
    typedef std::vector< polymorph::POscSender* >::iterator POscSenderListIter;

};

#endif /* POLYMORPH_OSCSENDER_H */

