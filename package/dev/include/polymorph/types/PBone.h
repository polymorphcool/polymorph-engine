/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2017 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
*/

/* 
 * File:   PBone.h
 * Author: frankiezafe
 *
 * Created on January 16, 2017, 6:52 PM
 */

#ifndef PBONE_H
#define PBONE_H

#include "Ogre.h"
#include "PCommon.h"

namespace polymorph {
    
    typedef Ogre::OldNode::TransformSpace PBoneSpace;
    typedef Ogre::OldBone PBone;
    typedef Ogre::OldSkeletonInstance PSkeleton;
    
    // debugging commons
    class PBoneDebug {
    public:
        Ogre::SceneManager* sceneMgr;
        Ogre::SceneNode* main;
        Ogre::SceneNode* head;
        Ogre::SceneNode* toparent;
        Ogre::SceneNode* toparent_display;
        Ogre::SceneNode* x_display;
        Ogre::SceneNode* y_display;
        Ogre::SceneNode* z_display;
        Ogre::Entity* head_e;
        Ogre::Entity* toparent_display_e;
        Ogre::Entity* x_display_e;
        Ogre::Entity* y_display_e;
        Ogre::Entity* z_display_e;
        
        PBoneDebug( Ogre::SceneManager* sm ):
            sceneMgr(sm),
            main(0),
            head(0),
            toparent(0),
            toparent_display(0),
            x_display(0),
            y_display(0),
            z_display(0),
            head_e(0),
            toparent_display_e(0),
            x_display_e(0),
            y_display_e(0),
            z_display_e(0)
        {}
        
        ~PBoneDebug() {
            if ( sceneMgr ) {
                if (head_e) 
                    sceneMgr->destroyEntity(head_e);
                if (toparent_display_e) 
                    sceneMgr->destroyEntity(toparent_display_e);
                if (x_display_e) 
                    sceneMgr->destroyEntity(x_display_e);
                if (y_display_e) 
                    sceneMgr->destroyEntity(y_display_e);
                if (z_display_e) 
                    sceneMgr->destroyEntity(z_display_e);
                if (main) 
                    sceneMgr->destroySceneNode(main);
                if (head) 
                    sceneMgr->destroySceneNode(head);
                if (toparent) 
                    sceneMgr->destroySceneNode(toparent);
                if (toparent_display) 
                    sceneMgr->destroySceneNode(toparent_display);
                if (x_display) 
                    sceneMgr->destroySceneNode(x_display);
                if (y_display) 
                    sceneMgr->destroySceneNode(y_display);
                if (z_display) 
                    sceneMgr->destroySceneNode(z_display);
            }
            sceneMgr=0;
            main=0;
            head=0;
            toparent=0;
            toparent_display=0;
            x_display=0;
            y_display=0;
            z_display=0;
            head_e=0;
            toparent_display_e=0;
            x_display_e=0;
            y_display_e=0;
            z_display_e=0;
        }
        
    };
    
    typedef std::map< std::string, PBoneDebug* > PBDebugMap;
    typedef std::map< std::string, PBoneDebug* >::iterator PBDebugMapIter;
    
    enum PNodeDebugMode {
        PDM_NONE = 0,
        PDM_ALL = 1,
        PDM_MESH = 2,
        PDM_SKELETON = 3
    };
    
};

#endif /* PBONE_H */

