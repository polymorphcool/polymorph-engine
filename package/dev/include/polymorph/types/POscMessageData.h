/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
*/

/* 
 * File:   POscMessageData.h
 * Author: frankiezafe
 *
 * Created on October 17, 2016, 1:06 PM
 */

#include <iostream>
#include <string.h>
#include "UdpSocket.h"
#include "OscReceivedElements.h"
#include "boost/date_time.hpp"
#include "POscArgPair.h"

#ifndef POLYMORPH_OSCMESSAGEDATA_H
#define POLYMORPH_OSCMESSAGEDATA_H

namespace polymorph {
    
    union double2uint64 {
        uint64_t i;
        double d;
    };

    class PMessageData {
    public:

        const bool& loaded; // only params you can't change

        uint64_t reception_timestamp;
        size_t addr_size;
        size_t typetags_size;

        char* addr;
        char* typetags;

        size_t int_size;
        size_t float_size;
        size_t char_size;
        size_t rgba_size;
        size_t midi_size;
        size_t int64_size;
        size_t time_size;
        size_t double_size;
        size_t string_size;
        size_t symbol_size;

		osc::int32* args_int;
        float* args_float;
        char* args_char;
		osc::int32* args_rgba;
		osc::int32* args_midi;
		osc::int64* args_int64;
		osc::int64* args_time;
        double* args_double;
        std::string* args_string;
        osc::Symbol* args_symbols;
        PArgumentPair* pairs;

        IpEndpointName remote;

        PMessageData() :
        loaded(_loaded),
        addr_size(0),
        typetags_size(0),
        addr(0),
        int_size(0),
        float_size(0),
        char_size(0),
        rgba_size(0),
        midi_size(0),
        int64_size(0),
        time_size(0),
        double_size(0),
        string_size(0),
        symbol_size(0),
        args_int(0),
        args_float(0),
        args_char(0),
        args_rgba(0),
        args_midi(0),
        args_int64(0),
        args_time(0),
        args_double(0),
        args_string(0),
        args_symbols(0),
        pairs(0) {
            _loaded = false;
        }

        inline bool asBool( uint16_t i ) {
            if ( !_loaded ) return false;
            if ( i >= typetags_size ) return false;
            switch( pairs[ i ].type ) {
                case osc::TRUE_TYPE_TAG:
                    return true;
                case osc::FALSE_TYPE_TAG:
                    return false;
                case osc::INT32_TYPE_TAG:
                    return args_int[ pairs[ i ].index ] != 0;
                case osc::FLOAT_TYPE_TAG:
                    // transtyping!
                    return args_float[ pairs[ i ].index ] != 0;
                case osc::CHAR_TYPE_TAG:
                    // transtyping!
                    return args_char[ pairs[ i ].index ] != 0;
                case osc::RGBA_COLOR_TYPE_TAG:
                    // transtyping!
                    return args_rgba[ pairs[ i ].index ] != 0;
                case osc::MIDI_MESSAGE_TYPE_TAG:
                    // transtyping!
                    return args_midi[ pairs[ i ].index ] != 0;
                case osc::INT64_TYPE_TAG:
                    // transtyping!
                    return args_int64[ pairs[ i ].index ] != 0;
                case osc::TIME_TAG_TYPE_TAG:
                    // transtyping!
                    return args_time[ pairs[ i ].index ] != 0;
                case osc::DOUBLE_TYPE_TAG:
                    // transtyping!
                    return args_double[ pairs[ i ].index ] != 0;
                case osc::STRING_TYPE_TAG:
                    // transtyping!
                    return args_string[ pairs[ i ].index ].compare( "true" ) == 0;
                default:
                    return false;
            }
        }
        
        inline bool asNil( uint16_t i ) {
            if ( !_loaded ) return false;
            if ( i >= typetags_size ) return false;
            return pairs[ i ].type == osc::NIL_TYPE_TAG;
        }
        
        inline bool asInfinite( uint16_t i ) {
            if ( !_loaded ) return false;
            if ( i >= typetags_size ) return false;
            return pairs[ i ].type == osc::INFINITUM_TYPE_TAG;
        }
        
        inline int32_t asInt( uint16_t i ) {
            if ( !_loaded ) return 0;
            if ( i >= typetags_size ) return 0;
            switch( pairs[ i ].type ) {
                case osc::TRUE_TYPE_TAG:
                    return 1;
                case osc::INT32_TYPE_TAG:
                    return args_int[ pairs[ i ].index ];
                case osc::FLOAT_TYPE_TAG:
                    // transtyping!
                    return int32_t( args_float[ pairs[ i ].index ] );
                case osc::CHAR_TYPE_TAG:
                    // transtyping!
                    return int32_t( args_char[ pairs[ i ].index ] );
                case osc::RGBA_COLOR_TYPE_TAG:
                    return args_rgba[ pairs[ i ].index ];
                case osc::MIDI_MESSAGE_TYPE_TAG:
                    return args_midi[ pairs[ i ].index ];
                case osc::INT64_TYPE_TAG:
                    // transtyping!
                    return int32_t( args_int64[ pairs[ i ].index ] );
                case osc::TIME_TAG_TYPE_TAG:
                    // transtyping!
                    return int32_t( args_time[ pairs[ i ].index ] );
                case osc::DOUBLE_TYPE_TAG:
                    // transtyping!
                    return int32_t( args_double[ pairs[ i ].index ] );
                default:
                    return 0;
            }
        }
        
        inline float asFloat( uint16_t i ) {
            if ( !_loaded ) return 0;
            if ( i >= typetags_size ) return 0;
            switch( pairs[ i ].type ) {
                case osc::TRUE_TYPE_TAG:
                    return 1;
                case osc::INT32_TYPE_TAG:
                    // transtyping!
                    return float( args_int[ pairs[ i ].index ] );
                case osc::FLOAT_TYPE_TAG:
                    return args_float[ pairs[ i ].index ];
                case osc::CHAR_TYPE_TAG:
                    // transtyping!
                    return float( args_char[ pairs[ i ].index ] );
                case osc::RGBA_COLOR_TYPE_TAG:
                    // transtyping!
                    return float( args_rgba[ pairs[ i ].index ] );
                case osc::MIDI_MESSAGE_TYPE_TAG:
                    // transtyping!
                    return float( args_midi[ pairs[ i ].index ] );
                case osc::INT64_TYPE_TAG:
                    // transtyping!
                    return float( args_int64[ pairs[ i ].index ] );
                case osc::TIME_TAG_TYPE_TAG:
                    // transtyping!
                    return float( args_time[ pairs[ i ].index ] );
                default:
                    return 0;
            }
        }
        
        inline char asChar( uint16_t i ) {
            if ( !_loaded ) return char( 0 );
            if ( i >= typetags_size ) return char( 0 );
            if ( pairs[ i ].type == osc::CHAR_TYPE_TAG ) {
                return args_char[ pairs[ i ].index ];
            }
            return char( 0 );
        }
        
        inline int32_t asRgba( uint16_t i ) {
            if ( !_loaded ) return 0;
            if ( i >= typetags_size ) return 0;
            switch( pairs[ i ].type ) {
                case osc::INT32_TYPE_TAG:
                    return args_int[ pairs[ i ].index ];
                case osc::RGBA_COLOR_TYPE_TAG:
                    return args_rgba[ pairs[ i ].index ];
                case osc::MIDI_MESSAGE_TYPE_TAG:
                    return args_midi[ pairs[ i ].index ];
                default:
                    return 0;
            }
        }
        
        inline int32_t asMidi( uint16_t i ) {
            if ( !_loaded ) return 0;
            if ( i >= typetags_size ) return 0;
            switch( pairs[ i ].type ) {
                case osc::INT32_TYPE_TAG:
                    return args_int[ pairs[ i ].index ];
                case osc::RGBA_COLOR_TYPE_TAG:
                    return args_rgba[ pairs[ i ].index ];
                case osc::MIDI_MESSAGE_TYPE_TAG:
                    return args_midi[ pairs[ i ].index ];
                default:
                    return 0;
            }
        }
        
        inline int64_t asInt64( uint16_t i ) {
            if ( !_loaded ) return 0;
            if ( i >= typetags_size ) return 0;
            switch( pairs[ i ].type ) {
                case osc::TRUE_TYPE_TAG:
                    return 1;
                case osc::INT32_TYPE_TAG:
                    // transtyping!
                    return int64_t( args_int[ pairs[ i ].index ] );
                case osc::FLOAT_TYPE_TAG:
                    // transtyping!
                    return int64_t( args_float[ pairs[ i ].index ] );
                case osc::CHAR_TYPE_TAG:
                    // transtyping!
                    return int64_t( args_char[ pairs[ i ].index ] );
                case osc::RGBA_COLOR_TYPE_TAG:
                    // transtyping!
                    return int64_t( args_rgba[ pairs[ i ].index ] );
                case osc::DOUBLE_TYPE_TAG:
                    // transtyping!
                    return int64_t( args_double[ pairs[ i ].index ] );
                case osc::MIDI_MESSAGE_TYPE_TAG:
                    return int64_t( args_midi[ pairs[ i ].index ] );
                case osc::INT64_TYPE_TAG:
                    return args_int64[ pairs[ i ].index ];
                case osc::TIME_TAG_TYPE_TAG:
                    return args_time[ pairs[ i ].index ];
                default:
                    return 0;
            }
        }
        
        inline int64_t asTime( uint16_t i ) {
            if ( !_loaded ) return 0;
            if ( i >= typetags_size ) return 0;
            switch( pairs[ i ].type ) {
                case osc::TRUE_TYPE_TAG:
                    return 1;
                case osc::INT32_TYPE_TAG:
                    // transtyping!
                    return int64_t( args_int[ pairs[ i ].index ] );
                case osc::FLOAT_TYPE_TAG:
                    // transtyping!
                    return int64_t( args_float[ pairs[ i ].index ] );
                case osc::CHAR_TYPE_TAG:
                    // transtyping!
                    return int64_t( args_char[ pairs[ i ].index ] );
                case osc::RGBA_COLOR_TYPE_TAG:
                    // transtyping!
                    return int64_t( args_rgba[ pairs[ i ].index ] );
                case osc::MIDI_MESSAGE_TYPE_TAG:
                    return int64_t( args_midi[ pairs[ i ].index ] );
                case osc::INT64_TYPE_TAG:
                    return args_int64[ pairs[ i ].index ];
                case osc::TIME_TAG_TYPE_TAG:
                    return args_time[ pairs[ i ].index ];
                default:
                    return 0;
            }
        }
        
        inline int64_t asDouble( uint16_t i ) {
            if ( !_loaded ) return 0;
            if ( i >= typetags_size ) return 0;
            switch( pairs[ i ].type ) {
                case osc::TRUE_TYPE_TAG:
                    return 1;
                case osc::DOUBLE_TYPE_TAG:
                    return args_double[ pairs[ i ].index ];
                default:
                    return 0;
            }
        }
        
        inline std::string asString( uint16_t i ) {
            if ( !_loaded ) return std::string();
            if ( i >= typetags_size ) return std::string();
            switch( pairs[ i ].type ) {
                case osc::STRING_TYPE_TAG:
                    // transtyping!
                    return args_string[ pairs[ i ].index ];
                case osc::TRUE_TYPE_TAG:
                    // transtyping!
                    return std::string("true");
                case osc::FALSE_TYPE_TAG:
                    // transtyping!
                    return std::string("false");
                case osc::NIL_TYPE_TAG:
                    // transtyping!
                    return std::string("nil");
                case osc::INFINITUM_TYPE_TAG:
                    // transtyping!
                    return std::string("infinite");
                case osc::INT32_TYPE_TAG:
                    // transtyping!
                    return boost::lexical_cast<std::string>( args_int[ pairs[ i ].index ] );
                case osc::FLOAT_TYPE_TAG:
                    // transtyping!
                    return boost::lexical_cast<std::string>( args_float[ pairs[ i ].index ] );
                case osc::CHAR_TYPE_TAG:
                    // transtyping!
                    return boost::lexical_cast<std::string>( args_char[ pairs[ i ].index ] );
                case osc::RGBA_COLOR_TYPE_TAG:
                    // transtyping!
                    return boost::lexical_cast<std::string>( args_rgba[ pairs[ i ].index ] );
                case osc::MIDI_MESSAGE_TYPE_TAG:
                    // transtyping!
                    return boost::lexical_cast<std::string>( args_midi[ pairs[ i ].index ] );
                case osc::INT64_TYPE_TAG:
                    // transtyping!
                    return boost::lexical_cast<std::string>( args_int64[ pairs[ i ].index ] );
                case osc::TIME_TAG_TYPE_TAG:
                    // transtyping!
                    return boost::lexical_cast<std::string>( args_time[ pairs[ i ].index ] );
                case osc::DOUBLE_TYPE_TAG:
                    return boost::lexical_cast<std::string>( args_double[ pairs[ i ].index ] );
                case osc::SYMBOL_TYPE_TAG:
                    return std::string( "symbol" );
                case osc::BLOB_TYPE_TAG:
                    return std::string( "blob" );
                default:
                    return std::string( "undefined" );
            }
        }
        
        inline osc::Symbol asSymbol( uint16_t i ) {
            if ( !_loaded ) return osc::Symbol();
            if ( i >= typetags_size ) return osc::Symbol();
            if ( pairs[ i ].type == osc::CHAR_TYPE_TAG ) {
                return args_symbols[ pairs[ i ].index ];
            }
            return osc::Symbol();
        }
        
        // cool methods for ogre
        inline bool floatCastable( uint16_t i ) {
            if ( !_loaded ) return false;
            if ( i >= typetags_size ) return false;
            switch( pairs[ i ].type ) {
                case osc::INT32_TYPE_TAG:
                case osc::FLOAT_TYPE_TAG:
                case osc::CHAR_TYPE_TAG:
                case osc::RGBA_COLOR_TYPE_TAG:
                case osc::MIDI_MESSAGE_TYPE_TAG:
                case osc::INT64_TYPE_TAG:
                case osc::TIME_TAG_TYPE_TAG:
                    return true;
                default:
                    return false;
            }
        }
        
        inline Ogre::Vector2 asVec2( uint16_t i ) {
            if ( !_loaded ) return Ogre::Vector2::ZERO;
            if ( i > typetags_size - 2 ) return Ogre::Vector2::ZERO;
            bool valid = true;
            Ogre::Vector2 v;
            for ( int j = 0; j < 2; ++j ) {
                valid = floatCastable( i + j );
                if ( valid ) {
                    v[ j ] = asFloat( i + j );
                } else { 
                    return Ogre::Vector2::ZERO;
                }
            }
            return v;
        }
        
        inline Ogre::Vector3 asVec3( uint16_t i ) {
            if ( !_loaded ) return Ogre::Vector3::ZERO;
            if ( i > typetags_size - 3 ) return Ogre::Vector3::ZERO;
            bool valid = true;
            Ogre::Vector3 v;
            for ( int j = 0; j < 3; ++j ) {
                valid = floatCastable( i + j );
                if ( valid ) {
                    v[ j ] = asFloat( i + j );
                } else { 
                    return Ogre::Vector3::ZERO;
                }
            }
            return v;
        }
        
        inline Ogre::Vector4 asVec4( uint16_t i ) {
            if ( !_loaded ) return Ogre::Vector4::ZERO;
            if ( i > typetags_size - 4 ) return Ogre::Vector4::ZERO;
            bool valid = true;
            Ogre::Vector4 v;
            for ( int j = 0; j < 4; ++j ) {
                valid = floatCastable( i + j );
                if ( valid ) {
                    v[ j ] = asFloat( i + j );
                } else { 
                    return Ogre::Vector4::ZERO;
                }
            }
            return v;
        }
        
        inline Ogre::Quaternion asQuaternion( uint16_t i ) {
            if ( !_loaded ) return Ogre::Quaternion::IDENTITY;
            if ( i > typetags_size - 4 ) return Ogre::Quaternion::IDENTITY;
            bool valid = true;
            Ogre::Quaternion q;
            for ( int j = 0; j < 4; ++j ) {
                valid = floatCastable( i + j );
                if ( valid ) {
                    q[ j ] = asFloat( i + j );
                } else { 
                    return Ogre::Quaternion::IDENTITY;
                }
            }
            return q;
        }
        
        inline Ogre::Matrix3 asMatrix3x3( uint16_t i ) {
            if ( !_loaded ) return Ogre::Matrix3::IDENTITY;
            if ( i > typetags_size - 9 ) return Ogre::Matrix3::IDENTITY;
            bool valid = true;
            Ogre::Matrix3 m;
            int j = 0;
            for ( int r = 0; r < 3; ++r ) {
                for ( int c = 0; c < 3; ++c ) {
                    valid = floatCastable( i + j );
                    if ( valid ) {
                        m[ r ][ c ] = asFloat( i + j );
                    } else { 
                        return Ogre::Matrix3::IDENTITY;
                    }
                    ++j;
                }
            }
            return m;
        }
        
        inline Ogre::Matrix4 asMatrix4x4( uint16_t i ) {
            if ( !_loaded ) return Ogre::Matrix4::IDENTITY;
            if ( i > typetags_size - 16 ) return Ogre::Matrix4::IDENTITY;
            bool valid = true;
            Ogre::Matrix4 m;
            int j = 0;
            for ( int r = 0; r < 4; ++r ) {
                for ( int c = 0; c < 4; ++c ) {
                    valid = floatCastable( i + j );
                    if ( valid ) {
                        m[ r ][ c ] = asFloat( i + j );
                    } else { 
                        return Ogre::Matrix4::IDENTITY;
                    }
                    ++j;
                }
            }
            return m;
        }
        
        void load(
                const osc::ReceivedMessage& msg,
                const IpEndpointName& rm) {

            boost::posix_time::ptime t =
                    boost::posix_time::microsec_clock::local_time();
            reception_timestamp = t.time_of_day().total_milliseconds();
            
            if (_loaded) {
                clear();
            }

            remote.address = rm.address;
            remote.port = rm.port;

            std::string s( msg.AddressPattern() );
            addr_size = s.length();
            addr = new char[ addr_size ];
            std::strcpy( addr, s.c_str() );

            s = msg.TypeTags();
            typetags_size = s.length();
            typetags = new char[ typetags_size ];
            std::strcpy( typetags, s.c_str() );
            
            // preparing counts
            int_size = 0;
            float_size = 0;
            char_size = 0;
            rgba_size = 0;
            midi_size = 0;
            int64_size = 0;
            time_size = 0;
            double_size = 0;
            string_size = 0;
            symbol_size = 0;
            pairs = new PArgumentPair[ typetags_size ];

            for (size_t i = 0; i < typetags_size; ++i) {
                switch (typetags[ i ]) {
                    case osc::TRUE_TYPE_TAG:
                    case osc::FALSE_TYPE_TAG:
                    case osc::NIL_TYPE_TAG:
                    case osc::INFINITUM_TYPE_TAG:
                        pairs[ i ] = PArgumentPair(typetags[ i ], 0);
                        break;
                    case osc::INT32_TYPE_TAG:
                        pairs[ i ] = PArgumentPair(typetags[ i ], int_size++);
                        break;
                    case osc::FLOAT_TYPE_TAG:
                        pairs[ i ] = PArgumentPair(typetags[ i ], float_size++);
                        break;
                    case osc::CHAR_TYPE_TAG:
                        pairs[ i ] = PArgumentPair(typetags[ i ], char_size++);
                        break;
                    case osc::RGBA_COLOR_TYPE_TAG:
                        pairs[ i ] = PArgumentPair(typetags[ i ], rgba_size++);
                        break;
                    case osc::MIDI_MESSAGE_TYPE_TAG:
                        pairs[ i ] = PArgumentPair(typetags[ i ], midi_size++);
                        break;
                    case osc::INT64_TYPE_TAG:
                        pairs[ i ] = PArgumentPair(typetags[ i ], int64_size++);
                        break;
                    case osc::TIME_TAG_TYPE_TAG:
                        pairs[ i ] = PArgumentPair(typetags[ i ], time_size++);
                        break;
                    case osc::DOUBLE_TYPE_TAG:
                        pairs[ i ] = PArgumentPair(typetags[ i ], double_size++);
                        break;
                    case osc::STRING_TYPE_TAG:
                        pairs[ i ] = PArgumentPair(typetags[ i ], string_size++);
                        break;
                    case osc::SYMBOL_TYPE_TAG:
                        pairs[ i ] = PArgumentPair(typetags[ i ], symbol_size++);
                        break;
                    default:
                        pairs[ i ] = PArgumentPair();
                        break;
                }
            }

            if (int_size > 0) args_int = new osc::int32[ int_size ];
            if (float_size > 0) args_float = new float[ float_size ];
            if (char_size > 0) args_char = new char[ char_size ];
            if (rgba_size > 0) args_rgba = new osc::int32[ rgba_size ];
            if (midi_size > 0) args_midi = new osc::int32[ midi_size ];
            if (int64_size > 0) args_int64 = new osc::int64[ int64_size ];
            if (time_size > 0) args_time = new osc::int64[ time_size ];
            if (double_size > 0) args_double = new double[ double_size ];
            if (string_size > 0) args_string = new std::string[ string_size ];
            if (symbol_size > 0) args_symbols = new osc::Symbol[ symbol_size ];

            int_size = 0;
            float_size = 0;
            char_size = 0;
            rgba_size = 0;
            midi_size = 0;
            int64_size = 0;
            time_size = 0;
            double_size = 0;
            string_size = 0;
            symbol_size = 0;
            bool trash;

            // message decompression
            osc::ReceivedMessageArgumentStream args = msg.ArgumentStream();
            for (size_t i = 0; i < typetags_size; ++i) {
                PArgumentPair& pair = pairs[ i ];
                switch (pair.type) {
                    case osc::TRUE_TYPE_TAG:
                    case osc::FALSE_TYPE_TAG:
                    case osc::NIL_TYPE_TAG:
                    case osc::INFINITUM_TYPE_TAG:
                        args >> trash;
                        break;
                    case osc::INT32_TYPE_TAG:
                        args >> args_int[int_size++];
                        break;
                    case osc::FLOAT_TYPE_TAG:
                        args >> args_float[ float_size++ ];
                        break;
                    case osc::CHAR_TYPE_TAG:
                        args >> args_char[ char_size++ ];
                        break;
                    case osc::RGBA_COLOR_TYPE_TAG:
                        args >> args_rgba[ rgba_size++ ];
                        break;
                    case osc::MIDI_MESSAGE_TYPE_TAG:
                        args >> args_midi[ midi_size++ ];
                        break;
                    case osc::INT64_TYPE_TAG:
                        args >> args_int64[ int64_size++ ];
                        break;
                    case osc::TIME_TAG_TYPE_TAG:
                        args >> args_time[ time_size++ ];
                        break;
                    case osc::DOUBLE_TYPE_TAG:
                        args >> args_double[ double_size++ ];
                        break;
                    case osc::STRING_TYPE_TAG:
                        args >> args_string[ string_size++ ];
                        break;
                    case osc::SYMBOL_TYPE_TAG:
                        args >> args_symbols[ symbol_size++ ];
                        break;
                    default:
                        break;
                }
            }

            _loaded = true;

        }

        void clear() {

            if (addr) delete [] addr;
            if (typetags) delete [] typetags;
            if (args_int) delete [] args_int;
            if (args_float) delete [] args_float;
            if (args_char) delete [] args_char;
            if (args_rgba) delete [] args_rgba;
            if (args_midi) delete [] args_midi;
            if (args_int64) delete [] args_int64;
            if (args_time) delete [] args_time;
            if (args_double) delete [] args_double;
            if (args_string) delete [] args_string;
            if (args_symbols) delete [] args_symbols;
            if (pairs) delete [] pairs;
            
            addr = 0;
            typetags = 0;
            args_int = 0;
            args_float = 0;
            args_char = 0;
            args_rgba = 0;
            args_midi = 0;
            args_int64 = 0;
            args_time = 0;
            args_double = 0;
            args_string = 0;
            args_symbols = 0;
            pairs = 0;
            
            int_size = 0;
            float_size = 0;
            char_size = 0;
            rgba_size = 0;
            midi_size = 0;
            int64_size = 0;
            time_size = 0;
            double_size = 0;
            string_size = 0;
            symbol_size = 0;
            _loaded = false;

        }

        ~PMessageData() {
            if (_loaded) clear();
        }

        void print() {

            if (!_loaded) {
                std::cout << "empty message" << std::endl;
                return;
            }

            std::cout <<
                    "message: " << addr << ", " << typetags << ", " << typetags_size << "\n" <<
                    "\t" << "reception time: " << reception_timestamp << "\n" <<
                    "\t" << "num of ints: " << int_size << "\n" <<
                    "\t" << "num of floats: " << float_size << "\n" <<
                    "\t" << "num of chars: " << char_size << "\n" <<
                    "\t" << "num of rgbas: " << rgba_size << "\n" <<
                    "\t" << "num of midis: " << midi_size << "\n" <<
                    "\t" << "num of int64s: " << int64_size << "\n" <<
                    "\t" << "num of times: " << time_size << "\n" <<
                    "\t" << "num of doubles: " << double_size << "\n" <<
                    "\t" << "num of strings: " << string_size << "\n" <<
                    "\t" << "num of symbols: " << symbol_size << "\n" <<
                    std::endl;
            
            // showing result
            for (size_t i = 0; i < typetags_size; ++i) {
                std::cout << "\t" << pairs[ i ].type << " at: " << pairs[ i ].index << ", value = ";
                switch (pairs[ i ].type) {
                    case osc::TRUE_TYPE_TAG:
                        std::cout << "true";
                        break;
                    case osc::FALSE_TYPE_TAG:
                        std::cout << "false";
                        break;
                    case osc::NIL_TYPE_TAG:
                        std::cout << "nil";
                        break;
                    case osc::INFINITUM_TYPE_TAG:
                        std::cout << "infinite";
                        break;
                    case osc::INT32_TYPE_TAG:
                        std::cout << args_int[ pairs[ i ].index ];
                        break;
                    case osc::FLOAT_TYPE_TAG:
                        std::cout << args_float[ pairs[ i ].index ];
                        break;
                    case osc::CHAR_TYPE_TAG:
                        std::cout << args_char[ pairs[ i ].index ];
                        break;
                    case osc::RGBA_COLOR_TYPE_TAG:
                        std::cout << args_rgba[ pairs[ i ].index ];
                        break;
                    case osc::MIDI_MESSAGE_TYPE_TAG:
                        std::cout << args_midi[ pairs[ i ].index ];
                        break;
                    case osc::INT64_TYPE_TAG:
                        std::cout << args_int64[ pairs[ i ].index ];
                        break;
                    case osc::TIME_TAG_TYPE_TAG:
                        std::cout << args_time[ pairs[ i ].index ];
                        break;
                    case osc::DOUBLE_TYPE_TAG:
                        std::cout << args_double[ pairs[ i ].index ];
                        break;
                    case osc::STRING_TYPE_TAG:
                        std::cout << args_string[ pairs[ i ].index ];
                        break;
                    case osc::SYMBOL_TYPE_TAG:
                        std::cout << args_symbols[ pairs[ i ].index ];
                        break;
                    default:
                        // nothing
                        break;
                }
                std::cout << std::endl;
            }
        }

        inline void operator=(const PMessageData& src) {

            if (_loaded) clear();

            _loaded = src.loaded;

            if (!_loaded) return;

            reception_timestamp = src.reception_timestamp;

            addr_size = src.addr_size;
            typetags_size = src.typetags_size;

            memccpy(addr, src.addr, 0, addr_size);
            memccpy(typetags, src.typetags, 0, typetags_size);

            pairs = new PArgumentPair[ typetags_size ];
            std::copy(
                        &(src.pairs[0]), &(src.pairs[typetags_size]),
                        pairs);
            
            int_size = src.int_size;
            float_size = src.float_size;
            char_size = src.char_size;
            rgba_size = src.rgba_size;
            midi_size = src.midi_size;
            int64_size = src.int64_size;
            time_size = src.time_size;
            double_size = src.double_size;
            string_size = src.string_size;
            symbol_size = src.symbol_size;

            if (int_size > 0) args_int = new osc::int32[ int_size ];
            if (float_size > 0) args_float = new float[ float_size ];
            if (char_size > 0) args_char = new char[ char_size ];
            if (rgba_size > 0) args_rgba = new osc::int32[ rgba_size ];
            if (midi_size > 0) args_midi = new osc::int32[ midi_size ];
            if (int64_size > 0) args_int64 = new osc::int64[ int64_size ];
            if (time_size > 0) args_time = new osc::int64[ time_size ];
            if (double_size > 0) args_double = new double[ double_size ];
            if (string_size > 0) args_string = new std::string[ string_size ];
            if (symbol_size > 0) args_symbols = new osc::Symbol[ symbol_size ];

            if (args_int) {
                memccpy(args_int, src.args_int, 0,
                        int_size * sizeof (int32_t));
            }
            if (args_float) {
                memccpy(args_float, src.args_float, 0,
                        float_size * sizeof (float));
            }
            if (args_char) {
                memccpy(args_char, src.args_char, 0,
                        char_size * sizeof (char));
            }
            if (args_rgba) {
                memccpy(args_rgba, src.args_rgba, 0,
                        rgba_size * sizeof (int32_t));
            }
            if (args_midi) {
                memccpy(args_midi, src.args_midi, 0,
                        midi_size * sizeof (int32_t));
            }
            if (args_int64) {
                memccpy(args_int64, src.args_int64, 0,
                        int64_size * sizeof (int64_t));
            }
            if (args_time) {
                memccpy(args_time, src.args_time, 0,
                        time_size * sizeof (int64_t));
            }
            if (args_double) {
                memccpy(args_double, src.args_double, 0,
                        double_size * sizeof (double));
            }
            if (args_string) {
                std::copy(
                        &(src.args_string[0]),&(src.args_string[string_size]),
                        args_string);
            }
            if (args_symbols) {
                std::copy(
                        &(src.args_symbols[0]), &(src.args_symbols[string_size]),
                        args_symbols);
            }

            remote.address = src.remote.address;
            remote.port = src.remote.port;

        }


    protected:
        bool _loaded;

    };

    typedef std::list< polymorph::PMessageData* > MsgQueue;
    typedef std::list< polymorph::PMessageData* >::iterator MsgQueueIter;

};

#endif /* POLYMORPH_OSCMESSAGEDATA_H */

