/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
*/

/* 
 * File:   PBulletRay.h
 * Author: frankiezafe
 *
 * Created on December 7, 2016, 10:11 AM
 */

#ifndef POLYMORPH_BULLETRAY_H
#define POLYMORPH_BULLETRAY_H

#include "PBulletTypes.h"

namespace polymorph {

    class PBulletRay {
    public:
        bool hit;
        bool callable;
        bool isPNode;
        bool isPLight;
        bool isPCam;
        void* object;
        Ogre::Vector3 point;

        PBulletRay() :
        hit(false),
        callable(false),
        isPNode(false),
        isPLight(false),
        isPCam(false),
        object(0) {
        }

        inline void reset() {
            hit = false;
            callable = false;
            isPNode = false;
            isPLight = false;
            isPCam = false;
            object = 0;
            point = Ogre::Vector3::ZERO;
        }
        
        inline void operator=(const PBulletRay& src) {
            hit = src.hit;
            callable = src.callable;
            isPNode = src.isPNode;
            isPLight = src.isPLight;
            isPCam = src.isPCam;
            object = src.object;
            point = src.point;
        }
         
    };
};

#endif /* POLYMORPH_BULLETRAY_H */

