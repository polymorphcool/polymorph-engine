/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
*/

/* 
 * File:   PBulletExchange.h
 * Author: frankiezafe
 *
 * Created on December 7, 2016, 10:14 AM
 */

#ifndef POLYMORPH_BULLETEXCHANGE_H
#define POLYMORPH_BULLETEXCHANGE_H

#include "PBulletTypes.h"
#include "PBulletRay.h"

namespace polymorph {

    /** Used to exchange information between an non-bullet object
        and bullet objects.
        @param v1 first Ogre::Vector3 to use
        @param v2 second Ogre::Vector3 to use 
        (reference, place the Ogre::Vector3::UNIT_Y here)
        @param secure_check set to true if you are not sure your vectors 
        are normalised
    @note 
        http://lolengine.net/blog/2013/09/18/beautiful-maths-quaternion-from-vectors
     */
    class PBulletExchange {
    public:
        // polymorph type
        BT_Type type;
        // bullet types
        btCollisionShape* collisionshape;
        btTransform transform;
        btDefaultMotionState* mstate;
        btRigidBody* body;
        btVector3 inertia;
        btScalar mass;
        btScalar volume; // change at your own risk...
        // creation params
        std::string meshgroup;
        std::string meshname;
        btVector3 position;
        btVector3 scale;
        btVector3 size; // original size multiplied by scale
        btQuaternion orientation;
        btScalar density; // will multiply the volume for mass calculation
        btScalar friction;
        btScalar restitution;
        btScalar damping_linear;
        btScalar damping_angular;
        // callable
        bool& callable;
        bool& isPNode;
        bool& isPLight;
        bool& isPCam;

        PBulletExchange() :
        type(BT_NONE),
        collisionshape(0),
        mstate(0),
        body(0),
        mass(0),
        volume(0),
        density(1),
        friction(0.5),
        restitution(0),
        damping_linear(0),
        damping_angular(0),
        callable(_callable),
        isPNode(_isPNode),
        isPLight(_isPLight),
        isPCam(_isPCam),
        _callable(false),
        _isPNode(false),
        _isPLight(false),
        _isPCam(false)
        {
            transform.setIdentity();
            inertia.setValue(0, 0, 0);
            position.setZero();
            scale.setZero();
            size.setZero();
            orientation = btQuaternion::getIdentity();
            
        }

        virtual ~PBulletExchange() {
        }

        virtual void clear() {
            type = BT_NONE;
            collisionshape = 0;
            mstate = 0;
            body = 0;
            mass = 0;
            volume = 0;
            density = 1;
            friction = 0.5;
            restitution = 0;
            damping_linear = 0;
            damping_angular = 0;
            _callable = false;
            _isPNode = false;
            _isPLight = false;
            _isPCam = false;
        }

        virtual void* caller() {
            return 0;
        }

        inline void ray(PBulletRay* rr) {
            rr->callable = callable;
            rr->object = caller();
            rr->isPNode = isPNode;
            rr->isPLight = isPLight;
            rr->isPCam = isPCam;
        }

    protected:

        bool _callable;
        bool _isPNode;
        bool _isPLight;
        bool _isPCam;

    };

    class PBulletExchangePNode : public PBulletExchange {
    public:

        PBulletExchangePNode() :
        PBulletExchange(),
        _caller(0) {
            _isPNode = true;
        }

        inline void set(void* t) {
            _caller = t;
            _callable = true;
        }

        virtual void clear() {
            PBulletExchange::clear();
            _caller = 0;
        }

        virtual void* caller() {
            return _caller;
        }

    protected:

        void* _caller;

    };

    class PBulletExchangePLight : public PBulletExchange {
    public:

        PBulletExchangePLight() :
        PBulletExchange(),
        _caller(0) {
            _isPLight = true;
        }

        inline void set(void* t) {
            _caller = t;
            _callable = true;
        }

        virtual void clear() {
            PBulletExchange::clear();
            _caller = 0;
        }

        virtual void* caller() {
            return _caller;
        }

    protected:

        void* _caller;

    };
    
    class PBulletExchangePCam : public PBulletExchange {
    public:

        PBulletExchangePCam() :
        PBulletExchange(),
        _caller(0) {
            _isPCam = true;
        }

        inline void set(void* t) {
            _caller = t;
            _callable = true;
        }

        virtual void clear() {
            PBulletExchange::clear();
            _caller = 0;
        }

        virtual void* caller() {
            return _caller;
        }

    protected:

        void* _caller;

    };
    
};

#endif /* POLYMORPH_BULLETEXCHANGE_H */

