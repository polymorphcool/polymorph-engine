/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
*/

/* 
 * File:   PRange.h
 * Author: frankiezafe
 *
 * Created on October 11, 2016, 9:00 PM
 */

#ifndef POLYMORPH_RANGE_H
#define POLYMORPH_RANGE_H

#include "Ogre.h"

namespace polymorph {
    
    template< typename T >
    class PRange {
    
    public:
        
        T min;
        T max;
        T init;
        T delta;
        bool enabled;
        
        PRange<T>():
        enabled(false)
        {
        }
        
        PRange<T>( const T& v ):
        enabled(false) {
            set( v, v, v );
        }
        
        PRange<T>( const T& min, const T& max ):
        enabled(false) {
            set( min, min, max );
        }
        
        PRange<T>( const T& min, const T& init, const T& max ):
        enabled(false) {
            set( min, init, max );
        }
        
        void set( const T& v ) {
            set( v, v, v );
        }
        
        void set( const T& min, const T& max ) {
            set( min, init, max );
        }
        
        inline void set( const T& min, const T& init, const T& max ) {
            this->min = min;
            this->init = init;
            this->max = max;
            process();
        }
        
        inline T lerp( Ogre::Real pc, bool caps = true ) {
            if ( caps ) {
                if ( pc <= 0 ) return T( min ); 
                if ( pc >= 1 ) return T( max );
            }
            return T( min + ( max - min ) * pc );
        }
        
        inline T percent( const T& v ) {
            return ( v - min ) / delta;
        }
        
        inline T diff( const T& other ) {
            return (*this) - other;
        }
        
        inline void merge( const T& other ) {
            if ( min < other.min ) min = other.min;
            if ( max > other.max ) max = other.max;
            process();
        }
        
        inline void process() {
            this->delta = max - min;
            if ( min == max ) {
                enabled = false;
            } else {
                enabled = true;
            }
        }
        
        inline void operator = ( const T& other ) const {
            min = other.minimum;
            max = other.maximum;
            init = other.init;
            delta = other.dist;
            enabled = other.enabled;
        }
        
        friend std::ostream& operator<<(std::ostream& os, const PRange<T>& d) {
            os <<
                    "min: " << d.min << 
                    ", max: " << d.max << 
                    ", init: " << d.init << 
                    ", delta: " << d.delta << 
                    ", enabled: " << d.enabled;
            return os;
        }
        
    };
    
};

#endif /* POLYMORPH_RANGE_H */

