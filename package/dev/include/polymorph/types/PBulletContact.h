/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
 */

/* 
 * File:   PBulletContact.h
 * Author: frankiezafe
 *
 * Created on December 6, 2016, 7:18 PM
 */

#ifndef POLYMORPH_BULLETCONTACT_H
#define POLYMORPH_BULLETCONTACT_H

#include "PCommon.h"
#include "PBulletTypes.h"
#include "PUtil.h"
#include "PBulletRigidbodyUser.h"

namespace polymorph {

    class PBulletContactSolo {
    public:

        bool first_time;
        unsigned long start;
        unsigned long last_update;
        uint32_t count;
        polymorph::PBulletRigidbodyUser* object;

        btScalar total_impulse;
        btScalar total_friction;
        btScalar total_rolling_friction;
        btScalar total_spinning_friction;

        btVector3 average_local;
        btVector3 average_world;

        PBulletContactSolo() :
        first_time(true),
        start(0),
        last_update(0),
        count(0),
        object(0),
        total_impulse(0),
        total_friction(0),
        total_rolling_friction(0),
        total_spinning_friction(0),
        average_local(0, 0, 0),
        average_world(0, 0, 0) {
        }

        inline void set(
                const unsigned long& ms,
                polymorph::PBulletRigidbodyUser* p,
                btManifoldPoint& cp,
                bool useA
                ) {
            start = ms;
            last_update = ms;
            object = p;
            add(cp, ms, useA);
        }

        inline void reset() {
            count = 0;
            last_update = 0;
            total_impulse = 0;
            total_friction = 0;
            total_rolling_friction = 0;
            total_spinning_friction = 0;
            average_local = btVector3(0, 0, 0);
            average_world = btVector3(0, 0, 0);
        }

        inline void add(
                btManifoldPoint& cp,
                const unsigned long& ms,
                bool useA) {
            ++count;
            last_update = ms;
            total_impulse += cp.m_appliedImpulse;
            total_friction += cp.m_combinedFriction;
            total_rolling_friction += cp.m_combinedRollingFriction;
            total_spinning_friction += cp.m_combinedSpinningFriction;
            if (useA) {
                average_local += cp.m_localPointA;
                average_world += cp.m_positionWorldOnA;
            } else {
                average_local += cp.m_localPointB;
                average_world += cp.m_positionWorldOnB;
            }
        }

        inline void render() {
            average_local *= count;
            average_world *= count;
            first_time = false;
        }

    };

    typedef polymorph::PBulletRigidbodyUser* PBCSkey;
    typedef std::map< PBCSkey, PBulletContactSolo >
    PBCSmap;
    typedef std::map< PBCSkey, PBulletContactSolo >::iterator
    PBCSiterator;

    class PBulletContactPairData {
    public:

        btScalar impulse;
        btScalar friction;
        btScalar rolling_friction;
        btScalar spinning_friction;
        btVector3 lateral_dir1;
        btVector3 lateral_dir2;

        btVector3 local0;
        btVector3 local1;
        btVector3 world;

        PBulletContactPairData() :
        impulse(0),
        friction(0),
        rolling_friction(0),
        spinning_friction(0),
        lateral_dir1(0, 0, 0),
        lateral_dir2(0, 0, 0),
        local0(0, 0, 0),
        local1(0, 0, 0),
        world(0, 0, 0) {
        }

        inline void reset() {
            impulse = 0;
            friction = 0;
            rolling_friction = 0;
            spinning_friction = 0;
            lateral_dir1 = btVector3(0, 0, 0);
            lateral_dir2 = btVector3(0, 0, 0);
            local0 = btVector3(0, 0, 0);
            local1 = btVector3(0, 0, 0);
            world = btVector3(0, 0, 0);
        }

        inline void operator+=(btManifoldPoint& cp) {
            impulse += cp.m_appliedImpulse;
            friction += cp.m_combinedFriction;
            rolling_friction += cp.m_combinedRollingFriction;
            spinning_friction += cp.m_combinedSpinningFriction;
            lateral_dir1 += cp.m_lateralFrictionDir1;
            lateral_dir2 += cp.m_lateralFrictionDir2;
            local0 += cp.m_localPointA;
            local1 += cp.m_localPointB;
            world += (
                    cp.m_positionWorldOnA + 
                    cp.m_positionWorldOnB 
                    ) * 0.5;
        }

        inline void operator=(const PBulletContactPairData& src) {
            impulse = src.impulse;
            friction = src.friction;
            rolling_friction = src.rolling_friction;
            spinning_friction = src.spinning_friction;
            lateral_dir1 = src.lateral_dir1;
            lateral_dir2 = src.lateral_dir2;
            local0 = src.local0;
            local1 = src.local1;
            world = src.world;
        }

        inline void operator-=(const PBulletContactPairData& src) {
            impulse -= src.impulse;
            friction -= src.friction;
            rolling_friction -= src.rolling_friction;
            spinning_friction -= src.spinning_friction;
            lateral_dir1 -= src.lateral_dir1;
            lateral_dir2 -= src.lateral_dir2;
            local0 -= src.local0;
            local1 -= src.local1;
            world -= src.world;
        }

        inline PBulletContactPairData operator-(const PBulletContactPairData& src) const {
            PBulletContactPairData out;
            out.impulse = impulse - src.impulse;
            out.friction = friction - src.friction;
            out.rolling_friction = rolling_friction - src.rolling_friction;
            out.spinning_friction = spinning_friction - src.spinning_friction;
            out.lateral_dir1 = lateral_dir1 - src.lateral_dir1;
            out.lateral_dir2 = lateral_dir2 - src.lateral_dir2;
            out.local0 = local0 - src.local0;
            out.local1 = local1 - src.local1;
            out.world = world - src.world;
            return out;
        }

        inline void operator*=(const Ogre::Real& div) {
            impulse *= div;
            friction *= div;
            rolling_friction *= div;
            spinning_friction *= div;
            lateral_dir1 *= div;
            lateral_dir2 *= div;
            local0 *= div;
            local1 *= div;
            world *= div;
        }

    };

    typedef std::pair< size_t, size_t > PBCPkey;

    class PBulletContactPair {
    public:

        PBulletContactPair() :
        _UID(0, 0),
        _initialised(false),
        _first_time(true),
        _start(0),
        _last_update(0),
        _last_count(0),
        _count(0),
        _idled(false),
        _destroyed(false),
        _object0(0),
        _object1(0) {
        }

        inline void init(
                const unsigned long& ms,
                const PBCPkey& id,
                polymorph::PBulletRigidbodyUser* p0,
                polymorph::PBulletRigidbodyUser* p1,
                btManifoldPoint& manifold
                ) {
            _UID.first = id.first;
            _UID.second = id.second;
            _start = ms;
            _last_update = ms;
            _object0 = p0;
            _object1 = p1;
            _initialised = true;
            // in case of recycling
            _idled = false;
            _destroyed = false;
            add(ms, manifold);
        }

        inline void reset() {
            _last_count = _count;
            _count = 0;
            // keeping the last values in the pair if it is idled
            if (_last_count == 0 && _idled) {
                return;
            }
            _first_time = false;
            _last_values = _current_values;
            _current_values.reset();
            _delta_values.reset();
        }

        inline void add(const unsigned long& ms, btManifoldPoint& manifold) {
            ++_count;
            // waking up the pair
            if (_idled) {
                _idled = false;
            }
            _last_update = ms;
            _current_values += manifold;
        }

        inline void render(const unsigned long& ms) {
            // there is no new info for this pair since 2 frames
            if (_last_count + _count == 0) {
                return;
            }
                // no more contact for this pair, switching it to idle status
            else if (_last_count > 0 && _count == 0) {
                idle(ms);
                return;
            }
            // normal process
            _current_values *= (1.0 / _count);
            _delta_values = _last_values - _current_values;
        }

        inline void destroy(const unsigned long& ms) {
            _last_update = ms;
            _destroyed = true;
        }

        inline void idle(const unsigned long& ms) {
            _last_update = ms;
            _idled = true;
        }

        // GETTERS

        inline const PBCPkey& UID() const {
            return _UID;
        }

        inline const bool& initialised() const {
            return _initialised;
        }

        inline const bool& first_time() const {
            return _first_time;
        }

        inline const unsigned long& start() const {
            return _start;
        }

        inline const unsigned long& last_update() const {
            return _last_update;
        }

        inline const size_t& last_count() const {
            return _last_count;
        }

        inline const size_t& count() const {
            return _count;
        }

        inline const bool& idled() const {
            return _idled;
        }

        inline const bool& destroyed() const {
            return _destroyed;
        }

        inline const polymorph::PBulletRigidbodyUser* object0() const {
            return _object0;
        }

        inline const polymorph::PBulletRigidbodyUser* object1() const {
            return _object1;
        }

        inline const PBulletContactPairData& last_values() const {
            return _last_values;
        }

        inline const PBulletContactPairData& current_values() const {
            return _current_values;
        }

        inline const PBulletContactPairData& delta_values() const {
            return _delta_values;
        }

    protected:

        PBCPkey _UID;
        bool _initialised;
        bool _first_time;
        unsigned long _start;
        unsigned long _last_update;
        size_t _last_count;
        size_t _count;
        bool _idled;
        bool _destroyed;

        polymorph::PBulletRigidbodyUser* _object0;
        polymorph::PBulletRigidbodyUser* _object1;

        PBulletContactPairData _last_values;
        PBulletContactPairData _current_values;
        PBulletContactPairData _delta_values;


    };

    typedef std::map< PBCPkey, PBulletContactPair >
    PBCPmap;
    typedef std::map< PBCPkey, PBulletContactPair >::iterator
    PBCPiterator;

};


#endif /* POLYMORPH_BULLETCONTACT_H */

