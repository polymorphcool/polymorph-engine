/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
 */

/* 
 * File:   DoublePtr.h
 * Author: frankiezafe
 *
 * Created on December 7, 2016, 10:05 AM
 */

#ifndef POLYMORPH_DOUBLEPTR_H
#define POLYMORPH_DOUBLEPTR_H

#include <boost/multiprecision/cpp_int.hpp>

#include "PBulletTypes.h"

namespace polymorph {

    class DoublePtr {
    public:

        const boost::multiprecision::uint128_t& id;
        const void* ptrA;
        const void* ptrB;

        DoublePtr(const void* A, const void* B) :
        id(_id),
        ptrA(A),
        ptrB(B) {

            assert(A);
            assert(B);
            uint64_t uA = (uint64_t) A;
            uint64_t uB = (uint64_t) B;

            if (uA > uB) {
                uint64_t tmp = uA;
                uA = uB;
                uB = tmp;
            }

            _id = uB | (boost::multiprecision::uint128_t(uA) << 64);

        }

        DoublePtr(const DoublePtr& src) :
        id(_id),
        ptrA(src.ptrA),
        ptrB(src.ptrB)
        {
            _id = src.id;
        }

        void verify() {

            boost::multiprecision::uint128_t i(_id >> 64);
            uint64_t rA = i.convert_to< uint64_t >();
            i = _id;
            i = i << 64;
            i = i >> 64;
            uint64_t rB = i.convert_to< uint64_t >();
            std::cout << ptrA << " + " << ptrB << std::endl <<
                    rA << " + " << rB << std::endl <<
                    std::endl;

        }
        
        inline bool operator == ( const DoublePtr& src ) const {
            return _id == src.id;
        }

    private:

        boost::multiprecision::uint128_t _id;

    };

};

#endif /* POLYMORPH_DOUBLEPTR_H */

