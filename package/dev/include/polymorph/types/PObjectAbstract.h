/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2017 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
 */

/* 
 * File:   PObjectAbstract.h
 * Author: frankiezafe
 *
 * Created on February 22, 2017, 7:01 PM
 */

#ifndef POLYMORPH_OBJECTABSTRACT_H
#define POLYMORPH_OBJECTABSTRACT_H

#include "PCommon.h"
#include "PUtil.h"
#include "PBullet.h"

namespace polymorph {

    class PObjectAbstract {
    public:

        virtual ~PObjectAbstract() {
        };

        virtual bool generated() const = 0;

        virtual void visible(bool enable) = 0;

        virtual void debug(bool enable) = 0;

        virtual void clear(void) = 0;

        virtual bool parent(Ogre::SceneNode* ogrenode) = 0;

        virtual bool attach(PObjectAbstract* child, bool absolute) = 0;

        virtual bool detach() = 0;

        virtual bool detach(bool to_root) = 0;

        virtual void release(PObjectAbstract* child) = 0;

        virtual void project(PObjectAbstract* other) = 0;

        virtual void move(
                Ogre::Real x,
                Ogre::Real y,
                Ogre::Real z,
                bool internal = false) = 0;

        virtual void move(
                const Ogre::Vector3& p,
                bool internal = false) = 0;

        virtual void scale(
                Ogre::Real s,
                bool internal = false) = 0;

        virtual void scale(
                Ogre::Real x,
                Ogre::Real y,
                Ogre::Real z,
                bool internal = false) = 0;

        virtual void scale(
                const Ogre::Vector3& s,
                bool internal = false) = 0;

        virtual void orientation(
                const Ogre::Real& x,
                const Ogre::Real& y,
                const Ogre::Real& z,
                bool internal = false) = 0;

        virtual void orientation(
                const Ogre::Euler& o,
                bool internal = false) = 0;

        virtual void orientation(
                const Ogre::Quaternion& q,
                bool internal = false) = 0;

        virtual void pitch(
                const Ogre::Radian& angle,
                bool internal = false,
                TransformSpace space = polymorph::TS_LOCAL) = 0;

        virtual void yaw(
                const Ogre::Radian& angle,
                bool internal = false,
                TransformSpace space = polymorph::TS_LOCAL) = 0;

        virtual void roll(
                const Ogre::Radian& angle,
                bool internal = false,
                TransformSpace space = polymorph::TS_LOCAL) = 0;

        virtual void slerp(
                Ogre::Vector3 p,
                Ogre::Quaternion q,
                Ogre::Real pc) = 0;

        // bullet specific

        virtual bool physics(
                BT_Type type,
                std::string meshgroup = "",
                std::string meshname = "") = 0;

        virtual bool physics(
                BT_Type type,
                Ogre::Vector3 scale) = 0;

        virtual void density(float d) = 0;

        virtual void restless(bool enabled) = 0;

        virtual void bulletTransform(const btTransform& trans) = 0;

        virtual void mass(btScalar m) = 0;

        virtual void impulse(
                Ogre::Real x,
                Ogre::Real y,
                Ogre::Real z
                ) = 0;

        virtual void impulse(const Ogre::Vector3& dir) = 0;

        virtual void impulse(
                const Ogre::Vector3& dir,
                const Ogre::Vector3& offset
                ) = 0;

        virtual void springAdd(
                PObjectAbstract* other,
                PBulletSpringData* conf = 0
                ) = 0;

        virtual void springRegister(
                btGeneric6DofSpringConstraint* spring,
                PObjectAbstract* other,
                bool call_other = true
                ) = 0;

        virtual void springRemove(PObjectAbstract* other) = 0;


        virtual void springDelete(
                PObjectAbstract* other,
                bool call_other = true) = 0;

        // getters
        virtual Ogre::uint32 getUID() = 0;

        virtual Ogre::String getName() = 0;

        virtual PObjectAbstract* getParent() = 0;

        virtual Ogre::MovableObject* movable() = 0;

        virtual Ogre::SceneNode* origin() = 0;

        virtual Ogre::IdObject* idobject() = 0;

        // called by getObjectScale if internal is true
        virtual Ogre::Vector3 getScale(
                bool absolute = true,
                bool internal = false) = 0;

        // called by getObjectTrans if internal is true
        virtual Ogre::Vector3 getTrans(
                bool absolute = true,
                bool internal = false) = 0;

        // called by getObjectOrientation if internal is true
        virtual Ogre::Quaternion getOrientation(
                bool absolute = true,
                bool internal = false) = 0;

        virtual bool isPhysics() = 0;

        virtual bool isGhost() = 0;
        
        virtual bool isStatic() = 0;
        
        virtual bool isDynamic() = 0;
        
        virtual bool isNotDynamic() = 0;

        virtual float getDensity() = 0;

        virtual bool isVisible() = 0;

        virtual bool isDebug() = 0;

        virtual void showAxis(bool enabled) = 0;

        virtual bool isAxisVisible() const = 0;

        virtual bool isObject() const = 0;

        virtual Ogre::Vector3 getWorldTrans() = 0;

        virtual Ogre::Vector3 getWorldScale() = 0;

        virtual Ogre::Matrix4 getWorldMatrix() = 0;

        virtual Ogre::Vector3 getTranslationUpdated() = 0;
        
        virtual Ogre::Quaternion getOrientationUpdated() = 0;

        virtual PBulletExchange* getBulletExchange() = 0;
        
        virtual bool recursiveContainsPhysics() = 0;
        
        virtual bool recursiveContainsStatic() = 0;
        
        virtual bool recursiveContainsDynamic() = 0;
        
        virtual void syncBullet( bool skip_this = false ) = 0;
        
        virtual btRigidBody* getRigidBody() = 0;

    protected:

        virtual void local_position(Ogre::Vector3 v) = 0;

        virtual void local_scale(Ogre::Vector3 v) = 0;

        virtual void local_orientation(Ogre::Quaternion q) = 0;

        virtual void local_pitch(
                const Ogre::Radian& angle,
                TransformSpace space = polymorph::TS_LOCAL) = 0;

        virtual void local_yaw(
                const Ogre::Radian& angle,
                TransformSpace space = polymorph::TS_LOCAL) = 0;

        virtual void local_roll(
                const Ogre::Radian& angle,
                TransformSpace space = polymorph::TS_LOCAL) = 0;

        // called by getOrientation( *, true)
        virtual Ogre::Quaternion getObjectOrientation(
                bool absolute = true) = 0;

        // called by getTrans( *, true)
        virtual Ogre::Vector3 getObjectTrans(
                bool absolute = true) = 0;

        // called by getScale( *, true)
        virtual Ogre::Vector3 getObjectScale(
                bool absolute = true) = 0;
        
        // physics / non-physics setters for rotation & translation
        virtual void move(
                const Ogre::Vector3& p,
                bool internal,
                bool externalCall) = 0;

        virtual void scale(
                const Ogre::Vector3& s,
                bool internal,
                bool externalCall) = 0;
        
        virtual void orientation(
                const Ogre::Quaternion& q,
                bool internal,
                bool externalCall) = 0;
        
    };

    typedef std::vector< PObjectAbstract* > PObjectList;
    typedef std::vector< PObjectAbstract* >::iterator PObjectListIter;
    typedef std::map< std::string, PObjectAbstract* > PObjectMap;
    typedef std::map< std::string, PObjectAbstract* >::iterator PObjectMapIter;

    typedef std::map<
    PObjectAbstract*,
    btGeneric6DofSpringConstraint*
    > SpringMap;

    typedef std::map<
    PObjectAbstract*,
    btGeneric6DofSpringConstraint*
    >::iterator SpringMapIter;

};

#endif /* POLYMORPH_OBJECTABSTRACT_H */

