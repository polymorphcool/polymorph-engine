/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2017 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
*/

/* 
 * File:   PRigidBody.h
 * Author: frankiezafe
 *
 * Created on May 6, 2017, 12:47 PM
 */

#ifndef POLYMORPH_RIGIDBODY_H
#define POLYMORPH_RIGIDBODY_H

#include "PBulletTypes.h"

namespace polymorph {

    class PRigidBody {
    public:

        // physics
        BT_Type type;
        std::string group;
        std::string file;
        bool restless;
        btScalar density;
        btScalar friction;
        btScalar rolling_friction;
        btScalar spinning_friction;
        btScalar restitution;

        btVector3 inertia;
        btScalar linear_damping;
        btScalar angular_damping;
        bool additional_damping;
        bool custom_gravity;
        btVector3 gravity;
        btVector3 init_linear_impulse;
        btVector3 init_angular_impulse;

        PRigidBody() :
        type(BT_NONE),
        restless(false),
        density(1),
        custom_gravity(false),
        gravity(0, 0, 0),
        init_linear_impulse(0, 0, 0),
        init_angular_impulse(0, 0, 0) {
            btBoxShape cs(btVector3(1, 1, 1));
            btDefaultMotionState ms;
            btRigidBody::btRigidBodyConstructionInfo i(1, &ms, &cs);
            friction = i.m_friction;
            rolling_friction = i.m_rollingFriction;
            spinning_friction = i.m_spinningFriction;
            restitution = i.m_restitution;
            inertia = i.m_localInertia;
            linear_damping = i.m_linearDamping;
            angular_damping = i.m_angularDamping;
            additional_damping = i.m_additionalDamping;
        }
        
        void clear() {
            btVector3 ZERO( 0,0,0 );
            type = BT_NONE;
            restless = false;
            density = 1;
            custom_gravity = false;
            gravity = ZERO;
            init_linear_impulse = ZERO;
            init_angular_impulse = ZERO;
            btBoxShape cs(btVector3(1, 1, 1));
            btDefaultMotionState ms;
            btRigidBody::btRigidBodyConstructionInfo i(1, &ms, &cs);
            friction = i.m_friction;
            rolling_friction = i.m_rollingFriction;
            spinning_friction = i.m_spinningFriction;
            restitution = i.m_restitution;
            inertia = i.m_localInertia;
            linear_damping = i.m_linearDamping;
            angular_damping = i.m_angularDamping;
            additional_damping = i.m_additionalDamping;
        }

        PRigidBody(const PRigidBody& src) {
            (*this) = src;
        }

        inline void operator=(const PRigidBody& src) {
            type = src.type;
            group = src.group;
            file = src.file;
            restless = src.restless;
            density = src.density;
            friction = src.friction;
            rolling_friction = src.rolling_friction;
            spinning_friction = src.spinning_friction;
            restitution = src.restitution;
            inertia = src.inertia;
            linear_damping = src.linear_damping;
            angular_damping = src.angular_damping;
            additional_damping = src.additional_damping;
            custom_gravity = src.custom_gravity;
            gravity = src.gravity;
            init_linear_impulse = src.init_linear_impulse;
            init_angular_impulse = src.init_angular_impulse;
        }

    };

};

#endif /* POLYMORPH_RIGIDBODY_H */

