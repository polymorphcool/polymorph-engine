/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2017 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
 */

/* 
 * File:   PApplicationConfiguration.h
 * Author: frankiezafe
 *
 * Created on October 4, 2017, 2:43 PM
 */

#include "Ogre.h"

#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE

#include <OIS/OISEvents.h>
#include <OIS/OISInputManager.h>
#include <OIS/OISKeyboard.h>
#include <OIS/OISMouse.h>
#include <OGRE/SdkTrays.h>

#elif OGRE_PLATFORM == OGRE_PLATFORM_ANDROID

#include <android_native_app_glue.h>
#include "Android/OgreAPKFileSystemArchive.h"
#include "Android/OgreAPKZipArchive.h"
#include "OISEvents.h"
#include "OISInputManager.h"
#include "OISKeyboard.h"
#include "OISMouse.h"
#include "SdkTrays.h"

#else

#include "OISEvents.h"
#include "OISInputManager.h"
#include "OISKeyboard.h"
#include "OISMouse.h"
#include "SdkTrays.h"

#endif

#ifndef POLYMORPH_APPLICATION_CONFIGURATION_H
#define POLYMORPH_APPLICATION_CONFIGURATION_H

namespace polymorph {

    class PApplicationConfiguration {
    public:

        /* Enabling tracking of the mouse (if true). 
         * @default
         *      true
         */
        bool input_mouse_enable;

        /* Linking mouse callbacks to current class (if true). 
         * @default
         *      true
         */
        bool input_mouse_callbacks;

        /* Do not allow mouse to leave current window (if true). 
         * @default
         *      false
         */
        bool input_mouse_capture;

        /* Hide the cursor (if true).
         * @default
         *      true
         */
        bool input_mouse_hide;

        /* Enabling tracking of the keyboard (if true).
         * @default
         *      true
         */
        bool input_keyboard_enable;

        /* Linking keyboard callbacks to current class (if true).
         * @default
         *      true
         */
        bool input_keyboard_callbacks;

        /* Utility class to configure a PolymorphApplication.
         * Enable and/or disable desired features in the bases class
         * by accessing its PApplicationConfiguration. See
         * PApplicationConfiguration.cpp for usage.
         * By default, mouse and keyboard inputs are enabled, and
         * mouse is not captive.
         */
        PApplicationConfiguration() :
        input_mouse_enable(true),
        input_mouse_callbacks(true),
        input_mouse_capture(false),
        input_mouse_hide(true),
        input_keyboard_enable(true),
        input_keyboard_callbacks(true) {
        }

        PApplicationConfiguration(const PApplicationConfiguration& src) {
            (*this) = src;
        }

        inline void operator=(const PApplicationConfiguration& src) {
            input_mouse_enable = src.input_mouse_enable;
            input_mouse_callbacks = src.input_mouse_callbacks;
            input_mouse_capture = src.input_mouse_capture;
            input_mouse_hide = src.input_mouse_hide;
            input_keyboard_enable = src.input_keyboard_enable;
            input_keyboard_callbacks = src.input_keyboard_callbacks;
        }

        void addParams(OIS::ParamList& pl) {

#if defined OIS_WIN32_PLATFORM

            std::cout << "!!! PApplicationConfiguration::addParams"
                    << " FINISH CONFIGURATION FOR WINDOWS!"
                    << std::endl;
            pl.insert(std::make_pair(std::string("w32_mouse"), std::string("DISCL_FOREGROUND")));
            pl.insert(std::make_pair(std::string("w32_mouse"), std::string("DISCL_NONEXCLUSIVE")));
            pl.insert(std::make_pair(std::string("w32_keyboard"), std::string("DISCL_FOREGROUND")));
            pl.insert(std::make_pair(std::string("w32_keyboard"), std::string("DISCL_NONEXCLUSIVE")));

#elif defined OIS_LINUX_PLATFORM

            if (input_mouse_enable) {
                if (input_mouse_capture) {
                    pl.insert(
                            std::make_pair(std::string("x11_mouse_grab"),
                            std::string("true")));
                } else {
                    pl.insert(std::make_pair(
                            std::string("x11_mouse_grab"),
                            std::string("false")));
                }
                if (input_mouse_hide) {
                    pl.insert(std::make_pair(
                            std::string("x11_mouse_hide"),
                            std::string("true")));
                }
            }
            if (input_keyboard_enable) {
                pl.insert(std::make_pair(
                        std::string("x11_keyboard_grab"),
                        std::string("false")));

            }
            pl.insert(std::make_pair(
                    std::string("XAutoRepeatOn"),
                    std::string("true")));

#endif
        }

    };

};

#endif /* POLYMORPH_APPLICATION_CONFIGURATION_H */

