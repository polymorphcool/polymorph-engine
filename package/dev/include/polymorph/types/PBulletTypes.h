/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
 */

/* 
 * File:   PBulletExchange.h
 * Author: frankiezafe
 *
 * Created on October 11, 2016, 9:02 PM
 */

#ifndef POLYMORPH_BULLETTYPES_H
#define POLYMORPH_BULLETTYPES_H

#include "Ogre.h"
#include "btBulletCollisionCommon.h"
#include "btBulletDynamicsCommon.h"
#include "CollisionDispatch/btGhostObject.h"
#include "Gimpact/btGImpactShape.h"
#include "Gimpact/btGImpactCollisionAlgorithm.h"
#include "GIMPACTUtils/btGImpactConvexDecompositionShape.h"

namespace polymorph {

    enum BT_CollisionType {
        BTCT_UNDEFINED = -1,
        BTCT_IMPACT = 0,
        BTCT_CONTACT = 1,
    };

    enum BT_CollisionStatus {
        BTCS_UNDEFINED = -1,
        BTCS_START = 0,
        BTCS_UPDATE = 1,
        BTCS_MODIFIED = 2,
        BTCS_END = 3
    };

    enum BT_Type {
        BT_NONE =               0,
        BT_DYNAMIC_BOX =        1,
        BT_DYNAMIC_SPHERE =     2,
        BT_DYNAMIC_COMPOUND =   3, // provide a compound mesh!
        BT_DYNAMIC_CONVEX =     4, // provide a mesh!
        BT_DYNAMIC_CONCAVE =    5, // provide a mesh!
        // statics
        BT_STATIC_BOX =         6,
        BT_STATIC_SPHERE =      7,
        BT_STATIC_COMPOUND =    8, // provide a mesh!
        BT_STATIC_CONVEX =      9, // provide a mesh!
        BT_STATIC_CONCAVE =     10, // provide a mesh!
        // just detect collision, no force
        BT_GHOST_BOX =          11,
        BT_GHOST_SPHERE =       12,
        BT_GHOST_COMPOUND =     13, // provide a mesh!
        BT_GHOST_CONVEX =       14, // provide a mesh!
        BT_GHOST_CONCAVE =      15 // provide a mesh!
    };

};

#endif /* POLYMORPH_BULLETTYPES_H */

