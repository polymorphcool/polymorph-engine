/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
*/

/* 
 * File:   PCamRig.h
 * Author: frankiezafe
 *
 * Created on October 11, 2016, 7:32 PM
 */

#ifndef PCAMRIG_H
#define PCAMRIG_H

#include "PCommon.h"
#include "PUtil.h"
#include "PCamRigData.h"

namespace polymorph {

    enum PCamRigStatus {
        CRS_IDLE =          -1,
        CRS_FREE =          0,
        CRS_TRANSITION =    1
    };
    
    class PCamRig {

    public:

        PCamRig();
        virtual ~PCamRig();

        inline void configure( const PCamRigData& src ) { 
            conf = src;
        }
        
        void fit( Ogre::RenderWindow * rw );
        void fit( const Ogre::Vector2& window_size );
        void init( Ogre::SceneManager* sm, Ogre::Camera * cam = 0 );
        
//        void frame( input::GameControllerFrame* gcf, Ogre::FrameEvent& evt );
        void update( void );
        
        void move( Ogre::Vector3 p, bool campivot = false );
        
        void debug( const bool& enable );
        
        // rotations
        void pitch( Ogre::Real a, bool campivot = false );
        void yaw( Ogre::Real a, bool campivot = false );
        void roll( Ogre::Real a, bool campivot = false );
        void pitchRange( Ogre::Real pc, bool campivot = false );
        void yawRange( Ogre::Real pc, bool campivot = false );
        void rollRange( Ogre::Real pc, bool campivot = false );
        
        // getters
        inline Ogre::Real getPitch( bool pivot = false ) {
            if ( pivot ) return pivot_eulers.x;
            else return origin_eulers.x;
        }
        inline Ogre::Real getYaw( bool pivot = false ) {
            if ( pivot ) return pivot_eulers.y;
            else return origin_eulers.y;
        }
        inline Ogre::Real getRoll( bool pivot = false ) {
            if ( pivot ) return pivot_eulers.z;
            else return origin_eulers.z;
        }
        
        inline const Ogre::Matrix4& getCamMat() { return cam_mat; }
        
        inline const Ogre::Matrix4& getCamMatI() { return cam_mat_inverse; }
        
        inline const Ogre::Matrix4& getWorldToOrigin() { return world2origin_orientation; }
        
        inline Ogre::Camera* getCam() { return _cam; }
        
        inline Ogre::SceneNode* getOrigin() { return _origin; }
        inline Ogre::SceneNode* getPivot() { return _pivot; }
        
        inline bool isTransiting() { return status == CRS_TRANSITION; }
        
        inline PCamRigStatus getStatus() { return status; }
        
        inline PCamRigStatus getFallbackStatus() { return fallback_status; }
        
        inline std::string getStringStatus() {
            switch( status ) {
                case CRS_IDLE:
                    return "idle";
                case CRS_FREE:
                    return "free";
                case CRS_TRANSITION:
                    return "transition";
                default:
                    return "undefined";
            }
        }
        inline std::string getStringFallbackStatus() {
            switch( fallback_status ) {
                case CRS_IDLE:
                    return "idle";
                case CRS_FREE:
                    return "free";
                case CRS_TRANSITION:
                    return "transition";
                default:
                    return "undefined";
            }
        }
        
    protected:
        
        PCamRigData conf;
        PCamRigStatus status;
        PCamRigStatus fallback_status;
        
        Ogre::SceneManager* _sm;
        Ogre::Camera*       _cam;
        Ogre::SceneNode*    _origin;
        Ogre::SceneNode*    _pivot;
        
        Ogre::Matrix4 world2origin_orientation;
        Ogre::Matrix4 cam_mat;
        Ogre::Matrix4 cam_mat_inverse;
        
        Ogre::Vector3 origin_eulers;
        Ogre::Vector3 pivot_eulers;
        
        // origin related
        Ogre::Real origin_move_duration;
        Ogre::Real origin_move_time;
        Ogre::Vector3 ori_pos_previous;
        Ogre::Vector3 ori_pos_current;
        Ogre::Vector3 ori_pos_target;
        Ogre::Quaternion ori_dir_previous;
        Ogre::Quaternion ori_dir_current;
        Ogre::Quaternion ori_dir_target;
        
        // cam related
        Ogre::Real cam_dist_duration;
        Ogre::Real cam_dist_time;
        Ogre::Real cam_dist_previous;
        Ogre::Real cam_dist_target;
        
        Ogre::Vector3       _axis_x;
        Ogre::Vector3       _axis_y;
        Ogre::Vector3       _axis_z;
        Ogre::SceneNode*    _node_x;
        Ogre::SceneNode*    _node_y;
        Ogre::SceneNode*    _node_z;
        Ogre::SceneNode*    _ball_x;
        Ogre::SceneNode*    _ball_y;
        Ogre::SceneNode*    _ball_z;
        
        void move( Ogre::Vector3 p, bool local, bool externalCall );

        void adaptfov();
        
        void orient( Ogre::Real a, bool x, bool y, bool z, bool campivot );
        
        inline Ogre::Real sinInterpolation( Ogre::Real duration, Ogre::Real time ) {
            return ( 0.5 + Ogre::Math::Sin( -Ogre::Math::HALF_PI + Ogre::Math::PI * ( 1 - ( time / duration ) ) ) * 0.5 );
        }
        
        void build3DAxis( void );
        void updateWorldToOrign( void );
        void update3DAxis( void );
        
    };

};

#endif /* PCAMRIG_H */

