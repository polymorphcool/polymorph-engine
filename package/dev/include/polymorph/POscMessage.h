/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2017 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
 */

/* 
 * File:   POscMessage.h
 * Author: frankiezafe
 *
 * Created on October 15, 2017, 5:41 PM
 */

#ifndef POLYMORPH_OSCMESSAGE_H
#define POLYMORPH_OSCMESSAGE_H

#include "Ogre.h"
#include "OscOutboundPacketStream.h"
#include "PCommon.h"
#include "POscData.h"

namespace polymorph {
    
    /* Class managing OSC message.
     */
    class POscMessage {
    public:

        POscMessage();

        POscMessage(const POscMessage& orig);

        virtual ~POscMessage();
        
        /* Some implementation of the OSC protocol do not stands more than 
         * 32bits numbers. This methods allows messages to be compatible with
         * these parsers. Limitation is disabled by default.
         */
        inline void enable32bits() {
            max_32bits = true;
        }

        /* Disabling 32bits numbers limitation in sent messages. 
         */
        inline void disable32bits() {
            max_32bits = false;
        }

        /* Prepare the message. Once called, use add() methods to append data
         * sequentially in the message.
         * @param
         *      a string starting the message, "/data/frame" for instance.
         * @important
         *      do not forget to call pack() before sending message!
         */
        void startMessage(const std::string& marker);

        /* Add a char array to message.
         * @param
         *      a reference to char array value
         * @remark
         *      will have no effect if startMessage() has not been called
         */
        void add(char* cs);

        /* Add a std::string to message.
         * @param
         *      a reference to std::string value
         * @remark
         *      will have no effect if startMessage() has not been called
         */
        void add(const std::string& str);

        /* Add an 8 bits integer to message (~char).
         * @param
         *      a reference to int8_t value
         * @remark
         *      will have no effect if startMessage() has not been called
         */
        void add(const int8_t& i);

        /* Add an unsigned 8 bits integer to message (~uchar).
         * @param
         *      a reference to uint8_t value
         * @remark
         *      will have no effect if startMessage() has not been called
         */
        void add(const uint8_t& i);

        /* Add a 32 bits integer to message (standard integer).
         * @param
         *      a reference to int32_t value
         * @remark
         *      will have no effect if startMessage() has not been called
         */
        void add(const int32_t& i);

        /* Add an unsigned 32 bits integer to message.
         * @param
         *      a reference to uint32_t value
         * @remark
         *      will have no effect if startMessage() has not been called
         */
        void add(const uint32_t& i);

        /* Add a 64 bits integer to message.
         * @param
         *      a reference to int64_t value
         * @remark
         *      will have no effect if startMessage() has not been called
         * @remark
         *      part of the specification but not implemented in all libraries
         */
        void add(const int64_t& i);

        /* Add an unsigned 64 bits integer to message.
         * @param
         *      a reference to uint64_t value
         * @remark
         *      will have no effect if startMessage() has not been called
         * @remark
         *      part of the specification but not implemented in all libraries
         */
        void add(const uint64_t& i);

        /* Add a float (32 bits) to message.
         * @param
         *      a reference to float value
         * @remark
         *      will have no effect if startMessage() has not been called
         */
        void add(const float& f);

        /* Add a double (64 bits) to message.
         * @param
         *      a reference to double value
         * @remark
         *      will have no effect if startMessage() has not been called
         * @remark
         *      part of the specification but not implemented in all libraries
         */
        void add(const double& d);

        /* Add a boolean to message. Note that no data value is added.
         * Boolean only appears in the Type of messages: char 'T' for true, 
         * char 'F' for false.
         * @param
         *      a reference to boolean value
         * @remark
         *      will have no effect if startMessage() has not been called
         * @remark
         *      part of the specification but not implemented in all libraries
         */
        void add(const bool& b);

        /* Helper to encode Ogre::Vector2. Appends 2 Ogre::Real to the message,
         * float by default.
         * @param
         *      a reference to Ogre::Vector2 value
         * @remark
         *      will have no effect if startMessage() has not been called
         */
        void add(const Ogre::Vector2& v);

        /* Helper to encode Ogre::Vector3. Appends 3 Ogre::Real to the message,
         * float by default.
         * @param
         *      a reference to Ogre::Vector3 value
         * @remark
         *      will have no effect if startMessage() has not been called
         */
        void add(const Ogre::Vector3& v);

        /* Helper to encode Ogre::Vector4. Appends 4 Ogre::Real to the message,
         * float by default.
         * @param
         *      a reference to Ogre::Vector4 value
         * @remark
         *      will have no effect if startMessage() has not been called
         */
        void add(const Ogre::Vector4& v);

        /* Helper to encode Ogre::Quaternion. Appends 4 Ogre::Real to the 
         * message float by default.
         * @param
         *      a reference to Ogre::Quaternion value
         * @remark
         *      will have no effect if startMessage() has not been called
         */
        void add(const Ogre::Quaternion& q);

        /* Helper to encode Ogre::Matrix3. Appends 9 Ogre::Real to the 
         * message float by default.
         * @param
         *      a reference to Ogre::Matrix3 value
         * @remark
         *      will have no effect if startMessage() has not been called
         */
        void add(const Ogre::Matrix3& m);

        /* Helper to encode Ogre::Matrix4. Appends 16 Ogre::Real to the 
         * message float by default.
         * @param
         *      a reference to Ogre::Matrix4 value
         * @remark
         *      will have no effect if startMessage() has not been called
         */
        void add(const Ogre::Matrix4& m);

        /* Helper to encode Ogre::Euler. Appends 4 Ogre::Real to the 
         * message float by default.
         * @param
         *      a reference to Ogre::Euler value
         * @remark
         *      will have no effect if startMessage() has not been called
         */
        void add(const Ogre::Euler& e);
        
        /* Finish the message and make it ready to be sent.
         * Message will be refused by POscSender if it has not been packed.
         */
        void packMessage();

        /* Delete the current content of the message. Use startMessage() to 
         * prepare a new message in this object.
         */
        void purgeMessage();

        // getters

        /* Check if the message is ready to be sent. Minimum is to 
         * startMessage() and packMessage() the message to have it ready.
         * @return
         *      true if it is empty or not set
         */
        inline bool ready() {
            if (!packed) {
                return false;
            }
            return true;
        }

        /* Returns the content of the message like a char array, with Address
         * and Types.
         * @return
         *      char array
         */
        inline const char * data() const {
            if (!packet) {
                return 0;
            }
            return packet->Data();
        }
        
        /* Returns the length of the message.
         * @return
         *      size_t
         */
        inline std::size_t size() const {
            if (!packet) {
                return 0;
            }
            return packet->Size();
        }

    protected:

        osc::OutboundPacketStream* packet;
        bool packed;
        bool max_32bits;

    };

};

#endif /* POLYMORPH_OSCMESSAGE_H */

