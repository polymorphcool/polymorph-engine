/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
*/

/* 
 * File:   OgreReceiver.h
 * Author: frankiezafe
 *
 * Created on September 16, 2016, 10:31 AM
 */

#ifndef OGRERECEIVER_H
#define OGRERECEIVER_H

#include <boost/thread/mutex.hpp>
#include "Ogre.h"

#if defined(__BORLANDC__) // workaround for BCB4 release build intrinsics bug
namespace std {
using ::__strcmp__;  // avoid error: E2316 '__strcmp__' is not a member of 'std'.
}
#endif

#include "OscPacketListener.h"
#include "UdpSocket.h"
#include "PCommon.h"
#include "POscArgPair.h"
#include "POscMessageData.h"
#include "POscData.h"

#include <iostream>
#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>

#define MAX_QUEUE_SIZE  30

namespace polymorph {
    
    class POscReceiver : public osc::OscPacketListener {
    
    public:
        
        POscReceiver( );
        POscReceiver(POscReceiverData& data);
        ~POscReceiver( );
        
        void configure(POscReceiverData& data);
        void setup( uint port );
        bool start();
        void stop();
        inline bool runs() { return _runs; }
        
        // asynchronous read
        bool hasPending();
        PMessageData* getNext();
        
        // setters
        void setMaximumQueueSize( uint max_queue );
    
    protected:
        
        void runThread();
        
    private:
        
        UdpListeningReceiveSocket* _socket;
        uint port;
        uint buffer_capacity;
        bool autostart;
        
        // writing head position
        uint write_index;
        uint write_last_index;
        uint read_index;
        uint read_last_index;
        uint read_start_index;
        
        // reading head position
        PMessageData* buffer01;
        PMessageData* buffer02;
        
        bool written;
        bool looped;
        PMessageData* write_buffer;
        PMessageData* read_buffer;
        PMessageData* read_msg;

        void openSocket();
        
        // overload
        virtual void ProcessMessage( const osc::ReceivedMessage& m, const IpEndpointName& remoteEndpoint );
        
        bool _runs;
        boost::shared_ptr<boost::thread> bthread;
        boost::mutex write_mutex;
        boost::mutex read_mutex;
        
    };
    
    typedef std::vector< polymorph::POscReceiver* > POscRcvrList;
    typedef std::vector< polymorph::POscReceiver* >::iterator POscRcvrListIter;

};

#endif /* OGRERECEIVER_H */

