/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
*/

/* 
 * File:   PObjectManager.h
 * Author: frankiezafe
 *
 * Created on November 25, 2016, 2:43 PM
 */

#ifndef POLYMORPH_OBJECTMANAGER_H
#define POLYMORPH_OBJECTMANAGER_H

//#include "PdEngine.h"
//#include "PSound.h"
#include "PBullet.h"
#include "PXmlLoader.h"
#include "PNode.h"
#include "PLight.h"

namespace polymorph {

    class PObjectManager : public PXmlLoader {
    public:
        
        PObjectManager();
        
        virtual ~PObjectManager();
        
        virtual bool parse();
        
        virtual bool parse( Ogre::String group, Ogre::String xml );
        
        virtual void clear();
        
        virtual bool setSceneManager( 
            Ogre::SceneManager* sm,
            bool force = false );
        
//        bool setPdEngine( 
//            libpd::PdEngine* pe,
//            bool force = false );
        
        virtual bool setBulletEngine( 
            PBullet* be,
            bool force = false );
        
        virtual bool destroyScene();
        
        virtual bool loadScene( 
            uint32_t& UID,
            bool force = false);
        
        virtual bool loadScene( 
            PSceneData* psd,
            bool force = false);
        
        virtual bool nextScene();
        
        virtual bool loadNode( 
            uint32_t& sceneUID, 
            std::string name, 
            bool force = false );
        
        virtual bool loadNode( 
            PNodeData* d, 
            bool force = false );
        
        virtual bool loadLight( 
            uint32_t& sceneUID, 
            std::string name, 
            bool force = false );
    
        virtual bool loadLight( 
            PLightData* d, 
            bool force = false );
        
//        bool loadSound( 
//            uint32_t& sceneUID, 
//            std::string name, 
//            bool force = false );
    
//        bool loadSound( 
//            PSoundData* d, 
//            bool force = false );
        
        // temp
//        void setupPdEngine( libpd::PdEngine* pde, std::string groupname = "" );
        
        // getters
        inline uint32_t getObjectCount() { return objects.size(); }
        
        inline uint32_t getNodeCount() { return nodes.size(); }
        
        inline uint32_t getLightCount() { return lights.size(); }
        
        inline bool isSceneManager() { return ( scene_manager != 0 ); }
        
//        inline bool isPdEngine() { return ( pd_engine != 0 ); }
        
        inline bool isBulletEngine() { return ( bullet_engine != 0 ); }
        
        inline bool isLoadedScene() { return ( loaded_scene != 0 ); }
        
        inline int32_t getSceneID() {
            if ( !loaded_scene ) return -1;
            return loaded_scene->UID;
        }
        
        PNode* node( std::string name );
        
        PNode* node( uint32_t i );
        
        PLight* light( std::string name );
        
        PLight* light( uint32_t i );
        
//        PSound* sound( std::string name );
//        PSound* sound( uint32_t i );
    
    protected:
        
        // ogre scene manager
        Ogre::SceneManager* scene_manager;
        // puredata engine
//        libpd::PdEngine* pd_engine;
        // bullet engine
        PBullet* bullet_engine;

        PSceneData* loaded_scene;
        
        PObjectList objects;
        PNodeList nodes;
        PLightList lights;
//        PSoundList sounds;
        
        PObjectMap omap;
        PNodeMap nmap;
        PLightMap lmap;
//        PSoundMap smap;
        
        size_t scenenum;
        uint32_t* sceneids;
        
    };
};

#endif /* POLYMORPH_OBJECTMANAGER_H */

