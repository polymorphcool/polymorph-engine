/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2017 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
*/

/* 
 * File:   PatchListener.h
 * Author: frankiezafe
 *
 * Created on April 6, 2017, 11:44 AM
 */

#ifndef PATCHLISTENER_H
#define PATCHLISTENER_H

#include "Ogre.h"
#include "PdBase.hpp"

namespace polymorph {

    class PatchEventListerner {
    public:

        virtual void patchLoaded( pd::Patch* p ) = 0;

        virtual void patchUnloaded( pd::Patch* p ) = 0;

        virtual void patchAllUnloaded( ) = 0;

    };

    typedef std::vector<PatchEventListerner*> PPLList;
    typedef std::vector<PatchEventListerner*>::iterator PPLListIter;

    class PatchEventBroadcaster {
    public:

        virtual bool containsPatchListener( PatchEventListerner* l ) {
            return std::find(
                    _pe_listeners.begin( ),
                    _pe_listeners.end( ),
                    l
                    ) != _pe_listeners.end( );
        }

        virtual void addPatchListener( PatchEventListerner* l ) {

            if ( containsPatchListener( l ) ) {
                return;
            }
            _pe_listeners.push_back( l );


        }

        virtual void removePatchListener( PatchEventListerner* l ) {

            PPLListIter it = std::find(
                    _pe_listeners.begin( ),
                    _pe_listeners.end( ),
                    l
                    );
            if ( it != _pe_listeners.end( ) ) {
                _pe_listeners.erase( it );
            }

        }

        virtual void patchLoaded( pd::Patch* p ) {

            PPLListIter it = _pe_listeners.begin( );
            PPLListIter ite = _pe_listeners.end( );
            for (; it != ite; ++it ) {
                ( *it )->patchLoaded( p );
            }

        }

        virtual void patchUnloaded( pd::Patch* p ) {

            PPLListIter it = _pe_listeners.begin( );
            PPLListIter ite = _pe_listeners.end( );
            for (; it != ite; ++it ) {
                ( *it )->patchUnloaded( p );
            }

        }

        virtual void patchAllUnloaded( ) {

            PPLListIter it = _pe_listeners.begin( );
            PPLListIter ite = _pe_listeners.end( );
            for (; it != ite; ++it ) {
                ( *it )->patchAllUnloaded( );
            }

        }

    protected:

        PPLList _pe_listeners;

    };

};

#endif /* PATCHLISTENER_H */

