/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2017 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
 */

/* 
 * File:   PPatch.h
 * Author: frankiezafe
 *
 * Created on April 5, 2017, 5:15 PM
 */

#ifndef PPATCH_H
#define PPATCH_H

#include "Ogre.h"
#include "PdBase.hpp"
#include "Puredata.h"
#include "PUtil.h"

namespace polymorph {

    class PPatch : public PatchEventListerner {
    public:

        PPatch( );
        virtual ~PPatch( );

        bool load(
                   Puredata* pd,
                   std::string resource_group,
                   std::string resource_name
                   );

        bool load( Puredata* pd, std::string path = "" );

        bool unload( );

        inline uint16_t uid( ) const {
            return _uid;
        }

        inline bool loaded( ) const {
            return _patch != 0;
        }

        inline pd::Patch* patch( ) const {
            return _patch;
        }

        // feedback from pd
        void patchLoaded( pd::Patch* p );

        void patchUnloaded( pd::Patch* p );

        void patchAllUnloaded( );
        
        std::string prefix( std::string s ) const {
            std::stringstream ss;
            ss << _uid << s;
            return ss.str();
        }
        
        std::string suffix( std::string s ) const {
            std::stringstream ss;
            ss << s << _uid;
            return ss.str();
        }

    private:

        uint16_t _uid;
        std::string _group;
        std::string _path;
        pd::Patch* _patch;
        Puredata* _pd;

    };

    typedef std::vector< PPatch* > PatchList;
    typedef std::vector< PPatch* >::iterator PatchListIter;

};


#endif /* PPATCH_H */

