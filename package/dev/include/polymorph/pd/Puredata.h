/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2017 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
*/

/* 
 * File:   Puredata.h
 * Author: frankiezafe
 *
 * Created on April 5, 2017, 4:43 PM
 */

#ifndef PUREDATA_H
#define PUREDATA_H

#include <iostream>
#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/random/linear_congruential.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/generator_iterator.hpp>

#if OGRE_PLATFORM == OGRE_PLATFORM_LINUX
#define PD_SDL_LINUX
#include "SDL.h"
#include "SDL_audio.h"
#endif

#include "PdBase.hpp"
#include "PUtil.h"

/// polymorph pd
#include "PuredataInOut.h"
#include "PatchListener.h"

/// serialisation
#include  "PDData.h"

/// edition
#include "POscReceiver.h"
#include "POscSender.h"

namespace polymorph {

    typedef std::vector<pd::Patch*> pdPList;
    typedef std::vector<pd::Patch*>::iterator pdPListIter;

    class Puredata :
    public PuredataInOut,
    public PatchEventBroadcaster {
    public:

        /** Forward the SDL audio device callback to object's instance
         */
        static void forwardCallback( void *userdata, Uint8 *stream, int len ) {
            static_cast < Puredata* > ( userdata )->audioCallback( stream, len );
        }

        Puredata( );

        virtual ~Puredata( );

        /** Configuration of puredata and SDL before initialisation via PDData
         @param PDData reference, containing the complete configuration
         */
        bool configure( PDData& data );

        /** Configuration of puredata and SDL before initialisation
         @param number of channels, 2 by default
         @param output sample rate, 44100 by default
         @param audio buffer size in samples (power of 2), 128 by default
         @param audio_driver, linux: pulseaudio by default
         */
        bool configure(
                        int output_cannels, int sample_rate,
                        int samples, std::string audio_driver );

        /** Add search folders to puredata, to be done before calling init()
         @param a relative path, such as ../media/etc
         */
        inline void addSearchPath( std::string path ) {
            _search_paths.push_back( path );
        }

        /** Clear all registered search path
         */
        inline void cleanSearchPath( std::string path ) {
            _search_paths.clear( );
        }

        /** Enable debug messages of all kinds
         @param true to turn it on
         */
        inline void verbose( bool v ) {
            _verbosed = v;
        }

        /** Getter, returns true if puredata is running
         */
        inline bool runs( ) const {
            return _runs;
        }

        /** Initialise puredata and SDL audio device.
         * Call configure before init if you need to modify default settings.
         */
        bool init( );

        /** Start the reception thread if puredata is initialised successfully
         */
        bool start( );

        /** Disable or enable the audio device callback
         @param true or false
         */
        void mute( bool m );

        /** Stop the reception thread and close the audio device
         */
        void stop( );

        // subscription to pd messages, delegate
        bool subscribe( const std::string& source );

        bool unsubscribe( const std::string& source );

        bool unsubscribeAll( );

        /** Load a puredata patch, and unload the previous one
         @param relative or absolute path to pd patch
         @remark do not use this unless you know what you're doing! The
         standard way is to load and unload patches trough PPatch objects.
         */
        pd::Patch* loadPatch( std::string patch_path );

        /** Close the current patch
         @remark do not use this unless you know what you're doing! The
         standard way is to load and unload patches trough PPatch objects.
         */
        void unloadPatch( pd::Patch* p );

        bool containsPatch( pd::Patch* p );

    private:

        // configuration backup
        PDData _configuration;
        
        // thread related
        boost::shared_ptr<boost::thread> _bthread;
        boost::mutex _bmutex;
        bool _runs;
        bool _verbosed;
        void runReleaseThread( );
        void runEditionThread( );

        // SDL related
        SDL_AudioDeviceID _audio_device_id;
        bool _sound_enabled; // general
        bool _pulseaudio_enabled; // LINUX
        int _channels;
        int _sample_rate;
        int _samples;
        std::string _audio_driver;

        // pd related
//        bool _pd_enabled;
//        pd::PdBase _pd; // the actual pd engine
//        std::vector<std::string> _search_paths;
        pdPList _loaded_patchs;

        // edition mode
        bool _edition;
        POscSenderList _edition_senders;
        POscRcvrList _edition_receivers;

        /** SDL audio device callback, see implementation for details
         */
        void audioCallback( unsigned char* stream, int len );

        /** registration of a new patch
         */
        void addPatch( pd::Patch* p );

        /** deletion of a patch
         */
        void removePatch( pd::Patch* p );

    };

};

#endif /* PUREDATA_H */

