/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2017 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
*/

/* 
 * File:   PuredataInOut.h
 * Author: frankiezafe
 *
 * Created on April 6, 2017, 1:59 PM
 */

#ifndef PUREDATAINOUT_H
#define PUREDATAINOUT_H

#include "PdBase.hpp"
#include "PUtil.h"

namespace polymorph {

    class PuredataInOut :
    public pd::PdReceiver,
    public pd::PdMidiReceiver {
    public:

        PuredataInOut( );

        virtual ~PuredataInOut( );

        /** Add search folders to puredata, to be done before calling init()
         @param a relative path, such as ../media/etc
         */
        inline void addSearchPath( std::string path ) {
            _search_paths.push_back( path );
        }

        /** Clear all registered search path
         */
        inline void cleanSearchPath( std::string path ) {
            _search_paths.clear( );
        }

        // subscription to pd messages, delegate
        bool subscribe( const std::string& source );
        
        bool unsubscribe( const std::string& source );
        
        bool unsubscribeAll( );

        // puredata message receiver callbacks
        void print( const std::string& message );
        
        void receiveBang( const std::string& dest );
        
        void receiveFloat( const std::string& dest, float num );
        
        void receiveSymbol( const std::string& dest, const std::string& symbol );
        
        void receiveList( const std::string& dest, const pd::List& list );
        
        void receiveMessage( const std::string& dest, const std::string& msg, const pd::List& list );
        
        // puredata midi receiver callbacks
        void receiveNoteOn( const int channel, const int pitch, const int velocity );
        
        void receiveControlChange( const int channel, const int controller, const int value );
        
        void receiveProgramChange( const int channel, const int value );
        
        void receivePitchBend( const int channel, const int value );
        
        void receiveAftertouch( const int channel, const int value );
        
        void receivePolyAftertouch( const int channel, const int pitch, const int value );
        
        void receiveMidiByte( const int port, const int byte );

        // external receivers

        /** Register an external receiver
         @param pointer must extends pd::PdReceiver base class
         */
        bool addReceiver( pd::PdReceiver* pdr );

        /** Unregister an external receiver
         @param pointer must extends pd::PdReceiver base class
         */
        bool removeReceiver( pd::PdReceiver* pdr );

        /** Clean all receivers
         */
        void removeAllReceivers( );

        /** Register an external midi receiver
         @param pointer must extends pd::PdMidiReceiver base class
         */
        bool addMidiReceiver( pd::PdMidiReceiver* pdr );

        /** Unregister an external midi receiver
         @param pointer must extends pd::PdMidiReceiver base class
         */
        bool removeMidiReceiver( pd::PdMidiReceiver* pdr );

        /** Clean all midi receivers
         */
        void removeAllMidiReceivers( );

        // sending to puredata, see https://github.com/danomatika/ofxPd for original source

        /// \section Sending Functions

        bool sendBang( const std::string& dest );
        
        bool sendFloat( const std::string& dest, float value );
        
        bool sendSymbol( const std::string& dest, const std::string& symbol );

        /// compound messages
        /// pd.startMessage();
        /// pd.addSymbol("hello");
        /// pd.addFloat(1.23);
        /// pd.finishList("test");  // "test" is the reciever name in pd
        ///
        /// sends [list hello 1.23( -> [r test],
        /// you will need to use the [list trim] object on the reciving end
        ///
        /// finishMsg sends a typed message -> [; test msg1 hello 1.23(
        ///
        /// pd.startMessage();
        /// pd.addSymbol("hello");
        /// pd.addFloat(1.23);
        /// pd.finishMessage("test", "msg1");
        bool startMessage( );
        
        void addFloat( const float value );
        
        void addSymbol( const std::string& symbol );
        
        bool finishList( const std::string& dest );
        
        bool finishMessage( const std::string& dest, const std::string& msg );

        /// compound messages using the ofxPd List type
        ///
        /// List list;
        /// list.addSymbol("hello");
        /// list.addFloat(1.23);
        /// pd.sendList("test", list);
        ///
        /// sends [list hello 1.23( -> [r test]
        ///
        /// clear the list:
        ///
        /// list.clear();
        ///
        /// stream operators work as well:
        ///
        /// list << "hello" << 1.23;
        /// pd.sendMessage("test", "msg1", list);
        ///
        /// sends a typed message -> [; test msg1 hello 1.23(
        bool sendList( const std::string& dest, const pd::List& list );
        
        bool sendMessage( const std::string& dest, const std::string& msg, const pd::List& list );

        /// midi
        ///
        /// send midi messages, any out of range messages will be silently ignored
        ///
        /// number ranges:
        /// channel     1 - 16 * dev# (dev #0: 1-16, dev #1: 17-32, etc)
        /// pitch       0 - 127
        /// velocity    0 - 127
        /// control value   0 - 127
        /// program value   1 - 128
        /// bend value     -8192 - 8191
        /// touch value     0 - 127
        ///
        /// note, in pd:
        /// [bendin] takes 0 - 16383 while [bendout] returns -8192 - 8192
        /// [pgmin] and [pgmout] are 1 - 128
        bool sendNoteOn( const int channel, const int pitch, const int velocity = 64 );
        
        bool sendControlChange( const int channel, const int controller, const int value );
        
        bool sendProgramChange( const int channel, const int value );
        
        bool sendPitchBend( const int channel, const int value );
        
        bool sendAftertouch( const int channel, const int value );
        
        bool sendPolyAftertouch( const int channel, const int pitch, const int value );

        /// raw midi bytes
        ///
        /// value is a raw midi byte value 0 - 255
        /// port is the raw portmidi port #, similar to a channel
        ///
        /// for some reason, [midiin], [sysexin] & [realtimein] add 2 to the port num,
        /// so sending to port 1 in ofxPd returns port 3 in pd
        ///
        /// however, [midiout], [sysexout], & [realtimeout] do not add to the port num,
        /// so sending port 1 to [midiout] returns port 1 in ofxPd
        ///
        bool sendMidiByte( const int port, const int value );
        
        bool sendSysex( const int port, const int value );
        
        bool sendSysRealTime( const int port, const int value );

        /// \section Sending Stream Interface

        /// single messages
        ///
        /// pd << Bang("test"); /// "test" is the reciever name in pd
        /// pd << Float("test", 100);
        /// pd << Symbol("test", "a symbol");
        ///
        /// compound messagesPObjectOSCflag
        ///
        /// pd << StartMessage() << 100 << 1.2 << "a symbol" << FinishList("test");
        ///
        /// midi
        ///
        /// pd << NoteOn(64) << NoteOn(64, 60) << NoteOn(64, 60, 1);
        /// pd << ControlChange(100, 64) << ProgramChange(100, 1) << PitchBend(2000, 1);
        /// pd << Aftertouch(127, 1) << PolyAftertouch(64, 127, 1);
        ///
        /// compound raw midi byte stream
        ///
        /// pd << StartMidi() << 0xEF << 0x45 << Finish();
        /// pd << StartSysex() << 0xE7 << 0x45 << 0x56 << 0x17 << Finish();
        ///
        /// see PdBase.h for function declarations

        /// \section Array Access

        /// get the size of a pd array
        /// returns 0 if array not found
        int arraySize( const std::string& arrayName );

        /// read from a pd array
        ///
        /// resizes given vector to readLen, checks readLen and offset
        ///
        /// returns true on success, false on failure
        ///
        /// calling without setting readLen and offset reads the whole array:
        ///
        /// vector<float> array1;
        /// readArray("array1", array1);
        bool readArray( const std::string& arrayName, std::vector<float>& dest,
                        int readLen = -1, int offset = 0 );

        /// write to a pd array
        ///
        /// calling without setting writeLen and offset writes the whole array:
        ///
        /// writeArray("array1", array1);
        bool writeArray( const std::string& arrayName, std::vector<float>& source,
                         int writeLen = -1, int offset = 0 );

        /// clear array and set to a specific value
        bool clearArray( const std::string& arrayName, int value = 0 );

    protected:

        // pd related
        bool _pd_enabled;
        pd::PdBase _pd; // the actual pd engine
        std::vector<std::string> _search_paths;

        // external receivers
        std::set< pd::PdReceiver* > receivers;
        std::set< pd::PdMidiReceiver* > midi_receivers;

    };

};

#endif /* PUREDATAINOUT_H */

