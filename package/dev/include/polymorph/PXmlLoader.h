/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
*/

/* 
 * File:   PXmlLoader.h
 * Author: frankiezafe
 *
 * Created on October 21, 2016, 11:51 AM
 */

#ifndef POLYMORPH_XMLLOADER_H
#define POLYMORPH_XMLLOADER_H

#include "tinyxml.h"
#include "PUtil.h"
#include "SerialisationCommon.h"
#include "PCamRigData.h"
#include "PProjectData.h"
#include "PBulletSpringData.h"

#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/gregorian/greg_month.hpp>
#include <boost/date_time/date_formatting.hpp>
#include <boost/date_time/gregorian/parsers.hpp>
#include <boost/date_time/gregorian/formatters.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

namespace polymorph {


    class PXmlLoader : public SerialisationUtil {
    
    public:
        
        PXmlLoader();
        virtual ~PXmlLoader();
    
        /**
         * Loading an XML called 'configuration.xml' 
         * from the 'bin' folder (containing the executable)
         * @return
         *      true if parsing is successful
         * @note
         *      the real job is done in protected parse( DataStreamPtr )
         */
        virtual bool parse();
        
        /**
         * Loading an XML from resource group
         * @return
         *      true if parsing is successful
         * @remark
         *      !REQUIRES! the presence of a resources.cfg file in
         *      the 'bin' folder (containing the executable)
         * @note
         *      the real job is done in protected parse( DataStreamPtr )
         */
        virtual bool parse( Ogre::String group, Ogre::String xml );
        
        // getters
        inline void verbose( bool enabled ) { verbosed = enabled; }
        
        inline PProjectData& getProject() { return pdata; }
        
        /* Util to load bullet spring data, not called automatically while
         * standard parsing. Must be done during parseCustom() is called.
         * @param
         *      an xml element containing the definition
         * @param
         *      destination object
         * @return
         *      true if success ( xml element is of the right type )
         */
        virtual bool parseBulletSpring(
            TiXmlElement* e,
            PBulletSpringData& spring_data
            );
        
    protected:
        
        Ogre::String group_name;
        Ogre::String xml_name;
                
        bool verbosed;
        
        PProjectData pdata;
        PSceneData* current_scene;
        
        virtual bool parse( Ogre::DataStreamPtr & ptr );
        
        /**
         * OVerload this method for any custom tag parsing.
         * This method is called at the end of the parsing by method
         * PXmlLoader::parse( Ogre::DataStreamPtr & stream )
         * @param XML root tag
         * @return 
         */
        virtual bool parseCustom( TiXmlElement* e );
        
        virtual bool parseResources( TiXmlElement* e );
        
        virtual bool parseCompositor( TiXmlElement* e );
        
        virtual bool parseScene( TiXmlElement* e );
        
        virtual bool parseNode( TiXmlElement* e );
        
        virtual bool parseLight( TiXmlElement* e );
        
        virtual bool parseBullet( TiXmlElement* e );
        
        virtual bool parseNetwork( TiXmlElement* e );
        
        virtual bool parsePuredata( TiXmlElement* e );
        
        virtual bool parseOscSender( TiXmlElement* e, POSDataList& list );
        
        virtual bool parseOscReceiver( TiXmlElement* e, PORDataList& list );
        
        // used for all PObjects
        virtual bool parseObjectData( TiXmlElement* e, PObjectData& d );
        
        virtual bool parseObjectBulletData( TiXmlNode* n, PObjectBulletData& d );

        virtual PCompositorWSValue<Ogre::ColourValue> parseCompositorColor( TiXmlElement* e );
        
        virtual PCompositorWSValue<float> parseCompositorVariable( TiXmlElement* e );
        
    };
    
};

#endif /* POLYMORPH_XMLLOADER_H */

