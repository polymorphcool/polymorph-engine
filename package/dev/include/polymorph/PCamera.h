/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2017 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
 */

/* 
 * File:   PCamera.h
 * Author: frankiezafe
 *
 * Created on January 21, 2017, 10:38 PM
 */

#ifndef POLYMORPH_CAMERA_H
#define POLYMORPH_CAMERA_H

#include "PCommon.h"
#include "PUtil.h"
#include "PObject.h"
#include "PBone.h"

namespace polymorph {

    class PCamera : public PObject<Ogre::Camera*>, public Ogre::Camera::Listener {
    public:
        
        PCamera();
        
        virtual ~PCamera();

        void init(Ogre::SceneManager* sceneMgr, std::string name);

        void visible(bool enable);

        void debug(bool enable);
        
        void showAxis( bool enabled );
        
        // call this before deletion - will clear ALL hierarchy!
        void clear();

        // configuration
        
        void fov( Ogre::Real degree );
        
        void fov( Ogre::Radian r );
        
        inline void near( float value ) {
            
            if ( _object ) {
                _object->setNearClipDistance( value );
            }
            
        }
        
        inline void far( float value ) {
            
            if ( _object ) {
                _object->setFarClipDistance( value );
            }
            
        }
        
        // utils
        
        Ogre::Vector3 worldToCam( Ogre::Vector3 v );
        
        Ogre::Quaternion worldToCam( Ogre::Quaternion q );
        
        // Ogre::Camera callbacks
        void cameraPreRenderScene(Ogre::Camera* cam);

        void cameraPostRenderScene(Ogre::Camera* cam) {}

        void cameraDestroyed(Ogre::Camera* cam) {}
                
        inline void lookAt( PObjectAbstract* lt ) {
            _look_at = lt;
        }
        
        void lookAt( Ogre::Vector3 p, bool force = false );
        
        // getters
        inline Ogre::Camera* getOgreCam() {
            return _object;
        }
        
        inline Ogre::Radian getFovy() {
            if ( !_object ) return Ogre::Radian( 0 );
            return _object->getFOVy();
        }
        
        inline PObjectAbstract* getLookAt() {
            return _look_at;
        }
        
    protected:
        
        void local_position(Ogre::Vector3 v) {
            _pivot->setPosition(v);
        }

        void local_scale(Ogre::Vector3 v) {
            _scale = v;
        }

        void local_orientation(Ogre::Quaternion q) {
            _pivot->setOrientation(q);
        }

        void local_pitch(
                const Ogre::Radian& angle,
                TransformSpace space = polymorph::TS_LOCAL) {
            _pivot->pitch(angle, PUtil::convert(space));
        }

        void local_yaw(
                const Ogre::Radian& angle,
                TransformSpace space = polymorph::TS_LOCAL) {
            _pivot->yaw(angle, PUtil::convert(space));
        }

        void local_roll(
                const Ogre::Radian& angle,
                TransformSpace space = polymorph::TS_LOCAL) {
            _pivot->roll(angle, PUtil::convert(space));
        }
        
        Ogre::Quaternion getObjectOrientation(bool absolute = true) {
            if ( absolute ) {
                return _pivot->_getDerivedOrientation();
            }
            return _pivot->getOrientation();
        };

        Ogre::Vector3 getObjectTrans(bool absolute = true) {
            if ( absolute ) {
                return _pivot->_getDerivedPosition();
            }
            return _pivot->getPosition();
        };

        Ogre::Vector3 getObjectScale(bool absolute = true) {
            if ( absolute ) {
                return _pivot->_getDerivedScale();
            }
            return _pivot->getScale();
        };
        
        Ogre::Vector3 getTranslationUpdated() {
            return _object->getRealPosition();
        }
        
        Ogre::Quaternion getOrientationUpdated() {
            return _object->getOrientationForViewUpdate();
        }
        
        void clearDebug();
        
        void renderUp();
        
        void applyLookat();
        
        void updateDebug();
        
//        void updateMatrices();
        
    private:
        
        Ogre::Vector3 _scale;
        Ogre::SceneNode* _pivot;
        PObjectAbstract* _look_at;
        
        Ogre::SceneNode* _debug_node;
        Ogre::Entity* _debug_entity;
        
        Ogre::Real _aspect_ratio;
        Ogre::Radian _fov_y;
        
        bool _request_lookat;
        Ogre::Vector3 _request_lookat_v3;
        
        // matrices
        Ogre::Matrix4 _mat_world;
        Ogre::Matrix4 _mat_worldi;
        
        // bullet
        PBulletExchangePCam _cam_exchange;

    };

    typedef std::vector< PCamera* > PCamList;
    typedef std::vector< PCamera* >::iterator PCameraListIter;
    
};

#endif /* POLYMORPH_CAMERA_H */

