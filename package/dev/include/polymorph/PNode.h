/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
 */

/* 
 * File:   PNode.h
 * Author: frankiezafe
 *
 * Created on September 20, 2016, 7:38 PM
 */

#ifndef POLYMORPH_NODE_H
#define POLYMORPH_NODE_H

#include "PCommon.h"
#include "PUtil.h"
#include "PNodeData.h"
#include "PObject.h"
#include "PBone.h"
#include "PMaterial.h"

namespace polymorph {

    class PNode;
    typedef std::vector< PNode* > PNodeList;
    typedef std::vector< PNode* >::iterator PNodeListIter;
    typedef std::map< std::string, PNode* > PNodeMap;
    typedef std::map< std::string, PNode* >::iterator PNodeMapIter;

    class PNode : public PObject<Ogre::SceneNode*> {
    public:

        PNode();
        PNode(PNode& src);
        PNode(Ogre::SceneManager* sceneMgr, const PNodeData* src);
        virtual ~PNode();

        // creation
        bool load(
                Ogre::SceneManager* sceneMgr,
                std::string meshgroup,
                std::string meshname,
                std::string name = "");
        
        bool sphere(
                Ogre::SceneManager* sceneMgr,
                std::string name = "");
        
        bool cube(
                Ogre::SceneManager* sceneMgr,
                std::string name = "");
        
        bool plane(
                Ogre::SceneManager* sceneMgr,
                std::string name = "");
        
        bool empty(
                Ogre::SceneManager* sceneMgr,
                std::string name = "");

        bool append(
                std::string meshgroup,
                std::string meshname
                );

        // modifiers
        void material(
                PMaterial* mat,
                bool overwrite_default = false,
                size_t entity = 0,
                int submesh = -1);

        void material(
                std::string matgroup,
                std::string matname,
                bool overwrite_default = false,
                size_t entity = 0,
                int submesh = -1);
        
        void material(
                Ogre::MaterialPtr ptr,
                bool overwrite_default,
                size_t entity,
                int submesh);

        // Reload default materials on meshes
        void resetMaterial();

        // utils
        void visible(bool enable);

        void debug(bool enable) {
            debug(PDM_ALL, enable);
        }

        void debug(PNodeDebugMode mode, bool enable);

        // bullet
        virtual bool physics(
                BT_Type type,
                std::string meshgroup = "",
                std::string meshname = "");

        // skeleton
        // always prefer access by pointer, uint and string are delegates!
        bool translateBone(
                std::string name, Ogre::Vector3 v,
                PBoneSpace ts = PBone::TS_LOCAL);

        bool translateBone(
                uint id, Ogre::Vector3 v,
                PBoneSpace ts = PBone::TS_LOCAL);

        bool translateBone(
                PBone* b, Ogre::Vector3 v,
                PBoneSpace ts = PBone::TS_LOCAL);

        bool moveBone(
                std::string name, Ogre::Vector3 v,
                PBoneSpace ts = PBone::TS_LOCAL);

        bool moveBone(
                uint id, Ogre::Vector3 v,
                PBoneSpace ts = PBone::TS_LOCAL);

        bool moveBone(
                PBone* b, Ogre::Vector3 v,
                PBoneSpace ts = PBone::TS_LOCAL);

        bool orientBone(
                std::string name, const Ogre::Quaternion& q,
                PBoneSpace ts = PBone::TS_LOCAL);

        bool orientBone(
                uint id, const Ogre::Quaternion& q,
                PBoneSpace ts = PBone::TS_LOCAL);

        bool orientBone(
                PBone* b, const Ogre::Quaternion& q,
                PBoneSpace ts = PBone::TS_LOCAL);

        bool scaleBone(
                std::string name, const Ogre::Vector3& v,
                PBoneSpace ts = PBone::TS_LOCAL);

        bool scaleBone(
                uint id, const Ogre::Vector3& v,
                PBoneSpace ts = PBone::TS_LOCAL);

        bool scaleBone(
                PBone* b, const Ogre::Vector3& v,
                PBoneSpace ts = PBone::TS_LOCAL);

        bool pitchBone(
                std::string name, Ogre::Radian r,
                PBoneSpace ts = PBone::TS_LOCAL);

        bool pitchBone(
                uint id, Ogre::Radian r,
                PBoneSpace ts = PBone::TS_LOCAL);

        bool pitchBone(
                PBone* b, Ogre::Radian r,
                PBoneSpace ts = PBone::TS_LOCAL);

        bool yawBone(
                std::string name, Ogre::Radian r,
                PBoneSpace ts = PBone::TS_LOCAL);

        bool yawBone(
                uint id, Ogre::Radian r,
                PBoneSpace ts = PBone::TS_LOCAL);

        bool yawBone(
                PBone* b, Ogre::Radian r,
                PBoneSpace ts = PBone::TS_LOCAL);

        bool rollBone(
                std::string name, Ogre::Radian r,
                PBoneSpace ts = PBone::TS_LOCAL);

        bool rollBone(
                uint id, Ogre::Radian r,
                PBoneSpace ts = PBone::TS_LOCAL);

        bool rollBone(
                PBone* b, Ogre::Radian r,
                PBoneSpace ts = PBone::TS_LOCAL);

        // call this before deletion - will clear ALL hierarchy!
        void clear();

        // getters
        PNodeDebugMode getDebugMode();

        bool isDebugMode(PNodeDebugMode mode);

        inline bool isVisible() {
            return _visible;
        }

        inline bool hasSkeleton() {
            return ( _skeleton != 0);
        }

        inline Ogre::Entity* entity( size_t i = 0 ) {
            return _entities[i];
        }
        
        virtual Ogre::Aabb getAabb() {
            return Ogre::Aabb( _bounding_box );
        }

        bool getBonePosition(
                std::string name, Ogre::Vector3& v,
                PBoneSpace ts = PBone::TS_LOCAL);

        bool getBonePosition(
                uint id, Ogre::Vector3& v,
                PBoneSpace ts = PBone::TS_LOCAL);

        bool getBonePosition(
                PBone* b, Ogre::Vector3& v,
                PBoneSpace ts = PBone::TS_LOCAL);

        bool getBoneOrientation(
                std::string name, Ogre::Quaternion& q,
                PBoneSpace ts = PBone::TS_LOCAL);

        bool getBoneOrientation(
                uint id, Ogre::Quaternion& q,
                PBoneSpace ts = PBone::TS_LOCAL);

        bool getBoneOrientation(
                PBone* b, Ogre::Quaternion& q,
                PBoneSpace ts = PBone::TS_LOCAL);

        bool getBoneScale(
                std::string name, Ogre::Vector3& s,
                PBoneSpace ts = PBone::TS_LOCAL);

        bool getBoneScale(
                uint id, Ogre::Vector3& s,
                PBoneSpace ts = PBone::TS_LOCAL);

        bool getBoneScale(
                PBone* b, Ogre::Vector3& s,
                PBoneSpace ts = PBone::TS_LOCAL);

        inline uint getBoneNum() {
            if (!_skeleton) return 0;
            return _skeleton->getNumBones();
        }

        void getBoneNames(size_t& bnum, std::string*& bnames);

        void getBones(size_t& bnum, PBone**& bones);

        PBone* bone(uint id);

        PBone* bone(std::string name);

        void showAxis(bool enabled);

        PMaterial* getMaterial(
                size_t entity = 0,
                int submesh = -1, 
                bool autocreate = true
                );
        
        inline Ogre::Real distance( const Ogre::Vector3& v ) {
            
            if ( !_entities ) return 0;
            return _entities[0]->getWorldAabb().distance( v );
            
        }

    protected:

        void local_position(Ogre::Vector3 v) {
            _object->setPosition(v);
        }

        void local_scale(Ogre::Vector3 v) {
            if (_internal && !_empty) v *= ONE_PERCENT;
            _object->setScale(v);
        }

        void local_orientation(Ogre::Quaternion q) {
            _object->setOrientation(q);
        }

        void local_pitch(
                const Ogre::Radian& angle,
                TransformSpace space = polymorph::TS_LOCAL) {
            _object->pitch(angle, PUtil::convert(space));
        }

        void local_yaw(
                const Ogre::Radian& angle,
                TransformSpace space = polymorph::TS_LOCAL) {
            _object->yaw(angle, PUtil::convert(space));
        }

        void local_roll(
                const Ogre::Radian& angle,
                TransformSpace space = polymorph::TS_LOCAL) {
            _object->roll(angle, PUtil::convert(space));
        }

        Ogre::Quaternion getObjectOrientation(bool absolute = true) {
            if (absolute) {
                return _object->_getDerivedOrientation();
            }
            return _object->getOrientation();
        };

        Ogre::Vector3 getObjectTrans(bool absolute = true) {
            if (absolute) {
                return _object->_getDerivedPosition();
            }
            return _object->getPosition();
        };

        Ogre::Vector3 getObjectScale(bool absolute = true) {
            if (absolute) {
                return _object->_getDerivedScale();
            }
            return _object->getScale();
        };

        Ogre::Vector3 getTranslationUpdated() {
            return _object->_getDerivedPositionUpdated();
        }
        
        Ogre::Quaternion getOrientationUpdated() {
            return _object->_getDerivedOrientationUpdated();
        }

        void loadAabb();

        bool loadSkeleton();

        bool loadMaterials();

        void debugMesh(bool enable);

        void debugSkeleton(bool enable);

        void debugBone(PBone* b);

        void debugBoneUpdate(PBone* b);

        void clearDebugMesh();

        void clearDebugSkeleton();

    private:

        bool _internal;
        bool _cube;
        bool _sphere;
        bool _plane;
        bool _empty;

        // ogre
        size_t _entity_num;
        Ogre::Entity** _entities;
        Ogre::String _group;
        Ogre::String _mesh_name;
        PSkeleton* _skeleton;

        // storage
        size_t _mat_num;
        MaterialInfo* _default_mat;
        
        // materials are created on demand, they are not loaded by default
        // see loadMaterials & getMaterial for creation
        PMaterial** _materials;
        
        // bullet
        PBulletExchangePNode _node_exchange;

        // debugging
        Ogre::SceneNode* _debug_node;
        Ogre::Entity* _debug_entity;
        Ogre::SceneNode* _debug_skeleton_root;
        PBDebugMap _debug_skeleton;

        void appendEntitySlot();
        
        void appendMaterialSlot( size_t add = 1 );

    };

};

#endif /* POLYMORPH_NODE_H */

