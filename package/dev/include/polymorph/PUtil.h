/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
 */

/* 
 * File:   PUtil.h
 * Author: frankiezafe
 *
 * Created on October 8, 2016, 3:51 PM
 */

#ifndef POLYMORPH_UTIL_H
#define POLYMORPH_UTIL_H

#include <iomanip>
#include "PCommon.h"
#include "PBulletTypes.h"
#include "PBulletRay.h"
#include "PBulletExchange.h"
//#include "PdCommon.h"

namespace polymorph {

    class PUtil {
    public:

        // globals - see cpp for instanciation
        // compounds material name prefixes
        static std::string mat_prefix_box;
        static uint mat_prefix_cube_l;
        static std::string mat_prefix_sphere;
        static uint mat_prefix_sphere_l;
        static std::string mat_prefix_cylinder;
        static uint mat_prefix_cylinder_l;
        static std::string mat_prefix_capsule;
        static uint mat_prefix_capsule_l;
        static std::string mat_prefix_cone;
        static uint mat_prefix_cone_l;

        // methods
    public:

        /** Verify that 2 Ogre::Vector3 are equals or opposite
            @param v1 first Ogre::Vector3 to compare
            @param v2 second Ogre::Vector3 to compare
            @param precise set to true if you need to prevents 
            floating points inconsistency (1e-5 precision), disabled by default
         */
        static bool isEqualOrInverse(
                const Ogre::Vector3& v1,
                const Ogre::Vector3& v2,
                bool precise = false);

        /** Verify that 2 Ogre::Vector3 are equals
            @param v1 first Ogre::Vector3 to compare
            @param v2 second Ogre::Vector3 to compare
            @param precise set to true if you need to prevents 
            floating points inconsistency (1e-5 precision), disabled by default
         */
        static bool isEqual(
                const Ogre::Vector3& v1,
                const Ogre::Vector3& v2,
                bool precise = false);

        /** Render a quaternion representing the rotation form v1 to v2
            @param v1 first Ogre::Vector3 to use
            @param v2 second Ogre::Vector3 to use 
            (reference, place the Ogre::Vector3::UNIT_Y here)
            @param secure_check set to true if you are not sure your vectors 
            are normalised
        @note 
            http://lolengine.net/blog/2013/09/18/beautiful-maths-quaternion-from-vectors
         */
        static Ogre::Quaternion fromTwoVectors(
                const Ogre::Vector3& v1,
                const Ogre::Vector3& v2,
                bool secure_check = true);

        /** Helper to test a zero length vector
         */
        static bool isZero(const Ogre::Vector3& v);

        /** Test if an Ogre::Vector3 is equal to any of the 
            positive or negative standard unit vector
         */
        static bool isAlignedWithWorld(const Ogre::Vector3& v);

        /** Conversion of btVector3 into Ogre::Vector3
         * @param btVector3 containing data to convert
         * @return Ogre::Vector3
         */
        static Ogre::Vector3 convert(const btVector3& src);

        /** Conversion of Ogre::Vector3 into btVector3
         * @param Ogre::Vector3 containing data to convert
         * @return btVector3
         */
        static btVector3 convert(const Ogre::Vector3& src);

        /** Conversion of btQuaternion into Ogre::Quaternion
         * @param btQuaternion containing data to convert
         * @return Ogre::Quaternion
         */
        static Ogre::Quaternion convert(const btQuaternion& src);

        /** Conversion of Ogre::Quaternion into btQuaternion
         * @param Ogre::Quaternion containing data to convert
         * @return btQuaternion
         */
        static btQuaternion convert(const Ogre::Quaternion& src);

        /** Conversion of polymorph::TransformSpace into Ogre::Node::TransformSpace
         * @param polymorph::T_SPACE
         * @return Ogre::Node::TransformSpace
         */
        static Ogre::Node::TransformSpace convert(polymorph::TransformSpace space) {
            switch (space) {
                case polymorph::TS_WORLD:
                    return Ogre::Node::TS_WORLD;
                case polymorph::TS_PARENT:
                    return Ogre::Node::TS_PARENT;
                default:
                    return Ogre::Node::TS_LOCAL;

            }
        }

        /** Conversion of btVector3 into Ogre::Vector3 without objects creation
         * @param btVector3 containing data to convert
         * @param Ogre::Vector3* to adapt, must be created before calling the method
         */
        static void copy(const btVector3& src, Ogre::Vector3* dest);

        /** Util to print of all parameters of a btExchange in console
         * @param PBulletExchange to print
         */
        static void print(PBulletExchange& e);

        /** Util to print of a Ogre::Vector2 in console
         * @param Ogre::Vector2 to print
         */
        static void print(const Ogre::Vector2& v) {
            std::cout << "Ogre::Vector2: " << v.x << ", " << v.y << std::endl;
        }

        /** Util to print of a Ogre::Vector3 in console
         * @param Ogre::Vector3 to print
         */
        static void print(const Ogre::Vector3& v) {
            std::cout << "Ogre::Vector3: " << v.x << ", " << v.y << ", " << v.z << std::endl;
        }

        /** Util to print of a btVector3 in console
         * @param btVector3 to print
         */
        static void print(const btVector3& v) {
            std::cout << "btVector3: " << v.x() << ", " << v.y() << ", " << v.z() << std::endl;
        }

        /** Util to print of a Ogre::Quaternion in console
         * @param Ogre::Quaternion to print
         */
        static void print(const Ogre::Quaternion& q) {
            std::cout << "Ogre::Quaternion: " << q.w << ", " << q.x << ", " << q.y << ", " << q.z << std::endl;
        }

        /** Util to print of a Ogre::Matrix4 in console
         * @param Ogre::Matrix4 to print
         */
        static void print(const Ogre::Matrix4& m);

        /** Returns a printable string showing values of passed Vector2
         * @param Ogre::Vector2 to convert into string
         * @return a std::string showing Vector2 data
         */
        static std::string str(const Ogre::Vector2& v) {
            std::stringstream ss;
            ss << "Vector2( x:" <<
                    v.x << ", y:" <<
                    v.y << " )";
            return ss.str();
        }

        /** Returns a printable string showing values of passed Vector3
         * @param Ogre::Vector3 to convert into string
         * @return a std::string showing Vector3 data
         */
        static std::string str(const Ogre::Vector3& v) {
            std::stringstream ss;
            ss << "Vector3( x:" <<
                    v.x << ", y:" <<
                    v.y << ", z:" <<
                    v.z << " )";
            return ss.str();
        }

        /** Returns a printable string showing values of passed Vector4
         * @param Ogre::Vector4 to convert into string
         * @return a std::string showing Vector4 data
         */
        static std::string str(const Ogre::Vector4& v) {
            std::stringstream ss;
            ss << "Vector4( x:" <<
                    v.x << ", y:" <<
                    v.y << ", z:" <<
                    v.z << ", w:" <<
                    v.w << " )";
            return ss.str();
        }

        /** Returns a printable string showing values of passed ColourValue
         * @param Ogre::ColourValue to convert into string
         * @param bool to avoid int conversion and shows float values, default: false
         * @return a std::string showing ColourValue
         * @remark channels are converted to int in range [0,255] by default
         */
        static std::string str(const Ogre::ColourValue& c, bool print_floats = false) {
            std::stringstream ss;
            if (print_floats) {
                ss << "ColourValue( R:" <<
                        c.r << ", G:" <<
                        c.g << ", B:" <<
                        c.b << ", A:" <<
                        c.a << " )";
            } else {
                ss << "ColourValue( R:" <<
                        int( c.r * 255) << ", G:" <<
                        int( c.g * 255) << ", B:" <<
                        int( c.b * 255) << ", A:" <<
                        int( c.a * 255) << " )";
            }
            return ss.str();
        }

        /** Returns a printable string showing values of passed Quaternion
         * @param Ogre::Quaternion to convert into string
         * @return a std::string showing Quaternion data
         */
        static std::string str(const Ogre::Quaternion& q) {
            std::stringstream ss;
            ss << "Quaternion( x:" <<
                    q.x << ", y:" <<
                    q.y << ", z:" <<
                    q.z << ", w:" <<
                    q.w << " )";
            return ss.str();
        }

        /** Returns a printable string showing values of passed Euler
         * @param Ogre::Quaternion to convert into string
         * @return a std::string showing Quaternion data
         */
        static std::string str(const Ogre::Euler& els) {
            std::stringstream ss;
            ss << "Euler( x:" <<
                    els.pitch() << ", y:" <<
                    els.yaw() << ", z:" <<
                    els.roll() << " )";
            return ss.str();
        }

        /** Returns a printable string showing values of passed Matrix3
         * @param Ogre::Matrix3 to convert into string
         * @return a std::string showing Matrix3 data
         */
        static std::string str(const Ogre::Matrix3& m) {
            std::stringstream ss;
            ss << "Matrix3" << std::endl <<
                    "\t" << m[0][0] <<
                    "\t" << m[0][1] <<
                    "\t" << m[0][2] << std::endl <<
                    "\t" << m[1][0] <<
                    "\t" << m[1][1] <<
                    "\t" << m[1][2] << std::endl <<
                    "\t" << m[2][0] <<
                    "\t" << m[2][1] <<
                    "\t" << m[2][2] << std::endl;
            return ss.str();
        }

        /** Returns a printable string showing values of passed Matrix4
         * @param Ogre::Matrix4 to convert into string
         * @return a std::string showing Matrix4 data
         */
        static std::string str(const Ogre::Matrix4& m) {
            std::stringstream ss;
            ss << "Matrix3" << std::endl <<
                    "\t" << m[0][0] <<
                    "\t" << m[0][1] <<
                    "\t" << m[0][2] <<
                    "\t" << m[0][3] << std::endl <<
                    "\t" << m[1][0] <<
                    "\t" << m[1][1] <<
                    "\t" << m[1][2] <<
                    "\t" << m[1][3] << std::endl <<
                    "\t" << m[2][0] <<
                    "\t" << m[2][1] <<
                    "\t" << m[2][2] <<
                    "\t" << m[2][3] << std::endl <<
                    "\t" << m[3][0] <<
                    "\t" << m[3][1] <<
                    "\t" << m[3][2] <<
                    "\t" << m[3][3] << std::endl;
            return ss.str();
        }

        /** Returns a printable string showing BT_Type
         * @param BT_Type to convert into string
         * @return a std::string showing BT_Type
         */
        static std::string str(const BT_Type& t);

        /** Validation of presence of a group name resources manager
         * @param std::string: name of the resource group
         * @return bool : true if group exists
         */
        static bool groupExists(std::string group);

        /** Creation of a new group in resources manager
         * @param std::string: name of the resource group
         * @return bool : true if group successfully created
         */
        static bool createGroup(std::string group);

        /** Validation of presence of a mesh in the available resources
         * @param std::string: name of the resource group
         * @param std::string: name of the mesh
         * @return bool : true if mesh exists
         */
        static bool meshExists(std::string group, std::string name);

        /** Validate if mesh is loaded in mesh manager
         * @param std::string: name of the resource group
         * @param std::string: name of the mesh
         * @return bool : true if mesh is loaded
         */
        static bool meshLoaded(std::string group, std::string name);

        /** Validation of presence of a material in the available resources
         * @param std::string: name of the resource group
         * @param std::string: name of the material
         * @return bool : true if material exists
         */
        static bool materialExists(std::string group, std::string name);

        /** Validate if material is loaded in material manager
         * @param std::string: name of the resource group
         * @param std::string: name of the material
         * @return bool : true if material is loaded
         */
        static bool materialLoaded(std::string group, std::string name);

        /** Retrieve a texture pointer of the texture manager
         * @param std::string: name of the resource group
         * @param std::string: name of the texture
         * @param bool : automatically loads the texture if not done, 
         * true by default
         * @return Ogre::MaterialPtr
         * @remark test the return pointer by using ptr.get() != 0
         */
        static Ogre::MaterialPtr getMaterial(
                std::string group,
                std::string name,
                bool autoload = true
                );

        /** Validation of presence of a texture in the available resources
         * @param std::string: name of the resource group
         * @param std::string: name of the texture
         * @return bool : true if texture exists
         */
        static bool textureExists(std::string group, std::string name);

        /** Validate if texture is loaded in texture manager
         * @param std::string: name of the resource group
         * @param std::string: name of the texture
         * @return bool : true if texture is loaded
         */
        static bool textureLoaded(std::string group, std::string name);

        /** Retrieve a texture pointer of the texture manager
         * @param std::string: name of the resource group
         * @param std::string: name of the texture
         * @param bool : automatically loads the texture if not done, 
         * true by default
         * @return Ogre::TexturePtr
         * @remark test the return pointer by using ptr.get() != 0
         */
        static Ogre::TexturePtr getTexture(
                std::string group,
                std::string name,
                bool autoload = true
                );

        /** Unzip a pd patch to the drive
         * @param std::string: name of the resource group
         * @param std::string: name of the texture
         * @param bool : automatically loads the texture if not done, 
         * true by default
         * @return file name on the harddrive, [group]:[name]
         * @remark if process fails, returns an empty string!
         */
        static std::string decompressPatch(
                std::string group,
                std::string name
                );

        /** Check if a btExchange has no physics
         */
        static bool isNone(const PBulletExchange& e) {
            return isNone(e.type);
        }

        /** Check if a BT_Type is to none
         */
        static bool isNone(const BT_Type& t) {
            return ( t == BT_NONE);
        }

        /** Check if a btExchange is a primitive
         */
        static bool isPrimitive(const PBulletExchange& e) {
            return isPrimitive(e.type);
        }

        /** Check if a BT_Type is a primitive
         */
        static bool isPrimitive(const BT_Type& t) {
            return (
                    t == BT_DYNAMIC_BOX ||
                    t == BT_DYNAMIC_SPHERE ||
                    t == BT_STATIC_BOX ||
                    t == BT_STATIC_SPHERE||
                    t == BT_GHOST_BOX ||
                    t == BT_GHOST_SPHERE
                    );
        }

        /** Check if a btExchange can be considered as a ghost
         */
        static bool isGhost(const PBulletExchange& e) {
            return isGhost(e.type);
        }

        /** Check if a BT_Type can be considered as a ghost
         */
        static bool isGhost(const BT_Type& t) {
            return ( 
                    t == BT_GHOST_BOX ||
                    t == BT_GHOST_SPHERE ||
                    t == BT_GHOST_COMPOUND ||
                    t == BT_GHOST_CONVEX ||
                    t == BT_GHOST_CONCAVE
                    );
        }

        /** Check if a btExchange can be considered as a cube (primitive)
         */
        static bool isBox(const PBulletExchange& e) {
            return isBox(e.type);
        }

        /** Check if a BT_Type can be considered as a cube (primitive)
         */
        static bool isBox(const BT_Type& t) {
            return (
                    t == BT_DYNAMIC_BOX ||
                    t == BT_STATIC_BOX ||
                    t == BT_GHOST_BOX
                    );
        }

        /** Check if a btExchange can be considered as a sphere (primitive)
         */
        static bool isSphere(const PBulletExchange& e) {
            return isSphere(e.type);
        }

        /** Check if a BT_Type can be considered as a sphere (primitive)
         */
        static bool isSphere(const BT_Type& t) {
            return (
                    t == BT_DYNAMIC_SPHERE ||
                    t == BT_STATIC_SPHERE ||
                    t == BT_GHOST_SPHERE
                    );
        }

        /** Check if a btExchange is a complex, not a primitive
         */
        static bool isComplex(const PBulletExchange& e) {
            return isComplex(e.type);
        }

        /** Check if a BT_Type is a complex, not a primitive
         */
        static bool isComplex(const BT_Type& t) {
            return (
                    t == BT_DYNAMIC_COMPOUND ||
                    t == BT_DYNAMIC_CONVEX ||
                    t == BT_DYNAMIC_CONCAVE ||
                    t == BT_STATIC_COMPOUND ||
                    t == BT_STATIC_CONVEX ||
                    t == BT_STATIC_CONCAVE ||
                    t == BT_GHOST_COMPOUND ||
                    t == BT_GHOST_CONVEX ||
                    t == BT_GHOST_CONCAVE
                    );
        }

        /** Check if a btExchange is dynamic and not static
         */
        static bool isDynamic(const PBulletExchange& e) {
            return isDynamic(e.type);
        }

        /** Check if a BT_Type is dynamic and not static
         */
        static bool isDynamic(const BT_Type& t) {
            return (
                    t == BT_DYNAMIC_BOX ||
                    t == BT_DYNAMIC_SPHERE ||
                    t == BT_DYNAMIC_COMPOUND ||
                    t == BT_DYNAMIC_CONVEX ||
                    t == BT_DYNAMIC_CONCAVE
                    );
        }

        /** Check if a btExchange is static and not dynamic
         */
        static bool isStatic(const PBulletExchange& e) {
            return isStatic(e.type);
        }

        /** Check if a BT_Type is static and not dynamic
         */
        static bool isStatic(const BT_Type& t) {
            return (
                    t == BT_STATIC_BOX ||
                    t == BT_STATIC_SPHERE ||
                    t == BT_STATIC_CONVEX ||
                    t == BT_STATIC_CONCAVE
                    );
        }

        /** Check if a btExchange is a convex hull
         */
        static bool isConvex(const PBulletExchange& e) {
            return isConvex(e.type);
        }

        /** Check if a BT_Type is a convex hull
         */
        static bool isConvex(const BT_Type& t) {
            return (
                    t == BT_DYNAMIC_CONVEX ||
                    t == BT_STATIC_CONVEX ||
                    t == BT_GHOST_CONVEX
                    );
        }

        /** Check if a btExchange is a compound shape
         */
        static bool isCompound(const PBulletExchange& e) {
            return isCompound(e.type);
        }

        /** Check if a BT_Type is a compound shape
         */
        static bool isCompound(const BT_Type& t) {
            return ( 
                    t == BT_DYNAMIC_COMPOUND ||
                    t == BT_STATIC_COMPOUND ||
                    t == BT_GHOST_COMPOUND
                    );
        }

        /** Check if a btExchange is a concave shape, 
            using a btGImpactMeshShape
         */
        static bool isConcave(const PBulletExchange& e) {
            return isConcave(e.type);
        }

        /** Check if a BT_Type is a concave shape, 
            using a btGImpactMeshShape
         */
        static bool isConcave(const BT_Type& t) {
            return (
                    t == BT_DYNAMIC_CONCAVE ||
                    t == BT_STATIC_CONCAVE ||
                    t == BT_GHOST_CONCAVE
                    );
        }

        static bool isBtCube(std::string mat) {
            return (
                    std::strncmp(
                    mat.c_str(),
                    mat_prefix_box.c_str(),
                    mat_prefix_cube_l
                    ) == 0
                    );
        }

        static bool isBtSphere(std::string mat) {
            return (
                    std::strncmp(
                    mat.c_str(),
                    mat_prefix_sphere.c_str(),
                    mat_prefix_sphere_l
                    ) == 0
                    );
        }

        static bool isBtCylinder(std::string mat) {
            return (
                    std::strncmp(
                    mat.c_str(),
                    mat_prefix_cylinder.c_str(),
                    mat_prefix_cylinder_l
                    ) == 0
                    );
        }

        static bool isBtCapsule(std::string mat) {
            return (
                    std::strncmp(
                    mat.c_str(),
                    mat_prefix_capsule.c_str(),
                    mat_prefix_capsule_l
                    ) == 0
                    );
        }

        static bool isBtCone(std::string mat) {
            return (
                    std::strncmp(
                    mat.c_str(),
                    mat_prefix_cone.c_str(),
                    mat_prefix_cone_l
                    ) == 0
                    );
        }


        // from http://www.ogre3d.org/tikiwiki/RetrieveVertexData
        static void meshData(
                const Ogre::Mesh * const mesh,
                btOgreMeshData& mdata,
                const Ogre::Vector3 &position = Ogre::Vector3::ZERO,
                const Ogre::Quaternion &orient = Ogre::Quaternion::IDENTITY,
                const Ogre::Vector3 &scale = Ogre::Vector3::UNIT_SCALE
                );

        static void entityMaterials(
                const Ogre::Entity* entity,
                std::string*& matgroups,
                std::string*& matnames,
                uint& matnum
                );

        static void subentityMaterial(
                const Ogre::SubEntity* se,
                std::string& matgroup,
                std::string& matname
                );

        static void meshToBtBox(
                btOgreMeshData* md,
                int subshape,
                btBoxShape*& box,
                btTransform& transform,
                btScalar& volume
                );

        static void meshToBtSphere(
                btOgreMeshData* md,
                int subshape,
                btSphereShape*& sphere,
                btTransform& transform,
                btScalar& volume
                );

        static void meshToBtCylinder(
                btOgreMeshData* md,
                int subshape,
                btCylinderShape*& cylinder,
                btTransform& transform,
                btScalar& volume
                );

        static void meshToBtCapsule(
                btOgreMeshData* md,
                int subshape,
                btCapsuleShape*& capsule,
                btTransform& transform,
                btScalar& volume
                );

        static void meshToBtCone(
                btOgreMeshData* md,
                int subshape,
                btConeShape*& cone,
                btTransform& transform,
                btScalar& volume
                );

        // debug materials
        static Ogre::MaterialPtr materialDebugNode();
        static Ogre::MaterialPtr materialDebugBone();
        static Ogre::MaterialPtr materialDebugLight();
        static Ogre::MaterialPtr materialDebugSoundOn();
        static Ogre::MaterialPtr materialDebugSoundOff();
        static Ogre::MaterialPtr materialAxisX();
        static Ogre::MaterialPtr materialAxisY();
        static Ogre::MaterialPtr materialAxisZ();

        // lights
        static std::string lightTypeToString(int& t);

        // sounds
        //        static std::string soundTypeToString( int& t );

        //slerpers
        static Ogre::Vector2 slerp(
                Ogre::Vector2 from,
                Ogre::Vector2 to,
                Ogre::Real percent);

        static Ogre::Vector3 slerp(
                Ogre::Vector3 from,
                Ogre::Vector3 to,
                Ogre::Real percent);

        // XYZ axis create and delete
        static bool initXYZ(XYZaxis& xyz);

        static bool createXYZ(
                XYZaxis& xyz,
                Ogre::SceneManager* sm,
                Ogre::SceneNode* parent,
                Ogre::Real length, Ogre::Real thickness
                );

        static bool destroyXYZ(XYZaxis& xyz);

        // creation of a mesh to debug camera
        static Ogre::String cameraDebugMesh();

        static Ogre::Vector3 eulers(const Ogre::Quaternion& q) {
            Ogre::Euler e(q);
            Ogre::Vector3 out(
                    e.pitch().valueDegrees(),
                    e.yaw().valueDegrees(),
                    e.roll().valueDegrees());
            return out;
        }

        static Ogre::Quaternion fromEulers(const Ogre::Vector3& v) {
            Ogre::Vector3 tmp(v);
            tmp *= Ogre::Math::PI / 180; // degrees to radians
            Ogre::Euler e(tmp.y, tmp.x, tmp.z);
            return e.toQuaternion();
        }

        static std::string timestamp() {
            struct tm *pTime;
            time_t ctTime;
            time(&ctTime);
            pTime = localtime(&ctTime);
            Ogre::StringStream oss;
            oss << std::setw(2) << std::setfill('0') << (pTime->tm_mon + 1)
                    << std::setw(2) << std::setfill('0') << pTime->tm_mday
                    << std::setw(2) << std::setfill('0') << (pTime->tm_year + 1900)
                    << "_" << std::setw(2) << std::setfill('0') << pTime->tm_hour
                    << std::setw(2) << std::setfill('0') << pTime->tm_min
                    << std::setw(2) << std::setfill('0') << pTime->tm_sec
                    << std::setw(3) << std::setfill('0') <<
                    (Ogre::Root::getSingleton().getTimer()->getMilliseconds() % 1000);
            return oss.str();
        }

        // log

        static void log(
                const Ogre::String& msg,
                Ogre::LogMessageLevel lml = Ogre::LML_NORMAL,
                bool maskDebug = false
                ) {
            Ogre::LogManager::getSingleton().logMessage(
                    msg, lml, maskDebug);
        }

        static void logCritical(
                const Ogre::String& msg,
                bool maskDebug = false) {
            Ogre::LogManager::getSingleton().logMessage(
                    msg, Ogre::LML_CRITICAL, maskDebug);
        }

        static void logNormal(
                const Ogre::String& msg,
                bool maskDebug = false) {
            Ogre::LogManager::getSingleton().logMessage(
                    msg, Ogre::LML_NORMAL, maskDebug);
        }

        static void logTrivial(
                const Ogre::String& msg,
                bool maskDebug = false) {
            Ogre::LogManager::getSingleton().logMessage(
                    msg, Ogre::LML_TRIVIAL, maskDebug);
        }

        static void logDetail(Ogre::LoggingLevel ll) {
            Ogre::LogManager::getSingleton().setLogDetail(ll);
        }

        /* Returns a 2d position in screen space of the position in the 3d
         * space. If Ogre::RenderWindow* is defined, the position is rendered 
         * in pixels, if not, it is mapped to [0,1] without clamping.
         * The 0,0 corresponds to top-left corner of the frustum, 1,1 
         * is the bottom-right one.
         * @param
         *      Camera to project to.
         * @param
         *      3d vector representing the position to project in 2d
         * @param
         *      Pointer to a render window, usually the one bounded to camera 
         *      (not mandatory). If not defined, vector2 is normalised to 
         *      camera's frustum.
         * @remark
         *      This method is not verifying if the point is in front or
         *      behind the camera. See http://www.ogre3d.org/tikiwiki/tiki-index.php?page=Projecting+3D+position+and+size+to+2D
         *      for this kind of implementation.
         */
        static Ogre::Vector2 projectOnScreen(
                Ogre::Camera* cam,
                const Ogre::Vector3 absolute_position,
                Ogre::RenderWindow* window = 0
                ) {

            if (!cam) {
                return Ogre::Vector2();
            }
            Ogre::Vector3 eyepos =
                    cam->getViewMatrix(true) *
                    absolute_position;
            Ogre::Vector3 screenpos = cam->getProjectionMatrix() * eyepos;
            Ogre::Vector2 pos2d(screenpos.x, screenpos.y);
            pos2d = Ogre::Vector2::UNIT_SCALE + pos2d * Ogre::Vector2(1, -1);
            pos2d *= 0.5;
            if (window) {
                return pos2d *
                        Ogre::Vector2(
                        window->getWidth(),
                        window->getHeight());
            }
            return pos2d;

        }

    };

};

#endif /* POLYMORPH_UTIL_H */

