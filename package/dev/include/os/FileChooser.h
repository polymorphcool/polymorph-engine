/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2017 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
 */

/* 
 * File:   FileChooser.h
 * Author: frankiezafe
 *
 * Created on February 6, 2017, 6:27 PM
 */

#ifndef POLYMORPH_FILECHOOSER_H
#define POLYMORPH_FILECHOOSER_H

#include "Ogre.h"

#ifdef OGRE_PLATFORM_LINUX
#include <gtk/gtk.h>
#endif

namespace polymorph {

    struct FileChooserResult {
        bool success;
        std::string folder;
        std::string file;
        std::vector< std::string > files;

        void init() {
            success = false;
            folder = "";
            file = "";
        }
    };

    class FileChooser {
    public:

        static void open(
                FileChooserResult& r,
                const std::string& path = "",
                const std::string& title = "");

        static void openDir(
                FileChooserResult& r,
                const std::string& path = "",
                const std::string& title = "");

        static void openOgreExport(
                FileChooserResult& r,
                const std::string& path = "",
                const std::string& title = "");

    protected:

    };

};

#endif /* POLYMORPH_FILECHOOSER_H */

