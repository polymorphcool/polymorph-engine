/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
*/

/* 
 * File:   InputCommon.h
 * Author: frankiezafe
 *
 * Created on August 25, 2016, 6:36 PM
 */

#ifndef INPUTCOMMON_H
#define INPUTCOMMON_H

#include "Ogre.h"
#include "SDL2/SDL.h"

namespace input {

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	typedef unsigned int uint;
#endif
    
    enum InputStatus {
        IS_UNDEFINED =      -1,
        IS_INACTIVE =       0,
        IS_ACTIVATED =      1,
        IS_ACTIVE =         2,
        IS_DEACTIVATED =    3,
        IS_MODIFIED =       4 // only for axis
    };
    
    struct GameControllerConfig {
        int16_t sticks_divider;
        int16_t sticks_min_delta;
        int16_t dead_sticks_divider;
        int16_t dead_zone_sticks;
        bool strict;        // enable for constant 0 in dead zone
    };
    
    enum InputRequestType {
        IRT_UNDEFINED =     -1, 
        IRT_CONNECTION =    0,
        IRT_DELETION =      1,
        IRT_VALIDATION =    2
    };
    
    struct InputRequest {
        InputRequestType type;
        int16_t gamecontroller; // -1 by default
        int16_t joystick;       // -1 by default
        int16_t keyboard;       // -1 by default
        int16_t mouse;          // -1 by default
        InputRequest(): 
            type( IRT_UNDEFINED ), 
            gamecontroller( -1 ),
            joystick( -1 ),
            keyboard( -1 ),
            mouse( -1 ) {}
        inline void operator = ( const InputRequest& src ) {
            type = src.type;
            gamecontroller = src.gamecontroller;
            joystick = src.joystick;
            keyboard = src.keyboard;
            mouse = src.mouse;
        }
    };
    typedef std::vector<InputRequest> IRList;
    typedef std::vector<InputRequest>::iterator IRListIter;
    typedef std::vector< Sint32 > SDLList;
    typedef std::vector< Sint32 >::iterator SDLListIter;
    
    enum GameControllerAxis {
        GCB_LEFTSTICK =     0,
        GCB_RIGHTSTICK =    1,
        GCB_TRIGGER =       2,
        GCB_MAX =           3
    };
    
    // used by InputManager
    class GameController;
    typedef std::map< Sint32, input::GameController*> GCMap;
    typedef std::map< Sint32, input::GameController*>::iterator GCMapIter;
    typedef std::map< Sint32, bool> GCEvent;
    typedef std::map< Sint32, bool>::iterator GCEventIter;
    typedef std::vector<input::GameController*> GCList;
    typedef std::vector<input::GameController*>::iterator GCListIter;
    
    // used in InputFrame
    class GameControllerFrame;
    typedef std::vector<GameControllerFrame> GCFList;
    typedef std::vector<GameControllerFrame>::iterator GCFListIter;
    
    // used in GameController & GameControllerFrame
    typedef std::map< GameControllerAxis, Ogre::Vector2 > AxisMap;
    typedef std::map< GameControllerAxis, Ogre::Vector2 >::iterator AxisIter;
    typedef std::map< SDL_GameControllerButton, bool > ButtonMap;
    typedef std::map< SDL_GameControllerButton, bool >::iterator ButtonMapIter;
    
    typedef std::map< GameControllerAxis, InputStatus > AxisStatus;
    typedef std::map< SDL_GameControllerButton, InputStatus > ButtonStatus;
    
};

#endif /* INPUTCOMMON_H */

