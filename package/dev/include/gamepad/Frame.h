/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
*/

/* 
 * File:   Frame.h
 * Author: frankiezafe
 *
 * Created on August 25, 2016, 3:10 PM
 */

#ifndef INPUT_FRAME_H
#define INPUT_FRAME_H

#include "Ogre.h"
#include <algorithm>

#include "InputCommon.h"

namespace input {
    
    class GameControllerFrame {
    public:
        
        uint id;
        InputStatus status;
        AxisMap axis;
        AxisMap axis_delta;         // only computed for externals frames
        ButtonMap buttons;
        AxisStatus axis_status;     // only computed for externals frames
        ButtonStatus button_status; // only computed for externals frames
        GameControllerFrame():
        id(0), status( IS_UNDEFINED )
        {}
        GameControllerFrame( const GameControllerFrame& src ) {
            (*this) = src;
        }
        ~GameControllerFrame() {}        
        inline void operator = ( const GameControllerFrame& src ) {
            id = src.id;
            status = src.status;
            axis = src.axis;
            buttons = src.buttons;
        }
        void init();
        void compare( GameControllerFrame& previous );
        
    };
    
    class DataFrame {
    
    public:
        
        // disable history when you don't need to compute anything
        DataFrame( bool enable_history = true );
        virtual ~DataFrame() {}
        inline void operator << ( const DataFrame& src ) {
            gcframes = src.gcframes;
        }
        GCFList gcframes;
        
        void store();
        void compute();
        
        GameControllerFrame* getGameController( uint id ) {
            GCFListIter it = gcframes.begin();
            GCFListIter it_end = gcframes.end();
            GameControllerFrame* output = 0;
            for ( ; it != it_end; ++it ) {
                if ( (*it).id == id ) {
                    output = &(*it);
                    break;
                }
            }
            return output;
        }
        
    private:
        bool use_history;
        GCFList previous_gcframes;    
    };

};
    
#endif /* INPUT_FRAME_H */

