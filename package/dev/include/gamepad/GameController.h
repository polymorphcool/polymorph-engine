/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
*/

/* 
 * File:   GameController.h
 * Author: frankiezafe
 *
 * Created on August 25, 2016, 2:32 PM
 */

#ifndef GAMECONTROLLER_H
#define GAMECONTROLLER_H

#include "PCommon.h"
#include "InputCommon.h"
#include "Frame.h"

namespace input {
    
    class GameController {
    public:
        
        GameController( SDL_GameController* gc, polymorph::uint id );
        virtual ~GameController();
        
        inline uint getID() { return _id; }
        inline void setConfig( GameControllerConfig* gcconf ) { _gcconf = gcconf; }
        bool update( GameControllerFrame* data );
        
    private:
        
        SDL_GameController* _gc;
        GameControllerConfig* _gcconf;
        uint _id;
        bool _active;
        bool _init;
        bool _modified;
        bool leftstick_move;
        bool rightstick_move;
        
        AxisMap axis;
        AxisMap dead_axis_previous;
        AxisMap norm_axis;
        
        ButtonMap buttons;
        
        void updateAxis( GameControllerAxis gca );
        void normAxis( GameControllerAxis gca );
        void normDeadAxis( GameControllerAxis gca );
        
    };
    
};

#endif /* GAMECONTROLLER_H */

