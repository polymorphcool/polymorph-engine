/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
*/

/* 
 * File:   InputManager.h
 * Author: frankiezafe
 *
 * Created on August 25, 2016, 2:07 PM
 */

#ifndef SDLINPUTMANAGER_H
#define SDLINPUTMANAGER_H

#include <iostream>
#include <limits>
#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/random/linear_congruential.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/generator_iterator.hpp>
#include <boost/date_time.hpp>
#include <boost/asio.hpp>

#include "InputCommon.h"
#include "Frame.h"
#include "GameController.h"

namespace input {

    class InputManager {
        
    public:
        
        static void setVebose( bool v );
        static bool isGameControllerAvailable( uint id );
        static void openGameController( uint id = 0 );
        static void closeGameController( uint id = 0 );
        static void getInputFrame( input::DataFrame* dst );
        static std::string toString( InputStatus s );
        
    private:
        
        static InputManager* get();
        static void before();
        static void after();
        
        InputManager();
        virtual ~InputManager();
        
        // thread related
        boost::shared_ptr<boost::thread> bthread;
        boost::mutex bmutex;
        boost::mutex read_mutex;
        boost::mutex request_mutex;
        bool runs;
        bool verbose;
        void runThread();
        
        boost::unique_lock<boost::mutex>* _lock; 
        boost::unique_lock<boost::mutex>* _read_lock; 
        boost::unique_lock<boost::mutex>* _request_lock; 
        void lock();
        void unlock();
        void read_lock();
        void read_unlock();
        void request_lock();
        void request_unlock();
        void syncFrame();
        
        // gamecontrollers
        GameControllerConfig gcconfiguration;
        GameControllerConfig* gcconf_ptr;
        GCMap gcs;
        GCList gc_list;
        GCEvent gce; // used to trigger update
        
        // frame data
        input::DataFrame* frame_external;
        input::DataFrame* frame_internal;
        
        // time      
        boost::posix_time::ptime _timestart;
        boost::posix_time::ptime _timecurrent;
        double _innertime;
        double _innertime_since_start;
        double _innertime_last;
        double _innertime_delta;
        void now();
        
        // request
        IRList request_new;
        IRList request_idle;
        IRList request_toexec;
        IRList request_failed;
        // never call this outsied of runThread or all heaven will break loose!
        void requests_processor();
        bool requests_connection( const InputRequest& ir );
        bool requests_validation( const InputRequest& ir );
        bool requests_deletion( const InputRequest& ir );
        
        SDL_Event device_event;
        SDLList available_gamecontrollers;
        SDLList available_joysticks;
        
    };
    
};

#endif /* SDLINPUTMANAGER_H */

