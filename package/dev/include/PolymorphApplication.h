/*
-----------------------------------------------------------------------------
Filename:    CMakeLists.txt
-----------------------------------------------------------------------------

 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      
 

 art & game engine
 
____________________________________  ?   ____________________________________
                                    (._.)
 
 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
___________________________________( ^3^)_____________________________________
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
*/

#ifndef __ExampleApplication_H__
#define __ExampleApplication_H__

#include "Ogre.h"
#include "OgreOverlaySystem.h"
#include "OgreConfigFile.h"
#include "OgreWindowEventUtilities.h"
#include "Compositor/OgreCompositorManager2.h"
#include "Compositor/OgreCompositorWorkspace.h"
#include "Compositor/OgreCompositorWorkspaceDef.h"
#include "Compositor/OgreCompositorNodeDef.h"
#include "Compositor/Pass/OgreCompositorPassDef.h"
#include "Compositor/Pass/PassScene/OgreCompositorPassSceneDef.h"
#include "Compositor/Pass/PassClear/OgreCompositorPassClearDef.h"
#include "Compositor/Pass/PassQuad/OgreCompositorPassQuadDef.h"
#include "Compositor/OgreCompositorShadowNode.h"

#include "OgreSubMesh.h"
#include "OgreResourceGroupManager.h"

#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE

#include <OIS/OISEvents.h>
#include <OIS/OISInputManager.h>
#include <OIS/OISKeyboard.h>
#include <OIS/OISMouse.h>
#include <OGRE/SdkTrays.h>

#elif OGRE_PLATFORM == OGRE_PLATFORM_ANDROID

#include <android_native_app_glue.h>
#include "Android/OgreAPKFileSystemArchive.h"
#include "Android/OgreAPKZipArchive.h"
#include "OISEvents.h"
#include "OISInputManager.h"
#include "OISKeyboard.h"
#include "OISMouse.h"
#include "SdkTrays.h"

#else

#include "OISEvents.h"
#include "OISInputManager.h"
#include "OISKeyboard.h"
#include "OISMouse.h"
#include "SdkTrays.h"

#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
#include <CoreFoundation/CoreFoundation.h>
// This function will locate the path to our application on OS X,
// unlike windows you can not rely on the curent working directory
// for locating your configuration files and resources.
std::string macBundlePath() {
    char path[1024];
    CFBundleRef mainBundle = CFBundleGetMainBundle();
    assert(mainBundle);
    CFURLRef mainBundleURL = CFBundleCopyBundleURL(mainBundle);
    assert(mainBundleURL);
    CFStringRef cfStringRef = CFURLCopyFileSystemPath(mainBundleURL, kCFURLPOSIXPathStyle);
    assert(cfStringRef);
    CFStringGetCString(cfStringRef, path, 1024, kCFStringEncodingASCII);
    CFRelease(mainBundleURL);
    CFRelease(cfStringRef);
    return std::string(path);
}
#endif

#include "Polymorph.h"
#include "Compositor/Pass/PassQuad/OgreCompositorPassQuad.h"

using namespace Ogre;

/** Base class which manages the standard startup of an Ogre application.
    Designed to be subclassed for specific examples if required.
 */
class PolymorphApplication : 
    public Ogre::FrameListener, 
    public Ogre::WindowEventListener, 
    public OIS::KeyListener, 
    public OIS::MouseListener, 
    public OgreBites::SdkTrayListener
{
public:
    /// Standard constructor

    PolymorphApplication();
    virtual ~PolymorphApplication();
    virtual void go(void);

protected:
    
    polymorph::PApplicationConfiguration appConfig;
    
    // dedicated to overwrite
    virtual void before_draw();
    virtual void draw(); // called at each frame
    virtual void after_draw();
    
    Root *                      root;
    Camera*                     cam;
    CompositorManager2*         compositorMgr;
    SceneManager*               sceneMgr;
    RenderWindow*               window;
    Ogre::String                resourcePath;
    FrameEvent                  draw_event;
    Ogre::OverlaySystem*        overlaySys;
    
    // compositors
    Ogre::String                compositorwpname;
    CompositorWorkspace*        compositorwp;
    
    // OgreBites
    OgreBites::SdkTrayManager*  trayMgr;
    OgreBites::InputContext     inputContext;
    
    // OIS Input devices
    OIS::InputManager*          inputMgr;
    OIS::Mouse*                 mouse;
    OIS::Keyboard*              keyboard;
        
    // picking objects
    Ogre::SceneNode*            overed;
    Ogre::String                overed_mat_name;
    Ogre::String                picked_mat_name;
    
    bool                        shutdownRequested;
    
    std::string                 window_title;
            
    virtual bool setup(void);
    virtual void setupResources(void);
    virtual void setupResources( polymorph::PProjectData& ppd );
    virtual bool configure(void);
    virtual void loadResources(void);
    virtual void createSceneManager(void);
    virtual void createCamera(void);
    virtual void createCompositor(void);
    virtual bool createCompositor( 
        polymorph::PCompositorWSData* compdata,
        Ogre::SceneManager* sm,
        Ogre::RenderWindow* win,
        Ogre::Camera* cam
        );
    virtual void createResourceListener(void);
    virtual void createInputListener(void);
    virtual void createFrameListener(void);
    virtual void createTray(void);
    virtual void createScene(void);
    
    virtual void destroyScene(void);
    
    // SdkTrayListener methods
    virtual void buttonHit(OgreBites::Button* button);
    virtual void itemSelected(OgreBites::SelectMenu* menu);
    virtual void labelHit(OgreBites::Label* label);
    virtual void sliderMoved(OgreBites::Slider* slider);
    virtual void checkBoxToggled(OgreBites::CheckBox* box);
    virtual void okDialogClosed(const Ogre::DisplayString& message);
    virtual void yesNoDialogClosed(const Ogre::DisplayString& question, bool yesHit);
    
    // KeyListener & MouseListener methods
    virtual bool keyPressed(const OIS::KeyEvent &arg);
    virtual bool keyReleased(const OIS::KeyEvent &arg);
    virtual bool mouseMoved(const OIS::MouseEvent &arg);
    virtual bool mousePressed(const OIS::MouseEvent &arg, OIS::MouseButtonID id);
    virtual bool mouseReleased(const OIS::MouseEvent &arg, OIS::MouseButtonID id);
#if OIS_WITH_MULTITOUCH
    virtual bool touchMoved(const OIS::MultiTouchEvent& evt);
    virtual bool touchPressed(const OIS::MultiTouchEvent& evt);
    virtual bool touchReleased(const OIS::MultiTouchEvent& evt);
    virtual bool touchCancelled(const OIS::MultiTouchEvent &evt);
#endif
    virtual void windowResized(Ogre::RenderWindow* rw);
    virtual void windowClosed(Ogre::RenderWindow* rw);
    
    // picking objects
    virtual void deselectObject();
    virtual bool pickObject( const OIS::MouseEvent &arg, OIS::MouseButtonID id );
    
    // FrameListener methods
    virtual bool frameStarted( const FrameEvent& evt );
    virtual bool frameRenderingQueued( const FrameEvent& evt );
    virtual bool frameEnded( const FrameEvent& evt );
    
    virtual void fit( Ogre::Camera * c, Ogre::Viewport * vp );
    virtual void fit( Ogre::Camera * c, Ogre::RenderWindow * rw );
    
    virtual void rebuildCompositorConnections();
    
    // debugs
    virtual Ogre::FileInfoList getFileList( Ogre::String group, bool print_to_console = false );
    virtual Ogre::StringVector getResourceList( Ogre::String group, bool print_to_console = false );
    
};

#endif

