# Locate Polymorph.
#
# use this to import all polymoprh addons at once
# there will be conflict with ogre dependencies!
#
# This script defines:
#  Polymorph_FOUND              	- True if Polymorph was found on your system
#  Polymorph_INCLUDE_DIR       	- List of directories of Polymorph and it's dependencies
#  Polymorph_LIBRARY       		- List of directories containing Polymorph' libraries

include(FindPkgMacros)

findpkg_begin(Polymorph)

getenv_path(OGRE_HOME)

#
# BOOST ###############################################################

if (NOT OGRE_BUILD_PLATFORM_IPHONE)
    if (WIN32 OR APPLE)
            set(Boost_USE_STATIC_LIBS TRUE)
    else ()
            # Statically linking boost to a dynamic Ogre build doesn't work on Linux 64bit
            set(Boost_USE_STATIC_LIBS ${OGRE_STATIC})
    endif ()
    if (MINGW)
            # this is probably a bug in CMake: the boost find module tries to look for
            # boost libraries with name libboost_*, but CMake already prefixes library
            # search names with "lib". This is the workaround.
            set(CMAKE_FIND_LIBRARY_PREFIXES ${CMAKE_FIND_LIBRARY_PREFIXES} "")
    endif ()
    set(Boost_ADDITIONAL_VERSIONS "1.44" "1.44.0" "1.42" "1.42.0" "1.41.0" "1.41" "1.40.0" "1.40" "1.39.0" "1.39" "1.38.0" "1.38" "1.37.0" "1.37" )
    # Components that need linking (NB does not include header-only components like bind)
    set(OGRE_BOOST_COMPONENTS thread date_time)
    find_package(Boost COMPONENTS ${OGRE_BOOST_COMPONENTS} QUIET)
    if (NOT Boost_FOUND)
            # Try again with the other type of libs
            set(Boost_USE_STATIC_LIBS NOT ${Boost_USE_STATIC_LIBS})
            find_package(Boost COMPONENTS ${OGRE_BOOST_COMPONENTS} QUIET)
    endif()
    find_package(Boost QUIET)
    # Set up referencing of Boost
#	include_directories(${Boost_INCLUDE_DIR})
#	add_definitions(-DBOOST_ALL_NO_LIB)
#	set(OGRE_LIBRARIES ${OGRE_LIBRARIES} ${Boost_LIBRARIES})
endif()

#
# BULLET ###############################################################

set(Bullet_PREFIX_PATH ${OGRE_HOME})
set(Bullet_INCLUDE_SEARCH_DIR ${Bullet_PREFIX_PATH}/include/bullet/)
set(Bullet_LIBRARY ${Bullet_PREFIX_PATH}/lib/)

# bullet includes

find_path(Bullet_INCLUDE_COLLISION_DIR btBulletCollisionCommon.h PATHS ${Bullet_INCLUDE_SEARCH_DIR})
if ( Bullet_INCLUDE_COLLISION_DIR )
	set(Bullet_INCLUDE_COLLISION_DIR ${Bullet_INCLUDE_SEARCH_DIR}/BulletCollision)
	set(Bullet_INCLUDE_DYNAMICS_DIR ${Bullet_INCLUDE_SEARCH_DIR}/BulletDynamics)
endif( Bullet_INCLUDE_COLLISION_DIR )
find_path(Bullet_INCLUDE_SOFTBODY_DIR btSoftBody.h PATHS ${Bullet_INCLUDE_SEARCH_DIR}/BulletSoftBody)
if ( Bullet_INCLUDE_SOFTBODY_DIR )
	set(Bullet_INCLUDE_SOFTBODY_DIR ${Bullet_INCLUDE_SEARCH_DIR}/BulletSoftBody)
endif( Bullet_INCLUDE_SOFTBODY_DIR )
find_path(CONVEXDECOMPOSITION_INCLUDE_DIR ConvexDecomposition.h PATHS ${Bullet_INCLUDE_SEARCH_DIR}/ConvexDecomposition)
if ( CONVEXDECOMPOSITION_INCLUDE_DIR )
	set(CONVEXDECOMPOSITION_INCLUDE_DIR ${Bullet_INCLUDE_SEARCH_DIR}/ConvexDecomposition)
endif( CONVEXDECOMPOSITION_INCLUDE_DIR )
find_path(GIMPACTUTILS_INCLUDE_DIR btGImpactConvexDecompositionShape.h PATHS ${Bullet_INCLUDE_SEARCH_DIR}/GIMPACTUtils)
if ( GIMPACTUTILS_INCLUDE_DIR )
	set(GIMPACTUTILS_INCLUDE_DIR ${Bullet_INCLUDE_SEARCH_DIR}/GIMPACTUtils)
endif( GIMPACTUTILS_INCLUDE_DIR )
find_path(HACD_INCLUDE_DIR hacdHACD.h PATHS ${Bullet_INCLUDE_SEARCH_DIR}/HACD)
if ( HACD_INCLUDE_DIR )
	set(HACD_INCLUDE_DIR ${Bullet_INCLUDE_SEARCH_DIR}/HACD)
endif( HACD_INCLUDE_DIR )
find_path(INVERSEDYNAMICS_INCLUDE_DIR BulletInverseDynamicsUtilsCommon.h PATHS ${Bullet_INCLUDE_SEARCH_DIR}/InverseDynamics)
if ( INVERSEDYNAMICS_INCLUDE_DIR )
	set(INVERSEDYNAMICS_INCLUDE_DIR ${Bullet_INCLUDE_SEARCH_DIR}/InverseDynamics)
endif( INVERSEDYNAMICS_INCLUDE_DIR )
find_path(LINEARMATH_INCLUDE_DIR btMatrix3x3.h PATHS ${Bullet_INCLUDE_SEARCH_DIR}/LinearMath)
if ( LINEARMATH_INCLUDE_DIR )
	set(LINEARMATH_INCLUDE_DIR ${Bullet_INCLUDE_SEARCH_DIR}/LinearMath)
endif( LINEARMATH_INCLUDE_DIR )

# bullet libs

find_library(Bullet_COLLISION_LIBRARY BulletCollision HINTS ${Bullet_LIBRARY} PATH_SUFFIXES "")
find_library(Bullet_DYNAMICS_LIBRARY BulletDynamics HINTS ${Bullet_LIBRARY} PATH_SUFFIXES "")
find_library(Bullet_SOFTBODY_LIBRARY BulletSoftBody HINTS ${Bullet_LIBRARY} PATH_SUFFIXES "")
find_library(CONVEXDECOMPOSITION_LIBRARY ConvexDecomposition HINTS ${Bullet_LIBRARY} PATH_SUFFIXES "")
find_library(GIMPACTUTILS_LIBRARY GIMPACTUtils HINTS ${Bullet_LIBRARY} PATH_SUFFIXES "")
find_library(HACD_LIBRARY HACD HINTS ${Bullet_LIBRARY} PATH_SUFFIXES "")
find_library(INVERSEDYNAMICS_LIBRARY BulletInverseDynamicsUtils HINTS ${Bullet_LIBRARY} PATH_SUFFIXES "")
find_library(LINEARMATH_LIBRARY LinearMath HINTS ${Bullet_LIBRARY} PATH_SUFFIXES "")

mark_as_advanced(Bullet_INCLUDE_COLLISION_DIR)
mark_as_advanced(Bullet_INCLUDE_DYNAMICS_DIR)
mark_as_advanced(Bullet_INCLUDE_SOFTBODY_DIR)
mark_as_advanced(Bullet_INCLUDE_CONVEXDECOMPOSITION_DIR)
mark_as_advanced(GIMPACTUTILS_INCLUDE_DIR)
mark_as_advanced(HACD_INCLUDE_DIR)
mark_as_advanced(INVERSEDYNAMICS_INCLUDE_DIR)
mark_as_advanced(LINEARMATH_INCLUDE_DIR)
mark_as_advanced(Bullet_COLLISION_LIBRARY)
mark_as_advanced(Bullet_DYNAMICS_LIBRARY)
mark_as_advanced(Bullet_SOFTBODY_LIBRARY)
mark_as_advanced(CONVEXDECOMPOSITION_LIBRARY)
mark_as_advanced(GIMPACTUTILS_LIBRARY)
mark_as_advanced(HACD_LIBRARY)
mark_as_advanced(INVERSEDYNAMICS_LIBRARY)
mark_as_advanced(LINEARMATH_LIBRARY)

#
# SDL2 ###############################################################

set(SDL2_PREFIX_PATH ${OGRE_HOME})
set(SDL2_INCLUDE_SEARCH_DIR ${SDL2_PREFIX_PATH}/include/SDL2/)
set(SDL2_LIBRARY ${SDL2_PREFIX_PATH}/lib/)

# sdl includes

find_path(SDL2_INCLUDE_MAIN_DIR SDL.h PATHS ${SDL2_INCLUDE_SEARCH_DIR})
if ( SDL2_INCLUDE_MAIN_DIR )
	set(SDL2_INCLUDE_MAIN_DIR ${SDL2_INCLUDE_SEARCH_DIR})
endif( SDL2_INCLUDE_MAIN_DIR )

# sdl lib

find_library(SDL2_MAIN_LIBRARY SDL2 HINTS ${SDL2_LIBRARY} PATH_SUFFIXES "")

mark_as_advanced(SDL2_PREFIX_PATH)
mark_as_advanced(SDL2_INCLUDE_SEARCH_DIR)
mark_as_advanced(SDL2_INCLUDE_MAIN_DIR)
mark_as_advanced(SDL2_MAIN_LIBRARY)
mark_as_advanced(SDL2_LIBRARY)

#
# PD ###############################################################

set(LibPD_PREFIX_PATH ${OGRE_HOME})
set(LibPD_INCLUDE_SEARCH_DIR ${LibPD_PREFIX_PATH}/include/libpd/)
set(LibPD_LIBRARY ${LibPD_PREFIX_PATH}/lib/)

# pd includes

find_path(LibPD_INCLUDE_MAIN_DIR m_pd.h PATHS ${LibPD_INCLUDE_SEARCH_DIR})
if ( LibPD_INCLUDE_MAIN_DIR )
	set(LibPD_INCLUDE_MAIN_DIR ${LibPD_INCLUDE_SEARCH_DIR})
endif( LibPD_INCLUDE_MAIN_DIR )
find_path(LibPD_INCLUDE_UTILS_DIR z_print_util.h PATHS ${LibPD_INCLUDE_SEARCH_DIR}util)
if ( LibPD_INCLUDE_UTILS_DIR )
	set(LibPD_INCLUDE_UTILS_DIR ${LibPD_INCLUDE_SEARCH_DIR}util)
endif( LibPD_INCLUDE_UTILS_DIR )
# find_path(OgrePD_INCLUDE_MAIN_DIR PdEngine.h PATHS ${OgrePD_INCLUDE_SEARCH_DIR})
# if ( OgrePD_INCLUDE_MAIN_DIR )
# 	set(OgrePD_INCLUDE_MAIN_DIR ${OgrePD_INCLUDE_SEARCH_DIR})
# endif( OgrePD_INCLUDE_MAIN_DIR )

# pd libs

find_library(LibPD_MAIN_LIBRARY pd HINTS ${LibPD_LIBRARY} PATH_SUFFIXES "")
find_library(LibPD_CPP_LIBRARY pdcpp HINTS ${LibPD_LIBRARY} PATH_SUFFIXES "")
# find_library(OgrePD_MAIN_LIBRARY OgrePD HINTS ${OgrePD_LIBRARY} PATH_SUFFIXES "")

mark_as_advanced(LibPD_PREFIX_PATH)
mark_as_advanced(LibPD_INCLUDE_SEARCH_DIR)
mark_as_advanced(LibPD_LIBRARY)
mark_as_advanced(LibPD_INCLUDE_MAIN_DIR)
mark_as_advanced(LibPD_INCLUDE_UTILS_DIR)
mark_as_advanced(LibPD_INCLUDE_DIR)
mark_as_advanced(LibPD_MAIN_LIBRARY)
mark_as_advanced(LibPD_CPP_LIBRARY)
mark_as_advanced(LibPD_LIBRARY)
# mark_as_advanced(OgrePD_PREFIX_PATH)
# mark_as_advanced(OgrePD_INCLUDE_SEARCH_DIR)
# mark_as_advanced(OgrePD_INCLUDE_MAIN_DIR)
# mark_as_advanced(OgrePD_INCLUDE_DIR)
# mark_as_advanced(OgrePD_MAIN_LIBRARY)
# mark_as_advanced(OgrePD_LIBRARY)

#
# POLYMORPH ###############################################################

set(Polymorph_PREFIX_PATH ${OGRE_HOME})
set(Polymorph_INCLUDE_SEARCH_DIR ${Polymorph_PREFIX_PATH}/include/OGRE/Polymorph/)
set(Polymorph_LIBRARY ${Polymorph_PREFIX_PATH}/lib/)

# gamepad includes

find_path(Polymorph_INCLUDE_MAIN_DIR Polymorph.h PATHS ${Polymorph_INCLUDE_SEARCH_DIR})
if ( Polymorph_INCLUDE_MAIN_DIR )
	set(Polymorph_INCLUDE_MAIN_DIR ${Polymorph_INCLUDE_SEARCH_DIR})
endif( Polymorph_INCLUDE_MAIN_DIR )

# gamepad libs

find_library(Polymorph_MAIN_LIBRARY Polymorph HINTS ${Polymorph_LIBRARY} PATH_SUFFIXES "")

mark_as_advanced(Polymorph_PREFIX_PATH)
mark_as_advanced(Polymorph_INCLUDE_SEARCH_DIR)
mark_as_advanced(Polymorph_INCLUDE_MAIN_DIR)
mark_as_advanced(Polymorph_MAIN_LIBRARY)

#
# SET VARIABLES ###############################################################

set(Polymorph_INCLUDE_DIR 
	${Bullet_INCLUDE_SEARCH_DIR}
	${Bullet_INCLUDE_COLLISION_DIR}
	${Bullet_INCLUDE_DYNAMICS_DIR}
	${Bullet_INCLUDE_SOFTBODY_DIR}
	${CONVEXDECOMPOSITION_INCLUDE_DIR}
	${GIMPACTUTILS_INCLUDE_DIR}
	${HACD_INCLUDE_DIR}
	${INVERSEDYNAMICS_INCLUDE_DIR}
	${LINEARMATH_INCLUDE_DIR}
	${SDL2_INCLUDE_MAIN_DIR}
	${LibPD_INCLUDE_MAIN_DIR}
	${LibPD_INCLUDE_UTILS_DIR}
	${Polymorph_INCLUDE_MAIN_DIR}
	)
	
set(Polymorph_LIBRARY
	${Bullet_COLLISION_LIBRARY}
	${Bullet_DYNAMICS_LIBRARY}
	${Bullet_SOFTBODY_LIBRARY}
	${CONVEXDECOMPOSITION_LIBRARY}
	${GIMPACTUTILS_LIBRARY}
	${HACD_LIBRARY}
	${INVERSEDYNAMICS_LIBRARY}
	${LINEARMATH_LIBRARY}
	${SDL2_MAIN_LIBRARY}
	${LibPD_MAIN_LIBRARY}
	${LibPD_CPP_LIBRARY}
	${Polymorph_MAIN_LIBRARY}
	)
	
mark_as_advanced(Polymorph_INCLUDE_DIR)
mark_as_advanced(Polymorph_LIBRARY)

make_library_set(Polymorph_LIBRARY)

findpkg_finish(Polymorph)
