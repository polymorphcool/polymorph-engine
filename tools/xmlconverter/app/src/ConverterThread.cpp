/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2017 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
 */

/* 
 * File:   ConverterThread.cpp
 * Author: frankiezafe
 * 
 * Created on February 9, 2017, 9:16 AM
 */

#include "ConverterThread.h"

using namespace Ogre;
//using namespace polymorph;

ConverterThread::ConverterThread( ) :
_file_runs( false ),
_convert_runs( false ),
//_convert_enabled( false ),
_listener( 0 ) {
}

ConverterThread::~ConverterThread( ) {

    stop( );
    clearQueues( );

}

bool ConverterThread::stop( ) {

    _file_runs = false;
    _convert_runs = false;

    std::cout << "stopping file thread" << std::endl;
    if ( !_file_runs ) return false;
    assert( file_thread );
    file_thread->join( );
    std::cout << "file thread stopped" << std::endl;

    if ( _convert_runs ) {
        std::cout << "stopping conversion thread" << std::endl;
        assert( convert_thread );
        convert_thread->join( );
        std::cout << "conversion thread stopped" << std::endl;
    }

    return true;

}

bool ConverterThread::start( const ConverterPaths& path ) {

    if ( _file_runs ) return false;
    
    if ( this->paths.hotfolder.compare( path.hotfolder ) != 0 ) {
        clearQueues( );
    }
    if ( this->paths.output_model.compare( path.output_model ) != 0 ) {
        clearMesh( );
    }
    if ( this->paths.output_skeleton.compare( path.output_skeleton ) != 0 ) {
        clearSkeleton( );
    }
    if ( this->paths.output_material.compare( path.output_material ) != 0 ) {
        clearMaterial( );
    }
    if ( this->paths.output_texture.compare( path.output_texture ) != 0 ) {
        clearImage( );
    }

    this->paths.output_model = path.output_model;
    this->paths.output_skeleton = path.output_skeleton;
    this->paths.output_material = path.output_material;
    this->paths.output_texture = path.output_texture;
    this->paths.hotfolder = path.hotfolder;
    this->paths.temporary = path.temporary;

    assert( !file_thread );
    _file_runs = true;
    file_thread = boost::shared_ptr<boost::thread>(
            new boost::thread( boost::bind( &ConverterThread::runFile, this ) ) );

    return true;

}

void ConverterThread::check_file( CFList* q, ConverterFile* cf ) {

    if ( !cf->modified ) return;

    cf->modified = false;

    if ( !cf->done ) {
        cf->message.clear( );
    }

    if ( cf->output_missing ) {
    
        broadcastEvent( CE_DST_MISSING, cf, "destination missing" );
        
        if ( !cf->pushed ) {
            q->push_back( cf );
            return;
        }
    
    } else if ( cf->src_missing ) {

        broadcastEvent( CE_SRC_MISSING, cf, "source missing" );
        return;
    
    }

    if ( cf->done ) {
        
        broadcastEvent( CE_DONE, cf, cf->message );
        return;
        
    }

    if ( !cf->pushed ) {
        
        broadcastEvent( CE_CONVERSION, cf, "conversion" );        
        q->push_back( cf );
        return;
        
    }
    
    broadcastEvent( CE_MODIFIED, cf, cf->message );

}

void ConverterThread::runFile( ) {

    while ( _file_runs ) {

        boost::unique_lock<boost::mutex>* lock =
                new boost::unique_lock<boost::mutex>( _file_mutex );

        if ( !collectFiles( ) ) {
            _file_runs = false;
            return;
        }

        CFListIter it;
        CFListIter ite;

        // avoiding to block the conversion thread if nothing to do
        CFList mesh_to_push;
        CFList skel_to_push;
        CFList res_to_copy;

        it = queue_mesh.begin( );
        ite = queue_mesh.end( );
        for (; it != ite; ++it ) check_file( &mesh_to_push, ( *it ) );

        it = queue_skeleton.begin( );
        ite = queue_skeleton.end( );
        for (; it != ite; ++it ) check_file( &skel_to_push, ( *it ) );

        it = queue_mat.begin( );
        ite = queue_mat.end( );
        for (; it != ite; ++it ) check_file( &res_to_copy, ( *it ) );

        it = queue_img.begin( );
        ite = queue_img.end( );
        for (; it != ite; ++it ) check_file( &res_to_copy, ( *it ) );

        // new files to process!
        if (
                !mesh_to_push.empty( ) ||
                !skel_to_push.empty( ) ||
                !res_to_copy.empty( )
                ) {

            boost::unique_lock<boost::mutex>* c_lock =
                    new boost::unique_lock<boost::mutex>( _convert_mutex );

            it = mesh_to_push.begin( );
            ite = mesh_to_push.end( );
            for (; it != ite; ++it ) push_convert( &convert_mesh, ( *it ) );

            it = skel_to_push.begin( );
            ite = skel_to_push.end( );
            for (; it != ite; ++it ) push_convert( &convert_skeleton, ( *it ) );

            it = res_to_copy.begin( );
            ite = res_to_copy.end( );
            for (; it != ite; ++it ) push_convert( &convert_copies, ( *it ) );

            delete c_lock;

            if ( !_convert_runs ) startConversion( );

        }

        if (
                !_convert_runs &&
                (
                !convert_mesh.empty( ) ||
                !convert_skeleton.empty( ) ||
                !convert_copies.empty( )
                )
                ) {

            clearConverts( );
            broadcastEvent( CE_FINISHED );

        }

        delete lock;

        boost::this_thread::sleep( boost::posix_time::milliseconds( 1000 ) );

    }

}

void ConverterThread::clearQueues( ) {

    clearMesh( );
    clearSkeleton( );
    clearMaterial( );
    clearImage( );
    clearConverts( );

}

void ConverterThread::clearMesh( ) {

    CFListIter it = queue_mesh.begin( );
    CFListIter ite = queue_mesh.end( );
    for (; it != ite; ++it ) {
        delete (*it );
    }
    queue_mesh.clear( );

}

void ConverterThread::clearSkeleton( ) {

    CFListIter it = queue_skeleton.begin( );
    CFListIter ite = queue_skeleton.end( );
    for (; it != ite; ++it ) {
        delete (*it );
    }
    queue_skeleton.clear( );

}

void ConverterThread::clearMaterial( ) {

    CFListIter it = queue_mat.begin( );
    CFListIter ite = queue_mat.end( );
    for (; it != ite; ++it ) {
        delete (*it );
    }
    queue_mat.clear( );

}

void ConverterThread::clearImage( ) {

    CFListIter it = queue_img.begin( );
    CFListIter ite = queue_img.end( );
    for (; it != ite; ++it ) {
        delete (*it );
    }
    queue_img.clear( );

}

void ConverterThread::clearConverts( ) {

    convert_mesh.clear( );
    convert_skeleton.clear( );
    convert_copies.clear( );

}

bool ConverterThread::collectFiles( ) {

    std::string s;

    if ( !dirExists( paths.hotfolder.c_str( ) ) ) {
        clearQueues( );
        return false;
    }

    // source http://stackoverflow.com/questions/612097/how-can-i-get-the-list-of-files-in-a-directory-using-c-or-c
    DIR *dir, *tmpd;
    struct dirent *ent;

    if (
            ( dir = opendir( paths.hotfolder.c_str( ) ) ) != NULL &&
            ( tmpd = opendir( paths.temporary.c_str( ) ) ) != NULL
            ) {

        ConverterFile* cf;
        CFList found_skeleton;
        CFList found_mesh;
        CFList found_mat;
        CFList found_img;

        while ( ( ent = readdir( dir ) ) != NULL ) {

            std::string tmpf = ent->d_name;
            std::string fname = ent->d_name;
            std::transform( tmpf.begin( ), tmpf.end( ), tmpf.begin( ), ::tolower );
            std::size_t found_dot = fname.find_last_of( "." );
            std::string ext = tmpf.substr( found_dot + 1 );
            fname = fname.substr( 0, found_dot );
            std::string fullp = paths.hotfolder + "/" + ent->d_name;

            if ( ext.compare( "xml" ) == 0 ) {

                tmpf = fname;
                std::transform( tmpf.begin( ), tmpf.end( ), tmpf.begin( ), ::tolower );
                found_dot = tmpf.find_last_of( "." );
                tmpf = tmpf.substr( found_dot + 1 );

                if ( isSkeleton( fname ) ) {

                    cf = push_path(
                            &queue_skeleton,
                            paths.output_skeleton,
                            fullp, fname,
                            "skeleton",
                            CT_SKELETON
                            );
                    found_skeleton.push_back( cf );
                    
                } else if ( isMesh( fname ) ) {

                    cf = push_path(
                            &queue_mesh,
                            paths.output_model,
                            fullp, fname,
                            "mesh",
                            CT_MESH );
                    found_mesh.push_back( cf );

                }


            } else if ( isMaterial( fullp ) ) {

                cf = push_path(
                        &queue_mat,
                        paths.output_material,
                        fullp, fname, ext,
                        CT_MATERIAL );
                found_mat.push_back( cf );

            } else if ( isImage( fullp ) ) {

                cf = push_path(
                        &queue_img,
                        paths.output_texture,
                        fullp, fname, ext,
                        CT_IMAGE );
                found_img.push_back( cf );

            }

        }

        closedir( dir );

        // checking missing files
        compare_src( &queue_skeleton, &found_skeleton );
        compare_src( &queue_mesh, &found_mesh );
        compare_src( &queue_mat, &found_mat );
        compare_src( &queue_img, &found_img );

        return true;

    }

    return false;

}

void ConverterThread::compare_src( CFList* local, CFList* foreign ) {
    CFListIter it = local->begin( );
    CFListIter ite = local->end( );
    for (; it != ite; ++it ) {
        if ( !contains_path( foreign, ( *it )->src_path ) && !( *it )->src_missing ) {
            ( *it )->src_missing = true;
            ( *it )->pushed = true;
            ( *it )->to_process = false;
            ( *it )->modified = true;
        }
    }
}

ConverterFile* ConverterThread::push_path(
        CFList* q,
        const std::string& output_path,
        const std::string& full_path,
        const std::string& filename,
        const std::string& ext,
        const ConvertType& type
        ) {

    if ( contains_path( q, full_path ) ) {
        return modified_path( q, full_path );
    }

    ConverterFile* cf = new ConverterFile( );
    cf->src_path = full_path;
    cf->filename = filename;
    cf->extension = ext;
    cf->temp_path = paths.temporary + "/" + filename;
    cf->output_path = output_path + "/" + filename;
    cf->type = type;

    if (
            cf->type == CT_MATERIAL ||
            cf->type == CT_IMAGE
            ) {
        cf->temp_path += "." + cf->extension;
        cf->output_path += "." + cf->extension;
        cf->filename += "." + cf->extension;
    }

    cf->output_missing = true;
    if ( fileExists( cf->output_path.c_str( ) ) ) {
        cf->output_missing = false;
    }

    cf->src_missing = false;
    cf->to_process = true;

    struct stat attrib;
    stat( cf->src_path.c_str( ), &attrib );
    cf->timestamp = attrib.st_ctime;
    cf->filesize = attrib.st_size;

    q->push_back( cf );

    broadcastEvent( CE_PUSHED, cf, "pushed" );

    return cf;

}

ConverterFile* ConverterThread::modified_path( CFList* q, const std::string& p ) {

    CFListIter it = q->begin( );
    CFListIter ite = q->end( );
    ConverterFile* cf = 0;
    for (; it != ite; ++it ) {
        if ( ( *( *it ) ) == p ) cf = ( *it );
    }

    if ( !cf ) return 0;

    if ( cf->src_missing ) {

        cf->src_missing = false;
        cf->pushed = false;
        cf->to_process = true;
        cf->modified = true;
        cf->done = false;

    }

    struct stat attrib;
    stat( cf->src_path.c_str( ), &attrib );

    if ( cf->timestamp != attrib.st_ctime || cf->filesize != attrib.st_size ) {

        cf->pushed = false;
        cf->to_process = true;
        cf->modified = true;
        cf->done = false;

        cf->timestamp = attrib.st_ctime;
        cf->filesize = attrib.st_size;

    }

    bool output_exists = fileExists( cf->output_path.c_str( ) );

    if ( !output_exists && !cf->output_missing ) {

        cf->output_missing = true;

        cf->pushed = false;
        cf->to_process = true;
        cf->modified = true;
        cf->done = false;

    } else if ( output_exists && cf->output_missing ) {

        cf->output_missing = false;
        cf->modified = true;

    }

    return cf;

}

bool ConverterThread::contains_path( CFList* q, const std::string& p ) {

    CFListIter it = q->begin( );
    CFListIter ite = q->end( );
    for (; it != ite; ++it ) {
        if ( ( *( *it ) ) == p ) return true;
    }
    return false;

}

bool ConverterThread::dirExists( const char* path ) {

#ifdef OGRE_PLATFORM_LINUX
    // source: https://bytes.com/topic/c/answers/584434-check-directory-exists-c
    if ( path == NULL ) return false;
    DIR *pDir;
    bool bExists = false;
    pDir = opendir( path );
    if ( pDir != NULL ) {
        bExists = true;
        ( void ) closedir( pDir );
    }
    return bExists;
#else
    return false;
#endif

}

bool ConverterThread::fileExists( const char* path ) {

    std::ifstream infile( path );
    return infile.good( );

}

bool ConverterThread::fileCopy( const char* src_path, const char* dest_path ) {

#ifdef OGRE_PLATFORM_LINUX

    if ( !fileExists( src_path ) ) return false;

    std::ifstream src( src_path, std::ios::binary );
    std::ofstream dst( dest_path, std::ios::binary );
    dst << src.rdbuf( );

    return true;

#else
    return false;
#endif

}

bool ConverterThread::isMesh( const std::string filename ) {

    std::string tmpf = filename;
    std::transform( tmpf.begin( ), tmpf.end( ), tmpf.begin( ), ::tolower );
    size_t found_dot = tmpf.find_last_of( "." );
    tmpf = tmpf.substr( found_dot + 1 );
    return tmpf.compare( "mesh" ) == 0;

}

bool ConverterThread::isSkeleton( const std::string filename ) {

    std::string tmpf = filename;
    std::transform( tmpf.begin( ), tmpf.end( ), tmpf.begin( ), ::tolower );
    size_t found_dot = tmpf.find_last_of( "." );
    tmpf = tmpf.substr( found_dot + 1 );
    return tmpf.compare( "skeleton" ) == 0;

}

bool ConverterThread::isMaterial( const std::string filename ) {

    std::string tmpf = filename;
    std::transform( tmpf.begin( ), tmpf.end( ), tmpf.begin( ), ::tolower );
    size_t found_dot = tmpf.find_last_of( "." );
    tmpf = tmpf.substr( found_dot + 1 );
    return tmpf.compare( "material" ) == 0;

}

bool ConverterThread::isImage( const std::string filename ) {

    std::string tmpf = filename;
    std::transform( tmpf.begin( ), tmpf.end( ), tmpf.begin( ), ::tolower );
    size_t found_dot = tmpf.find_last_of( "." );
    tmpf = tmpf.substr( found_dot + 1 );
    return (
            tmpf.compare( "jpg" ) == 0 ||
            tmpf.compare( "jpeg" ) == 0 ||
            tmpf.compare( "png" ) == 0 ||
            tmpf.compare( "tiff" ) == 0 ||
            tmpf.compare( "tif" ) == 0 ||
            tmpf.compare( "tga" ) == 0
            );

}

void ConverterThread::push_convert( CFList* q, ConverterFile* cf ) {

    cf->pushed = true;
    cf->to_process = true;
    cf->success = false;
    cf->done = false;
    cf->modified = true;
    if ( !contains_path( q, cf->src_path.c_str( ) ) ) {
        q->push_back( cf );
    }

}

bool ConverterThread::startConversion( ) {

    if ( _convert_runs ) return false;

    assert( !convert_thread );
    _convert_runs = true;
    file_thread = boost::shared_ptr<boost::thread>(
            new boost::thread( boost::bind( &ConverterThread::runConversion, this ) ) );

    return true;

}

void ConverterThread::runConversion( ) {

    while ( _convert_runs && !_listener ) {
        std::cout << "ConverterThread::runConversionCron, "
                "do not forget to set the ConvertListener "
                "with ConverterThread::setListener() "
                "and process the requests by calling "
                "ConvertListener::process() in the "
                "OpenGL thread" << std::endl;
        boost::this_thread::sleep( boost::posix_time::milliseconds( 1000 ) );
    }

    if ( !_convert_runs ) return;

    CFListIter it;
    CFListIter ite;

    // copies: easy first
    it = convert_copies.begin( );
    ite = convert_copies.end( );
    if (
            !dirExists( paths.output_texture.c_str( ) ) ||
            !dirExists( paths.output_material.c_str( ) )
            ) {

        for (; it != ite; ++it ) {
            if ( !( *it )->to_process ) continue;
            ( *it )->to_process = false;
            ( *it )->done = true;
            ( *it )->modified = true;
            std::cerr << "!! Folders " << paths.output_texture << " and/or " <<
                    paths.output_material << " do not exist anymore !!" <<
                    std::endl << "file: " << ( *it )->src_path <<
                    " can not be processed!" <<
                    std::endl;
        }

    } else {

        for (; it != ite; ++it ) {

            if ( !( *it )->to_process ) continue;
            if ( !_listener ) return;

            boost::unique_lock<boost::mutex>* c_lock =
                    new boost::unique_lock<boost::mutex>( _convert_mutex );

            ( *it )->message = "Successfully moved";
            ( *it )->success = fileCopy(
                    ( *it )->src_path.c_str( ),
                    ( *it )->output_path.c_str( )
                    );

            if ( !( *it )->success )
                ( *it )->message = "Move failed";

            ( *it )->to_process = false;
            ( *it )->done = true;
            ( *it )->modified = true;

            delete c_lock;

        }

    }

    // processing meshes
    it = convert_mesh.begin( );
    ite = convert_mesh.end( );

    for (; it != ite; ++it ) {

        if ( !( *it )->to_process ) continue;
        if ( !_listener ) return;

        // pushing the request to ConvertListener
        _listener->convert( ( *it ) );

        // waiting for ConvertListener to finish the task
        boost::unique_lock<boost::mutex>* c_lock =
                new boost::unique_lock<boost::mutex>( _convert_mutex );
        bool cf_processing = ( *it )->processing;
        delete c_lock;

        // checking if file is processed
        while ( cf_processing ) {
            boost::unique_lock<boost::mutex>* c_lock =
                    new boost::unique_lock<boost::mutex>( _convert_mutex );
            cf_processing = ( *it )->processing;
            std::cout << "mesh conversion, waiting for " << ( *it )->src_path << std::endl;
            delete c_lock;
            boost::this_thread::sleep( boost::posix_time::milliseconds( 5 ) );
        }

        // if conversion is successful
        if ( ( *it )->success ) {

            boost::unique_lock<boost::mutex>* c_lock =
                    new boost::unique_lock<boost::mutex>( _convert_mutex );

            // moving the temp file to the destination path
            ( *it )->message = "Successfully converted & moved";
            ( *it )->success = fileCopy(
                    ( *it )->temp_path.c_str( ),
                    ( *it )->output_path.c_str( )
                    );

            if ( !( *it )->success )
                ( *it )->message = "Successfully converted, move failed";

            delete c_lock;

        } else {

            ( *it )->message = "Conversion failed";

        }

        //        ( *it )->print( );
        ( *it )->to_process = false;
        ( *it )->done = true;
        ( *it )->modified = true;

    }

    // processing skeletons
    it = convert_skeleton.begin( );
    ite = convert_skeleton.end( );

    for (; it != ite; ++it ) {

        if ( !( *it )->to_process ) continue;
        if ( !_listener ) return;

        // pushing the request to ConvertListener
        _listener->convert( ( *it ) );

        // waiting for ConvertListener to finish the task
        boost::unique_lock<boost::mutex>* c_lock =
                new boost::unique_lock<boost::mutex>( _convert_mutex );
        bool cf_processing = ( *it )->processing;
        delete c_lock;

        // checking if file is processed
        while ( cf_processing ) {
            boost::unique_lock<boost::mutex>* c_lock =
                    new boost::unique_lock<boost::mutex>( _convert_mutex );
            cf_processing = ( *it )->processing;
            std::cout << "skeleton conversion, waiting for " << ( *it )->src_path << std::endl;
            delete c_lock;
            boost::this_thread::sleep( boost::posix_time::milliseconds( 5 ) );
        }

        // if conversion is successful
        if ( ( *it )->success ) {

            boost::unique_lock<boost::mutex>* c_lock =
                    new boost::unique_lock<boost::mutex>( _convert_mutex );

            // moving the temp file to the destination path
            ( *it )->message = "Successfully converted & moved";
            ( *it )->success = fileCopy(
                    ( *it )->temp_path.c_str( ),
                    ( *it )->output_path.c_str( )
                    );

            if ( !( *it )->success )
                ( *it )->message = "Successfully converted, move failed";

            delete c_lock;

        } else {

            ( *it )->message = "Conversion failed";

        }

        //        ( *it )->print( );
        ( *it )->to_process = false;
        ( *it )->done = true;
        ( *it )->modified = true;

    }

    _convert_runs = false;

}

void ConverterThread::setListener( ConvertListener* l ) {

    boost::unique_lock<boost::mutex>* c_lock =
            new boost::unique_lock<boost::mutex>( _convert_mutex );
    _listener = l;
    delete c_lock;

}

void ConverterThread::broadcastEvent(
        const ConverterEvent& ev,
        ConverterFile* cf,
        std::string msg
        ) {

    if ( !_listener ) return;
    
    ConverterInfo ci;
    ci.event = ev;
    ci.message = msg;
    if ( cf ) {
        ci.filename = cf->filename;
        ci.extension = cf->extension;
        ci.type = cf->type;
    }
    _listener->convertEvent( ci );
    
}