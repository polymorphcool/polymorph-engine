/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ___________________________________( ^3^)_____________________________________
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 */

/* 
 * File:   ConverterApp.h
 * Author: frankiezafe
 *
 * Created on February 06, 2016, 6:00 PM
 */

#ifndef CONVERTER_APP
#define CONVERTER_APP

#include <dirent.h>
#include "PolymorphApplication.h"
#include "FileThread.h"
#include "ConverterThread.h"

#define PATH_FILE                   "path.cfg"

#define OP_MOD_EMPTY                std::string("? models") 
#define OP_SKE_EMPTY                std::string("? skeletons")
#define OP_MAT_EMPTY                std::string("? materials" ) 
#define OP_TEX_EMPTY                std::string("? textures") 
#define OP_HOT_EMPTY                std::string("? hot folder")
#define OP_TMP_EMPTY                std::string("? temporary")

#define OP_MOD_PREFIX               std::string("_models: ")  
#define OP_SKE_PREFIX               std::string("_skeletons: ")
#define OP_MAT_PREFIX               std::string("_materials: ")
#define OP_TEX_PREFIX               std::string("_textures: ")
#define OP_HOT_PREFIX               std::string("_hot_folder: ")
#define OP_TMP_PREFIX               std::string("_temp: ")

#define OP_WRONG_PATH               std::string(" :: wrong path")

#define RESOURCES_LABEL_SUFFIX      std::string("rscll")
#define RESOURCES_BUTTON_SUFFIX     std::string("rscbt")

#define RESOURCES_LABEL_PREFIX      std::string("resources::")
#define LOAD_PREFIX                 std::string("_load: ")
#define LOADED_PREFIX               std::string("_selected: ")

#define CONVERTER_RESOURCE_GROUP    "CONVERTER"

class ConverterApp :
public PolymorphApplication,
public FileListener,
public ConvertListener
{
public:

    ConverterApp();

    void createTray(void);

    void createScene(void);

    void draw();
    
    void after_draw();

    bool keyPressed(const OIS::KeyEvent &arg);

    bool mouseMoved(const OIS::MouseEvent &arg);

    // file listener callback
    void fileOpened(polymorph::FileChooserResult& r);

    // tray listener
    void buttonHit(OgreBites::Button* button);
    
    void convertEvent( const ConverterInfo& ci);
    
private:

    FileThread fthread;

    ConverterThread cthread;

    std::string exec_path;
    ConverterPaths paths;
    std::string* current_path;

    OgreBites::Button* output_model_bt;
    OgreBites::Button* output_skeleton_bt;
    OgreBites::Button* output_material_bt;
    OgreBites::Button* output_texture_bt;
    OgreBites::Button* hotfolder_bt;
    OgreBites::Button* temporary_bt;
    OgreBites::Button* current_bt;

    bool _pathvalidated;
    
    void loadPath();

    void storePath();

    void invalidatePath();

    bool allPathValid();
    
    void validatePath();
    
    void rebuildResources();

    CIList conversion_info;
    
    std::map< std::string, OgreBites::Label* > convert_labels;
    std::map< std::string, std::string > convert_msgs;
    
    std::map< std::string, OgreBites::Button* > converted_buttons;
    
    ConverterInfo* selected_resource;
    OgreBites::Button* selected_resource_bt;
    
    polymorph::PLight* sun;
    polymorph::PLight* back_sun;
    polymorph::PNode* demo_obj;
    polymorph::PNode demo_cube;
    polymorph::PNode demo_plane;
    
    bool _rendertray;
    bool _rebuildresources;
    bool _convert_finished;
    bool _resource_loading;
    
    void renderTray();
        
    void loadResource();
    
    void clearDemo();

};

#endif /* CONVERTER_APP */

