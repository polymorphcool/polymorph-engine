/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2017 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
*/

/* 
 * File:   ConvertListener.cpp
 * Author: frankiezafe
 * 
 * Created on February 10, 2017, 4:55 PM
 */

#include "ConvertListener.h"

using namespace Ogre;

ConvertListener::ConvertListener():
meshSerializer(0),
xmlMeshSerializer(0),
skeletonSerializer(0),
xmlSkeletonSerializer(0),
to_convert(0)
{

}

ConvertListener::~ConvertListener() {

    if ( meshSerializer ) OGRE_DELETE meshSerializer;
    if ( xmlMeshSerializer ) OGRE_DELETE xmlMeshSerializer;
    if ( skeletonSerializer ) OGRE_DELETE skeletonSerializer;
    if ( xmlSkeletonSerializer ) OGRE_DELETE xmlSkeletonSerializer;

}

void ConvertListener::convert( ConverterFile* cf ) {
    to_convert = cf;
    to_convert->processing = true;
}

void ConvertListener::convert() {
    
    if ( !to_convert ) return;
    
//    std::cout << "ConverterThread::convert " << to_convert->path << std::endl;
    
    if ( !meshSerializer ) meshSerializer = new MeshSerializer();
    if ( !xmlMeshSerializer ) xmlMeshSerializer = new XMLMeshSerializer();
    if ( !skeletonSerializer ) skeletonSerializer = new SkeletonSerializer();
    if ( !xmlSkeletonSerializer ) xmlSkeletonSerializer = new XMLSkeletonSerializer();
    
    ConverterOptions opts;
    opts.nuextremityPoints = 0;
    opts.mergeTexcoordResult = 0;
    opts.mergeTexcoordToDestroy = 0;
    opts.usePercent = true;
    opts.tangentSemantic = VES_TANGENT;
    opts.tangentUseParity = false;
    opts.tangentSplitMirrored = false;
    opts.tangentSplitRotated = false;
    opts.reorganiseBuffers = true;
    opts.optimiseAnimations = true;
    opts.quietMode = false;
    opts.d3d = false;
    opts.gl = true;
    opts.endian = Serializer::ENDIAN_NATIVE;

    // Read root element and decide from there what type
    String response;
    TiXmlDocument* doc = new TiXmlDocument( to_convert->src_path.c_str( ) );

    // Some double-parsing here but never mind
    if ( !doc->LoadFile( ) ) {

        std::cout << "Unable to open file " << to_convert->src_path << " - fatal error." << std::endl;
        delete doc;
        to_convert->src_missing = true;
        to_convert->success = false;
        to_convert->processing = false;
        to_convert = 0;
        return;

    }

    TiXmlElement* root = doc->RootElement( );

    if ( !stricmp( root->Value( ), "mesh" ) ) {

        delete doc;
        
        MeshPtr newMesh = MeshManager::getSingleton( ).createManual( "conversion",
                ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME );
        
        VertexElementType colourElementType;
        if ( opts.d3d )
            colourElementType = VET_COLOUR_ARGB;
        else
            colourElementType = VET_COLOUR_ABGR;
        
        xmlMeshSerializer->importMesh(
                to_convert->src_path,
                colourElementType,
                newMesh.getPointer( ) );

        // Re-jig the buffers?
        // Make sure animation types are up to date first
        newMesh->_determineAnimationTypes( );
        if ( opts.reorganiseBuffers ) {
//            logMgr.logMessage( "Reorganising vertex buffers to automatic layout..." );
            // Shared geometry
            if ( newMesh->sharedVertexData ) {
                // Automatic
                VertexDeclaration* newDcl =
                        newMesh->sharedVertexData->vertexDeclaration->getAutoOrganisedDeclaration(
                        newMesh->hasSkeleton( ), newMesh->hasVertexAnimation( ), newMesh->getSharedVertexDataAnimationIncludesNormals( ) );
                if ( *newDcl != *( newMesh->sharedVertexData->vertexDeclaration ) ) {
                    // Usages don't matter here since we're onlly exporting
                    BufferUsageList bufferUsages;
                    for ( size_t u = 0; u <= newDcl->getMaxSource( ); ++u )
                        bufferUsages.push_back( HardwareBuffer::HBU_STATIC_WRITE_ONLY );
                    newMesh->sharedVertexData->reorganiseBuffers( newDcl, bufferUsages );
                }
            }
            // Dedicated geometry
            Mesh::SubMeshIterator smIt = newMesh->getSubMeshIterator( );
            while ( smIt.hasMoreElements( ) ) {
                SubMesh* sm = smIt.getNext( );
                if ( !sm->useSharedVertices ) {
                    const bool hasVertexAnim = sm->getVertexAnimationType( ) != Ogre::VAT_NONE;

                    // Automatic
                    VertexDeclaration* newDcl =
                            sm->vertexData->vertexDeclaration->getAutoOrganisedDeclaration(
                            newMesh->hasSkeleton( ), hasVertexAnim, sm->getVertexAnimationIncludesNormals( ) );
                    if ( *newDcl != *( sm->vertexData->vertexDeclaration ) ) {
                        // Usages don't matter here since we're onlly exporting
                        BufferUsageList bufferUsages;
                        for ( size_t u = 0; u <= newDcl->getMaxSource( ); ++u )
                            bufferUsages.push_back( HardwareBuffer::HBU_STATIC_WRITE_ONLY );
                        sm->vertexData->reorganiseBuffers( newDcl, bufferUsages );
                    }
                }
            }

        }

        if ( opts.mergeTexcoordResult != opts.mergeTexcoordToDestroy ) {
            newMesh->mergeAdjacentTexcoords( opts.mergeTexcoordResult, opts.mergeTexcoordToDestroy );
        }

        if ( opts.nuextremityPoints ) {
            Mesh::SubMeshIterator smIt = newMesh->getSubMeshIterator( );
            while ( smIt.hasMoreElements( ) ) {
                SubMesh* sm = smIt.getNext( );
                sm->generateExtremes( opts.nuextremityPoints );
            }
        }

        meshSerializer->exportMesh(
                newMesh.getPointer( ),
                to_convert->temp_path,
                opts.endian );

        // Clean up the conversion mesh
        MeshManager::getSingleton( ).remove( "conversion" );

    } else if ( !stricmp( root->Value( ), "skeleton" ) ) {

        delete doc;

        SkeletonPtr newSkel = OldSkeletonManager::getSingleton( ).create( "conversion",
                ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME );
        xmlSkeletonSerializer->importSkeleton( to_convert->src_path, newSkel.getPointer( ) );
        if ( opts.optimiseAnimations ) {
            newSkel->optimiseAllAnimations( );
        }
        skeletonSerializer->exportSkeleton(
                newSkel.getPointer( ), to_convert->temp_path,
                SKELETON_VERSION_LATEST, opts.endian );

        // Clean up the conversion skeleton
        OldSkeletonManager::getSingleton( ).remove( "conversion" );

    } else {

        delete doc;
        to_convert->success = false;
        to_convert->processing = false;
        to_convert = 0;
        return;

    }

    to_convert->success = true;
    to_convert->processing = false;
    to_convert = 0;
    return;
    
}