/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2017 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
 */

/* 
 * File:   ConverterCommon.h
 * Author: frankiezafe
 *
 * Created on February 9, 2017, 11:39 AM
 */

#ifndef CONVERTERCOMMON_H
#define CONVERTERCOMMON_H

#include "Ogre.h"

enum ConverterEvent {

    CE_UNDEFINED = -1,
    CE_FINISHED = 0,
    CE_PUSHED = 1,
    CE_MODIFIED = 2,
    CE_CONVERSION = 3,
    CE_SRC_MISSING = 4,
    CE_DST_MISSING = 5,
    CE_DONE = 6,
    CE_COUNT = 7

};

enum ConvertType {
    CT_UNDEFINED = -1,
    CT_MESH = 0,
    CT_SKELETON = 1,
    CT_MATERIAL = 2,
    CT_IMAGE = 3,
    CT_COUNT = 4

};

struct ConverterPaths {
    std::string output_model;
    std::string output_skeleton;
    std::string output_material;
    std::string output_texture;
    std::string hotfolder;
    std::string temporary;
};

struct ConverterOptions {
    size_t nuextremityPoints;
    size_t mergeTexcoordResult;
    size_t mergeTexcoordToDestroy;
    bool usePercent;
    Ogre::VertexElementSemantic tangentSemantic;
    bool tangentUseParity;
    bool tangentSplitMirrored;
    bool tangentSplitRotated;
    bool reorganiseBuffers;
    bool optimiseAnimations;
    bool quietMode;
    bool d3d;
    bool gl;
    Ogre::Serializer::Endian endian;
};

class ConverterInfo {
public:

    ConverterEvent event;
    std::string filename;
    std::string extension;
    std::string message;
    ConvertType type;

    ConverterInfo() :
    event(CE_UNDEFINED),
    type(CT_UNDEFINED) {
    }

    ConverterInfo(const ConverterInfo& src) {
        (*this) = src;
    }

    inline void operator=(const ConverterInfo& src) {
        event = src.event;
        filename = src.filename;
        extension = src.extension;
        message = src.message;
        type = src.type;
    }

    inline bool operator==(const ConverterInfo& src) const {
        return (
                filename.compare(src.filename) == 0 &&
                extension.compare(src.extension) == 0 &&
                type == src.type
                );
    }

};

typedef std::vector< ConverterInfo > CIList;
typedef std::vector< ConverterInfo >::iterator CIListIter;

class ConverterFile {
public:

    std::string src_path;
    std::string filename;
    std::string extension;
    std::string temp_path;
    std::string output_path;
    unsigned long long filesize;
    unsigned long long timestamp;
    bool pushed;
    bool to_process;
    bool processing;
    bool success;
    bool done;
    bool src_missing;
    bool output_missing;
    bool modified;
    std::string message;
    ConvertType type;

    ConverterFile() :
    filesize(0),
    timestamp(0),
    pushed(false),
    to_process(true),
    processing(false),
    success(false),
    done(false),
    src_missing(false),
    output_missing(false),
    modified(true),
    type(CT_UNDEFINED) {
    }

    inline bool operator==(const std::string& path) {
        return ( this->src_path.compare(path) == 0);
    }

    void print() {
        std::cout << "ConverterFile" << std::endl <<
                "\textension: " << extension << std::endl <<
                "\tpath: " << src_path << std::endl <<
                "\ttemp_path: " << temp_path << std::endl <<
                "\toutput_path: " << output_path << std::endl <<
                "\tfilesize: " << filesize << std::endl <<
                "\ttimestamp: " << timestamp << std::endl <<
                "\tpushed: " << pushed << std::endl <<
                "\tto_process: " << to_process << std::endl <<
                "\tprocessing: " << processing << std::endl <<
                "\tsuccess: " << success << std::endl <<
                "\tdone: " << done << std::endl <<
                "\tsource_missing: " << src_missing << std::endl <<
                "\toutput_missing: " << output_missing << std::endl <<
                "\tmodified: " << modified << std::endl <<
                "\tmessage: " << message << std::endl;
        switch (type) {
            case CT_MESH:
                std::cout << "\ttype: MESH" << std::endl;
                break;
            case CT_SKELETON:
                std::cout << "\ttype: SKELETON" << std::endl;
                break;
            case CT_MATERIAL:
                std::cout << "\ttype: MATERIAL" << std::endl;
                break;
            case CT_IMAGE:
                std::cout << "\ttype: IMAGE" << std::endl;
                break;
            default:
                std::cout << "\ttype: UNDEFINED" << std::endl;
                break;
        }
    }

};

typedef std::vector< ConverterFile* > CFList;
typedef std::vector< ConverterFile* >::iterator CFListIter;

#endif /* CONVERTERCOMMON_H */

