/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2017 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
 */

/* 
 * File:   FileThread.h
 * Author: frankiezafe
 *
 * Created on February 8, 2017, 3:51 PM
 */

#ifndef FILETHREAD_H
#define FILETHREAD_H

#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/shared_ptr.hpp>
#include "FileListener.h"

class FileThread {
public:

    FileThread();

    virtual ~FileThread();

    void stop();

    inline bool runs() {
        return _runs;
    }

    bool openDir(
            const std::string& path = "",
            const std::string& title = "");

    bool openOgreExport(
            const std::string& path = "",
            const std::string& title = "");

    bool addListener(FileListener* l);

    bool removeListener(FileListener* l);

    inline bool containsListener(FileListener* l) {
        return std::find(listeners.begin(), listeners.end(), l) != listeners.end();
    }

private:

    boost::shared_ptr<boost::thread> bthread;
    boost::mutex bmutex;
    bool _runs;
    FLList listeners;

    std::string start_path;
    std::string dialog_title;

    void runOpenDir();

    void runOpenOgreExport();

};

#endif /* FILETHREAD_H */

