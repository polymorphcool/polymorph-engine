/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2017 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
 */

/* 
 * File:   ConverterThread.h
 * Author: frankiezafe
 *
 * Created on February 9, 2017, 9:16 AM
 */

#ifndef CONVERTERTHREAD_H
#define CONVERTERTHREAD_H

#include <dirent.h>
#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/shared_ptr.hpp>

#include "ConvertListener.h"

class ConverterThread {
public:

    ConverterThread();

    virtual ~ConverterThread();

    static bool dirExists(const char* path);

    static bool fileExists(const char* path);

    static bool fileCopy(const char* src, const char* dest);

    static bool isMesh(const std::string filename);

    static bool isSkeleton(const std::string filename);
    
    static bool isMaterial(const std::string filename);

    static bool isImage(const std::string filename);

    //    inline void enableConversion() {
    //        _convert_enabled = true;
    //    }

    inline bool fileThreadRuns() {
        return _file_runs;
    }

    inline bool convertThreadRuns() {
        return _convert_runs;
    }

    inline void lock() {
        lockFile();
        lockConvert();
    }

    inline void unlock() {
        unlockConvert();
        unlockFile();
    }

    inline void lockFile() {
        assert(!_external_file_thread);
        _external_file_thread = new boost::unique_lock<boost::mutex>(_file_mutex);
    }

    inline void unlockFile() {
        assert(_external_file_thread);
        delete _external_file_thread;
    }

    inline void lockConvert() {
        assert(!_external_convert_lock);
        _external_convert_lock = new boost::unique_lock<boost::mutex>(_convert_mutex);
    }

    inline void unlockConvert() {
        assert(_external_convert_lock);
        delete _external_convert_lock;
    }

    bool start(const ConverterPaths& path);

    bool stop();

    void setListener(ConvertListener* l);

private:

    ///////////////////////////////////
    // file related params & methods //
    ///////////////////////////////////

    ConverterPaths paths;

    boost::shared_ptr<boost::thread> file_thread;
    boost::mutex _file_mutex;
    boost::unique_lock<boost::mutex>* _external_file_thread;
    bool _file_runs;

    CFList queue_mesh;
    CFList queue_skeleton;
    CFList queue_mat;
    CFList queue_img;

    void runFile();

    void check_file(CFList* q, ConverterFile* cf);

    bool collectFiles();

    void compare_src(CFList* local, CFList* foreign);

    ConverterFile* push_path(
            CFList* q,
            const std::string& output_path,
            const std::string& full_path,
            const std::string& filename,
            const std::string& ext,
            const ConvertType& type
            );

    ConverterFile* modified_path(CFList* q, const std::string& p);

    bool contains_path(CFList* q, const std::string& p);

    void clearQueues();
    void clearMesh();
    void clearSkeleton();
    void clearMaterial();
    void clearImage();
    void clearConverts();

    ////////////////////////////////////
    // self-managed conversion thread //
    ////////////////////////////////////

    boost::shared_ptr<boost::thread> convert_thread;
    boost::mutex _convert_mutex;
    boost::unique_lock<boost::mutex>* _external_convert_lock;
    bool _convert_runs;
    //    bool _convert_enabled;

    CFList convert_skeleton;
    CFList convert_mesh;
    CFList convert_copies;

    void push_convert(CFList* q, ConverterFile* cf);

    bool startConversion();

    void runConversion();
    
    ////////////////////////////////
    // listeners params & methods //
    ////////////////////////////////
    
    ConvertListener* _listener;
    
    void broadcastEvent(
        const ConverterEvent& ev,
        ConverterFile* cf = 0,
        std::string msg = ""
    );

};

#endif /* CONVERTERTHREAD_H */

