/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2017 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
*/

/* 
 * File:   ConvertListener.h
 * Author: frankiezafe
 *
 * Created on February 10, 2017, 4:55 PM
 */

#ifndef CONVERTLISTENER_H
#define CONVERTLISTENER_H

#include "Ogre.h"
#include "ConverterCommon.h"
#include "OgreXMLMeshSerializer.h"
#include "OgreXMLSkeletonSerializer.h"
#include "OgreMeshLodGenerator.h"
#include "OgreLodStrategyManager.h"
#include "OgreDistanceLodStrategy.h"
#include "OgreDefaultHardwareBufferManager.h"

class ConvertListener {
public:

    ConvertListener();
    
    ~ConvertListener();
    
    virtual void convertEvent( const ConverterInfo& ci) = 0;
    
    virtual void convert( ConverterFile* cf );
    
    inline bool pending() {
        return to_convert != 0;
    }
    
protected:
    Ogre::MeshSerializer* meshSerializer;
    Ogre::XMLMeshSerializer* xmlMeshSerializer;
    Ogre::SkeletonSerializer* skeletonSerializer;
    Ogre::XMLSkeletonSerializer* xmlSkeletonSerializer;
    
    ConverterFile* to_convert;

    void convert();
};

#endif /* CONVERTLISTENER_H */

