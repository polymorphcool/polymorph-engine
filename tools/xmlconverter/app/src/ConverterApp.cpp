/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ___________________________________( ^3^)_____________________________________
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 */

/* 
 * File:   ConverterApp.cpp
 * Author: frankiezafe
 * 
 * Created on February 06, 2016, 6:00 PM
 */

#include <map>

#include "ConverterApp.h"

using namespace std;
using namespace Ogre;
using namespace OgreBites;
using namespace polymorph;

ConverterApp::ConverterApp() :
PolymorphApplication(),
_pathvalidated(false),
selected_resource(0),
selected_resource_bt(0),
demo_obj(0),
_rendertray(false),
_rebuildresources(false),
_convert_finished(false),
_resource_loading(false) {

#ifdef OGRE_PLATFORM_LINUX
    char buf[256];
    readlink("/proc/self/exe", buf, sizeof (buf));
    exec_path = buf;
    std::size_t found = exec_path.find_last_of("/\\");
    exec_path = exec_path.substr(0, found + 1);
#endif

    loadPath();
    validatePath();

    std::stringstream ss;
    ss << "Polymorph asset convertor v" <<
            POLYMORPH_VERSION << "." <<
            POLYMORPH_SUBVERSION;
    window_title = ss.str();

}

void ConverterApp::createTray(void) {

    trayMgr = new SdkTrayManager("SampleControls", window, inputContext, this);

    trayMgr->setListener(this);

    renderTray();

}

void ConverterApp::createScene(void) {

    // keep default behavior
    PolymorphApplication::createScene();

    cam->setPosition(0, 0, 5);
    cam->setNearClipDistance(0.1);

    sun = new PLight();
    sun->sun(sceneMgr);
    sun->yaw(Radian(-0.1));
    
    back_sun = new PLight();
    back_sun->sun(sceneMgr);
    back_sun->roll(Radian(3));
    back_sun->diffuse( 0.3,0.3,0.35 );

    demo_cube.cube(sceneMgr);
    demo_cube.visible(false);

    demo_plane.cube(sceneMgr);
    PMaterial* mat = demo_plane.getMaterial();
    std::cout << "demo_plane: " << mat->getName() << std::endl;
    mat->emissive(1, 1, 1, 1);
    demo_plane.visible(false);

#ifdef OGRE_PLATFORM_LINUX
    gtk_init(0, 0);
#endif

    fthread.addListener(this);
    cthread.setListener(this);

}

void ConverterApp::draw() {

    if (_pathvalidated) {

        if (_rendertray) {
            _rendertray = false;
            renderTray();
        }

        if (_rebuildresources) {
            _rebuildresources = false;
            rebuildResources();
        }

        if (_resource_loading) {
            _resource_loading = false;
            loadResource();
        }

    }
    
    if ( demo_obj ) {
        demo_obj->yaw( Radian( draw_event.timeSinceLastFrame * 0.2 ) );
        demo_obj->pitch( Radian( draw_event.timeSinceLastFrame * 0.1 ) );
    }

}

void ConverterApp::after_draw() {

    if (ConvertListener::pending()) {
        cthread.lock();
        ConvertListener::convert();
        cthread.unlock();
    }

}

void ConverterApp::buttonHit(OgreBites::Button* button) {


    if (
            button == hotfolder_bt ||
            button == temporary_bt ||
            button == output_model_bt ||
            button == output_skeleton_bt ||
            button == output_material_bt ||
            button == output_texture_bt
            ) {

        invalidatePath();

        bool ok = false;

        if (button == hotfolder_bt) {

            if (fthread.openDir(paths.hotfolder, "Select hot folder")) {
                current_path = &paths.hotfolder;
                current_bt = button;
            }
            return;

        } else if (button == temporary_bt) {

            if (fthread.openDir(paths.hotfolder, "Select temp folder")) {
                current_path = &paths.temporary;
                current_bt = button;
            }
            return;

        }

        if (
                button == output_model_bt ||
                button == output_skeleton_bt ||
                button == output_material_bt ||
                button == output_texture_bt
                ) {

            std::string wtitle = "Select models' output folder";
            if (button == output_skeleton_bt) {
                wtitle = "Select skeletons' output folder";
            } else if (button == output_material_bt) {
                wtitle = "Select materials' output folder";
            } else if (button == output_texture_bt) {
                wtitle = "Select textures' output folder";
            }

            if (button == output_model_bt && !paths.output_model.empty()) {
                ok = fthread.openDir(paths.output_model, wtitle);
            } else if (button == output_skeleton_bt && !paths.output_skeleton.empty()) {
                ok = fthread.openDir(paths.output_skeleton, wtitle);
            } else if (button == output_material_bt && !paths.output_material.empty()) {
                ok = fthread.openDir(paths.output_material, wtitle);
            } else if (button == output_texture_bt && !paths.output_texture.empty()) {
                ok = fthread.openDir(paths.output_texture, wtitle);
            } else {
                ok = fthread.openDir("", wtitle);
            }

            if (ok) {
                if (button == output_model_bt) {
                    current_path = &paths.output_model;
                    current_bt = button;
                    return;
                } else if (button == output_skeleton_bt) {
                    current_path = &paths.output_skeleton;
                    current_bt = button;
                    return;
                } else if (button == output_material_bt) {
                    current_path = &paths.output_material;
                    current_bt = button;
                    return;
                } else if (button == output_texture_bt) {
                    current_path = &paths.output_texture;
                    current_bt = button;
                    return;
                }
            }

        }

        current_path = 0;
        current_bt = 0;

        return;

    }

    std::map< std::string, OgreBites::Button* >::iterator itb = converted_buttons.begin();
    std::map< std::string, OgreBites::Button* >::iterator itbe = converted_buttons.end();

    for (; itb != itbe; ++itb) {

        if (itb->second == button) {

            if (selected_resource && selected_resource->filename.compare(itb->first) == 0) {
                // same resource!
                return;
            }

            if (selected_resource_bt) {

                selected_resource_bt->setCaption(
                        LOAD_PREFIX + selected_resource->filename);
                selected_resource = 0;
                selected_resource_bt = 0;

            }

            CIListIter iti = conversion_info.begin();
            CIListIter itie = conversion_info.end();

            for (; iti != itie; ++iti) {

                if ((*iti).filename == itb->first) {

                    selected_resource = &(*iti);
                    cout << "BUTTON CLICKED: " << (*iti).filename << endl;
                    break;

                }

            }

            if (!selected_resource) {

                cout << "IMPOSSIBLE TO LOCATE RESOURCE: " << (*iti).filename << endl;
                return;

            }

            selected_resource_bt = button;
            selected_resource_bt->setCaption(LOADED_PREFIX + (*iti).filename);
            _resource_loading = true;

        }
    }

}

bool ConverterApp::keyPressed(const OIS::KeyEvent &arg) {

    // keep default behavior
    PolymorphApplication::keyPressed(arg);

    switch (arg.key) {
        case OIS::KC_D:
        {
            fthread.openDir();
        }
            break;
        case OIS::KC_X:
        {
            fthread.openOgreExport();
        }
            break;
        default:
            break;

    }

}

void ConverterApp::fileOpened(polymorph::FileChooserResult& r) {

    if (current_path && r.success) {

        if (
                (
                current_path == &paths.hotfolder &&
                r.folder.compare(paths.hotfolder) == 0
                ) ||
                (
                current_path == &paths.temporary &&
                r.folder.compare(paths.temporary) == 0
                ) ||
                (
                current_path == &paths.output_model &&
                r.folder.compare(paths.output_model) == 0
                ) ||
                (
                current_path == &paths.output_skeleton &&
                r.folder.compare(paths.output_skeleton) == 0
                ) ||
                (
                current_path == &paths.output_material &&
                r.folder.compare(paths.output_material) == 0
                ) ||
                (
                current_path == &paths.output_texture &&
                r.folder.compare(paths.output_texture) == 0
                )
                ) {

            // no change in path
            return;

        }

        if (current_path == &paths.hotfolder) {

            if (
                    r.folder.compare(paths.temporary) == 0 ||
                    r.folder.compare(paths.output_model) == 0 ||
                    r.folder.compare(paths.output_skeleton) == 0 ||
                    r.folder.compare(paths.output_material) == 0 ||
                    r.folder.compare(paths.output_texture) == 0
                    ) {

                if (paths.hotfolder.empty()) {
                    current_bt->setCaption(OP_HOT_EMPTY + OP_WRONG_PATH);
                }

            } else {

                (*current_path) = r.folder;
                current_bt->setCaption(OP_HOT_PREFIX + (*current_path));

            }

        } else if (current_path == &paths.temporary) {

            if (
                    r.folder.compare(paths.hotfolder) == 0 ||
                    r.folder.compare(paths.output_model) == 0 ||
                    r.folder.compare(paths.output_skeleton) == 0 ||
                    r.folder.compare(paths.output_material) == 0 ||
                    r.folder.compare(paths.output_texture) == 0
                    ) {

                if (paths.temporary.empty()) {
                    current_bt->setCaption(OP_TMP_EMPTY + OP_WRONG_PATH);
                }

            } else {

                (*current_path) = r.folder;
                current_bt->setCaption(OP_TMP_PREFIX + (*current_path));

            }

        } else {

            if (
                    r.folder.compare(paths.temporary) == 0 ||
                    r.folder.compare(paths.hotfolder) == 0
                    ) {

                if (current_path->empty()) {

                    string msg = OP_MOD_EMPTY + OP_WRONG_PATH;
                    if (current_path == &paths.output_skeleton) {
                        msg = OP_SKE_EMPTY + OP_WRONG_PATH;
                    } else if (current_path == &paths.output_material) {
                        msg = OP_MAT_EMPTY + OP_WRONG_PATH;
                    } else if (current_path == &paths.output_texture) {
                        msg = OP_TEX_EMPTY + OP_WRONG_PATH;
                    }
                    current_bt->setCaption(msg);
                }

            } else {

                (*current_path) = r.folder;

                std::string prefix = OP_MOD_PREFIX;
                if (current_path == &paths.output_skeleton) {
                    prefix = OP_SKE_PREFIX;
                } else if (current_path == &paths.output_material) {
                    prefix = OP_MAT_PREFIX;
                } else if (current_path == &paths.output_texture) {
                    prefix = OP_TEX_PREFIX;
                }
                current_bt->setCaption(prefix + (*current_path));

                if (current_bt == output_model_bt) {
                    if (paths.output_skeleton.empty()) {
                        paths.output_skeleton = r.folder;
                        output_skeleton_bt->setCaption(OP_SKE_PREFIX + paths.output_skeleton);
                    }
                    if (paths.output_material.empty()) {
                        paths.output_material = r.folder;
                        output_material_bt->setCaption(OP_MAT_PREFIX + paths.output_material);
                    }
                    if (paths.output_texture.empty()) {
                        paths.output_texture = r.folder;
                        output_texture_bt->setCaption(OP_TEX_PREFIX + paths.output_texture);
                    }
                }

            }

        }

        current_path = 0;
        current_bt = 0;
        validatePath();

    }

    cout << "ConverterApp::fileOpened:" << endl <<
            "success:" << r.success << endl <<
            "folder:" << r.folder << endl <<
            "file:" << r.file << endl <<
            "files num:" << r.files.size() << endl;
    if (r.files.empty()) return;
    for (
            std::vector<string>::iterator it = r.files.begin();
            it != r.files.end();
            ++it) {
        cout << "\t" << (*it) << endl;
    }

}

void ConverterApp::loadResource() {

    if (!selected_resource) return;

    switch (selected_resource->type) {

        case CT_IMAGE:

            if (demo_obj) {
                delete demo_obj;
                demo_obj = 0;
            }
            demo_cube.visible(false);
        {
            Ogre::Image im;

            Ogre::TexturePtr tex_ptr = PUtil::getTexture(
                    CONVERTER_RESOURCE_GROUP,
                    selected_resource->filename,
                    &im
                    );
            PMaterial* mat = demo_plane.getMaterial();
            mat->addTexture(tex_ptr, 0, 0);
            float ratio = im.getWidth() * 1.f / im.getHeight();
            demo_plane.scale(ratio, 1, 1);
            demo_plane.visible(true);
        }
            break;

        case CT_MESH:

            if (demo_obj) {
                delete demo_obj;
                demo_obj = 0;
            }
            demo_cube.visible(false);
            demo_plane.visible(false);
        {
            demo_obj = new PNode();
            demo_obj->load(
                    sceneMgr,
                    CONVERTER_RESOURCE_GROUP,
                    selected_resource->filename);
            if (demo_obj->hasSkeleton()) {
                demo_obj->debug(PDM_SKELETON, true);
            }
        }
            break;

        default:

            clearDemo();

            break;

    }

}

void ConverterApp::invalidatePath() {

    cthread.stop();
    _pathvalidated = false;
    _rebuildresources = false;
    _convert_finished = false;

    converted_buttons.clear();

}

bool ConverterApp::allPathValid() {

    if (
            !paths.output_model.empty() &&
            !paths.output_skeleton.empty() &&
            !paths.output_material.empty() &&
            !paths.output_texture.empty() &&
            !paths.hotfolder.empty() &&
            !paths.temporary.empty()
            ) {
        bool keepon = true;
        if (keepon) {
            keepon = ConverterThread::dirExists(paths.output_model.c_str());
            if (!keepon) {
                output_model_bt->setCaption(paths.output_model + OP_WRONG_PATH);
            }
        }
        if (keepon) {
            keepon = ConverterThread::dirExists(paths.output_skeleton.c_str());
            if (!keepon) {
                output_skeleton_bt->setCaption(paths.output_skeleton + OP_WRONG_PATH);
            }
        }
        if (keepon) {
            keepon = ConverterThread::dirExists(paths.output_material.c_str());
            if (!keepon) {
                output_material_bt->setCaption(paths.output_material + OP_WRONG_PATH);
            }
        }
        if (keepon) {
            keepon = ConverterThread::dirExists(paths.output_texture.c_str());
            if (!keepon) {
                output_texture_bt->setCaption(paths.output_texture + OP_WRONG_PATH);
            }
        }
        if (keepon) {
            keepon = ConverterThread::dirExists(paths.hotfolder.c_str());
            if (!keepon) {
                hotfolder_bt->setCaption(paths.hotfolder + OP_WRONG_PATH);
            }
        }
        if (keepon) {
            keepon = ConverterThread::dirExists(paths.temporary.c_str());
            if (!keepon) {
                temporary_bt->setCaption(paths.temporary + OP_WRONG_PATH);
            }
        }
        if (keepon) {
            return true;
        }
    }

    return false;

}

void ConverterApp::validatePath() {

    if (_pathvalidated) return;

    if (allPathValid()) {
        _pathvalidated = true;
        _rendertray = true;
        _rebuildresources = true;
        storePath();
        cthread.start(paths);
    }

}

void ConverterApp::loadPath() {

    std::string p = exec_path + PATH_FILE;
    ifstream in(p.c_str());
    std::string line;
    int i = 0;
    while (std::getline(in, line)) {
        if (line.empty()) break;
        switch (i) {
            case 0:
                if (ConverterThread::dirExists(line.c_str())) paths.output_model = line;
                break;
            case 1:
                if (ConverterThread::dirExists(line.c_str())) paths.output_skeleton = line;
                break;
            case 2:
                if (ConverterThread::dirExists(line.c_str())) paths.output_material = line;
                break;
            case 3:
                if (ConverterThread::dirExists(line.c_str())) paths.output_texture = line;
                break;
            case 4:
                if (ConverterThread::dirExists(line.c_str())) paths.hotfolder = line;
                break;
            case 5:
                if (ConverterThread::dirExists(line.c_str())) paths.temporary = line;
                break;
            default:
                break;
        }
        ++i;
    }
    in.close();

}

void ConverterApp::storePath() {

    std::string p = exec_path + PATH_FILE;
    ofstream out;
    out.open(p.c_str());
    out.clear();
    out.flush();
    out << paths.output_model << endl;
    out << paths.output_skeleton << endl;
    out << paths.output_material << endl;
    out << paths.output_texture << endl;
    out << paths.hotfolder << endl;
    out << paths.temporary << endl;
    out.flush();
    out.close();

}

bool ConverterApp::mouseMoved(const OIS::MouseEvent &arg) {

    // keep default behavior
    PolymorphApplication::mouseMoved(arg);

}

void ConverterApp::renderTray() {

    trayMgr->clearAllTrays();
    OGRE_DELETE trayMgr;
    trayMgr = new SdkTrayManager("SampleControls", window, inputContext, this);

    trayMgr->setTrayWidgetAlignment(TL_TOPLEFT, GHA_LEFT);
    trayMgr->setTrayPadding(2);

    std::string omol = OP_MOD_EMPTY;
    if (!paths.output_model.empty()) omol = OP_MOD_PREFIX + paths.output_model;
    std::string oskl = OP_SKE_EMPTY;
    if (!paths.output_skeleton.empty()) oskl = OP_SKE_PREFIX + paths.output_skeleton;
    std::string omal = OP_MAT_EMPTY;
    if (!paths.output_material.empty()) omal = OP_SKE_PREFIX + paths.output_material;
    std::string otel = OP_TEX_EMPTY;
    if (!paths.output_texture.empty()) otel = OP_TEX_PREFIX + paths.output_texture;
    std::string ohol = OP_HOT_EMPTY;
    if (!paths.hotfolder.empty()) ohol = OP_HOT_PREFIX + paths.hotfolder;
    std::string otml = OP_TMP_EMPTY;
    if (!paths.temporary.empty()) otml = OP_TMP_PREFIX + paths.temporary;

    trayMgr->createLabel(TL_TOPLEFT, "outputl", "output folders", 200);
    output_model_bt = trayMgr->createButton(TL_TOPLEFT, "outputmodbt", omol, 500);
    output_skeleton_bt = trayMgr->createButton(TL_TOPLEFT, "outputskebt", oskl, 500);
    output_material_bt = trayMgr->createButton(TL_TOPLEFT, "outputmatbt", omal, 500);
    output_texture_bt = trayMgr->createButton(TL_TOPLEFT, "outputtexbt", otel, 500);
    trayMgr->createLabel(TL_TOPLEFT, "space1", "", 200);
    trayMgr->createLabel(TL_TOPLEFT, "workingl", "work folders", 200);
    hotfolder_bt = trayMgr->createButton(TL_TOPLEFT, "hotfolderbt", ohol, 500);
    temporary_bt = trayMgr->createButton(TL_TOPLEFT, "tmpfolderbt", otml, 500);

    trayMgr->createLabel(TL_TOPLEFT, "space2", "", 200);
    trayMgr->createLabel(TL_TOPLEFT, "msgsl", "conversion results", 200);

    std::map< std::string, std::string >::iterator itm = convert_msgs.begin();
    std::map< std::string, std::string >::iterator itme = convert_msgs.end();
    for (; itm != itme; ++itm) {
        convert_labels[ itm->first ] = trayMgr->createLabel(
                TL_TOPLEFT,
                itm->first + RESOURCES_LABEL_SUFFIX,
                itm->second, 200);
    }

    if (!_convert_finished) return;

    if (conversion_info.empty()) return;

    selected_resource_bt = 0;

    bool add_spacer = true;
    bool add_label = false;
    CIListIter iti;
    CIListIter itie;

    for (int i = 0; i < CT_COUNT; ++i) {

        iti = conversion_info.begin();
        itie = conversion_info.end();
        add_label = true;

        for (; iti != itie; ++iti) {

            if (
                    (*iti).type != (ConvertType) i ||
                    (*iti).event != CE_DONE
                    ) continue;

            if (add_spacer) {
                trayMgr->createLabel(TL_TOPLEFT, "space_bts", "", 200);
                add_spacer = false;
            }

            if (add_label) {
                switch ((ConvertType) i) {
                    case CT_MESH:
                        trayMgr->createLabel(TL_TOPLEFT, "meshbtsl", RESOURCES_LABEL_PREFIX + "mesh", 200);
                        break;
                    case CT_SKELETON:
                        trayMgr->createLabel(TL_TOPLEFT, "skelbtsl", RESOURCES_LABEL_PREFIX + "skeleton", 200);
                        break;
                    case CT_MATERIAL:
                        trayMgr->createLabel(TL_TOPLEFT, "matbtsl", RESOURCES_LABEL_PREFIX + "material", 200);
                        break;
                    case CT_IMAGE:
                        trayMgr->createLabel(TL_TOPLEFT, "imgbtsl", RESOURCES_LABEL_PREFIX + "image", 200);
                        break;
                }
                add_label = false;
            }


            converted_buttons[ (*iti).filename ] =
                    trayMgr->createButton(TL_TOPLEFT,
                    (*iti).filename + RESOURCES_BUTTON_SUFFIX,
                    LOAD_PREFIX + (*iti).filename, 500);

            if (selected_resource && selected_resource->filename.compare((*iti).filename) == 0) {
                selected_resource_bt = converted_buttons[ (*iti).filename ];
                selected_resource_bt->setCaption(
                        LOADED_PREFIX + (*iti).filename
                        );
            }

        }

    }

}

void ConverterApp::convertEvent(const ConverterInfo& ci) {

    if (ci.event == CE_FINISHED) {
        _convert_finished = true;
        _rendertray = true;
        _rebuildresources = true;
        return;
    }

    CIListIter it = find(
            conversion_info.begin(),
            conversion_info.end(),
            ci);

    if (it == conversion_info.end()) {

        conversion_info.push_back(ConverterInfo(ci));

    } else {

        (*it) = ci;
        if (selected_resource == &(*it)) {
            selected_resource = 0;
            selected_resource_bt = 0;
            clearDemo();
        }

    }

    std::string n = ci.filename;
    std::string m = ci.message;
    if (m.empty()) {
        switch (ci.event) {
            case CE_CONVERSION:
                m = "conversion";
                break;
            case CE_PUSHED:
                m = "pushed";
                break;
            case CE_MODIFIED:
                m = "modified";
                break;
            case CE_SRC_MISSING:
                m = "missing!";
                break;
            case CE_DONE:
                m = "done!";
                break;
            default:
                m = "???";
                break;
        }
    }

    if (convert_labels.find(n) != convert_labels.end()) {
        convert_msgs[ n ] = n + " [" + m + "]";
        _rendertray = true;
    } else {
        convert_msgs[ n ] = n + " [" + m + "]";
        _rendertray = true;
    }

}

void ConverterApp::rebuildResources() {

    if (!allPathValid()) return;

    // cleanup and reloading of all resources

    StringVector grps = ResourceGroupManager::getSingleton().getResourceGroups();
    if (find(grps.begin(), grps.end(), CONVERTER_RESOURCE_GROUP) != grps.end()) {
        ResourceGroupManager::getSingleton().clearResourceGroup(CONVERTER_RESOURCE_GROUP);
    } else {
        ResourceGroupManager::getSingleton().createResourceGroup(CONVERTER_RESOURCE_GROUP);
    }

    std::vector<std::string> to_load;
    to_load.push_back(paths.output_model);
    if (find(to_load.begin(), to_load.end(), paths.output_skeleton) == to_load.end()) {
        to_load.push_back(paths.output_skeleton);
    }
    if (find(to_load.begin(), to_load.end(), paths.output_material) == to_load.end()) {
        to_load.push_back(paths.output_material);
    }
    if (find(to_load.begin(), to_load.end(), paths.output_texture) == to_load.end()) {
        to_load.push_back(paths.output_texture);
    }

    std::vector<std::string>::iterator it = to_load.begin();
    std::vector<std::string>::iterator ite = to_load.end();
    for (; it != ite; ++it) {
        ResourceGroupManager::getSingleton().addResourceLocation(
                (*it),
                "FileSystem",
                CONVERTER_RESOURCE_GROUP
                );
    }

    ResourceGroupManager::getSingleton().initialiseResourceGroup(CONVERTER_RESOURCE_GROUP);
    while (!ResourceGroupManager::getSingleton().isResourceGroupInitialised(CONVERTER_RESOURCE_GROUP)) {
        boost::this_thread::sleep(boost::posix_time::milliseconds(1));
    }

    ResourceGroupManager::getSingleton().loadResourceGroup(CONVERTER_RESOURCE_GROUP);

}

void ConverterApp::clearDemo() {

    if (demo_obj) {
        delete demo_obj;
        demo_obj = 0;
    }
    demo_cube.visible(false);
    demo_plane.visible(false);

}