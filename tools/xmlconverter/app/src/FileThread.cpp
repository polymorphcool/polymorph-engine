/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2017 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
 */

/* 
 * File:   FileThread.cpp
 * Author: frankiezafe
 * 
 * Created on February 8, 2017, 3:51 PM
 */

#include "FileThread.h"

using namespace std;
using namespace polymorph;

FileThread::FileThread( ) :
_runs( false ) {
}

FileThread::~FileThread( ) {
    stop( );
}

void FileThread::stop( ) {
    if ( !_runs ) return;
    assert( bthread );
    _runs = false;
    bthread->join( );
}

bool FileThread::openDir(
        const std::string& path,
        const std::string& title ) {

    if ( _runs ) return false;
    assert( !bthread );
    start_path = path;
    dialog_title = title;
    _runs = true;
    bthread = boost::shared_ptr<boost::thread>(
            new boost::thread( boost::bind( &FileThread::runOpenDir, this ) ) );
    return true;

}

bool FileThread::openOgreExport(
        const std::string& path,
        const std::string& title ) {

    if ( _runs ) return false;
    assert( !bthread );
    start_path = path;
    dialog_title = title;
    _runs = true;
    bthread = boost::shared_ptr<boost::thread>(
            new boost::thread( boost::bind( &FileThread::runOpenOgreExport, this ) ) );
    return true;

}

void FileThread::runOpenDir( ) {

    FileChooserResult r;
    r.init( );
    FileChooser::openDir( r, start_path, dialog_title );

    FLListIter it = listeners.begin( );
    FLListIter ite = listeners.end( );
    for (; it != ite; ++it ) {
        ( *it )->fileOpened( r );
    }
    _runs = false;

}

void FileThread::runOpenOgreExport( ) {

    FileChooserResult r;
    r.init( );
    FileChooser::openOgreExport( r, start_path, dialog_title );

    FLListIter it = listeners.begin( );
    FLListIter ite = listeners.end( );
    for (; it != ite; ++it ) {
        ( *it )->fileOpened( r );
    }
    _runs = false;

}

bool FileThread::addListener( FileListener* l ) {

    assert( f );
    if ( containsListener( l ) ) return false;
    listeners.push_back( l );
    return true;

}

bool FileThread::removeListener( FileListener* l ) {

    assert( f );
    if ( !containsListener( l ) ) return false;
    listeners.erase( find( listeners.begin( ), listeners.end( ), l ) );
    return true;

}
