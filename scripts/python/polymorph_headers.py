import os
import sys
hfile = []
cppfile = []
folders = []

rootdir = "../../package/dev/"
#rootdir = "../../tools/xmlconverter/app/"
#rootdir = "example.0.basics/app/src/"

needle = '''/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
'''

header_h = '''/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2017 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
*/
'''

header_cpp = header_h


for root, subFolders, files in os.walk(rootdir):
	for dir in subFolders:
		f = os.path.join(root,dir)
		f = f[len(rootdir):]
		folders.append( f )
	for file in files:
		f = os.path.join(root,file)
		if file[-2:] == ".h":
			hfile.append(f)
		elif file[-4:] == ".cpp":
			cppfile.append(f)

folders = sorted( folders )
hfile = sorted( hfile )
cppfile = sorted( cppfile )
			
#print header_h

print "\n*** SEARCH PATH ***\n"
for f in folders:
	print "\t", f

print "\n*** H ***\n"
for h in hfile:
	print "\t", h[len(rootdir):]
	f = open( h, 'r' )
	data = f.read()
	data = data.replace(needle, header_h)
	f.close()
	f = open( h, 'w' )
	f.write( data )
	f.close()
	#print data
	
print "\n*** CPP ***\n"
for cpp in cppfile:
	print "\t", cpp[len(rootdir):]
	f = open( cpp, 'r' )
	data = f.read()
	data = data.replace(needle, header_h)
	f.close()
	f = open( cpp, 'w' )
	f.write( data )
	f.close()
