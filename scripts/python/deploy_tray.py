import os
import sys
from shutil import copyfile

targets = []

def findAll():
	global rootdir
	global src
	global targets
	for root, subFolders, files in os.walk(rootdir):
		for file in files:
			f = os.path.join(root,file)
			try:
				if file.index( "PolymorphTrays.zip" ) != -1:
					targets.append(f)
			except:
				pass
	targets = sorted( targets )

rootdir = "../../samples/"
src = "../../assets/graphid/PolymorphTrays.zip"
findAll()

rootdir = "../../empty.0.1/"
findAll()

rootdir = "../../../../workspace/"
findAll()

print "\n*** TO REPLACE ***\n"
for f in targets:
	print "\t", f, " with ", src
	copyfile(src, f)