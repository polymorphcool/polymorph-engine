# https://github.com/AppImage/AppImageKit/wiki/Creating-AppImages

import os, sys, shutil, stat, subprocess

appname = "Empty.0.1"

current_path = os.path.dirname(os.path.abspath(__file__))
project_name = current_path[ current_path.rfind('/') + 1 : ]
appdir = project_name + ".AppDir"
libdir = appdir + "/lib"

if not os.path.exists("build/dist/"):
    sys.exit("no folder 'build/dist/' found!")

if not os.path.exists("build/dist/bin/"+appname):
    sys.exit("no file \'build/dist/bin/"+appname+"\' found!")

if not os.path.exists("release"):
    sys.exit("no folder 'release' found!")
appdir = "release/" + appdir
libdir = appdir + "/lib"
if os.path.exists(appdir):
    shutil.rmtree(appdir)
os.makedirs(appdir)
os.makedirs(libdir)

if not os.path.exists("LAUNCH.sh"):
    sys.exit("no script 'LAUNCH.sh' found!")

# listing libraries
try:
    os.system('ldd build/dist/bin/'+appname+' > all_libs.log')
except:
    sys.exit("failed to generate the list of libraries")

skip_list = ["libstdc++","libc.", "libX","libpthread","libdl","libdl","ld-linux-"]
# only these libraries must be relinked locally
libraries_copied = [":"]
folder_to_replace = []

# installing libraries
with open("all_libs.log") as f:
    for line in f:
        #print( line )
        try:
            slash = line.index('/')
        except:
            continue
        last_slash = line.rfind('/')
        next_space = line.index(" ",slash)
        dir_path = line[slash:last_slash]
        file_path = line[slash:next_space]
        libname = line[last_slash+1:next_space]
        oktogo = True
        for sk in skip_list:
            if libname[:len(sk)] == sk:
                print "! lib skipped: " + libname
                oktogo = False
                break
        if oktogo == False:
            continue
        found = False
        for f in folder_to_replace:
            if f == dir_path:
                found = True
                break
        if found == False:
            folder_to_replace.append( dir_path )
        #if not os.path.exists(dir_release):
        #    os.makedirs(dir_release)
        shutil.copyfile( line[slash:next_space], libdir + "/" + libname )
        libraries_copied.append( libname )
        print "> lib copied: " + libname
        #print  file_path + " => " + libdir + "/" + libname
        
# copying exec folder
if os.path.exists(appdir + '/build/'):
    shutil.rmtree(appdir + '/build/')
shutil.copytree('build/dist/', appdir + '/build/dist/')

# copying launcher
shutil.copyfile( "LAUNCH.sh", appdir + "/AppRun" )
# and making it executable
st = os.stat( appdir + "/AppRun" )
os.chmod( appdir + "/AppRun", st.st_mode | stat.S_IEXEC)

shutil.copyfile( "release/myapp.png", appdir + "/myapp.png" )
shutil.copyfile( "release/myapp.desktop", appdir + "/myapp.desktop" )

print "\n******* RELATIVE PATH - start *********\n"

for l in libraries_copied:
    print "******* " + libdir + "/" + l
    print "**** /usr/lib check"
    print "\t" + str( os.system("strings " + libdir + "/" + l + " | grep /usr/lib" ) )
    print "******* " + libdir + "/" + l
    print "**** /home check"
    print "\t" + str( os.system("strings " + libdir + "/" + l + " | grep /home" ) )

# fixing the exec
for f in folder_to_replace:
    #print "replacing all occurences of\n\t" + f + " in \n\t"+appdir+"/build/dist/bin/"+appname +"\n\tcommand:\n\t" + "sed -i -e 's#" + f + "#./././lib#g' "+appdir+"/build/dist/bin/"+appname
    print "replacing all occurences of " + f
    os.system("strings release/empty.0.1.AppDir/build/dist/bin/Empty.0.1 | grep " + f + " > app_strings.log" )
    with open("app_strings.log") as f:
        for line in f:
            for l in libraries_copied:
                try:
                    line.index(l)
                    print "\tmaking " + line[:-1] + " relative"
                    os.system("sed -i -e 's#" + line[:-1] + "#./././lib/"+ l +"#g' "+appdir+"/build/dist/bin/"+appname )
                except:
                    pass
    #os.system("sed -i -e 's#" + f + "#./././lib#g' "+appdir+"/build/dist/bin/"+appname )

# fixing libs
    
print "\n******* RELATIVE PATH - stop *********\n"

# patching the absolute path
'''
try:
    os.system("sed -i -e 's#/usr#././#g' "+appdir+"/build/dist/bin/"+appname +"")
except:
    sys.exit("failed to patch the absolute path of the exec")
'''

print( appdir )

'''
# source: https://github.com/AppImage/AppImageKit/wiki/Creating-AppImages
>> patch the exec with:
strings "+appdir+"/build/dist/bin/"+appname +" | grep /usr
sed -i -e 's#/usr#././#g' "+appdir+"/build/dist/bin/"+appname
>> verify the other exec in the release folder
cd "+appdir+"/usr/ 
find . -type f -exec sed -i -e 's#/usr#././#g' {} \; 
cd -
'''

