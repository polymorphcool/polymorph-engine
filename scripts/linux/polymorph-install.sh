#!/bin/bash


# 
# _________ ____  .-. _________/ ____ .-. ____ 
# __|__  (_)_/(_)(   )____<    \|    (   )  (_)
#                 `-'                 `-'      
#
#
# art & game engine
#
# ____________________________________  ?   ____________________________________
#                                     (._.)
#
# 
# This file is part of polymorph package
# For the latest info, see http://polymorph.cool/
# 
# Copyright (c) 2017 polymorph.cool
# 
# One line installer for polymorph package.
# It will install the SDK in a ~/forge.polymorph by default.
# 
# run using sudo chmod +x polymorph-install.sh && ./polymorph-install.sh
# 
# Tested on xubuntu 16.04 64 bits
#
# ___________________________________( ^3^)_____________________________________


function polymorph_comment(){
echo "+-------------------------------------------------+"
echo "|  _________ ____  .-. _________/ ____ .-. ____   |"
echo "|  __|__  (_)_/(_)(   )____<    \|    (   )  (_)  |"
echo "|                  ˇ                   ˇ          |"
echo "+-------------------------------------------------+"
echo "|                                                 |"
echo "| $comment"
echo "|                                                 |"
echo "+-------------------------------------------------+"
}

function polymorph_subcomment(){
echo "+-------------------------------------------------+"
echo "|                                                 |"
echo "| $comment"
echo "|                                                 |"
echo "+-------------------------------------------------+"
}

function polymorph_report(){
echo "+---- $comment"
}

INSTALL_TOOLS=0

comment="installation configuration"
polymorph_comment
echo "Do you want to install tools: tortoisehg, cmake-qt-gui, sublime-text & blender2ogre?"
read -p "'y' to confirm: " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
	INSTALL_TOOLS=1
fi

comment="updating system"
polymorph_comment

comment="update"
polymorph_subcomment
sudo apt update

comment="system libs"
polymorph_subcomment
sudo apt install build-essential mercurial git make cmake automake libtool libboost-chrono-dev libboost-thread-dev libboost-date-time-dev mesa-common-dev libxt-dev libxaw7-dev libxrandr-dev libxxf86vm-dev libglu-dev libglu1-mesa-dev alsa-tools alsa-utils alsa-source libasound2-dev libcppunit-dev doxygen pkg-config libgtk2.0-dev

if [[ $INSTALL_TOOLS = 1 ]]
then
	comment="gui tools"
	polymorph_subcomment
	sudo apt install cmake-qt-gui tortoisehg sublime-text
fi

comment="nvidia"
polymorph_subcomment
sudo apt-get install nvidia-cg-toolkit

comment="system ready"
polymorph_report

comment="workspace"
polymorph_comment

mkdir ~/forge.polymorph

cd ~/forge.polymorph

mkdir sdk && mkdir repositories && mkdir workspace

comment="sdk, repositories & workspace folders created"
polymorph_report

comment="engine repositories"
polymorph_comment

comment="sources of ogre3d"
polymorph_subcomment
hg clone https://bitbucket.org/sinbad/ogre repositories/ogre
cd repositories/ogre && hg pull -b v2-0 && hg update -C -r v2-0 && cd ../../

comment="sources of ogre3d dependencies"
polymorph_subcomment
hg clone https://bitbucket.org/cabalistic/ogredeps repositories/ogredeps

comment="sources of SDL"
polymorph_subcomment
hg clone http://hg.libsdl.org/SDL repositories/SDL

comment="sources of bullet"
polymorph_subcomment
git clone https://github.com/bulletphysics/bullet3.git repositories/bullet

comment="sources of puredata"
polymorph_subcomment
git clone git://git.code.sf.net/p/pure-data/pure-data repositories/pd

comment="sources of libpd"
polymorph_subcommentcd 
git clone https://github.com/libpd/libpd.git repositories/libpd && cd repositories/libpd && git checkout 0.8.3 && git submodule init && git submodule update && cd ../../

comment="sources of polymorph engine"
polymorph_comment
hg clone https://bitbucket.org/polymorphteam/pe.2.0 repositories/polymorphengine

if [[ $INSTALL_TOOLS = 1 ]]
then

	comment="UI tools repositories"
	polymorph_comment

	comment="blender2ogre"
	polymorph_subcomment
	git clone https://github.com/OGRECave/blender2ogre.git repositories/blender2ogre

	comment="sublime text addon for materials, compositors and programs"
	polymorph_subcomment
	git clone https://github.com/TheSHEEEP/ST-OgreScripts.git repositories/sublime-text-ogre

fi

comment="source code complete"
polymorph_report

comment="compilation"
polymorph_comment

# detection of number of cores
CORE=$(nproc --all)
comment="available CPU cores: $CORE"
polymorph_report

comment="freeimage,freetype,zlib,zziplib,OIS & rapidjson"
polymorph_subcomment
cd repositories && mkdir build_deps && cd build_deps && cmake -D CMAKE_INSTALL_PREFIX:PATH=../../sdk ../ogredeps && make -j$CORE install && cd ../../

comment="building SDL2"
polymorph_subcomment
#cd repositories && mkdir build && cd build && cmake -D CMAKE_INSTALL_PREFIX:PATH=../../sdk ../ogredeps/src/SDL2 && make -j$CORE install && cd ../ && rm -R build && cd ../
# compile SDL from main repository
cd repositories && mkdir build_sdl && cd build_sdl && cmake -D CMAKE_INSTALL_PREFIX:PATH=../../sdk ../SDL && make -j$CORE install && cd ../../

comment="building Bullet"
polymorph_subcomment
cd repositories && mkdir build_bullet && cd build_bullet && cmake -D BUILD_BULLET2_DEMO:BOOL=0 -D BUILD_BULLET2_DEMOS:BOOL=0 -D BUILD_SHARED_LIBS:BOOL=1 -D BUILD_UNIT_TESTS:BOOL=0 -D BUILD_CPU_DEMOS:BOOL=0 -D INSTALL_EXTRA_LIBS:BOOL=1 -D CMAKE_INSTALL_PREFIX:PATH=../../sdk ../bullet && make -j$CORE install && cd ../../

comment="building PureData"
polymorph_subcomment
cd repositories/pd/ && ./autogen.sh && ./configure && sudo make install && cd ../../

comment="puredata installed, launch with ./usr/local/bin/pd"
polymorph_report

comment="building libpd"
polymorph_subcomment
cd repositories/libpd/ && make UTIL=true EXTRA=true && make UTIL=true EXTRA=true cpplib && make prefix=../../sdk install && cd ../../

cd ~/forge.polymorph

comment="patches"
polymorph_subcomment
mkdir -p sdk/lib/OGRE/cmake/ && cp repositories/ogredeps/cmake/FindPkgMacros.cmake sdk/lib/OGRE/cmake/FindPkgMacros.cmake
cp repositories/polymorphengine/custom/repository/OgreMain/src/GLX/GLX_backdrop.h repositories/ogre/OgreMain/src/GLX/GLX_backdrop.h
cp repositories/polymorphengine/custom/repository/OgreMain/src/GLX/OgreConfigDialog.cpp repositories/ogre/OgreMain/src/GLX/OgreConfigDialog.cpp

comment="building ogre3d"
polymorph_subcomment
cd repositories && mkdir build_ogre && cd build_ogre && cmake -D OGRE_BUILD_SAMPLES:BOOL=0 -D OGRE_INSTALL_SAMPLES:BOOL=0 -D OGRE_DEPENDENCIES_DIR:PATH=../../sdk -D CMAKE_INSTALL_PREFIX:PATH=../../sdk ../ogre && make -j$CORE install && cd ../../

comment="building polymorph package"
polymorph_subcomment
cd repositories && mkdir -p polymorphengine/package/build/ && cd polymorphengine/package/build/ && cmake -D OGRE_HOME:PATH=../../../../sdk ../dev && make -j$CORE install && cd ../../../../

comment="installing empty example"
polymorph_subcomment
cp -R repositories/polymorphengine/empty.0.1 workspace/empty.0.1 && cd workspace/empty.0.1/ && mkdir build && cd build && cmake -D EXEC_NAME:STRING=Empty.0.1 -D OGRE_HOME:PATH=../../../sdk ../app && make install

comment="all compilation done, we are ready to rock!"
polymorph_report

comment="running empty example"
polymorph_comment
cd dist/bin/ && ./Empty.0.1

comment="You are now ready to build your own projects with polymorph engine!"
polymorph_report
comment="Go to https://polymorph.org/ for tutorials and resources."
polymorph_report
