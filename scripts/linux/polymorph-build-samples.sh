#!/bin/bash


# 
# _________ ____  .-. _________/ ____ .-. ____ 
# __|__  (_)_/(_)(   )____<    \|    (   )  (_)
#                 `-'                 `-'      
#
#
# art & game engine
#
# ____________________________________  ?   ____________________________________
#                                     (._.)
#
# 
# This file is part of polymorph package
# For the latest info, see http://polymorph.cool/
# 
# Copyright (c) 2017 polymorph.cool
# 
# One line installer for polymorph examples.
# 
# run using sudo chmod +x polymorph-build-samples.sh && ./polymorph-build-samples.sh
#
# ___________________________________( ^3^)_____________________________________


# CONFIGURATION

POLYMORPH_ROOT=~/forge.polymorph/

SAMPLES_FOLDER="${POLYMORPH_ROOT}repositories/polymorphengine/samples/0.1/"
TARGET_FOLDER="${POLYMORPH_ROOT}polymorph/"

# FUNCTIONS

function polymorph_comment(){
echo "+-------------------------------------------------+"
echo "|  _________ ____  .-. _________/ ____ .-. ____   |"
echo "|  __|__  (_)_/(_)(   )____<    \|    (   )  (_)  |"
echo "|                  ˇ                   ˇ          |"
echo "+-------------------------------------------------+"
echo "|                                                 |"
echo "| $comment"
echo "|                                                 |"
echo "+-------------------------------------------------+"
}

function polymorph_subcomment(){
echo "+-------------------------------------------------+"
echo "|                                                 |"
echo "| $comment"
echo "|                                                 |"
echo "+-------------------------------------------------+"
}

function polymorph_report(){
echo "+---- $comment"
}

# CORES DETECTION

CORE=$(nproc --all)
comment="available CPU cores: $CORE"
polymorph_report

# BASICS EXAMPLE

comment="Basics Example"
polymorph_comment

comment="installation"
polymorph_subcomment

cp -R ${SAMPLES_FOLDER}example.basics ${TARGET_FOLDER}example.basics/
cd ${TARGET_FOLDER}example.basics/
mkdir build
cd build
cmake -D EXEC_NAME:STRING=ExampleBasic -D OGRE_HOME:PATH=../../../sdk ../app

comment="compilation"
polymorph_subcomment

make -j$CORE install
cd dist/bin/
./ExampleBasic

# SOUND EXAMPLE

comment="Sound Example"
polymorph_comment

comment="installation"
polymorph_subcomment

cp -R ${SAMPLES_FOLDER}example.sound ${TARGET_FOLDER}example.sound/
cd ${TARGET_FOLDER}example.sound/
mkdir build
cd build
cmake -D EXEC_NAME:STRING=ExampleSound -D OGRE_HOME:PATH=../../../sdk ../app

comment="compilation"
polymorph_subcomment

make -j$CORE install
cd dist/bin/
./ExampleSound

# XML EXAMPLE

comment="XML Example"
polymorph_comment

comment="installation"
polymorph_subcomment

cp -R ${SAMPLES_FOLDER}example.xml ${TARGET_FOLDER}example.xml/
cd ${TARGET_FOLDER}example.xml/
mkdir build
cd build
cmake -D EXEC_NAME:STRING=ExampleXML -D OGRE_HOME:PATH=../../../sdk ../app

comment="compilation"
polymorph_subcomment

make -j$CORE install
cd dist/bin/
./ExampleXML
