#!/bin/bash


# 
# _________ ____  .-. _________/ ____ .-. ____ 
# __|__  (_)_/(_)(   )____<    \|    (   )  (_)
#                 `-'                 `-'      
#
#
# art & game engine
#
# ____________________________________  ?   ____________________________________
#                                     (._.)
#
# 
# This file is part of polymorph package
# For the latest info, see http://polymorph.cool/
# 
# Copyright (c) 2017 polymorph.cool
# 
# One line installer for polymorph examples.
# 
# run using sudo chmod +x polymorph-build-samples.sh && ./polymorph-build-samples.sh
#
# ___________________________________( ^3^)_____________________________________


# CONFIGURATION

POLYMORPH_ROOT=~/forge.polymorph/

BASE_FOLDER="${POLYMORPH_ROOT}repositories/polymorphengine/empty.0.1"
TARGET_FOLDER="${POLYMORPH_ROOT}polymorph/"

# FUNCTIONS

function polymorph_comment(){
echo "+-------------------------------------------------+"
echo "|  _________ ____  .-. _________/ ____ .-. ____   |"
echo "|  __|__  (_)_/(_)(   )____<    \|    (   )  (_)  |"
echo "|                  ˇ                   ˇ          |"
echo "+-------------------------------------------------+"
echo "|                                                 |"
echo "| $comment"
echo "|                                                 |"
echo "+-------------------------------------------------+"
}

function polymorph_subcomment(){
echo "+-------------------------------------------------+"
echo "|                                                 |"
echo "| $comment"
echo "|                                                 |"
echo "+-------------------------------------------------+"
}

function polymorph_report(){
echo "+---- $comment"
}

# CORES DETECTION

CORE=$(nproc --all)

# VERIFICATION

projectname=$1

if [ -z "$projectname" ]; then
	
	comment="You have to pass a project name variable to the script, using ./polymorph-new-project.sh [PROJECT NAME]"
	polymorph_report

else

	comment="New project '${projectname}'"
	polymorph_comment
	
	comment="available CPU cores: $CORE"
	polymorph_report
	
	comment="installation"
	polymorph_subcomment
	
	cp -R ${BASE_FOLDER} ${TARGET_FOLDER}${projectname}/
	cd ${TARGET_FOLDER}${projectname}/
	mkdir build
	cd build
	cmake -D EXEC_NAME:STRING=$projectname -D OGRE_HOME:PATH=../../../sdk ../app
	
	comment="compilation"
	polymorph_subcomment
	
	make -j$CORE install
	cd dist/bin/
	./$projectname
	
fi

